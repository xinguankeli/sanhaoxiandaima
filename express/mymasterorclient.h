#ifndef MYMASTERORCLIENT_H
#define MYMASTERORCLIENT_H

#include <QObject>
#include <QPair>
#include <QTimer>
#include <QUdpSocket>
#include <QMap>
class MyMasterOrClient : public QUdpSocket
{
    Q_OBJECT
public:
    explicit MyMasterOrClient(const QMap<QString, QPair<QString, quint16> >, const QString , QObject *parent = nullptr);
private:
    //主从设备的监听地址和端口
    QMap<QString, QPair<QString, quint16> > m_masterOrClientAddress;
    //主从设备的收数据计数器
    quint64 m_recvCounter;
    //主从标志
    QString m_masterOrClientSign;
    //发送心跳包定时器
    QTimer *m_sendUDPMessageTimer;
private:
    void initUdpServer();//启动UDP监听
    //处理从设备发来的IO点的开关状态
    void processIOStatus(QByteArray);
private slots:
    void recvUDPMessage();//接收来自对方的消息
    void sendUDPMessage(QByteArray);//向对方发送消息
signals:
    void sendStatus(QString, QString);//第一个参数是主从，第二个是故障和正常
    void updateIOStatus(QHash<QString, QHash<QString, bool> >);//key是总IO名，values.key是总IO下的子IO输出点，values.value是总IO下的IO输出点的状态
};

#endif // MYMASTERORCLIENT_H
