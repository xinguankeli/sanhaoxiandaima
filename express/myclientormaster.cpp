#include "myclientormaster.h"
#include <QNetworkDatagram>
MyClientOrMaster::MyClientOrMaster(const QMap<QString, QPair<QString, quint16> > master, const QString masterSign, QObject *parent)
    :QUdpSocket(parent)
    ,m_recvCounter(0)
    ,m_masterOrClientSign(masterSign)
    ,m_masterOrClientAddress(master)
    ,m_ioStatusStr("1")
{
    initUdpServer();
}

void MyClientOrMaster::updateIOOutPutStatus(QHash<QString, QHash<QString, bool> > hash)
{
    m_ioDeviceOutputStatus = hash;
    m_ioStatusStr = getIOStatus();
    qDebug() <<"m_ioStatusStr"<< m_ioStatusStr;
}

void MyClientOrMaster::initUdpServer()
{
    quint16 _port = m_masterOrClientAddress["client"].second;
    if(!bind(_port))
    {
#ifdef Q_OS_LINUX
        qDebug() << "master client udp server start failed";
#else
        qDebug() << "从udp监听启动失败";
#endif
        exit(-1);
    }else
    {
#ifdef Q_OS_LINUX
        qDebug() << "master client udp server start succ";
#else
        qDebug() << "从udp监听启动成功";
#endif
    }
    connect(this, &QUdpSocket::readyRead, this, &MyClientOrMaster::recvUDPMessage);
    m_recvMessageTimer = new QTimer;
    connect(m_recvMessageTimer, &QTimer::timeout, this, &MyClientOrMaster::processDeviceOutLine);
    m_recvMessageTimer->setInterval(1000 * 60);//6秒处理一次
    m_recvMessageTimer->start();
}

void MyClientOrMaster::resetTimer()
{
    if(m_recvMessageTimer->isActive())
    {
#ifdef Q_OS_LINUX
        qDebug() << "reset timer";
#else
        qDebug() << "重置定时器";
#endif
        m_recvMessageTimer->stop();
        m_recvMessageTimer->start();
    }
}

QString MyClientOrMaster::getIOStatus()
{
    //遍历Core更新过来的所有IO点，将所有开启的输出点同步到字符串里
    QString _data = "";
    QList<QString> _keys = m_ioDeviceOutputStatus.keys();
    for(int i =0;i<_keys.size();i++)
    {
        QHash<QString, bool> _hash = m_ioDeviceOutputStatus[_keys.at(i)];
        QList<QString> _subKeys = _hash.keys();
        for(int j =0;j<_subKeys.size();j++)
        {
            QString _str("%1,%2,%3#");
            bool _status = _hash.value(_subKeys.at(j));
            if(_status == true)
            {
                _str = _str
                        .arg(_keys.at(i))
                        .arg(_subKeys.at(j))
                        .arg(_status == true ? "true" : "false");
                _data += _str;
            }
        }
    }
//    qDebug() << _data.mid(0, _data.size() - 1);
    return _data;
}


void MyClientOrMaster::recvUDPMessage()
{
    while (hasPendingDatagrams()) {
        QNetworkDatagram _datagram = receiveDatagram();
        QByteArray _arr = _datagram.data();
        if(_arr != "")
        {
            if(_arr == "ok")
            {
                m_ioStatusStr = "1";
            }
            QByteArray arr = m_ioStatusStr.toUtf8();
            sendUDPMessage(arr);
            resetTimer();
            emit sendStatus(m_masterOrClientSign, "normal");
        }
    }
}

void MyClientOrMaster::sendUDPMessage(QByteArray arr)
{
    QString _ip = m_masterOrClientAddress["client"].first;
    quint16 _port = m_masterOrClientAddress["client"].second;
    writeDatagram(arr, QHostAddress(_ip), _port);
}

void MyClientOrMaster::processDeviceOutLine()
{
    emit sendStatus(m_masterOrClientSign, "fault");
}

bool MyClientOrMaster::sendIOStatusToMaster()
{
    //ret为true，就是可以发送，为false就无需发送
    bool _ret = false;
    QString _str = getIOStatus();
    if(_str != "")
    {
        _ret = true;
    }
    return _ret;
}
