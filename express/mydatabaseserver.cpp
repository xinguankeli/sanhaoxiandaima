#include "mydatabaseserver.h"
#include <QDebug>
MyDataBaseServer::MyDataBaseServer(const quint16 &port,QObject *parent)
    :QTcpServer(parent),m_port(port)
    ,m_sign(false)
{

}

void MyDataBaseServer::sendData(QList<QString> list)
{
    if(m_sign == false)
    {
        return;
    }
    if(m_client->state() == QAbstractSocket::ConnectedState)
    {
        QByteArray  _str;
        for(int i =0;i<list.size();i++)
        {
            _str += list.at(i) + "@";
        }
        _str = _str.mid(0, _str.size() - 1);
        qDebug() << _str;
        qDebug() << _str.size();
        m_client->write(QString("size=%1").arg(_str.size()).toLocal8Bit().data());
        m_client->waitForReadyRead();
        m_client->write(_str);
        m_client->waitForReadyRead();
    }
}

void MyDataBaseServer::startDataBaseServer()
{
    if(!listen(QHostAddress::AnyIPv4, m_port))
    {
        qDebug() << "database server listen error";
        exit(0);
    }else
    {
        qDebug() << "database server listen succ";
    }
    connect(this, &QTcpServer::newConnection, this, &MyDataBaseServer::whenNewConnected);
}

void MyDataBaseServer::whenNewConnected()
{
    m_client = nextPendingConnection();
    m_sign = true;
    connect(m_client, &QTcpSocket::readyRead, this, &MyDataBaseServer::whenDataComing);
    connect(m_client, &QTcpSocket::disconnected, this, &MyDataBaseServer::whenDisConnected);
    qDebug() << "hi database client";
}

void MyDataBaseServer::whenDisConnected()
{
    m_sign = false;
    m_client->deleteLater();
    qDebug() << "bye database client";
}

void MyDataBaseServer::whenDataComing()
{

}
