#ifndef MYTCPSERVER_H
#define MYTCPSERVER_H

#include <QObject>
#include <QTcpServer>
#include <QTcpSocket>
#include <QJsonObject>
#include <QJsonDocument>
class MyTcpServer : public QTcpServer
{
    Q_OBJECT
public:
    MyTcpServer(const quint16 &, QObject *parent = nullptr);
public slots:
    void whenNewConnected();
    void whenDisConnected();
    void whenComingMessage();
    //外部使用该函数向UI发送数据
    void sendMessage(const QJsonObject&);

    void startExpressServer();
signals:
    void haveMessage(const QJsonObject&);
private:
    bool m_online;
    QTcpSocket *m_client;
    quint16 m_port;
private:
    QJsonObject getJson(const QByteArray );
};

#endif // MYTCPSERVER_H
