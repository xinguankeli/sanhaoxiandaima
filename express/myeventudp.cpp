#include "myeventudp.h"
#include <QHostAddress>
#include <QDebug>
MyEventUDP::MyEventUDP(QObject *parent):QUdpSocket(parent)
{
    if(!bind(QHostAddress::AnyIPv4, 60000))
    {
        qDebug() << "[事件监听服务器启动失败]";
        exit(0);
    }else{
        qDebug() << "[事件监听服务器启动成功]";
    }
    connect(this, &QUdpSocket::readyRead, this, &MyEventUDP::readPendingDatagrams);
}

void MyEventUDP::addEvent(MyDataBaseWord word)
{
    m_queue.enqueue(word);
    sendEvent();
}

void MyEventUDP::addEvent(QJsonObject obj)
{
    m_execqueue.enqueue(obj);
    sendEvent();
}

void MyEventUDP::setEventAddressList(const QList<MyDeviceBaseInfo> list)
{
    m_udpEventAddressList = list;
}

void MyEventUDP::sendEvent()
{
    if(!m_queue.isEmpty())
    {
        MyDataBaseWord _word = m_queue.dequeue();
        QJsonDocument _doc;
        QJsonObject _obj;
        _obj["type"] = _word.type;
        _obj["devicename"] = _word.event_word.devicename;
        _obj["date"] = QString::number(_word.event_word.date);
        _obj["cause"] = _word.event_word.casue;
        _obj["isclear"] = _word.event_word.isclear;
        _doc.setObject(_obj);
        for(int i =0;i<m_udpEventAddressList.size();i++)
        {
            MyDeviceBaseInfo _address = m_udpEventAddressList.at(i);
            writeDatagram(_doc.toJson(), QHostAddress(_address.ip), _address.port);
        }
    }
    if(!m_execqueue.isEmpty())
    {
        QJsonObject _obj = m_execqueue.dequeue();
        QJsonDocument _doc;
        if(_obj["type"].toString() == "setqxz")
        {
            _doc.setObject(_obj);
        }else if(_obj["type"].toString() == "setlmd")
        {
            _doc.setObject(_obj);
        }else if(_obj["type"].toString() == "setiostatus")
        {
            _doc.setObject(_obj);
        }else if(_obj["type"].toString() == "resetstatus")
        {
            _obj["func"] = "telluiresetstatus";
            _doc.setObject(_obj);
        }else if(_obj["type"].toString() == "updategatestatus")//UI控制阀门开关的信号同步
        {
            _doc.setObject(_obj);
        }else if(_obj["type"].toString() == "updatefaultmessage")//主从设备同步UI的故障消息，比如：液位低、氮气瓶压力低等
        {
            _doc.setObject(_obj);
        }else if(_obj["type"].toString() == "updateeventmessage")//主从设备同步UI的事件信息，比如：复位、释放、清除按钮按下、加热器永久故障，加热器开启等
        {
            _doc.setObject(_obj);
        }else if(_obj["type"].toString() == "updateqimiestatus")//主从设备同步气灭开关状态，比如：一车D柜气灭启动
        {
            _doc.setObject(_obj);
        }
        for(int i =0;i<m_udpEventAddressList.size();i++)
        {
            MyDeviceBaseInfo _address = m_udpEventAddressList.at(i);
            writeDatagram(_doc.toJson(), QHostAddress(_address.ip), _address.port);
        }
    }
}

void MyEventUDP::readPendingDatagrams()
{
    while (hasPendingDatagrams()) {
        QNetworkDatagram datagram = receiveDatagram();
        processTheDatagram(datagram);
    }
}

void MyEventUDP::processTheDatagram(QNetworkDatagram data)
{
    QJsonDocument _doc;
    _doc = QJsonDocument::fromJson(data.data(), nullptr);
    QJsonObject _obj;
    _obj = _doc.object();
    MyDataBaseWord _word;
    _word.type = _obj["type"].toString();
    if(data.data().toInt() > 0 && data.data().toInt() < 500 && _word.type == "")
    {
        emit setDebugNumber(data.data());
    }
    if(_word.type == "感温屏蔽状态")
    {
        QString _devicename = _obj["devicename"].toString();
        QString _status = _obj["cause"].toString();
        emit fireShieldStauts(_devicename, _status);
    }else if(_word.type == "感烟屏蔽状态")
    {
        QString _devicename = _obj["devicename"].toString();
        QString _subdevicename = _obj["isclear"].toString();
        QString _status = _obj["cause"].toString();
        emit smogShieldStatus(_devicename, _subdevicename, _status);
    }else if(_word.type == "setqxz"){
        QString _one, _two, _three, _four;
        _one = _obj["one"].toString();
        _two = _obj["two"].toString();
        _three = _obj["three"].toString();
        _four = _obj["four"].toString();
        emit setQXZ(_one, _two, _three, _four);

    }else if(_word.type == "setlmd")
    {
        QString _one;
        _one = _obj["one"].toString();
        emit setLMD(_one);
    }
    else if(_word.type == "setiostatus")
    {
        emit setIOStatus(_obj);
    }else if(_word.type == "resetstatus")
    {
        emit resetStatus();
    }else if(_word.type == "updategatestatus")
    {
        QString _devicename = _obj["devicename"].toString();
        QString _subdevicename = _obj["subdevicename"].toString();
        QString _status = _obj["status"].toString();
        bool _st;
        if(_status == "true")
        {
            _st = true;
        }else{
            _st = false;
        }
        emit updateGateStatus(_devicename, _subdevicename, _st);
    }else if(_word.type == "updatefaultmessage")
    {
        //将字符串解析出来并通知从设备UI
        QString _data = _obj["data"].toString();
        emit updateFaultMessage(_data);
    }else if(_word.type == "updateeventmessage")
    {
        QString _data = _obj["data"].toString();
        emit updateEventMessage(_data);
    }else if(_word.type == "updateqimiestatus")
    {
        _obj.remove("updateqimiestatus");
        emit updateQiMieStatus(_obj);
    }
    else
    {
        _word.event_word.date = _obj["date"].toString().toUInt();
        _word.event_word.devicename = _obj["devicename"].toString();
        _word.event_word.casue = _obj["cause"].toString();
        _word.event_word.isclear = _obj["isclear"].toString();
        emit sendUdpEvent(_word);
    }
}
