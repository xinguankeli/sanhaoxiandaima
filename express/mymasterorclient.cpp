#include "mymasterorclient.h"
#include <QNetworkDatagram>
#include <QHostAddress>
MyMasterOrClient::MyMasterOrClient(const QMap<QString, QPair<QString, quint16> > master, const QString masterSign, QObject *parent) : QUdpSocket(parent)
  ,m_masterOrClientAddress(master)
  ,m_recvCounter(0)
  ,m_masterOrClientSign(masterSign)
{
    //启动监听
    initUdpServer();
}

void MyMasterOrClient::initUdpServer()
{
    quint16 _port = m_masterOrClientAddress["client"].second;
    if(!bind(_port))
    {
#ifdef Q_OS_LINUX
        qDebug() << "master client udp server start failed";
#else
        qDebug() << "主udp监听启动失败";
#endif
        exit(-1);
    }else
    {
#ifdef Q_OS_LINUX
        qDebug() << "master client udp server start succ";
#else
        qDebug() << "主udp监听启动成功";
#endif
    }
    connect(this, &MyMasterOrClient::readyRead, this, &MyMasterOrClient::recvUDPMessage);
    m_sendUDPMessageTimer = new QTimer;
    connect(m_sendUDPMessageTimer, &QTimer::timeout, this, [&]{
        QByteArray _arr;
        _arr.append(0x01);
        sendUDPMessage(_arr);
    });
    m_sendUDPMessageTimer->setInterval(2000);
    m_sendUDPMessageTimer->start();

}

void MyMasterOrClient::processIOStatus(QByteArray arr)
{
    qDebug() << "收到从设备同步的IO点开关状态：" << arr;
    QHash<QString, QHash<QString, bool> > _hash;
    QString _str = arr;
    if(_str.indexOf("#") != -1)
    {
        QList<QString> _list = _str.split("#");
        for(int i = 0;i<_list.size();i++)
        {
            if(_list.at(i).indexOf(",") != -1)
            {
                QList<QString> _data = _list.at(i).split(",");
                QString _devicename = _data.at(0);
                QString _subdevicename = _data.at(1);
                QString _st = _data.at(2);
                bool _status;
                if(_st == "true")
                {
                    _status = true;
                }else
                {
                    _status = false;
                }
                QHash<QString ,bool> _subHash;
                _subHash.insert(_subdevicename, _status);
                _hash.insert(_devicename, _subHash);
            }
        }
    }else
    {
        if(_str.indexOf(",") != -1)
        {
            QList<QString> _data = _str.split(",");
            QString _devicename = _data.at(0);
            QString _subdevicename = _data.at(1);
            QString _st = _data.at(2);
            bool _status;
            if(_st == "true")
            {
                _status = true;
            }else
            {
                _status = false;
            }
            QHash<QString ,bool> _subHash;
            _subHash.insert(_subdevicename, _status);
            _hash.insert(_devicename, _subHash);
        }
    }
    emit updateIOStatus(_hash);
}

void MyMasterOrClient::recvUDPMessage()
{
    while (hasPendingDatagrams()) {
        QNetworkDatagram datagram = receiveDatagram();
        QByteArray _arr = datagram.data();
        if(_arr.size() == 1)
        {
            //如果从设备回复的是一个字节，那么是没状态同步
            qDebug() << "从给我回复的是这个";
            emit sendStatus(m_masterOrClientSign, "normal");
            //接收计数器清零
            m_recvCounter = 0;
        }else if(_arr.size() > 1)//就是回复的IO的输出点开关状态字符串
        {
            //否则就是同步IO点的开关状态
            processIOStatus(_arr);
            //主给从回复一个ok字符串
            sendUDPMessage("ok");
            //接收计数器清零
            m_recvCounter = 0;
        }
    }
}

void MyMasterOrClient::sendUDPMessage(QByteArray arr)
{
    QString _ip = m_masterOrClientAddress["client"].first;
    quint16 _port = m_masterOrClientAddress["client"].second;
//    qDebug() << _ip << _port << ":" << arr;
    writeDatagram(arr, QHostAddress(_ip), _port);
    //判断发送计速器是否超过上限
    //发送计数器+1
    m_recvCounter += 1;
    if(m_recvCounter > 5)
    {
        //告知UI对方掉线
        emit sendStatus(m_masterOrClientSign, "fault");
        m_recvCounter = 0;
    }
}

