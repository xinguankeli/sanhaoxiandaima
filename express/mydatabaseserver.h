#ifndef MYDATABASESERVER_H
#define MYDATABASESERVER_H

#include <QObject>
#include <QTcpServer>
#include <QTcpSocket>
class MyDataBaseServer : public QTcpServer
{
    Q_OBJECT
public:
    MyDataBaseServer(const quint16 &, QObject *parent = nullptr);
public slots:
    void sendData(QList<QString>);
    void startDataBaseServer();
private:
    quint16 m_port;
    QTcpSocket *m_client;
    bool m_sign;
private slots:
    void whenNewConnected();
    void whenDisConnected();
    void whenDataComing();
};

#endif // MYDATABASESERVER_H
