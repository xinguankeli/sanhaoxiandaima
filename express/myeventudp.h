#ifndef MYEVENTUDP_H
#define MYEVENTUDP_H

#include <QObject>
#include <QUdpSocket>
#include <QQueue>
#include "global/Global.h"
#include <QJsonDocument>
#include <QNetworkDatagram>
#include <QJsonObject>
class MyEventUDP : public QUdpSocket
{
    Q_OBJECT
public:
    MyEventUDP(QObject *parent = nullptr);
    void addEvent(MyDataBaseWord);
    void addEvent(QJsonObject);//发送IO的输出点开关状态用的
    void setEventAddressList(const QList<MyDeviceBaseInfo>);
private:
    QQueue<MyDataBaseWord> m_queue;//感烟和感温设备的屏蔽状态
    QQueue<QJsonObject> m_execqueue;//PDU设置、复位UI、IO开关状态
    QList<MyDeviceBaseInfo> m_udpEventAddressList;
private:
    void sendEvent();
private slots:
    void readPendingDatagrams();
    void processTheDatagram(QNetworkDatagram);
signals:
    void sendUdpEvent(MyDataBaseWord);
    void smogShieldStatus(QString, QString, QString);//设置感烟屏蔽状态
    void fireShieldStauts(QString, QString);//设置感温屏蔽状态
    void setQXZ(QString, QString, QString, QString);//设置感温曲线值
    void setLMD(QString);//设置感烟灵敏度
    void setIOStatus(QJsonObject);//设置IO状态
    void resetStatus();//复位
    void updateGateStatus(QString, QString, bool);//更新阀门的开关状态
    void updateFaultMessage(QString);//通知Core处理UI的故障消息
    void updateEventMessage(QString);//通知Core处理UI的事件消息
    void updateQiMieStatus(QJsonObject);//通知Core处理主设备发来的消息
    void setDebugNumber(QString);

};

#endif // MYEVENTUDP_H
