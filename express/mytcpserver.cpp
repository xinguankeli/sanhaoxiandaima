#include "mytcpserver.h"
#include <QDebug>

MyTcpServer::MyTcpServer(const quint16& port,  QObject *parent):QTcpServer(parent),m_port(port),m_online(false)
{

}

void MyTcpServer::whenNewConnected()
{
    m_online = true;
    m_client = nextPendingConnection();
    connect(m_client, &QTcpSocket::disconnected, this, &MyTcpServer::whenDisConnected);
    connect(m_client, &QTcpSocket::readyRead, this, &MyTcpServer::whenComingMessage);
}

void MyTcpServer::whenDisConnected()
{
    m_online = false;
    m_client->deleteLater();
}

void MyTcpServer::whenComingMessage()
{
    QJsonObject _obj = getJson(m_client->readAll());
    //在这里解析消息，并执行对应操作
    emit haveMessage(_obj);
}

void MyTcpServer::sendMessage(const QJsonObject &obj)
{
    if(m_online)
    {
//        qDebug() << obj["devicename"].toString() << obj["cause"].toString() << __LINE__ << __FUNCTION__;

//        qDebug() << obj["devicename"].toString() << obj["date"].toString() << __LINE__ << __FUNCTION__;
        QJsonDocument _doc;
        _doc.setObject(obj);
        m_client->write(_doc.toJson());
        m_client->flush();
    }
}

void MyTcpServer::startExpressServer()
{
    if(!listen(QHostAddress::AnyIPv4, m_port))
    {
       qDebug() << "mytcpserver listen error";
       exit(0);
    }
    connect(this, &MyTcpServer::newConnection, this, &MyTcpServer::whenNewConnected, Qt::QueuedConnection);
}

QJsonObject MyTcpServer::getJson(const QByteArray data)
{
    QJsonParseError _err;
    QJsonDocument _doc = QJsonDocument::fromJson(data, &_err);
    if(_err.error == QJsonParseError::NoError)
    {
        return _doc.object();
    }
    return _doc.object();
}

