#ifndef MYCLIENTORMASTER_H
#define MYCLIENTORMASTER_H

#include <QObject>
#include <QUdpSocket>
#include <QTimer>
class MyClientOrMaster : public QUdpSocket
{
    Q_OBJECT
public:
    explicit MyClientOrMaster(const QMap<QString, QPair<QString, quint16> >, const QString, QObject *parent = nullptr);
    void updateIOOutPutStatus(QHash<QString, QHash<QString, bool> >);
private:
    //主从设备的监听地址和端口
    QMap<QString, QPair<QString, quint16> > m_masterOrClientAddress;
    //主从设备的收数据计数器
    quint64 m_recvCounter;
    //主从标志
    QString m_masterOrClientSign;
    //为了减少主从之间的网络交互，从设备使用定时器来判断主设备是否掉线
    QTimer *m_recvMessageTimer;
    //IO设备的字符串
    QString m_ioStatusStr;
    //从设备的IO开关状态
    QHash<QString, QHash<QString, bool> > m_ioDeviceOutputStatus;
private:
    //启动UDP监听
    void initUdpServer();
    //重置定时器
    void resetTimer();
    //获取给主设备同步的IO输出点开关状态的字符串
    QString getIOStatus();
private slots:
    //接收来自对方的消息
    void recvUDPMessage();
    //向对方发送消息
    void sendUDPMessage(QByteArray);
    //处理主设备掉线
    void processDeviceOutLine();
    //发送IO的设备状态
    bool sendIOStatusToMaster();
signals:
    //第一参数是主从，第二个是故障和正常
    void sendStatus(QString, QString);
};

#endif // MYCLIENTORMASTER_H
