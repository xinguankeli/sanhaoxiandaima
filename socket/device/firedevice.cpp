#include "firedevice.h"

FireDevice::FireDevice(QObject *parent) : QObject(parent), m_mutex(new QMutex), m_typeClass("zuigao"),m_resetSign(false)
{
    QByteArray _firearr;
    _firearr.append(0x3A);
    _firearr.append(0x48);
    _firearr.append(0x4A);
    _firearr.append(0x3B);
    m_defaultLoopByteArrayHash.insert("HJ", _firearr);

    QByteArray _zharr;
    _zharr.append(0x3A);
    _zharr.append(0x5A);
    _zharr.append(0x48);
    _zharr.append(0x3B);
    m_defaultLoopByteArrayHash.insert("ZH", _zharr);


    QByteArray _taarr;
    _taarr.append(0x3A);
    _taarr.append(0x54);
    _taarr.append(0x41);
    _taarr.append(0x3B);
    m_defaultLoopByteArrayHash.insert("TA", _taarr);


    QByteArray _fwarr;
    _fwarr.append(0x3A);
    _fwarr.append(0x48);
    _fwarr.append(0x4A);
    _fwarr.append(0x46);
    _fwarr.append(0x57);
    _fwarr.append(0x3B);
    m_defaultLoopByteArrayHash.insert("FW", _fwarr);

    m_resetTimer = new QTimer(this);
    connect(m_resetTimer, &QTimer::timeout, this, &FireDevice::resendReset);
    m_resetTimer->setInterval(2500);
    m_resetTimer->setSingleShot(true);
}

void FireDevice::setDeviceInfo(QHash<QString, MyFireAlarmSensorInfo> hash)
{
    m_fireAlarmSensorInfoList = hash;
}


void FireDevice::startALLDevice()
{
    QList<QString> _keys = m_fireAlarmSensorInfoList.keys();
    for(int i =0;i<_keys.size();i++)
    {
        MyFireAlarmSensorInfo _struct = m_fireAlarmSensorInfoList[_keys.at(i)];
        MySocket *_socket = new MySocket(_struct.base.alias, _struct.base.ip, _struct.base.port, _struct.base.recv_length);
        m_socketHash.insert(_struct.base.alias, _socket);
        connect(_socket, &MySocket::deviceStatus, this, &FireDevice::processDeviceStatus, Qt::QueuedConnection);
        connect(_socket, &MySocket::deviceData, this, &FireDevice::processDeviceData, Qt::QueuedConnection);
        connect(_socket, &MySocket::writeMyDataSignal, _socket, &MySocket::writeMyDataSlot, Qt::BlockingQueuedConnection);

        QThread *thread = new QThread;
        m_socketThreadHash.insert(_struct.base.alias, thread);
        _socket->moveToThread(thread);
        connect(thread, &QThread::started, _socket, &MySocket::startTcpSocket);
    }
    for(int i =0;i<_keys.size();i++)
    {
        m_socketThreadHash[_keys.at(i)]->start();
    }
    for(int i =0;i<_keys.size();i++)
    {
        m_resetStatusSaveHash.insert(_keys.at(i), false);
    }
    for(int i =0;i<_keys.size();i++)
    {
        FireDeviceStatus _status;
        _status.firestatus = "无火警";
        _status.resetstatus = "未复位";
        _status.networkstatus = NETWORKSUCC;
        _status.shieldstatus = m_fireAlarmSensorInfoList[_keys.at(i)].base.shield;
        m_deviceStatusHash.insert(_keys.at(i), _status);
        m_typeHash.insert(_keys.at(i), "");
    }
    m_timer = new QTimer(this);
    connect(m_timer, &QTimer::timeout, this, [&]{
        for(int i =0;i<m_socketHash.size();i++)
        {

            emit m_socketHash[m_socketHash.keys().at(i)]->writeMyDataSignal(m_defaultLoopByteArrayHash["HJ"]);
        }
        m_zuigaowenduAndhuanjingwenduTimer->start();
    });
    m_timer->start(3000);
    m_zuigaowenduAndhuanjingwenduTimer = new QTimer(this);
    m_zuigaowenduAndhuanjingwenduTimer->setSingleShot(true);
    m_zuigaowenduAndhuanjingwenduTimer->setInterval(2000);
    connect(m_zuigaowenduAndhuanjingwenduTimer, &QTimer::timeout, this, [&]{
        QString _type;
        QString _str;
        if(m_typeClass == "zuigao")
        {
            _type = "zuigao";
            for(int i =0;i<m_typeHash.size();i++)
            {
                m_typeHash[m_typeHash.keys().at(i)] = _type;
            }
            for(int i = 0 ;i <m_socketHash.size();i++)
            {
                emit m_socketHash[m_socketHash.keys().at(i)]->writeMyDataSignal(m_defaultLoopByteArrayHash["ZH"]);
            }
            m_typeClass = "huanjing";
        }else if(m_typeClass == "huanjing")
        {
            _type = "huanjing";
            for(int i =0;i<m_typeHash.size();i++)
            {
                m_typeHash[m_typeHash.keys().at(i)] = _type;
            }
            for(int i = 0 ;i <m_socketHash.size();i++)
            {
                emit m_socketHash[m_socketHash.keys().at(i)]->writeMyDataSignal(m_defaultLoopByteArrayHash["TA"]);
            }
            m_typeClass = "zuigao";
        }
    });
}

QList<QString> FireDevice::getDeviceNames()
{
    return m_socketHash.keys();
}

void FireDevice::setNetworkStatus(const QString &key, const QString value)
{
    if(m_deviceStatusHash.contains(key))
    {
//        qDebug() << key << ":" << value << __LINE__ << __FUNCTION__;
        //取出当前值
        m_mutex->lock();
        FireDeviceStatus _status = m_deviceStatusHash.value(key);
        _status.networkstatus = value;
        m_deviceStatusHash[key] = _status;
        m_mutex->unlock();
        emit updateDeviceStatus(key, _status);
    }
}

void FireDevice::setResetStatus(const QString &key, const QString value)
{
    if(m_deviceStatusHash.contains(key))
    {
        m_mutex->lock();
        FireDeviceStatus _status = m_deviceStatusHash.value(key);
        _status.firestatus = "无火警";
        _status.resetstatus = value;
        m_deviceStatusHash[key] = _status;
        m_mutex->unlock();
        emit updateDeviceStatus(key, _status);
    }
}

void FireDevice::setFireStatus(const QString &key, const QString value)
{
    if(m_deviceStatusHash.contains(key))
    {
        m_mutex->lock();
        FireDeviceStatus _status = m_deviceStatusHash.value(key);
        _status.firestatus = value;
        _status.resetstatus = "未复位";
        m_deviceStatusHash[key] = _status;
        m_mutex->unlock();
        emit updateDeviceStatus(key, _status);
    }
}

void FireDevice::setSheildStatus(const QString & key, const QString value)
{
    if(m_deviceStatusHash.contains(key))
    {
        m_mutex->lock();
        FireDeviceStatus _status = m_deviceStatusHash.value(key);
        _status.shieldstatus = value;
        _status.firestatus = "无火警";
        m_deviceStatusHash[key] = _status;
        m_mutex->unlock();
        emit updateDeviceStatus(key, _status);
    }
}

void FireDevice::setMaxWenDu(const QString & key, const quint16 value)
{
    if(m_deviceStatusHash.contains(key))
    {
        m_mutex->lock();
        FireDeviceStatus _status = m_deviceStatusHash.value(key);
        _status.maxwendu = value;
        m_deviceStatusHash[key] = _status;
        m_mutex->unlock();
        emit updateDeviceStatus(key, _status);
    }
}

void FireDevice::setHuanJingWenDu(const QString & key, const quint16 value)
{
    if(m_deviceStatusHash.contains(key))
    {
        m_mutex->lock();
        FireDeviceStatus _status = m_deviceStatusHash.value(key);
        _status.huanjingwendu = value;
        m_deviceStatusHash[key] = _status;
        m_mutex->unlock();
        emit updateDeviceStatus(key, _status);
    }
}

void FireDevice::setFourValues(const QString & key, const QString v1, const QString v2, const QString v3, const QString v4)
{
    if(m_deviceStatusHash.contains(key))
    {
        m_mutex->lock();
        FireDeviceStatus _status = m_deviceStatusHash.value(key);
        _status.onevalue = v1;
        _status.twovalue = v2;
        _status.threeValue = v3;
        _status.fourValue = v4;
        m_deviceStatusHash[key] = _status;
        m_mutex->unlock();
        emit updateDeviceStatus(key, _status);
    }
}


FireDeviceStatus FireDevice::getDeviceStatus(const QString &devicename)
{
    m_mutex->lock();
    FireDeviceStatus _status = m_deviceStatusHash[devicename];
    m_mutex->unlock();
    return _status;
}

void FireDevice::resetSaveHash()
{
//    qDebug() << "重置复位结构";
    for(int i =0;i<m_resetStatusSaveHash.size();i++)
    {
        m_resetStatusSaveHash[m_resetStatusSaveHash.keys().at(i)] = false;
    }
}

bool FireDevice::computeSaveHash()
{

    bool _ret = true;
    for(int i =0;i<m_resetStatusSaveHash.size();i++)
    {
        FireDeviceStatus _status = getDeviceStatus(m_resetStatusSaveHash.keys().at(i));
        if(_status.shieldstatus == "已屏蔽" || _status.networkstatus == NETWORKERR)
        {
            _ret &= true;
            continue;
        }
        _ret &= m_resetStatusSaveHash[m_resetStatusSaveHash.keys().at(i)];
//        qDebug() << _ret << ": &" << m_resetStatusSaveHash[m_resetStatusSaveHash.keys().at(i)];
    }
    return _ret;
}

void FireDevice::setDeviceResetHash(const QString devicename, const bool status)
{
    m_resetStatusSaveHash[devicename] = status;
//    qDebug() << devicename << "收到重置标识：" << status;
}

void FireDevice::resendReset()
{
    for(int i = 0 ;i < m_socketHash.size();i++)
    {
//        qDebug() << m_socketHash.keys().at(i) << m_defaultLoopByteArrayHash["FW"];
        emit m_socketHash[m_socketHash.keys().at(i)]->writeMyDataSignal(m_defaultLoopByteArrayHash["FW"]);
    }
}


void FireDevice::processDeviceStatus(QString devicename, QString status)
{
    if(getDeviceStatus(devicename).shieldstatus == "已屏蔽")
    {
//        qDebug() << devicename << getDeviceStatus(devicename).shieldstatus;
        setSheildStatus(devicename, "已屏蔽");
        return;
    }
    //通过信号将设备状态发送出去
    setNetworkStatus(devicename, status);
}

void FireDevice::processDeviceData(QString devicename, QByteArray buff)
{
    if(getDeviceStatus(devicename).shieldstatus == "已屏蔽")
    {
//        qDebug() << devicename << getDeviceStatus(devicename).shieldstatus;
        setSheildStatus(devicename, "已屏蔽");
        return;
    }
//    if(buff == ":FWOK;")
//    {
//        qDebug() << devicename << buff << buff.size();
//    }
    //将处理的数据分解成各种状态，并通过统一信号发出去
    if(buff.indexOf(":HJ1NNNNNN;") != -1 || buff.indexOf(":NNNHJ2NNN;") != -1 || buff.indexOf(":NNNNNNHJ3;") != -1){
        //火警
        setFireStatus(devicename, "有火警");
    }else if(buff.indexOf(":NNNNNNNNN;") != -1){
//                qDebug() << devicename << "复位成功" << buff;

        setFireStatus(devicename, "无火警");


    }else if(buff.indexOf(":FWOK;") != -1)
    {
        //复位成功
        if(m_resetSign)
        {
            setDeviceResetHash(devicename, true);
            if(computeSaveHash())
            {
                //如果所有复位成功，就不在继续复位
                if(m_resetTimer->isActive())
                {
                    qDebug() << "所有设备复位成功，关闭定时器";
                    if(!m_timer->isActive())
                    {
                        m_timer->start(3000);
                    }
                    m_resetTimer->stop();
                }
                emit resetOk();
                m_resetSign = false;
            }else
            {
                //如果未成功，继续执行复位
                if(!m_resetTimer->isActive())
                {
                    if(m_resetSign)
                    {
                        qDebug() << "激活重置定时器";
                        m_resetTimer->start();
                    }
                }
            }
        }
        setResetStatus(devicename, "复位成功");
    }
    else if(buff.size() == 4)//就是环境温度和最高温度了
    {
        QByteArray _arr;
        _arr.append(buff.at(1));
        _arr.append(buff.at(2));
        qint16 _value;
        memset(&_value, 0, sizeof(qint16));
        memcpy(&_value, _arr.data(), sizeof(qint16));
        if(m_typeClass == "zuigao")
        {
//            qDebug() << "最高温度:" << _value;
            setMaxWenDu(devicename, _value);
        }else if(m_typeClass == "huanjing")
        {
//            qDebug() << "环境温度:" << _value;
            setHuanJingWenDu(devicename, _value);
        }
    }
}

void FireDevice::writeDataTo(const QString devicename, const QByteArray buff)
{
    if(m_socketHash.contains(devicename))
    {
        emit m_socketHash[devicename]->writeMyDataSignal(buff);
    }
}

void FireDevice::resetFireDeviceSlot(const QString devicename)
{
    if(m_timer->isActive())
    {
//        qDebug() << "停止定时器";
        m_timer->stop();
    }
    if(m_zuigaowenduAndhuanjingwenduTimer->isActive())
    {
        m_zuigaowenduAndhuanjingwenduTimer->stop();
    }
//    if(m_socketHash.contains(devicename))
//    {
////        qDebug() << devicename << m_defaultLoopByteArrayHash["FW"];
//        emit m_socketHash[devicename]->writeMyDataSignal(m_defaultLoopByteArrayHash["FW"]);
//    }
    //重置复位数据结构
    resetSaveHash();
    //启动重置标志位
    m_resetSign = true;
//    for(int i = 0 ;i < m_socketHash.size();i++)
//    {
//        qDebug() << m_socketHash.keys().at(i) << m_defaultLoopByteArrayHash["FW"];
        emit m_socketHash[devicename]->writeMyDataSignal(m_defaultLoopByteArrayHash["FW"]);
//    }
    if(!m_resetTimer->isActive())
    {
        m_resetTimer->start();
    }

}

void FireDevice::normalFireDeviceSlot(const QString devicename)
{
//    for(int i =0;i<m_socketHash.size();i++)
//    {
//        //将复位状态清空
//        setResetStatus(m_socketHash.keys().at(i), "无复位");
//        //将火警状态清空
//        setFireStatus(m_socketHash.keys().at(i), "无火警");
//    }
    if(m_resetTimer->isActive())
    {
        m_resetTimer->stop();
    }
    if(!m_timer->isActive())
    {
        m_timer->start(3000);
    }
}

void FireDevice::setSheildStatusSlot(const QString &devicename, const QString status)
{
    //更新屏蔽状态
    qDebug() << devicename << ":" << status;
    setSheildStatus(devicename, status);
}

void FireDevice::setQXZ(const QString one, const QString two, const QString three, const QString four)
{
    quint16 _one, _two, _three, _four;
    _one = one.toUInt() * 10;
    _two = two.toUInt() * 10;
    _three = three.toUInt() * 10;
    _four = four.toUInt() * 10;

    //上面是转完十六进制ide数
    quint8 _one1, _two1, _three1, _four1;
    _one1 &= 0;
    _two1 &= 0;
    _three1 &= 0;
    _four1 &= 0;

    _one1 = _one &  0xFF;
    _two1 = _two &  0xFF;
    _three1 = _three & 0xFF;
    _four1 = _four & 0xFF;
    //上面放到7里面

    quint16 _one2, _two2, _three2, _four2;
    _one2 &= 0;
    _two2 &= 0;
    _three2 &= 0;
    _four2 &= 0;

    _one2 = _one & 0xFF00;
    _two2 = _two & 0xFF00;
    _three2 = _three & 0xFF00;
    _four2 = _four & 0xFF00;

    quint8 _one3, _two3, _three3, _four3;
    _one3 &= 0;
    _two3 &= 0;
    _three3 &= 0;
    _four3 &= 0;
    _one2 = _one2 >> 8;
    _two2 = _two2 >> 8;
    _three2 = _three2 >> 8;
    _four2 = _four2 >> 8;



    _one3 = _one2;
    _two3 =  _two2;
    _three3 = _three2;
    _four3 = _four2;

    QByteArray _data;
    TEMPDATA _tempdata;
    memset(&_tempdata, 0, sizeof(TEMPDATA));
    _tempdata.data[0] = _one1;
    _tempdata.data[1] = _one3;
    _tempdata.data[2] = _two1;
    _tempdata.data[3] = _two3;
    _tempdata.data[4] = _three1;
    _tempdata.data[5] = _three3;
    _tempdata.data[6] = _four1;
    _tempdata.data[7] = _four3;
//    qDebug() << _data << ":" << _tempdata.data[0] << _tempdata.data[1] << _tempdata.data[2]
//                 << _tempdata.data[3] << _tempdata.data[4] << _tempdata.data[5] << _tempdata.data[6] << _tempdata.data[7];

    QByteArray _arr;
    _arr.append(0x3A);
    _arr.append(0x53);
    _arr.append(0x45);
    _arr.append(0x54);
    _arr.append(0x51);
    _arr.append(0x58);
    _arr.append(0x5A);
    _arr.append(_tempdata.data[0]);
    _arr.append(_tempdata.data[1]);
    _arr.append(_tempdata.data[2]);
    _arr.append(_tempdata.data[3]);
    _arr.append(_tempdata.data[4]);
    _arr.append(_tempdata.data[5]);
    _arr.append(_tempdata.data[6]);
    _arr.append(_tempdata.data[7]);

    _arr.append(0x3B);
    for(int i =0;i<m_socketHash.size();i++)
    {
        QString _key = m_socketHash.keys().at(i);
        setFourValues(_key, one, two, three, four);
//        qDebug() << m_socketHash.keys().at(i) << _arr;
        emit m_socketHash[_key]->writeMyDataSignal(_arr);
    }
}
