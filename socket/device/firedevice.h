#ifndef FIREDEVICE_H
#define FIREDEVICE_H

#include <QObject>
#include <QHash>
#include <QThread>
#include <QMutex>
#include "../mysocket.h"
#include <QList>

//1.获取火警设备的状态
//2.设置火警设备的底层
//3.执行火警设备的复位
struct TEMPDATA
{
    quint8 data[8];
};

class FireDevice : public QObject
{
    Q_OBJECT
public:
    explicit FireDevice(QObject *parent = nullptr);
    //配置文件设置火警设备信息
    void setDeviceInfo(QHash<QString, MyFireAlarmSensorInfo>);
    //启动所有火警设备
    void startALLDevice();

    QList<QString> getDeviceNames();
private:
    //所有火警设备
    QHash<QString, MySocket*> m_socketHash;

    //所有火警设备的子线程
    QHash<QString, QThread*> m_socketThreadHash;

    //所有火警设备信息
    QHash<QString, MyFireAlarmSensorInfo> m_fireAlarmSensorInfoList;

    //存放所有设备的状态
    QHash<QString, FireDeviceStatus> m_deviceStatusHash; 

    //存放获取的是环境温度还是最高温度
    QHash<QString, QString> m_typeHash;

    //记录要接收的是什么数据
    //zuigao 和 huanjing
    QString m_typeClass;

    //写入数据的锁
    QMutex *m_mutex;

    //轮询所有设备火警的定时器
    QTimer *m_timer;

    //轮询所有设备最高温度和环境的定时器
    QTimer *m_zuigaowenduAndhuanjingwenduTimer;

    //存放默认询问状态的字节
    QHash<QString, QByteArray> m_defaultLoopByteArrayHash;

    //在复位开始那一刻，将该数据结构全部置为false，直到全部接满true，否在再次发送复位
    QHash<QString,bool> m_resetStatusSaveHash;

    //复位定时器,复位不成功，一直复位
    QTimer *m_resetTimer;
    //用来重置复位标识后，将该标志设置为true，开始收集重置标志
    bool m_resetSign;

private:
    //设置网络连接故障状态
    //key是设备名，value是网络状态的值
    void setNetworkStatus(const QString &, const QString);

    //设置复位状态
    //key是设备名，value是复位状态的值
    void setResetStatus(const QString &, const QString);

    //设置火警状态
    //key是设备名，value是火警状态的值
    void setFireStatus(const QString &, const QString);

    //设置屏蔽状态
    //key是设备名，value是屏蔽状态的值
    void setSheildStatus(const QString &, const QString);

    //设置最高温度
    //key是设备名，value是最高温度
    void setMaxWenDu(const QString &, const quint16);

    //设置环境温度
    //key是设备名，value是环境温度
    void setHuanJingWenDu(const QString &, const quint16);

    //设置四个值
    //key是设备名，v1 v2 v3 v4是四个值
    void setFourValues(const QString &, const QString, const QString, const QString, const QString);

    //获取屏蔽状态
    //key是设备名
    FireDeviceStatus getDeviceStatus(const QString &);
private:
    //重置所有复位接收数据结构
    void resetSaveHash();
    //计算一次复位数据结构，屏蔽的不算，全复位成功返回true
    bool computeSaveHash();
    //设置某个设备重置成功
    void setDeviceResetHash(const QString, const bool);

private slots:
    //发送复位函数
    void resendReset();
public slots:

    //处理设备的在线状态
    //设备的网络连接状态
    //第一个参数为设备名
    //第二个参数为网络连接错误或网络连接成功
    void processDeviceStatus(QString, QString);


    //处理设备的数据
    //设备发送上来的数据处理后解析成各种信号发出去
    void processDeviceData(QString, QByteArray);

    //根据key来执行对应的逻辑
    //设置火警的温度曲线
    //设置xxx等
    void writeDataTo(const QString, const QByteArray);

    //内部执行复位操作
    void resetFireDeviceSlot(const QString );

    //内部执行正常操作
    void normalFireDeviceSlot(const QString );

    //外部使用信号连接此函数，设置感温设备的屏蔽状态
    //第一个参数是设备名
    //第二个参数是已屏蔽、未屏蔽
    void setSheildStatusSlot(const QString &, const QString);

    //外部使用信号连接此函数，设置感温设备的曲线值
    //第一个参数是8个字节组成的四个值
    void setQXZ(const QString, const QString, const QString, const QString);
signals:
    //通过该信号将设备的状态发送出去
    //复位成功，火警状态等
    void updateDeviceStatus(const QString, FireDeviceStatus);

    //resetok
    void resetOk();

};

#endif // FIREDEVICE_H
