#include "smogdevice.h"

SmogDevice::SmogDevice(QObject *parent) : QObject(parent), m_mutex(new QMutex),m_enquireFireCount(0),m_enquireFaultCount(0),m_faultCount(0),m_jiaoti(false)
{

}

void SmogDevice::setDeviceInfo(QHash<QString, MySmogSensorInfo> hash)
{
    m_smogSensorInfoList = hash;
}

void SmogDevice::setDeviceLMD(QString lmd)
{
    m_lingmindu = lmd.toInt();
}

void SmogDevice::startALLDevice()
{
    QList<QString> _keys = m_smogSensorInfoList.keys();
    for(int i =0;i<_keys.size();i++)
    {
        MySmogSensorInfo _struct = m_smogSensorInfoList[_keys.at(i)];
        MySocket *_socket = new MySocket(_struct.base.alias, _struct.base.ip, _struct.base.port, _struct.base.recv_length);
        m_socketHash.insert(_struct.base.alias, _socket);
        connect(_socket, &MySocket::deviceStatus, this, &SmogDevice::processDeviceStatus, Qt::QueuedConnection);
        connect(_socket, &MySocket::deviceData, this, &SmogDevice::processDeviceData, Qt::QueuedConnection);
        connect(_socket, &MySocket::writeMyDataSignal, _socket, &MySocket::writeMyDataSlot, Qt::QueuedConnection);

        QThread *thread = new QThread;
        m_socketThreadHash.insert(_struct.base.alias, thread);
        _socket->moveToThread(thread);
        connect(thread, &QThread::started, _socket, &MySocket::startTcpSocket);

        QHash<int, QString> _indexHash;
        _indexHash.insert(0, _struct.address1);
        _indexHash.insert(1, _struct.address2);
        _indexHash.insert(2, _struct.address3);
        _indexHash.insert(3, _struct.address4);
        _indexHash.insert(4, _struct.address5);
        _indexHash.insert(5, _struct.address6);
        _indexHash.insert(6, _struct.address7);
        _indexHash.insert(7, _struct.address8);
        _indexHash.insert(8, _struct.address9);
        _indexHash.insert(9, _struct.address10);
        _indexHash.insert(10, _struct.address11);
        _indexHash.insert(11, _struct.address12);
        _indexHash.insert(12, _struct.address13);
        _indexHash.insert(13, _struct.address14);
        _indexHash.insert(14, _struct.address15);
        _indexHash.insert(15, _struct.address16);
        _indexHash.insert(16, _struct.address17);
        m_aliasByIndexHash.insert(_struct.base.alias, _indexHash);
    }
    for(int i =0;i<_keys.size();i++)
    {
        m_socketThreadHash[_keys.at(i)]->start();
    }
    for(int i =0;i<_keys.size();i++)
    {
        MySmogSensorInfo _struct = m_smogSensorInfoList[_keys.at(i)];
        SmogDeviceStatus _status;
        _status.firestatus.insert(_struct.address1, "无火警");
        _status.firestatus.insert(_struct.address2, "无火警");
        _status.firestatus.insert(_struct.address3, "无火警");
        _status.firestatus.insert(_struct.address4, "无火警");
        _status.firestatus.insert(_struct.address5, "无火警");
        _status.firestatus.insert(_struct.address6, "无火警");
        _status.firestatus.insert(_struct.address7, "无火警");
        _status.firestatus.insert(_struct.address8, "无火警");
        _status.firestatus.insert(_struct.address9, "无火警");
        _status.firestatus.insert(_struct.address10, "无火警");
        _status.firestatus.insert(_struct.address11, "无火警");
        _status.firestatus.insert(_struct.address12, "无火警");
        _status.firestatus.insert(_struct.address13, "无火警");
        _status.firestatus.insert(_struct.address14, "无火警");
        _status.firestatus.insert(_struct.address15, "无火警");
        _status.firestatus.insert(_struct.address16, "无火警");
        _status.firestatus.insert(_struct.address17, "无火警");

        _status.faultstatus.insert(_struct.address1, "无故障");
        _status.faultstatus.insert(_struct.address2, "无故障");
        _status.faultstatus.insert(_struct.address3, "无故障");
        _status.faultstatus.insert(_struct.address4, "无故障");
        _status.faultstatus.insert(_struct.address5, "无故障");
        _status.faultstatus.insert(_struct.address6, "无故障");
        _status.faultstatus.insert(_struct.address7, "无故障");
        _status.faultstatus.insert(_struct.address8, "无故障");
        _status.faultstatus.insert(_struct.address9, "无故障");
        _status.faultstatus.insert(_struct.address10, "无故障");
        _status.faultstatus.insert(_struct.address11, "无故障");
        _status.faultstatus.insert(_struct.address12, "无故障");
        _status.faultstatus.insert(_struct.address13, "无故障");
        _status.faultstatus.insert(_struct.address14, "无故障");
        _status.faultstatus.insert(_struct.address15, "无故障");
        _status.faultstatus.insert(_struct.address16, "无故障");
        _status.faultstatus.insert(_struct.address17, "无故障");

        _status.resetstatus.insert(_struct.address1 , "未复位");
        _status.resetstatus.insert(_struct.address2 , "未复位");
        _status.resetstatus.insert(_struct.address3 , "未复位");
        _status.resetstatus.insert(_struct.address4 , "未复位");
        _status.resetstatus.insert(_struct.address5 , "未复位");
        _status.resetstatus.insert(_struct.address6 , "未复位");
        _status.resetstatus.insert(_struct.address7 , "未复位");
        _status.resetstatus.insert(_struct.address8 , "未复位");
        _status.resetstatus.insert(_struct.address9 , "未复位");
        _status.resetstatus.insert(_struct.address10 , "未复位");
        _status.resetstatus.insert(_struct.address11 , "未复位");
        _status.resetstatus.insert(_struct.address12 , "未复位");
        _status.resetstatus.insert(_struct.address13 , "未复位");
        _status.resetstatus.insert(_struct.address14 , "未复位");
        _status.resetstatus.insert(_struct.address15 , "未复位");
        _status.resetstatus.insert(_struct.address16 , "未复位");
        _status.resetstatus.insert(_struct.address17 , "未复位");


        _status.shieldstatus.insert(_struct.address1, _struct.address1shield);
        _status.shieldstatus.insert(_struct.address2, _struct.address2shield);
        _status.shieldstatus.insert(_struct.address3, _struct.address3shield);
        _status.shieldstatus.insert(_struct.address4, _struct.address4shield);
        _status.shieldstatus.insert(_struct.address5, _struct.address5shield);
        _status.shieldstatus.insert(_struct.address6, _struct.address6shield);
        _status.shieldstatus.insert(_struct.address7, _struct.address7shield);
        _status.shieldstatus.insert(_struct.address8, _struct.address8shield);
        _status.shieldstatus.insert(_struct.address9, _struct.address9shield);
        _status.shieldstatus.insert(_struct.address10, _struct.address10shield);
        _status.shieldstatus.insert(_struct.address11, _struct.address11shield);
        _status.shieldstatus.insert(_struct.address12, _struct.address12shield);
        _status.shieldstatus.insert(_struct.address13, _struct.address13shield);
        _status.shieldstatus.insert(_struct.address14, _struct.address14shield);
        _status.shieldstatus.insert(_struct.address15, _struct.address15shield);
        _status.shieldstatus.insert(_struct.address16, _struct.address16shield);
        _status.shieldstatus.insert(_struct.address17, _struct.address17shield);

        _status.networkstatus = "网络连接成功";
        m_deviceStatusHash.insert(_keys.at(i), _status);

        QHash<int, int> _faultHash;
        _faultHash.insert(0, 0);
        _faultHash.insert(1, 0);
        _faultHash.insert(2, 0);
        _faultHash.insert(3, 0);
        _faultHash.insert(4, 0);
        _faultHash.insert(5, 0);
        _faultHash.insert(6, 0);
        _faultHash.insert(7, 0);
        _faultHash.insert(8, 0);
        _faultHash.insert(9, 0);
        _faultHash.insert(10, 0);
        _faultHash.insert(11, 0);
        _faultHash.insert(12, 0);
        _faultHash.insert(13, 0);
        _faultHash.insert(14, 0);
        _faultHash.insert(15, 0);
        _faultHash.insert(16, 0);
        m_smogSensorFaultHash.insert(_keys.at(i), _faultHash);
    }
    m_fireTimer = new QTimer;
    connect(m_fireTimer, &QTimer::timeout, this, [&]{
        if(m_jiaoti)
        {
            for(int i =0;i<m_socketHash.size();i++)
            {
                writeDataTo(m_socketHash.keys().at(i), getSendBuff("fire"));
            }
            m_jiaoti = false;
        }else
        {
            for(int i =0;i<m_socketHash.size();i++)
            {
                writeDataTo(m_socketHash.keys().at(i), getSendBuff("fault"));
            }
            m_jiaoti = true;
        }

        m_faultCount += 1;
        if(m_faultCount > 4)
        {
            //计算一次是否有设备故障
//            computeSmogDeviceFault();
            m_faultCount = 0;
        }
    });
    m_fireTimer->start(3000);
}

QList<QString>  SmogDevice::getDeviceNames()
{
    return m_socketHash.keys();
}

void SmogDevice::setNetworkStatus(const QString & key, const QString value)
{
    if(m_deviceStatusHash.contains(key))
    {
        m_mutex->lock();
        SmogDeviceStatus _status = m_deviceStatusHash.value(key);
        _status.networkstatus = value;
        QList<QString> _keys = _status.faultstatus.keys();
        QHash<QString, QString> _faultHash;
        if(value == NETWORKERR)
        {
            for(int i =0;i<_keys.size();i++)
            {
                _faultHash.insert(_keys.at(i), "有故障");
            }
        }else if(value == NETWORKSUCC)
        {
            for(int i =0;i<_keys.size();i++)
            {
                _faultHash.insert(_keys.at(i), "无故障");
            }
        }

        _status.faultstatus = _faultHash;
        m_deviceStatusHash[key] = _status;
        m_mutex->unlock();
        emit updateDeviceStatus(key, _status);
    }
}

void SmogDevice::setResetStatus(const QString & key, const QHash<QString, QString> value)
{
    if(m_deviceStatusHash.contains(key))
    {
        m_mutex->lock();
        SmogDeviceStatus _status = m_deviceStatusHash.value(key);
        _status.resetstatus = value;
        QHash<QString, QString> _fireHash;
        for(int i =0;i<value.size();i++)
        {
            _fireHash.insert(value.keys().at(i), "无火警");
        }
        _status.firestatus = _fireHash;
        m_deviceStatusHash[key] = _status;
        m_mutex->unlock();
        emit updateDeviceStatus(key, _status);
    }
}

void SmogDevice::setFireStatus(const QString &key, const QHash<QString, QString> value)
{
    if(m_deviceStatusHash.contains(key))
    {
        m_mutex->lock();
        SmogDeviceStatus _status = m_deviceStatusHash.value(key);
        _status.firestatus = value;
        QHash<QString, QString> _resetHash;
        for(int i =0;i<value.size();i++)
        {
            _resetHash.insert(value.keys().at(i), "未复位");
        }
        _status.resetstatus = _resetHash;
        m_deviceStatusHash[key] = _status;
        m_mutex->unlock();
        emit updateDeviceStatus(key, _status);
    }
}

void SmogDevice::setFaultStatus(const QString &key, const QHash<QString, QString> value)
{
    if(m_deviceStatusHash.contains(key))
    {
        m_mutex->lock();
        SmogDeviceStatus _status = m_deviceStatusHash.value(key);
        _status.faultstatus = value;
        m_deviceStatusHash[key] = _status;
        m_mutex->unlock();
        emit updateDeviceStatus(key, _status);
    }
}

void SmogDevice::setSheildStatus(const QString &key, const QString &sub, const QString value)
{
    qDebug() << key << "-" << sub << ":" << value;
    if(m_deviceStatusHash.contains(key))
    {
        m_mutex->lock();
        SmogDeviceStatus _status = m_deviceStatusHash.value(key);
        _status.shieldstatus[sub] = value;
        m_deviceStatusHash[key] = _status;
        m_mutex->unlock();
        emit updateDeviceStatus(key, _status);
    }
}

SmogDeviceStatus SmogDevice::getShieldStatus(const QString &devicename)
{
    m_mutex->lock();
    SmogDeviceStatus _status = m_deviceStatusHash[devicename];
    m_mutex->unlock();
    return _status;
}

QString SmogDevice::getSubDeviceNameByIndex(const QString &devicename, const int &index)
{
    return m_aliasByIndexHash[devicename].value(index);
}

QByteArray SmogDevice::getSendBuff(const QString &type)
{
    QByteArray _arr;
    if(type == "fire")
    {
        _arr.append(0xba);
        _arr.append(0x52);
        _arr.append(0x42);
        _arr.append(0x46);
        _arr.append((char)0x00);
        _arr.append((char)0x00);
        _arr.append(0xbb);
    }else if(type == "fault")
    {
        _arr.append(0xba);
        _arr.append(0x52);
        _arr.append(0x42);
        _arr.append(0x44);
        _arr.append((char)0x00);
        _arr.append((char)0x00);
        _arr.append(0xbb);
    }else if(type == "reset")
    {
        _arr.append(0xba);
        _arr.append(0x52);
        _arr.append(0x45);
        _arr.append(0x53);
        _arr.append(0x45);
        _arr.append(0x54);
        _arr.append(0xbb);
    }
    return _arr;
}

void SmogDevice::processDeviceStatus(QString devicename, QString status)
{
//    if(devicename.indexOf("感烟传感器B区") != -1)
//    {
//        qDebug() << devicename << status;
//    }

    setNetworkStatus(devicename, status);
}

void SmogDevice::processDeviceData(QString devicename, QByteArray buff)
{

    RecvData _data;
    memset(&_data, 0, sizeof(RecvData));
    memcpy(&_data, buff.data(), sizeof(RecvData));
    if(_data.data[0] == 0xba)//报警数据
    {
        QHash<QString, QString> _tempFireHash;
        for(int i = 1;i<18;i++)
        {
            m_smogSensorFaultHash[devicename][i - 1] = _data.data[i];
            if(_data.data[i] > m_lingmindu)
            {
                if(getShieldStatus(devicename).shieldstatus.value(getSubDeviceNameByIndex(devicename, i - 1)) == "已屏蔽")
                {
                    setSheildStatus(devicename, getSubDeviceNameByIndex(devicename, i - 1), "已屏蔽");
                    //                    qDebug() << getSubDeviceNameByIndex(devicename, i - 1) << "有火警" << __FUNCTION__;
                    _tempFireHash.insert(getSubDeviceNameByIndex(devicename, i - 1), "无火警");
                }else{
                    _tempFireHash.insert(getSubDeviceNameByIndex(devicename, i - 1), "有火警");
                }
            }else
            {
                _tempFireHash.insert(getSubDeviceNameByIndex(devicename, i - 1), "无火警");
            }
        }
        setFireStatus(devicename, _tempFireHash);
    }else if(_data.data[0] == 0xca)
    {
        QHash<QString, QString> _tempFaultHash;
        for(int i = 1;i<18;i++)
        {
            if(_data.data[i] == 0x01)//1是故障，报故障状态
            {
                if(getShieldStatus(devicename).shieldstatus.value(getSubDeviceNameByIndex(devicename, i - 1)) == "已屏蔽")
                {
                    setSheildStatus(devicename, getSubDeviceNameByIndex(devicename, i - 1), "已屏蔽");
                    _tempFaultHash.insert(getSubDeviceNameByIndex(devicename, i - 1), "无故障");
//                    qDebug() << getSubDeviceNameByIndex(devicename, i - 1) << "无故障";
                }else
                {
                    _tempFaultHash.insert(getSubDeviceNameByIndex(devicename, i - 1), "有故障");
//                    qDebug() << getSubDeviceNameByIndex(devicename, i - 1) << "有故障";
                }
            }else//f是正常状态
            {
                _tempFaultHash.insert(getSubDeviceNameByIndex(devicename, i - 1), "无故障");
//                qDebug() << getSubDeviceNameByIndex(devicename, i - 1) << "无故障";
            }
        }
        setFaultStatus(devicename, _tempFaultHash);
    }else if(_data.data[0] == 0x73)
    {
        QHash<QString, QString> _tempResetHash;
        for(int i =0;i<18;i++)
        {
            _tempResetHash.insert(getSubDeviceNameByIndex(devicename, i - 1), "复位成功");
        }
        setResetStatus(devicename, _tempResetHash);
    }
}

void SmogDevice::writeDataTo(const QString devicename, const QByteArray buff)
{
    if(m_socketHash.contains(devicename))
    {
        emit m_socketHash[devicename]->writeMyDataSignal(buff);
    }
}

void SmogDevice::resetDeviceSlot(const QString  devicename)
{
    if(m_fireTimer->isActive())
    {
        m_fireTimer->stop();
    }
    if(m_socketHash.contains(devicename))
    {
        emit m_socketHash[devicename]->writeMyDataSignal(getSendBuff("reset"));
    }
    m_fireTimer->start();
}

void SmogDevice::normalSmogDeviceSlot(const QString devicename)
{
    //正常操作是将所有的子设备都设置为正常状态
    if(m_socketHash.contains(devicename))
    {
        //获取到所有子设备名字
        QList<QString> _list;
        MySmogSensorInfo _struct = m_smogSensorInfoList.value(devicename);
        _list << _struct.address1 << _struct.address2
              << _struct.address3 << _struct.address4
              << _struct.address5 << _struct.address6
              << _struct.address7 << _struct.address8
              << _struct.address9 << _struct.address10
              << _struct.address11 << _struct.address12
              << _struct.address13 << _struct.address14
              << _struct.address15 << _struct.address16
              << _struct.address17;
        QHash<QString, QString> _resetstatus;
        QHash<QString, QString> _firestatus;

        for(int i =0;i<_list.size();i++)
        {
            _resetstatus.insert(_list.at(i), "未复位");
            _firestatus.insert(_list.at(i), "无火警");
        }
        setResetStatus(devicename, _resetstatus);
        setFireStatus(devicename, _firestatus);
        setNetworkStatus(devicename, NETWORKSUCC);
    }
}

void SmogDevice::setSheildStatusSlot(const QString &devicename, const QString subdevicename, const QString status)
{
    qDebug() <<subdevicename << status << __FUNCTION__;
    setSheildStatus(devicename, subdevicename, status);
}

void SmogDevice::computeSmogDeviceFault()
{
    for(int i=0;i<m_smogSensorFaultHash.size();i++)
    {
        QString devicename = m_smogSensorFaultHash.keys().at(i);
        QHash<int ,int> _hash = m_smogSensorFaultHash[devicename];
        //        qDebug() << _hash;
        QHash<QString, QString> _tempFaultHash;
        SmogDeviceStatus _status = getShieldStatus(devicename);
        for(int j =0;j<_hash.size();j++)
        {
            //            qDebug() << _hash[_hash.keys().at(j)];
            if(_hash[_hash.keys().at(j)] == 0)
            {
                //设置故障状态
                //                qDebug() << devicename << ":" << getSubDeviceNameByIndex(devicename, j);
                //                qDebug() << getShieldStatus(devicename).shieldstatus.value(getSubDeviceNameByIndex(devicename, j));
                if(getShieldStatus(devicename).shieldstatus.value(getSubDeviceNameByIndex(devicename, _hash.keys().at(j))) == "已屏蔽")
                {
                    _tempFaultHash.insert(getSubDeviceNameByIndex(devicename, _hash.keys().at(_hash.keys().at(j))), "无故障");

//                                        qDebug() << getSubDeviceNameByIndex(devicename, _hash.keys().at(j))<< __LINE__ << "无故障";
                }else
                {
                                        qDebug() << getSubDeviceNameByIndex(devicename, _hash.keys().at(j))<< __LINE__<< "有故障";
                    _tempFaultHash.insert(getSubDeviceNameByIndex(devicename, _hash.keys().at(j)), "有故障");
                }
            }else{
                //                qDebug() << getSubDeviceNameByIndex(devicename, _hash.keys().at(j)) << __LINE__<< "无故障";
                _tempFaultHash.insert(getSubDeviceNameByIndex(devicename, _hash.keys().at(j)), "无故障");
            }
        }
        //        qDebug() << _tempFaultHash;
        setFaultStatus(devicename, _tempFaultHash);
    }
}

void SmogDevice::setLMD(QString lmd)
{
    m_lingmindu = lmd.toInt();
}
