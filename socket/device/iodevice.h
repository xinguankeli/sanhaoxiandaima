#ifndef IODEVICE_H
#define IODEVICE_H

#include <QObject>
#include <QHash>
#include <QThread>
#include <QMutex>
#include <QQueue>
#include <QTimer>
#include "../mysocket.h"
//1.获取IO模块的输入数据
//2.设置IO模块的输出数据
struct LastOpenIODot
{
    QString devicename;
    QString subdevicename;
    bool status;
};
struct Data
{
    quint16 input1;
    quint16 input2;
    quint16 input3;
    quint16 input4;
    quint16 input5;
    quint16 input6;
};

class IODevice : public QObject
{
    Q_OBJECT
public:
    explicit IODevice(QObject *parent = nullptr);
    //配置文件设置火警设备信息
    void setDeviceInfo(QHash<QString, MyControllerInfo>);
    //启动所有火警设备
    void startALLDevice();
    //将表达式的触发数据设置到
    void setMultiLambdaEventHash(const QHash<QString, QList<MultiLambdaDeviceInfo> >);
    //外部设置温度的浮动值
    void setWenDuFuDongZhi(const double);
    //设置0.015值
    void setLingDianLingYiWu(const double);
    //设置固定插值
    void setGuDingChaZhi(QHash<QString, int>);
    //设置主还是从
    //true是主
    //false是从
    void setCloseOutPutStatus(bool);
    void setCloseMasterOrClient(bool);
    //传入当前的温度值，判断是否需要开启
    void needOpenOrCloseOutPut(QString devicename, QHash<QString, quint16> _hash);
private:
    //当前设备是主还是从
    bool m_masterStatus;
    bool m_masterClientStatus;
    //所有火警设备
    QHash<QString, MySocket*> m_socketHash;

    //所有火警设备的子线程
    QHash<QString, QThread*> m_socketThreadHash;

    //所有火警设备信息
    QHash<QString, MyControllerInfo> m_ioSensorInfoList;

    //存放所有设备的状态
    QHash<QString, IODeviceStatus> m_deviceStatusHash;

    //存放所有输入点的上一次的状态，
    QHash<QString, IODeviceStatus> m_frontDeviceStatusHash;

    //写入数据的锁
    QMutex *m_mutex;

    //轮询所有输入点的数据的定时器
    QTimer *m_timer;

    //存放所有的基于名字的输出点
    QHash<QString, QHash<QString, int> > m_outputNameByIndex;

    //存放所有的输出点的名字
    QHash<QString, QHash<int, QString> > m_outputIndexByName;

    //存放所有输入点的别名，根据输入点的索引
    QHash<QString, QHash<int, QString> > m_inputNameByIndex;

    //表达式的触发条件hash
    QHash<QString, QList<MultiLambdaDeviceInfo> > m_multiLambdaEventHash;


    //IO的温度滤波
    QHash<QString, QQueue<double> > m_wenduLvBoHash;


    //临时的A、B框架液位延时处理函数
    QHash<QString, int> m_tempABYeWeiCountHash;

    //温度的浮动值，是之前的20.44
    double m_wenduFuDongZhi;
    //0.015
    double m_lingDianLingYiWuZhi;
    //固定插值
    QHash<QString, int> m_guDingChaZhiHash;

    QByteArray _arr;
    QByteArray _arr2;

    //用来判断是否需要计算差值
    QHash<QString, bool> m_isneedcomputer;
    //计算的差值
    QHash<QString, quint16> m_chazhi;
    //两个水温，哪个大
    QHash<QString, QString> m_whatBig;
    //用来跳过第一次，防止都是35
    QHash<QString, bool> m_tiaoguo;
    QMutex *m_chazhiMutex;



    //A、B框架两个定时器
    QTimer *m_ajiareqiTimer;
    QTimer *m_bjiareqiTimer;
    QTimer *m_anuanfengjiTimer;
    QTimer *m_bnuanfengjiTimer;
    bool m_ajiareqiStatus;
    bool m_bjiareqiStatus;
    bool m_anuanfengjiStatus;
    bool m_bnuanfengjiStatus;
    quint64 m_ajiareqiCount;
    quint64 m_bjiareqiCount;
    quint64 m_anuanfengjiCount;
    quint64 m_bnuanfengjiCount;
private:
    //设备网络连接故障状态
    //key是设备名，value是网络状态
    void setNetworkStatus(const QString &, const QString);

    //设置input值
    //key是设备名，vlaue是六个输入点的状态
    void setInputStatus(const QString &, QHash<QString, quint16>);

    //设置output值
    //key是设备名，value是六个输出点的状态
    void setOutputStatus(const QString &, QHash<QString, quint8>);

    //设置上一次的input值
    void setFrontInputStatus(const QString &, QHash<QString, quint16>);

    //处理input值
    void processInputStatus(const QString &, QHash<QString, quint16>);

    //判断触发条件是否成熟
    //第一个参数是设备名
    //第二个参数是输入点别名
    //第三个参数是输入点当前值
    bool isEvented(const QString &, const QString &, const quint16 &);

    //处理液位算法
    quint16 processYeWei(const quint16 &);
    //处理压力算法
    quint16 processYali(const double &);
    //处理温度算法
    double processWenDu(const double &);

    //获取当前设备的状态
    IODeviceStatus getDeviceStatus(const QString &);

    //获取上一次的input值
    IODeviceStatus getFrontDeviceStatus(const QString &);

    //差值补偿
    //参数为当前的IO设备名
    //第二个参数为当前io的水温输入值
    void getChaZhi(const QString, const QString);

    //计算是否两个水温的输入点都来到
    bool computerTwoInputComing();

    //传入控制器，判断是否开启加热器定时器
    bool isOpenJiaReQiTimer(const QString devicename, QHash<QString, quint16>);
    //传入控制器，判断是否开启暖风机定时器
    bool isOpenNuanFengJiTimer(const QString devicename, QHash<QString, quint16>);

public slots:
    //处理设备的在线状态
    //设备的网络连接状态
    //第一个参数为设备名
    //第二个参数为网络连接错误或网络连接成功
    void processDeviceStatus(QString, QString);


    //处理设备的数据
    //设备发送上来的数据处理后解析成各种信号发出去
    void processDeviceData(QString, QByteArray);

    //根据key来执行对应的逻辑
    //询问输入点的值
    void writeDataTo(const QString, const QByteArray);

    //设置输出点的开关
    //第一个参数是设备名
    //第二个参数第一个输出点的名
    //第三个参数是第一个点的开关状态 true开 false关
    //......
    //最后一个参数是第几个有几个点要开关
    void setIODeviceStatus(const QString,
                           const QString, const bool,
                           const QString, const bool,
                           const QString, const bool,
                           const QString, const bool,
                           const QString, const bool,
                           const QString, const bool
                           ,const int );

    //更新IO设备的状态，只是主从使用
    //第一个参数是设备名
    //第二个参数是子设备名
    //第三个参数是状态
    void updateIODeviceStatus(const QString,
                              const int,
                              const QString, const bool,
                              const QString, const bool,
                              const QString, const bool,
                              const QString, const bool,
                              const QString, const bool,
                              const QString, const bool);

signals:
    //通过该信号将设备的状态发送出去
    void updateDeviceStatus(const QString, IODeviceStatus);


};

#endif // IODEVICE_H
