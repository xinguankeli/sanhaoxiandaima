#include "gatedevice.h"

GateDevice::GateDevice(QObject *parent) : QObject(parent), m_mutex(new QMutex),m_unknowCountMutex(new QMutex),m_getGateStatusSign(false),m_reGetGateStatusMutex(new QMutex)
{

}

void GateDevice::setDeviceInfo(QHash<QString, MyGateDeviceInfo> hash)
{
    m_gateSensorInfoList = hash;
}

void GateDevice::startALLDevice()
{
    QList<QString> _keys = m_gateSensorInfoList.keys();
    for(int i =0;i<_keys.size();i++)
    {
        MyGateDeviceInfo _struct = m_gateSensorInfoList[_keys.at(i)];
        MySocket *_socket = new MySocket(_struct.base.alias, _struct.base.ip, _struct.base.port, _struct.base.recv_length);
        m_socketHash.insert(_struct.base.alias, _socket);
        connect(_socket, &MySocket::deviceStatus, this, &GateDevice::processDeviceStatus, Qt::QueuedConnection);
        connect(_socket, &MySocket::deviceData, this, &GateDevice::processDeviceData, Qt::QueuedConnection);
        connect(_socket, &MySocket::writeMyDataSignal, _socket, &MySocket::writeMyDataSlot, Qt::QueuedConnection);

        QThread *thread = new QThread;
        m_socketThreadHash.insert(_struct.base.alias, thread);
        _socket->moveToThread(thread);
        connect(thread, &QThread::started, _socket, &MySocket::startTcpSocket);
    }
    for(int i =0;i<_keys.size();i++)
    {
        m_socketThreadHash[_keys.at(i)]->start();
    }
    for(int i =0;i<_keys.size();i++)
    {
        GateDeviceStatus _status;
        _status.networkstatus  = "网络连接成功";
        _status.leftgatestatus = "关到位";
        _status.rightgatestatus = "关到位";
        _status.leftname = m_gateSensorInfoList[_keys.at(i)].leftalias;
        _status.rightname = m_gateSensorInfoList[_keys.at(i)].rightalias;
        _status.leftshieldstatus = m_gateSensorInfoList[_keys.at(i)].leftstatus;
        _status.rightshieldstatus = m_gateSensorInfoList[_keys.at(i)].rightstatus;
        m_deviceStatusHash.insert(_keys.at(i), _status);
    }
    for(int i =0;i<_keys.size();i++)
    {
        GateDeviceStatus _status;
        _status = m_deviceStatusHash.value(_keys.at(i));
        QHash<QString, bool> _hash;
        _hash.insert(_status.leftname, false);
        _hash.insert(_status.rightname, false);
        m_unknowStatusHash.insert(_keys.at(i), _hash);
        QHash<QString, int> _hash2;
        _hash2.insert(_status.leftname, 0);
        _hash2.insert(_status.rightname, 0);
        m_unknowCountHash.insert(_keys.at(i), _hash2);
        m_gateStatusHash.insert(_keys.at(i), _hash);
    }
    m_timer = new QTimer(this);
    connect(m_timer, &QTimer::timeout, this, [&]{
        for(int i =0;i<m_socketHash.size();i++)
        {
            QByteArray _arr;
            _arr.append(0x01);
            _arr.append(0x03);
//            qDebug() << m_socketHash.keys().at(i) << _arr;
            emit m_socketHash[m_socketHash.keys().at(i)]->writeMyDataSignal(_arr);
        }
    });
    m_timer->start(1000);
}

bool GateDevice::computerGateStatus()
{
    //在这里判断，如果总阀门状态掉线了，将那两个都设置为true
    QList<QString> _keys1 = m_socketHash.keys();
    for(int i =0;i<_keys1.size();i++)
    {
        GateDeviceStatus _status = getGateStatus(_keys1.at(i));
        if(_status.networkstatus == NETWORKERR)
        {
            m_gateStatusHash[_keys1.at(i)][_status.leftname] = true;
            m_gateStatusHash[_keys1.at(i)][_status.rightname] = true;
        }
    }
    bool _ret = true;
    QList<QString> _keys = m_gateStatusHash.keys();
    for(int i =0;i<_keys.size();i++)
    {
        QHash<QString, bool> _hash = m_gateStatusHash[_keys.at(i)];
        for(int j =0;j<_hash.size();j++)
        {
            _ret &= _hash[_hash.keys().at(j)];
        }
    }
    return _ret;
}

void GateDevice::setNetworkStatus(const QString &key, const QString value)
{
    if(m_deviceStatusHash.contains(key))
    {
        m_mutex->lock();
        GateDeviceStatus _status = m_deviceStatusHash.value(key);
        _status.networkstatus = value;
        m_deviceStatusHash[key] = _status;
        m_mutex->unlock();
        emit updateDeviceStatus(key, _status);
    }
}

void GateDevice::setOpenCloseStatus(const QString &key, const QString lstatus, const QString rstatus)
{
    if(m_deviceStatusHash.contains(key))
    {
        m_mutex->lock();
        GateDeviceStatus _status = m_deviceStatusHash.value(key);
        _status.leftgatestatus = lstatus;
        _status.rightgatestatus = rstatus;
        m_deviceStatusHash[key] = _status;
        m_mutex->unlock();
        emit updateDeviceStatus(key, _status);
    }
}

void GateDevice::setLanJieCount(const QString & devicename, const QString & subdevicename, const int count)
{
    m_unknowCountMutex->lock();
    m_unknowCountHash[devicename][subdevicename] = count;
//    qDebug() << __FUNCTION__ << m_unknowCountHash[devicename][subdevicename] << devicename << subdevicename;
    m_unknowCountMutex->unlock();
}

void GateDevice::subLanJieCount(const QString & devicename, const QString & subdevicename)
{
//    qDebug() << devicename << subdevicename << "-1";
    int _count = -1;
    m_unknowCountMutex->lock();
    _count = m_unknowCountHash[devicename][subdevicename];
//    qDebug() << _count;
    _count -= 1;
    m_unknowCountHash[devicename][subdevicename] = _count;
//    qDebug() << m_unknowCountHash[devicename][subdevicename];
    m_unknowCountMutex->unlock();
}

int GateDevice::getLanJieCount(const QString & devicename, const QString & subdevicename)
{
    int _count = -1;
    m_unknowCountMutex->lock();
    _count = m_unknowCountHash[devicename][subdevicename];
//    qDebug() << devicename << subdevicename << ":" << _count;
    m_unknowCountMutex->unlock();
    return _count;
}

GateDeviceStatus GateDevice::getGateStatus(const QString &devicename)
{
    m_mutex->lock();
    GateDeviceStatus _status = m_deviceStatusHash[devicename];
    m_mutex->unlock();
    return _status;
}

void GateDevice::setReGetGateStatus(bool status)
{
    qDebug() << "设置重新收集阀门状态标志位为:" << status;
    m_reGetGateStatusMutex->lock();
    m_getGateStatusSign = status;
    m_reGetGateStatusMutex->unlock();
}

bool GateDevice::getReGetGateStatus()
{
    bool _ret;
    m_reGetGateStatusMutex->lock();
    _ret = m_getGateStatusSign;
    m_reGetGateStatusMutex->unlock();
    return _ret;
}


void GateDevice::processDeviceStatus(QString devicename, QString status)
{
    setNetworkStatus(devicename, status);
}

void GateDevice::processDeviceData(QString devicename, QByteArray buff)
{

    //如果第一次阀门的状态还未收集完毕，并且收集标志为置为true
    //不等于true相当于没收集完成，继续收集
    if(computerGateStatus() == true)
    {
        if(m_getGateStatusSign == false)
        {
//            qDebug() << "ok";
            m_getGateStatusSign = true;
            emit sendMyStatus(m_unknowStatusHash);
        }
    }else
    {
        //将所有的数据更新到里面并返回
        //m_unknowStatusHash是存放左右阀门的开关状态，true为开，false为关，收集第一次阀门的数据
        //提取出来左侧右侧的状态
//        qDebug() << "收集数据";
        QString _left, _right;
        unsigned char _l, _r;
        bool _lb, _rb;
        memset(&_l, 0, sizeof(unsigned char));
        memset(&_r, 0, sizeof(unsigned char));
        memcpy(&_l, buff.data(), sizeof(unsigned char));
        memcpy(&_r, buff.data() + sizeof(unsigned char), sizeof(unsigned char));
        if(_l == 2)
        {
            _left = "关到位";
            _lb = false;
        }
        if(_l == 1)
        {
            _left = "开到位";
            _lb = true;
        }
        if(_l == 0)
        {
            _left = "未知";
            _lb = false;
        }
        if(_r == 2)
        {
            _right = "关到位";
            _rb = false;
        }
        if(_r == 1)
        {
            _right = "开到位";
            _rb = true;
        }
        if(_r == 0)
        {
            _right = "未知";
            _rb = false;
        }
        if(m_gateSensorInfoList[devicename].leftstatus == "无效")
        {
            _lb = false;
        }
        if(m_gateSensorInfoList[devicename].rightstatus == "无效")
        {
            _rb = false;
        }
        m_unknowStatusHash[devicename][m_gateSensorInfoList[devicename].leftalias] = _lb;
        m_unknowStatusHash[devicename][m_gateSensorInfoList[devicename].rightalias] = _rb;
//        qDebug() << m_gateSensorInfoList[devicename].leftalias << ":" << _lb << "[]" << m_gateSensorInfoList[devicename].rightalias << ":" << _rb;
        //这里要加一个如果是掉线状态，就默认设置为true，照常收集
        m_gateStatusHash[devicename][m_gateSensorInfoList[devicename].leftalias] = true;
        m_gateStatusHash[devicename][m_gateSensorInfoList[devicename].rightalias] = true;
        return;
    }
    GateDeviceStatus _status = getGateStatus(devicename);
    if(getLanJieCount(devicename, _status.leftname) > 1)
    {
//        qDebug() << devicename << _status.leftname << ">0";
        subLanJieCount(devicename, _status.leftname);

    }else
    {
        //小于1，可以收数据
        QString _left, _right;
        unsigned char _l;
        memset(&_l, 0, sizeof(unsigned char));
        memcpy(&_l, buff.data(), sizeof(unsigned char));
        unsigned char _r;
        memset(&_r, 0, sizeof(unsigned char));
        memcpy(&_r, buff.data() + sizeof(unsigned char), sizeof(unsigned char));
        if(_l == 2)
        {
            _left = "关到位";
        }
        if(_l == 1)
        {
            _left = "开到位";
        }
        if(_l == 0)
        {
            _left = "未知";
        }
        if(_r == 2)
        {
            _right = "关到位";
        }
        if(_r == 1)
        {
            _right = "开到位";
        }
        if(_r == 0)
        {
            _right = "未知";
        }
        if(m_gateSensorInfoList[devicename].leftstatus == "无效")
        {
            _left = "关到位";
        }
        if(m_gateSensorInfoList[devicename].rightstatus == "无效")
        {
            _right = "关到位";
        }
    //    qDebug() << devicename << _left << _right;
        if(m_getGateStatusSign)
        {
//            qDebug() << "执行更新状态" << __LINE__;
            setOpenCloseStatus(devicename, _left, _right);
        }
    }
    if(getLanJieCount(devicename, _status.rightname) > 1)
    {
//        qDebug() << devicename << _status.rightname << ">0";
        subLanJieCount(devicename, _status.rightname);

    }else{
        QString _left, _right;
        unsigned char _l;
        memset(&_l, 0, sizeof(unsigned char));
        memcpy(&_l, buff.data(), sizeof(unsigned char));
        unsigned char _r;
        memset(&_r, 0, sizeof(unsigned char));
        memcpy(&_r, buff.data() + sizeof(unsigned char), sizeof(unsigned char));
        if(_l == 2)
        {
            _left = "关到位";
        }
        if(_l == 1)
        {
            _left = "开到位";
        }
        if(_l == 0)
        {
            _left = "未知";
        }
        if(_r == 2)
        {
            _right = "关到位";
        }
        if(_r == 1)
        {
            _right = "开到位";
        }
        if(_r == 0)
        {
            _right = "未知";
        }
        if(m_gateSensorInfoList[devicename].leftstatus == "无效")
        {
            _left = "关到位";
        }
        if(m_gateSensorInfoList[devicename].rightstatus == "无效")
        {
            _right = "关到位";
        }
    //    qDebug() << devicename << _left << _right;
        if(m_getGateStatusSign)
        {
//            qDebug() << "执行更新状态" << __LINE__;
            setOpenCloseStatus(devicename, _left, _right);
        }
    }
}

void GateDevice::writeDataTo(const QString devicename, const QByteArray buff)
{
    if(m_socketHash.contains(devicename))
    {
        emit m_socketHash[devicename]->writeMyDataSignal(buff);
    }
}

//第一个参数为总阀门名
//第二个参数为第一个阀门名字
//第三个参数为第一个阀门的开关状态
//第四个参数为第二个阀门的名字
//第五个参数为第二个阀门的状态
void GateDevice::setGateOpenCloseStatus(const QString devicename, const QString lname, const bool lstatus, const QString rname, const bool rstatus)
{
    if(m_getGateStatusSign == false)
    {
        return;
    }
//    qDebug() << "devicename:" << devicename
//             << "lname:" << lname
//             << "rname:" << rname
//             << "lstatus:" << lstatus
//             << "rstatus:" << rstatus;
    GateDeviceStatus _status = getGateStatus(devicename);        

    //1.参数顺序不是必须左右阀门，自己判断是左阀门还是右阀门
    QByteArray _arr;
    _arr.append(0x01);
    _arr.append(0x04);
    _arr.append((unsigned char)0x00);
    _arr.append(lstatus == true ? 0x01 : (unsigned char)0x00);
    _arr.append(rstatus == true ? 0x01 : (unsigned char)0x00);
    setLanJieCount(devicename, lname, 3);
    setLanJieCount(devicename, rname, 3);
    writeDataTo(devicename, _arr);

}

void GateDevice::reGetGateStatus()
{
    setReGetGateStatus(false);
    qDebug() << "清空阀门状态标志位的hash";
    //清空阀门状态
    QList<QString> _keys = m_gateSensorInfoList.keys();
    for(int i =0;i<_keys.size();i++)
    {
        GateDeviceStatus _status;
        _status = m_deviceStatusHash.value(_keys.at(i));
        QHash<QString, bool> _hash;
        _hash.insert(_status.leftname, false);
        _hash.insert(_status.rightname, false);
        m_unknowStatusHash.insert(_keys.at(i), _hash);
        QHash<QString, int> _hash2;
        _hash2.insert(_status.leftname, 0);
        _hash2.insert(_status.rightname, 0);
        m_unknowCountHash.insert(_keys.at(i), _hash2);
        m_gateStatusHash.insert(_keys.at(i), _hash);
    }
}
