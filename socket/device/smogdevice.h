#ifndef SMOGDEVICE_H
#define SMOGDEVICE_H

#include <QObject>
#include <QHash>
#include <QThread>
#include <QMutex>
#include <QTimer>
#include "../mysocket.h"

struct RecvData
{
    quint8 data[97];
};

class SmogDevice : public QObject
{
    Q_OBJECT
public:
    explicit SmogDevice(QObject *parent = nullptr);
    //配置文件设置感烟设备信息
    void setDeviceInfo(QHash<QString, MySmogSensorInfo>);
    //配置文件设置感烟的灵敏度
    void setDeviceLMD(QString);
    //启动所有感烟设备
    void startALLDevice();

    QList<QString> getDeviceNames();
private:
    //所有感烟设备
    QHash<QString, MySocket*> m_socketHash;

    //所有感温设备的子线程
    QHash<QString, QThread*> m_socketThreadHash;

    //所有感温设备信息
    QHash<QString, MySmogSensorInfo> m_smogSensorInfoList;

    //存放所有设备的状态
    QHash<QString, SmogDeviceStatus> m_deviceStatusHash;

    //写入数据的锁
    QMutex *m_mutex;

    //存放感烟探测器的对应索引的别名
    QHash<QString, QHash<int, QString> > m_aliasByIndexHash;

    //判断是否是设备故障
    //每次接收数据都存到QHash里，如果到达固定次数，还是0，那么就是故障
    QHash<QString, QHash<int, int> > m_smogSensorFaultHash;


    //累加定时器，到三次就计算一次是否有设备故障没收到数据
    int m_faultCount;

    //探测火警的定时器
    QTimer *m_fireTimer;
    int m_enquireFireCount;

    //探测故障的定时器
    QTimer *m_faultTimer;
    int m_enquireFaultCount;

    //火警和故障交替发
    bool m_jiaoti;

    //感烟探测器的灵敏度也就是默认的150
    int m_lingmindu;
private:
    //设置网络连接故障状态
    //key是设备名，value是网络状态的值
    void setNetworkStatus(const QString &, const QString);

    //设置复位状态
    //key是设备名，value是复位状态的值
    void setResetStatus(const QString &, const QHash<QString, QString>);

    //设置火警状态
    //key是设备名，sub是子设备名，value是火警状态的值
    void setFireStatus(const QString &, const QHash<QString, QString>);

    //设置故障状态
    //key是设备名，sub是子设备名，value是故障状态的值
    void setFaultStatus(const QString &, const QHash<QString, QString>);

    //设置屏蔽状态
    //key是设备名，sub是子设备名，value是屏蔽状态的值
    void setSheildStatus(const QString &, const QString &, const QString);

    //获取屏蔽状态
    //key是设备名 sub是子设备名
    SmogDeviceStatus getShieldStatus(const QString &);

    //获取对应索引的子设备名
    QString getSubDeviceNameByIndex(const QString &, const int &);

    //获取询问火警状态的QByteArray
    //参数为fire、fault、reset
    QByteArray getSendBuff(const QString &);
public slots:

    //处理设备的在线状态
    //设备的网络连接状态
    //第一个参数为设备名
    //第二个参数为子设备名
    //第三个参数为网络连接错误或网络连接成功
    void processDeviceStatus(QString, QString);

    //处理设备的数据
    //设备发送上来的数据处理后解析成各种信号发出去
    void processDeviceData(QString, QByteArray);

    //根据key来执行对应的逻辑
    //设置火警的温度曲线
    //设置xxx等
    void writeDataTo(const QString, const QByteArray );

    //内部执行复位操作
    void resetDeviceSlot(const QString );

    //内部执行正常操作
    void normalSmogDeviceSlot(const QString );

    //外部使用信号连接此函数，设置感烟设备的屏蔽状态
    //第一个参数是设备名
    //第二个参数是已屏蔽、未屏蔽
    void setSheildStatusSlot(const QString &, const QString, const QString);

    //计算一次是否有设备故障
    void computeSmogDeviceFault();

    //UI设置感烟探测器灵敏度的值
    void setLMD(QString);
signals:
    //通过该信号将设备的状态发送出去
    //复位成功，火警状态等
    void updateDeviceStatus(const QString, SmogDeviceStatus);
};

#endif // SMOGDEVICE_H
