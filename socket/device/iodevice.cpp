#include "iodevice.h"
#define CONSTVALUE  12.412//液位的固定值
#define QUEUE_LENGTH 60
#define SHUIWEN "水温"

#define ABYEWEIBIAOZHUN 70
IODevice::IODevice(QObject *parent) : QObject(parent),m_mutex(new QMutex),m_ajiareqiStatus(false),
    m_bjiareqiStatus(false),m_anuanfengjiStatus(false),m_bnuanfengjiStatus(false),
    m_ajiareqiCount(0),m_bjiareqiCount(0),m_anuanfengjiCount(0),m_bnuanfengjiCount(0)
{
    _arr.append(0x01);
    _arr.append(0x04);
    _arr.append((unsigned char)0x00);

    _arr.append((unsigned char)0x00);
    _arr.append((unsigned char)0x00);
    _arr.append((unsigned char)0x00);
    _arr.append((unsigned char)0x00);
    _arr.append((unsigned char)0x01);
    _arr.append((unsigned char)0x00);


    _arr2.append(0x01);
    _arr2.append(0x04);
    _arr2.append((unsigned char)0x00);

    _arr2.append((unsigned char)0x00);
    _arr2.append((unsigned char)0x00);
    _arr2.append((unsigned char)0x00);
    _arr2.append((unsigned char)0x00);
    _arr2.append((unsigned char)0x00);
    _arr2.append((unsigned char)0x01);

    m_isneedcomputer.insert(SHUIWEN, false);
    m_chazhi.insert(SHUIWEN, 0);
    m_chazhiMutex = new QMutex;

    m_tempABYeWeiCountHash.insert("A框架一区控制器", 0);
    m_tempABYeWeiCountHash.insert("B框架一区控制器", 0);

}

void IODevice::setDeviceInfo(QHash<QString, MyControllerInfo> hash)
{
    m_ioSensorInfoList = hash;
}

void IODevice::startALLDevice()
{
    QList<QString> _keys = m_ioSensorInfoList.keys();
    for(int i =0;i<_keys.size();i++)
    {
        MyControllerInfo _struct = m_ioSensorInfoList[_keys.at(i)];
        MySocket *_socket = new MySocket(_struct.base.alias, _struct.base.ip, _struct.base.port, _struct.base.recv_length);
        m_socketHash.insert(_struct.base.alias, _socket);
        connect(_socket, &MySocket::deviceStatus, this, &IODevice::processDeviceStatus, Qt::QueuedConnection);
        connect(_socket, &MySocket::deviceData, this, &IODevice::processDeviceData, Qt::QueuedConnection);
        connect(_socket, &MySocket::writeMyDataSignal, _socket, &MySocket::writeMyDataSlot, Qt::QueuedConnection);

        QThread *thread = new QThread;
        m_socketThreadHash.insert(_struct.base.alias, thread);
        _socket->moveToThread(thread);
        connect(thread, &QThread::started, _socket, &MySocket::startTcpSocket);

        if(_struct.input1.indexOf(SHUIWEN) != -1)
        {
            m_tiaoguo.insert(_struct.input1, false);
        }
        if(_struct.input2.indexOf(SHUIWEN) != -1)
        {
            m_tiaoguo.insert(_struct.input2, false);
        }
        if(_struct.input3.indexOf(SHUIWEN) != -1)
        {
            m_tiaoguo.insert(_struct.input3, false);
        }
        if(_struct.input4.indexOf(SHUIWEN) != -1)
        {
            m_tiaoguo.insert(_struct.input4, false);
        }
        if(_struct.input5.indexOf(SHUIWEN) != -1)
        {
            m_tiaoguo.insert(_struct.input5, false);
        }
        if(_struct.input6.indexOf(SHUIWEN) != -1)
        {
            m_tiaoguo.insert(_struct.input6, false);
        }
    }
    for(int i =0;i<_keys.size();i++)
    {
        m_socketThreadHash[_keys.at(i)]->start();
    }
    for(int i =0;i<_keys.size();i++)
    {
        IODeviceStatus _status;
        MyControllerInfo _struct = m_ioSensorInfoList[_keys.at(i)];
        _status.networkstatus = "网络连接成功";
        QHash<QString, quint16> _inputs;
        _inputs.insert(_struct.input1, 0);
        _inputs.insert(_struct.input2, 0);
        _inputs.insert(_struct.input3, 0);
        _inputs.insert(_struct.input4, 0);
        _inputs.insert(_struct.input5, 0);
        _inputs.insert(_struct.input6, 0);
        _status.inputs = _inputs;
        QHash<QString, quint8> _outputs;
        _outputs.insert(_struct.output1, 0x01);
        _outputs.insert(_struct.output2, 0x01);
        _outputs.insert(_struct.output3, 0x01);
        _outputs.insert(_struct.output4, 0x01);
        _outputs.insert(_struct.output5, 0x01);
        _outputs.insert(_struct.output6, 0x01);
        _status.outputs = _outputs;
        QList<bool> _chuDianList;
        _chuDianList.append(false);//false默认是关
        _chuDianList.append(false);
        _chuDianList.append(false);
        _chuDianList.append(false);
        _chuDianList.append(false);
        _chuDianList.append(false);
        m_deviceStatusHash.insert(_keys.at(i), _status);
        m_frontDeviceStatusHash.insert(_keys.at(i), _status);

        QHash<QString, int> _outputNameByIndex;
        _outputNameByIndex.insert(_struct.output1, 0);
        _outputNameByIndex.insert(_struct.output2, 1);
        _outputNameByIndex.insert(_struct.output3, 2);
        _outputNameByIndex.insert(_struct.output4, 5);
        _outputNameByIndex.insert(_struct.output5, 4);
        _outputNameByIndex.insert(_struct.output6, 3);
        m_outputNameByIndex.insert(_keys.at(i), _outputNameByIndex);
        QHash<int, QString> _outputIndexByName;
        _outputIndexByName.insert(0, _struct.output1);
        _outputIndexByName.insert(1, _struct.output2);
        _outputIndexByName.insert(2, _struct.output3);
        _outputIndexByName.insert(5, _struct.output4);
        _outputIndexByName.insert(4, _struct.output5);
        _outputIndexByName.insert(3, _struct.output6);
        m_outputIndexByName.insert(_keys.at(i), _outputIndexByName);
        QHash<int, QString> _inputNameByIndex;
        _inputNameByIndex.insert(0, _struct.input1);
        _inputNameByIndex.insert(1, _struct.input2);
        _inputNameByIndex.insert(2, _struct.input3);
        _inputNameByIndex.insert(3, _struct.input4);
        _inputNameByIndex.insert(4, _struct.input5);
        _inputNameByIndex.insert(5, _struct.input6);
        m_inputNameByIndex.insert(_keys.at(i), _inputNameByIndex);
    }
    for(int i =0;i<_keys.size();i++)
    {
        if(_keys.at(i).indexOf("A框架一区控制器") != -1)
        {
            QQueue<double> _queue1;
            m_wenduLvBoHash.insert("A框架水温", _queue1);
            QQueue<double> _queue2;
            m_wenduLvBoHash.insert("A框架内部温度", _queue2);
            QQueue<double> _queue3;
            m_wenduLvBoHash.insert("A框架环境温度", _queue3);
        }
        if(_keys.at(i).indexOf("B框架一区控制器") != -1)
        {
            QQueue<double> _queue1;
            m_wenduLvBoHash.insert("B框架水温", _queue1);
            QQueue<double> _queue2;
            m_wenduLvBoHash.insert("B框架内部温度", _queue2);
            QQueue<double> _queue3;
            m_wenduLvBoHash.insert("B框架环境温度", _queue3);
        }
    }
    m_timer = new QTimer(this);
    connect(m_timer, &QTimer::timeout, this, [&]{

        for(int i =0;i<m_socketHash.size();i++)
        {
            QByteArray _arr;
            _arr.append(0x01);
            _arr.append(0x03);
            emit m_socketHash[m_socketHash.keys().at(i)]->writeMyDataSignal(_arr);
        }
    });
    if(m_masterStatus == true)
    {
        QTimer *_closeOutPutTimer = new QTimer(this);
        connect(_closeOutPutTimer, &QTimer::timeout, this, [&]{
            QList<QString> _keys = m_socketHash.keys();
            for(int i =0;i<_keys.size();i++)
            {
                QString _devicename = _keys.at(i);
                IODeviceStatus _status = getDeviceStatus(_devicename);
                QList<QString> _outputkeys = _status.outputs.keys();
                if(_outputkeys.size() == 6)
                {
                    setIODeviceStatus(_devicename,
                                      _outputkeys.at(0), false,
                                      _outputkeys.at(1), false,
                                      _outputkeys.at(2), false,
                                      _outputkeys.at(3), false,
                                      _outputkeys.at(4), false,
                                      _outputkeys.at(5), false, 6);
                }
            }
            m_timer->start(1000);

        });
        _closeOutPutTimer->setSingleShot(true);
        _closeOutPutTimer->setInterval(1000);
        _closeOutPutTimer->start();
    }else
    {
        m_timer->start(1000);
    }

    m_ajiareqiTimer = new QTimer(this);
    m_bjiareqiTimer = new QTimer(this);
    m_ajiareqiTimer->setInterval(5000);
    m_bjiareqiTimer->setInterval(4000);
    m_ajiareqiTimer->start();
    m_bjiareqiTimer->start();
    m_anuanfengjiTimer = new QTimer(this);
    m_bnuanfengjiTimer = new QTimer(this);
    m_anuanfengjiTimer->setInterval(3000);
    m_bnuanfengjiTimer->setInterval(2000);
    m_anuanfengjiTimer->start();
    m_bnuanfengjiTimer->start();
    connect(m_ajiareqiTimer, &QTimer::timeout, this, [&]{
        if(m_masterClientStatus == false)
        {
            return;
        }
        if(m_ajiareqiStatus)
        {
            //判断A框架加热器开启是否开启
            IODeviceStatus _status = getDeviceStatus("A框架二区控制器");
            if(isOpenJiaReQiTimer("A框架二区控制器", _status.inputs))
            {
                qDebug() << "#A加热器启动中。。。，关闭它#";
                setIODeviceStatus("A框架二区控制器", "A框架加热器", false, "", true, "", true, "", true, "", true, "", true, 1);
            }
        }

    });
    connect(m_bjiareqiTimer, &QTimer::timeout, this, [&]{
        if(m_masterClientStatus == false)
        {
            return;
        }
        if(m_bjiareqiStatus)
        {
            //判断B框架加热器开启是否开启
            IODeviceStatus _status = getDeviceStatus("B框架二区控制器");
            if(isOpenJiaReQiTimer("B框架二区控制器", _status.inputs))
            {
                qDebug() << "#B加热器启动中。。。，关闭它#";
                setIODeviceStatus("B框架二区控制器", "B框架加热器", false, "", true, "", true, "", true, "", true, "", true, 1);
            }
        }
    });
    connect(m_anuanfengjiTimer, &QTimer::timeout, this, [&]{
        if(m_masterClientStatus == false)
        {
            return;
        }
        if(m_anuanfengjiStatus)
        {
            IODeviceStatus _status = getDeviceStatus("A框架二区控制器");
            if(isOpenNuanFengJiTimer("A框架二区控制器", _status.inputs))
            {
                qDebug() << "#A加热器启动中。。。，关闭它#";
                setIODeviceStatus("A框架二区控制器", "A框架暖风机", false, "", true, "", true, "", true, "", true, "", true, 1);
            }
        }
    });
    connect(m_bnuanfengjiTimer, &QTimer::timeout, this, [&]{
        if(m_masterClientStatus == false)
        {
            return;
        }
        if(m_bnuanfengjiStatus)
        {
            IODeviceStatus _status = getDeviceStatus("B框架二区控制器");
            if(isOpenNuanFengJiTimer("B框架二区控制器", _status.inputs))
            {
                qDebug() << "#A加热器启动中。。。，关闭它#";
                setIODeviceStatus("B框架二区控制器", "B框架暖风机", false, "", true, "", true, "", true, "", true, "", true, 1);
            }
        }
    });
}

void IODevice::setMultiLambdaEventHash(const QHash<QString, QList<MultiLambdaDeviceInfo> > hash)
{
    m_multiLambdaEventHash = hash;
}

void IODevice::setWenDuFuDongZhi(const double wendu)
{
    m_wenduFuDongZhi = wendu;
}

void IODevice::setLingDianLingYiWu(const double v)
{
    m_lingDianLingYiWuZhi =v ;
}

void IODevice::setGuDingChaZhi(QHash<QString, int> v)
{
    m_guDingChaZhiHash = v;
}

void IODevice::setCloseOutPutStatus(bool status)
{
    m_masterStatus = status;
}

void IODevice::setCloseMasterOrClient(bool stauts)
{
    m_masterClientStatus = stauts;
}

void IODevice::needOpenOrCloseOutPut(QString devicename, QHash<QString, quint16> _hash)
{
    qint16 _shuiwen;
    qint16 _neibuwendu;
    qint16 _huanjingwendu;
    quint16 _kuangjiayewei;
    QString _ab;
    if(devicename.indexOf("A框架一区控制器") != -1)
    {
        _ab = "a";
        _shuiwen = (qint16)_hash.value("A框架水温");
        _neibuwendu = (qint16)_hash.value("A框架内部温度");
        _huanjingwendu = (qint16)_hash.value("A框架环境温度");
        _kuangjiayewei = _hash.value("A框架液位");
    }
    if(devicename.indexOf("B框架一区控制器") != -1)
    {
        _ab = "b";
        _shuiwen = (qint16)_hash.value("B框架水温");
        _neibuwendu = (qint16)_hash.value("B框架内部温度");
        _huanjingwendu = (qint16)_hash.value("B框架环境温度");
        _kuangjiayewei = _hash.value("B框架液位");
    }
    bool kuangjiahuanjingwendudayushi = false;
    bool kuangjiahuanjingwendufuershidaoshi = false;
    bool kuangjiahuanjingwendufusanshiwudaofuershi = false;

    if(_huanjingwendu > 9)
    {
        kuangjiahuanjingwendudayushi = true;
    }else if(_huanjingwendu < 11 && _huanjingwendu > -21)
    {
        kuangjiahuanjingwendufuershidaoshi = true;
    }else if(_huanjingwendu < -20)
    {
        kuangjiahuanjingwendufusanshiwudaofuershi = true;
    }

    bool _nuanfengjidakai = false;

    if(_neibuwendu  < 21 && kuangjiahuanjingwendufuershidaoshi == true)
    {
        //暖风机打开
        _nuanfengjidakai = true;
    }else if(_neibuwendu > 29 && kuangjiahuanjingwendufuershidaoshi == true)
    {
        //暖风机关闭
    }else if(_neibuwendu < 29 && kuangjiahuanjingwendufusanshiwudaofuershi == true)
    {
        //暖风机打开
        _nuanfengjidakai = true;
    }else if(_neibuwendu > 34 && kuangjiahuanjingwendufusanshiwudaofuershi == true)
    {
        //暖风机关闭
    }


    bool _jiareqidakai = false;
    if(_shuiwen < 8 && kuangjiahuanjingwendudayushi == true && _kuangjiayewei > 69)
    {
        //加热器打开
        _jiareqidakai = true;
    }else if(_shuiwen > 14 && kuangjiahuanjingwendudayushi == true)
    {
        //加热器关闭
    }else if(_shuiwen < 21 && kuangjiahuanjingwendufuershidaoshi == true && _kuangjiayewei > 69)
    {
        //加热器打开
        _jiareqidakai = true;

    }else if(_shuiwen > 29 && kuangjiahuanjingwendufuershidaoshi == true)
    {
        //加热器关闭
    }else if(_shuiwen < 21 && kuangjiahuanjingwendufusanshiwudaofuershi == true && _kuangjiayewei > 69)
    {
        //加热器打开
        _jiareqidakai = true;
    }else if(_shuiwen > 29 && kuangjiahuanjingwendufusanshiwudaofuershi == true)
    {
        //加热器关闭
    }


    if(_ab == "a")
    {
        if(!m_anuanfengjiStatus)
        {
            setIODeviceStatus("A框架二区控制器", "A框架暖风机", _nuanfengjidakai, "", true, "", true, "", true, "", true, "", true, 1);

        }
    }else if(_ab == "b")
    {
        if(!m_bnuanfengjiStatus)
        {
            setIODeviceStatus("B框架二区控制器", "B框架暖风机", _nuanfengjidakai, "", true, "", true, "", true, "", true, "", true, 1);
        }
    }
    if(_ab == "a")
    {
        if(!m_ajiareqiStatus)
        {
            setIODeviceStatus("A框架二区控制器", "A框架加热器", _jiareqidakai, "", true, "", true, "", true, "", true, "", true, 1);
        }
    }else if(_ab == "b")
    {
        if(!m_bjiareqiStatus)
        {
            setIODeviceStatus("B框架二区控制器", "B框架加热器", _jiareqidakai, "", true, "", true, "", true, "", true, "", true, 1);
        }
    }

}

void IODevice::setNetworkStatus(const QString & key, const QString value)
{
    if(m_deviceStatusHash.contains(key))
    {
        //取出当前值
        m_mutex->lock();
        IODeviceStatus _status = m_deviceStatusHash.value(key);
        _status.ajiareqiguzhang = m_ajiareqiStatus;
        _status.bjiareqiguzhang = m_bjiareqiStatus;
        _status.anuanfengjiguzhang = m_anuanfengjiStatus;
        _status.bnuanfengjiguzhang = m_bnuanfengjiStatus;
        _status.networkstatus = value;
        m_deviceStatusHash[key] = _status;
        m_mutex->unlock();
        emit updateDeviceStatus(key, _status);
    }
}

void IODevice::setInputStatus(const QString & key, QHash<QString, quint16> value)
{
    if(m_deviceStatusHash.contains(key))
    {
        m_mutex->lock();
        IODeviceStatus _status = m_deviceStatusHash.value(key);
        _status.ajiareqiguzhang = m_ajiareqiStatus;
        _status.bjiareqiguzhang = m_bjiareqiStatus;
        _status.anuanfengjiguzhang = m_anuanfengjiStatus;
        _status.bnuanfengjiguzhang = m_bnuanfengjiStatus;
        _status.inputs = value;
        m_deviceStatusHash[key] = _status;
        m_mutex->unlock();
        emit updateDeviceStatus(key, _status);
    }
}

void IODevice::setOutputStatus(const QString & key, QHash<QString, quint8> value)
{
    if(m_deviceStatusHash.contains(key))
    {
        m_mutex->lock();
        IODeviceStatus _status = m_deviceStatusHash.value(key);
        _status.ajiareqiguzhang = m_ajiareqiStatus;
        _status.bjiareqiguzhang = m_bjiareqiStatus;
        _status.anuanfengjiguzhang = m_anuanfengjiStatus;
        _status.bnuanfengjiguzhang = m_bnuanfengjiStatus;
        _status.outputs = value;
        m_deviceStatusHash[key] = _status;
        m_mutex->unlock();
        emit updateDeviceStatus(key, _status);
    }
}

void IODevice::setFrontInputStatus(const QString & key, QHash<QString, quint16> value)
{
    if(m_frontDeviceStatusHash.contains(key))
    {
        m_mutex->lock();
        IODeviceStatus _status = m_frontDeviceStatusHash.value(key);
        _status.inputs = value;
        m_frontDeviceStatusHash[key] = _status;
        m_mutex->unlock();
    }
}

void IODevice::processInputStatus(const QString &key, QHash<QString, quint16>  hash)
{
    QList<QString> _inputKeys = hash.keys();
    for(int i =0;i< _inputKeys.size();i++)
    {
        QString _inputName = _inputKeys.at(i);
        if(_inputName.indexOf("按钮") != -1)
        {
            //判断
            IODeviceStatus _frontstatus = getFrontDeviceStatus(key);
            //如果上一次的值小于2000 并且这一次的值大于2000，说明按钮被按下
            if(_frontstatus.inputs.value(_inputName) < 2000 && hash.value(_inputName) > 2000 )
            {
                //按钮被按下,通过状态更新发送给core
                //                qDebug() << key << _inputKeys.at(i) << "按钮被按下";
                //要将对应的按钮设置为5000值
                hash[_inputKeys.at(i)] = 0xffff;
                setInputStatus(key, hash);
            }
            if(_frontstatus.inputs.value(_inputName) > 2000 && hash.value(_inputName) < 2000)
            {
                //按钮被弹起，通过状态更新发送给core
                //                qDebug() << key << _inputKeys.at(i)  << "按钮被弹起";
                hash[_inputKeys.at(i)] = 0xfffe;
                setInputStatus(key, hash);
            }
        }else
        {
            if(isEvented(key, _inputName, hash.value(_inputName)) == true)
            {
                //可以被触发
                hash[_inputName] = 0xffff;
                setInputStatus(key, hash);
            }else
            {
                hash[_inputName] = 0xfffe;
                setInputStatus(key, hash);
            }
        }

    }
}

bool IODevice::isEvented(const QString & devicename, const QString & subdevicename, const quint16 & value)
{
    QList<QString> _otherKeys = m_multiLambdaEventHash.keys();
    QList<MultiLambdaDeviceInfo> _newList;
    for(int i =0;i<_otherKeys.size();i++)
    {
        QList<MultiLambdaDeviceInfo> _values = m_multiLambdaEventHash.value(_otherKeys.at(i));
        for(int j =0;j<_values.size();j++)
        {
            MultiLambdaDeviceInfo _struct = _values.at(j);
            if(_struct.type == "io")
            {
                _newList.append(_struct);
            }
        }
    }
    //1.将对应每个表达式内的规则解析出来，如果有一样的算作一个规则来计算即可
    for(int i =0;i<_newList.size();i++)
    {
        MultiLambdaDeviceInfo _struct = _newList.at(i);
        if(_struct.io.secondstatus == "有")
        {
            if(_struct.io.type == "分" && _struct.io.devicename == devicename && _struct.io.subdevicename == subdevicename)
            {
                bool _ret = false;
                if(_struct.io.doit == ">")
                {
                    if(_struct.io.value < value)//如果
                    {
                        _ret = true;
                    }
                }
                if(_struct.io.doit == "<")
                {
                    if(_struct.io.value > value)
                    {
                        _ret = true;
                    }
                }
                bool _ret2 = false;
                if(_struct.io.doit2 == ">")
                {
                    if(_struct.io.value2 < value)//如果
                    {
                        _ret2 = true;
                    }
                }
                if(_struct.io.doit2 == "<")
                {
                    if(_struct.io.value2 > value)
                    {
                        _ret2 = true;
                    }
                }
                return _ret && _ret2;
            }
        }else if(_struct.io.secondstatus == "无")
        {
            if(_struct.io.type == "分" && _struct.io.devicename == devicename && _struct.io.subdevicename == subdevicename)
            {
                if(_struct.io.doit == ">")
                {
                    if(_struct.io.value < value)//如果
                    {
                        return true;
                    }
                }
                if(_struct.io.doit == "<")
                {
                    if(_struct.io.value > value)
                    {
                        return true;
                    }
                }
            }
        }
    }
    return false;
}

quint16 IODevice::processYeWei(const quint16 & _tempValue)
{
    double _v = CONSTVALUE * 0.6;
    double _v2 = 0.0;
    double _sheng = 0.0;
    double _allsheng = 0.0;
    if(_tempValue > 760)//大于760时才判断
    {
        _v2 = _tempValue / 17.9;
        double _v3 = _v2 / 10;
        //        qDebug() << _sheng << _v3  << 66;
        _allsheng =  _v3 + 66;
        //_allSheng是总升数，可以存起来
        _allsheng -= 4;
    }else
    {
        _allsheng = 66;
    }
    return _allsheng;
}
double changeYaLiZhi(double temp)
{
    //    qDebug() << temp;
    QList<double> _list;//电流值
    _list << 4.0 << 7.2 << 8.7 << 10.4
          << 12.7 << 13.6 << 14.0 << 14.1
          << 14.2 << 14.3 << 14.4 << 14.5
          << 14.6 << 14.7 << 14.8 << 14.9
          << 15.0 << 15.1 << 15.2 << 15.3
          << 15.4 << 15.5 << 15.6 << 15.7
          << 15.8 << 15.9 << 16.0 << 16.1
          << 16.2 << 16.3 << 16.4 << 16.5
          << 16.6 << 16.7 << 16.8 << 17.6
          << 20;
    QList<double> _list2;//气压值
    _list2 << 0.0 << 5.0 << 7.0 << 10.0
           << 13.0 << 15.0 << 16.0 << 16.1
           << 16.3 << 16.4 << 16.6 << 16.7
           << 16.8 << 16.9 << 17.0 << 17.1
           << 17.3 << 17.4 << 17.6 << 17.7
           << 17.8 << 18.0 << 18.1 << 18.3
           << 18.4 << 18.6 << 18.7 << 18.8
           << 19.0 << 19.1 << 19.3 << 19.4
           << 19.6 << 19.7 << 19.9 << 20.0
           << 21.0 << 25.0;
    int _index = 0;
    double _value = 0.0;
    for(int i =0;i<_list.size() - 1;i++)
    {
        if(temp >= _list.at(i) && temp <= _list.at(i+1)){
            _index = i;
            _value = temp - _list.at(i);
        }
    }
    //确定在哪个区间 _index
    //确定剩余的值 _value
    QList<double> _list3;
    _list3.append((_list.at(1) - _list.at(0)) / 10);
    _list3.append((_list.at(2) - _list.at(1)) / 10);
    _list3.append((_list.at(3) - _list.at(2)) / 10);
    _list3.append((_list.at(4) - _list.at(3)) / 10);
    _list3.append((_list.at(5) - _list.at(4)) / 10);
    _list3.append((_list.at(6) - _list.at(5)) / 10);
    _list3.append((_list.at(7) - _list.at(6)) / 10);
    _list3.append((_list.at(8) - _list.at(7)) / 10);
    _list3.append((_list.at(9) - _list.at(8)) / 10);
    _list3.append((_list.at(10) - _list.at(9)) / 10);
    _list3.append((_list.at(11) - _list.at(10)) / 10);
    _list3.append((_list.at(12) - _list.at(11)) / 10);
    _list3.append((_list.at(13) - _list.at(12)) / 10);
    _list3.append((_list.at(14) - _list.at(13)) / 10);
    _list3.append((_list.at(15) - _list.at(14)) / 10);
    _list3.append((_list.at(16) - _list.at(15)) / 10);
    _list3.append((_list.at(17) - _list.at(16)) / 10);
    _list3.append((_list.at(18) - _list.at(17)) / 10);
    _list3.append((_list.at(19) - _list.at(18)) / 10);
    _list3.append((_list.at(20) - _list.at(19)) / 10);
    _list3.append((_list.at(21) - _list.at(20)) / 10);
    _list3.append((_list.at(22) - _list.at(21)) / 10);
    _list3.append((_list.at(23) - _list.at(22)) / 10);
    _list3.append((_list.at(24) - _list.at(23)) / 10);
    _list3.append((_list.at(25) - _list.at(24)) / 10);
    _list3.append((_list.at(26) - _list.at(25)) / 10);
    _list3.append((_list.at(27) - _list.at(26)) / 10);
    _list3.append((_list.at(28) - _list.at(27)) / 10);
    _list3.append((_list.at(29) - _list.at(28)) / 10);
    _list3.append((_list.at(30) - _list.at(29)) / 10);
    _list3.append((_list.at(31) - _list.at(30)) / 10);
    _list3.append((_list.at(32) - _list.at(31)) / 10);
    _list3.append((_list.at(33) - _list.at(32)) / 10);
    _list3.append((_list.at(34) - _list.at(33)) / 10);
    _list3.append((_list.at(35) - _list.at(34)) / 10);
    _list3.append((_list.at(36) - _list.at(35)) / 10);

    int _v =_value / _list.at(_index);
    double _vvvv = 0.0;
    for(int i =0;i<_v;i++)
    {
        _vvvv += _list3.at(_index);
    }
    return _list2.at(_index) + _v;
}
quint16 IODevice::processYali(const double & _v1)
{
    //    qDebug() << _v1;
    double _v = _v1 / 124;
    double _v2 = _v * 100;
    double _v3 = _v2 / 150;
    return changeYaLiZhi(_v3);
}

double IODevice::processWenDu(const double & temp)
{
    QList<double> _list;
    _list << 60.26 << 64.30 << 68.33 << 72.33 << 76.33
          << 80.31 << 84.27 << 88.22 << 92.16 << 96.09
          << 100.00 << 103.90 << 107.79 << 111.67 << 115.54
          << 119.40 << 123.24 << 127.08 << 130.90 << 134.71
          << 138.51 << 142.29;
    if(temp < _list.at(0)){
        return -100;
    }
    if(temp > _list.at(_list.size() - 1)){
        return 110;
    }
    QList<int> _list2;
    _list2 << -100 << -90 << -80 << -70 << -60 << -50 << -40
           << -30 << -20 << -10 << 0 << 10 << 20 << 30 << 40
           << 50 << 60 << 70 << 80 << 90 << 100 << 110;

    int _index = 0;
    double _value;
    for(int i =0;i<_list.size() - 1;i++){
        if(temp > _list.at(i) && temp < _list.at(i+1)){
            _index = i;
            _value = temp - _list.at(i);
        }
    }
    //确定在哪个区间 _index
    //确定剩余的值 _value
    //1.使用剩余的值 / 对应索引的值
    QList<double> _list3;
    _list3.append((_list.at(1) - _list.at(0)) / 10);
    _list3.append((_list.at(2) - _list.at(1)) / 10);
    _list3.append((_list.at(3) - _list.at(2)) / 10);
    _list3.append((_list.at(4) - _list.at(3)) / 10);
    _list3.append((_list.at(5) - _list.at(4)) / 10);
    _list3.append((_list.at(6) - _list.at(5)) / 10);
    _list3.append((_list.at(7) - _list.at(6)) / 10);
    _list3.append((_list.at(8) - _list.at(7)) / 10);
    _list3.append((_list.at(9) - _list.at(8)) / 10);
    _list3.append((_list.at(10) - _list.at(9)) / 10);
    _list3.append((_list.at(11) - _list.at(10)) / 10);
    _list3.append((_list.at(12) - _list.at(11)) / 10);
    _list3.append((_list.at(13) - _list.at(12)) / 10);
    _list3.append((_list.at(14) - _list.at(13)) / 10);
    _list3.append((_list.at(15) - _list.at(14)) / 10);
    _list3.append((_list.at(16) - _list.at(15)) / 10);
    _list3.append((_list.at(17) - _list.at(16)) / 10);
    _list3.append((_list.at(18) - _list.at(17)) / 10);
    _list3.append((_list.at(19) - _list.at(18)) / 10);
    _list3.append((_list.at(20) - _list.at(19)) / 10);
    _list3.append((_list.at(21) - _list.at(20)) / 10);
#ifdef IO_DEBUG_OUTPUT
    qDebug() << "当前索引："<<_index << "当前平均值:" << _value;
#endif
    double _v = _value / _list3.at(_index);
#ifdef IO_DEBUG_OUTPUT
    qDebug() << "当前平均数量：" << _v << "_list3:" << _list3.at(_index);
#endif
    double _vvvv = 0.000000;
    for(int i =0;i<_v;i++){
        _vvvv += _list3.at(_index);
    }
    return _list2.at(_index) + _v;
}


IODeviceStatus IODevice::getDeviceStatus(const QString &devicename)
{
    m_mutex->lock();
    IODeviceStatus _status = m_deviceStatusHash[devicename];
    m_mutex->unlock();
    return _status;
}

IODeviceStatus IODevice::getFrontDeviceStatus(const QString & devicename)
{
    m_mutex->lock();
    IODeviceStatus _status = m_frontDeviceStatusHash[devicename];
    m_mutex->unlock();
    return _status;
}

void IODevice::getChaZhi(const QString devicename, const QString subdevicename)
{
    QHash<QString, QVariant> _hash;
    //参数为当前的设备名
    QHash<QString, quint16> _currenthash;
    QHash<QString, quint16> _otherhash;
    //1.获取另一个车的水温值
    QList<QString> _keys = m_deviceStatusHash.keys();
    QString _otherDevicename;
    //另一个设备的子名
    QString _otherSubDevicename;
    //另一个设备的值
    quint16 _otherValue;
    //当前设备的值
    quint16 _currentValue;
    for(int i =0;i<_keys.size();i++)
    {
        IODeviceStatus _status = m_deviceStatusHash.value(_keys.at(i));
        _otherDevicename = _keys.at(i);
        QList<QString> _subKeys = _status.inputs.keys();
        for(int j =0;j<_subKeys.size();j++)
        {
            if(_subKeys.at(j).indexOf(SHUIWEN) != -1 && _subKeys.at(j) != subdevicename)
            {
                _otherSubDevicename = _subKeys.at(j);
                //                qDebug() << _otherDevicename << ":" << _otherSubDevicename << "一二有";
                _otherValue = _status.inputs[_otherSubDevicename];
                IODeviceStatus _currStatus = m_deviceStatusHash.value(devicename);
                _currentValue = _currStatus.inputs.value(subdevicename);
                if(_otherValue >= 105)
                {
                    //                    qDebug() << "大于150";
                }else
                {
                    _otherhash = _status.inputs;
                    _currenthash = _currStatus.inputs;
                    quint16 _value;
                    //判断哪个大，哪个小
                    if(_otherValue > _currentValue)
                    {
                        _value = _otherValue - _currentValue;
                    }else if(_otherValue < _currentValue)
                    {
                        _value = _currentValue - _otherValue;
                    }else if(_otherValue == _currentValue)
                    {
                        _value = _otherValue;
                    }
                    //如果为true，说明这是第一次计算，更新值
                    //                    qDebug() << "到这里了:" << _value << ":" << _otherValue << ":" << _currentValue;

                    quint16 _half = _value / 2;
                    if(_otherValue > _currentValue)
                    {
                        _otherValue -= _half;
                        _currentValue += _half;
                        m_whatBig.insert(devicename+subdevicename, "false");
                        m_whatBig.insert(_otherDevicename+_otherSubDevicename, "true");
                        //                        qDebug() << _otherDevicename << ":" << _otherSubDevicename << ":大,差值为:" << _half << "原始值为:" << _otherValue << ":" << _currentValue;
                        //                        qDebug() << devicename << ":" << subdevicename;
                    }else if(_otherValue < _currentValue)
                    {
                        _otherValue += _half;
                        _currentValue -= _half;
                        m_whatBig.insert(devicename+subdevicename, "true");
                        m_whatBig.insert(_otherDevicename+_otherSubDevicename, "false");
                        //                        qDebug() << devicename << ":" << subdevicename << ":大,值为:" << _half << "原始值为:" << _currentValue << ":" << _otherValue;
                        //                        qDebug() << _otherDevicename << ":" << _otherSubDevicename;

                    }
                    m_chazhi[SHUIWEN] = _half;
                }
            }
        }
    }
}

bool IODevice::computerTwoInputComing()
{
    bool _ret = true;
    for(int i =0;i<m_tiaoguo.size();i++)
    {
        _ret &= m_tiaoguo.value(m_tiaoguo.keys().at(i));
    }
    return _ret;
}

void IODevice::processDeviceStatus(QString devicename, QString status)
{
    setNetworkStatus(devicename, status);
}

void IODevice::processDeviceData(QString devicename, QByteArray buff)
{
    //处理六个输入点
    Data _data;
    memset(&_data, 0, sizeof(Data));
    memcpy(&_data, buff.data(), sizeof(Data));
    QHash<QString, quint16> _hash;
    //        qDebug() << devicename;
    //        qDebug() << m_inputNameByIndex.value(devicename).value(0)
    //                 << m_inputNameByIndex.value(devicename).value(1)
    //                 << m_inputNameByIndex.value(devicename).value(2)
    //                 << m_inputNameByIndex.value(devicename).value(3)
    //                 << m_inputNameByIndex.value(devicename).value(4)
    //                 << m_inputNameByIndex.value(devicename).value(5);
    //左侧是配置文件中的顺序，右侧是设备的输入点
    _hash.insert(m_inputNameByIndex.value(devicename).value(0), _data.input2);//复位按钮 第二个字节
    _hash.insert(m_inputNameByIndex.value(devicename).value(1), _data.input1);//释放按钮 第一个字节
    _hash.insert(m_inputNameByIndex.value(devicename).value(2), _data.input6);//测试按钮 第六个字节
    _hash.insert(m_inputNameByIndex.value(devicename).value(3), _data.input5);//
    _hash.insert(m_inputNameByIndex.value(devicename).value(4), _data.input4);
    _hash.insert(m_inputNameByIndex.value(devicename).value(5), _data.input3);
    //    processInputStatus(devicename, _hash);
    //    if(devicename.indexOf("五车一区控制器") != -1)
    //    {
    //        qDebug() << m_inputNameByIndex.value(devicename).value(0)
    //                 << m_inputNameByIndex.value(devicename).value(1)
    //                 << m_inputNameByIndex.value(devicename).value(2)
    //                 << m_inputNameByIndex.value(devicename).value(3)
    //                 << m_inputNameByIndex.value(devicename).value(4)
    //                 << m_inputNameByIndex.value(devicename).value(5);
    //        qDebug() << devicename
    //                 << ":"
    //                 << _data.input1
    //                 << ":"
    //                 << _data.input2
    //                 << ":"
    //                 << _data.input3
    //                 << ":"
    //                 << _data.input4
    //                 << ":"
    //                 << _data.input5
    //                 << ":"
    //                 << _data.input6;
    //    }
    //2370

    int tongyi = 2370;
    if(m_masterClientStatus == false)
    {
        tongyi = 2365;
    }
    if(devicename.indexOf("A框架二区控制器") != -1)
    {
        if(isOpenJiaReQiTimer(devicename, _hash))
        {
            m_ajiareqiCount += 1;
            qDebug() << "Ajiareqi:" << m_ajiareqiCount;
            if(m_ajiareqiCount >= 40000)
            {
                m_ajiareqiCount = 0;
            }
            if(m_ajiareqiCount >= tongyi)
            {
                m_ajiareqiStatus = true;
            }
        }else
        {
            qDebug() << "A反馈点<2000";
            m_ajiareqiCount = 0;
        }
        if(isOpenNuanFengJiTimer(devicename, _hash))
        {
            m_anuanfengjiCount += 1;
            qDebug() << "Anuanfengji:" << m_anuanfengjiCount;

            if(m_anuanfengjiCount >= 40000)
            {
                m_anuanfengjiCount = 0;
            }
            if(m_anuanfengjiCount >= tongyi)
            {
                m_anuanfengjiStatus = true;
            }
        }else
        {
            m_anuanfengjiCount = 0;
        }
    }else if(devicename.indexOf("B框架二区控制器") != -1)
    {
        if(isOpenJiaReQiTimer(devicename, _hash))
        {
            m_bjiareqiCount += 1;
            qDebug() << "Bjiareqi:" << m_bjiareqiCount;
            if(m_bjiareqiCount >= 40000)
            {
                m_bjiareqiCount = 0;
            }
            if(m_bjiareqiCount >= tongyi)
            {
                m_bjiareqiStatus = true;
            }
        }else
        {
            qDebug() << "B反馈点<2000";

            m_bjiareqiCount = 0;
        }
        if(isOpenNuanFengJiTimer(devicename, _hash))
        {
            m_bnuanfengjiCount += 1;
            qDebug() << "Bnuanfengji:" << m_bnuanfengjiCount;

            if(m_bnuanfengjiCount >= 40000)
            {
                m_bnuanfengjiCount = 0;
            }
            if(m_bnuanfengjiCount >= tongyi)
            {
                m_bnuanfengjiStatus = true;
            }
        }else
        {
            m_bnuanfengjiCount = 0;
        }

    }

    //这里暂时只处理液位、压力的算法
    QList<QString> _keys = _hash.keys();
    for(int i =0;i<_keys.size();i++)
    {

        quint16 _value;
        QString _key = _keys.at(i);
        quint16 _v = _hash.value(_key);
        if(_key.indexOf("压力") != -1)
        {
            //            qDebug() <<"yali yuanshizhi:" << _v;
            _value = processYali(_v);
            _hash[_key] = _value;
        }
        if(_key.indexOf("液位") != -1 && _key.indexOf("柜") == -1)
        {
            //            qDebug() << devicename << _v;
            //            qDebug() << "yewei yuanshizhi:" << _v;
            if(_v < 400)
            {
                _hash[_key] = 0;
            }else
            {
                _value = processYeWei(_v);
                //                qDebug() << "算完的液位值:" << _value << "原始值：" << _v;
                if(_value < ABYEWEIBIAOZHUN)
                {
                    //开始计数，当达到五分钟的数量时候开始插入
                    m_tempABYeWeiCountHash[devicename] += 1;
                    if(m_tempABYeWeiCountHash[devicename] >= 300)
                    {
                        //                        _hash[_key] = _value;
                    }
                    _hash[_key] = _value;
                }else if(_value >= ABYEWEIBIAOZHUN)
                {
                    m_tempABYeWeiCountHash[devicename] = 0;
                    _hash[_key] = _value;
                }
            }
        }
        if(_key.indexOf("水温") != -1 || _key.indexOf("内部温度") != -1 || _key.indexOf("环境温度") != -1)
        {
            //                        qDebug() << "devicename:" << devicename << ":" << _key << ":" << _v;
            //            double _input;
            //            double _data;
            //            //计算水温值
            //            double _v1 = 3.3 / 4096;
            //            double _v2 = _v1 * _v;
            //            double _v3 = _v2 + 0.11;
            //            _v3 = _v3 / 150;

            //            double _v4 = m_wenduFuDongZhi / _v3;
            //            double _v5 = _v4 - 1150;
            //            _data = processWenDu(_v5);
            //            if(m_wenduLvBoHash[devicename].count() < QUEUE_LENGTH)
            //            {
            //                m_wenduLvBoHash[devicename].enqueue(_data);
            ////                qDebug() << _key << _data;
            //                _data = 35;
            //                _hash[_key] = _data;
            //            }else
            //            {
            //                m_wenduLvBoHash[devicename].dequeue();
            //                m_wenduLvBoHash[devicename].enqueue(_data);
            //                double _sum = 0;
            //                for(int i =0;i<m_wenduLvBoHash[devicename].count();i++)
            //                {
            //                    _sum += m_wenduLvBoHash[devicename].at(i);
            //                }
            //                _input = _sum / m_wenduLvBoHash[devicename].count();
            ////                qDebug() << _key << _input;
            ////                qDebug() << "名字:" << _key << "->这次计算的值：" << _data << "->累计总和:" << _sum << "->累计总个数:" << m_wenduLvBoHash[devicename].count() << "->计算后的值：" << _input;
            //                _hash[_key] = _input;
            //                continue;
            //            }


            //            double _data;
            //            double _input = _v;
            //            //计算水温值
            //            double _v1 = 3.300000 / 4096;
            //            double _v2 = _v1 * _input;
            //            double _v3 = _v2 + 0.115000;
            //            _v3 = _v3 / 150;

            //            double _v4 = m_wenduFuDongZhi / _v3;
            //            double _v5 = _v4 - 1150;
            //            _data = processWenDu(_v5);
            //            qDebug() << _key << ":" << _data << ":原始值：" << _v;





            int _data;
            if(m_wenduLvBoHash[_key].count() < QUEUE_LENGTH)
            {
                _data = 35;
                m_wenduLvBoHash[_key].enqueue(_v);
                qDebug() << _key << _data << "队列长度:" << m_wenduLvBoHash[_key].size();
                _hash[_key] = _data;
            }else
            {
                m_wenduLvBoHash[_key].enqueue(_v);
                m_wenduLvBoHash[_key].dequeue();
                QQueue<double> _queue;
                _queue = m_wenduLvBoHash[_key];
                qSort(_queue);
                double _sum = 0;
                for(int j =0;j<m_wenduLvBoHash[_key].count();j++)
                {
                    _sum += m_wenduLvBoHash[_key].at(j);
                }
                double _input;
                //                _input = _sum / m_wenduLvBoHash[_key].count();
                _input = _queue.at(QUEUE_LENGTH / 2);
                //                qDebug() << "数组数据:" << m_wenduLvBoHash << "->中间值：" << _input;
                //                qDebug() << _key << _input;
                //计算水温值
                double _v1 = 3.300000 / 4096;
                double _v2 = _v1 * _input;
                double _v3 = _v2 + m_lingDianLingYiWuZhi;
                double _v33 = _v3 / 150;

                double _v4 = ( m_wenduFuDongZhi - _v3 )/ _v33;
                double _v5 = _v4 ;//- 1150;
                int _tempData;
                _data = processWenDu(_v5);
                _tempData = _data;
                m_tiaoguo[_key] = true;
                if(m_isneedcomputer[SHUIWEN])
                {
                    //                    _hash[_key] = _data;
                    //                    setInputStatus(devicename, _hash);
                    //                    if(computerTwoInputComing())
                    //                    {
                    //                        m_isneedcomputer[SHUIWEN] = false;
                    //                        m_chazhiMutex->lock();
                    //                        getChaZhi(devicename, _key);
                    //                        m_chazhiMutex->unlock();
                    //                        continue;
                    //                    }else
                    //                    {
                    ////                        qDebug() << "未打标";
                    //                    }
                }
                if(m_isneedcomputer[SHUIWEN] == false)
                {
                    //                    qDebug() << devicename + _key << "-=========================" << m_whatBig.keys();
                    //更新值,先判断当前值大还是另一个值大
                    //                    if(m_whatBig.contains(devicename + _key))
                    //                    {
                    //                        if(m_whatBig.value(devicename + _key) == "true")
                    //                        {
                    //                            qDebug() << devicename+_key << "减去" << m_chazhi[SHUIWEN];
                    //                            //当前值大,将当前值
                    //                            _data -= m_chazhi[SHUIWEN];
                    //                        }else if(m_whatBig.value(devicename + _key) == "false")
                    //                        {
                    //                            qDebug() << devicename+_key << "加上" << m_chazhi[SHUIWEN];
                    //                            //另一个值大
                    //                            _data += m_chazhi[SHUIWEN];
                    //                        }
                    //                    }
                }
                _data -= m_guDingChaZhiHash[_key];
                _hash[_key] = _data;
                //                qDebug() << _hash[_key];
                //下面是更新同步计算后的值
                QString _tempsubkey;
                if(_key.indexOf("水温") != -1)
                {
                    _tempsubkey = "shuiwen";
                }else if(_key.indexOf("内部温度") != -1)
                {
                    _tempsubkey = "neibuwendu";
                }else if(_key.indexOf("环境温度") != -1)
                {
                    _tempsubkey = "huanjingwendu";
                }
                qDebug() << devicename  << ":" << "配读:" << m_wenduFuDongZhi << "->名字:" << _tempsubkey << "->这次计算的值：" << _tempData << "->补偿后的值:" << _data << "->累计总和:" << _sum << "->累计总个数:" << m_wenduLvBoHash[_key].count() << "->滤波"<< QUEUE_LENGTH <<"个数的值:" << _input << "->接收的原始值:" << _v;
                //                qDebug() << "peidu:" << m_wenduFuDongZhi << "->mingzi:" << _key << "->zhecijisuandezhi:" << _tempData << "->buchanghoudezhi:" << _data << "->leijizonghe:" << _sum << "->leijizonghegeshu:" << m_wenduLvBoHash[_key].count() << "->lvbo:"<< QUEUE_LENGTH <<"gesjidezjo:" << _input << "->jieshoudeyuanshizhi:" << _v;

                continue;
            }
        }
    }
//    needOpenOrCloseOutPut(devicename,_hash);
    setInputStatus(devicename, _hash);
    setFrontInputStatus(devicename, _hash);
}

void IODevice::writeDataTo(const QString devicename, const QByteArray buff)
{
    if(m_socketHash.contains(devicename))
    {
        emit m_socketHash[devicename]->writeMyDataSignal(buff);
    }
}

void IODevice::setIODeviceStatus(const QString devicename,
                                 const QString outputname1, const bool outputstatus1,
                                 const QString outputname2, const bool outputstatus2,
                                 const QString outputname3, const bool outputstatus3,
                                 const QString outputname4, const bool outputstatus4,
                                 const QString outputname5, const bool outputstatus5,
                                 const QString outputname6, const bool outputstatus6, const int count)
{
    //        qDebug() << devicename << ":" << outputname1 << outputstatus1
    //                 << ":" << outputname2 << outputstatus2
    //                 << ":" << outputname3 << outputstatus3
    //                 << ":" << outputname4 << outputstatus4
    //                 << ":" << outputname5 << outputstatus5
    //                 << ":" << outputname6 << outputstatus6;


    IODeviceStatus _status = getDeviceStatus(devicename);
    //这里也需要触底反弹逻辑

    QByteArray _arr;

    _arr.append(0x01);
    _arr.append(0x04);
    _arr.append((char)0x00);
    _arr.append(_status.outputs[m_outputIndexByName.value(devicename).value(0)]);
    _arr.append(_status.outputs[m_outputIndexByName.value(devicename).value(1)]);
    _arr.append(_status.outputs[m_outputIndexByName.value(devicename).value(2)]);
    _arr.append(_status.outputs[m_outputIndexByName.value(devicename).value(3)]);
    _arr.append(_status.outputs[m_outputIndexByName.value(devicename).value(4)]);
    _arr.append(_status.outputs[m_outputIndexByName.value(devicename).value(5)]);
    //    qDebug() << m_outputIndexByName.value(devicename).value(0)
    //             << m_outputIndexByName.value(devicename).value(1)
    //             << m_outputIndexByName.value(devicename).value(2)
    //             << m_outputIndexByName.value(devicename).value(3)
    //             << m_outputIndexByName.value(devicename).value(4)
    //             << m_outputIndexByName.value(devicename).value(5);

    if(count == 1)//只是第一个点开，其他点都从上一次的状态中获取
    {
        quint8 _sendArr = (unsigned char)0x00;
        //如果是
        if(devicename.indexOf("A框架二区控制器") != -1)
        {
            if(outputname1.indexOf("A框架加热器") != -1)
            {
                if(m_ajiareqiStatus)
                {
                    _sendArr = (unsigned char)0x00;
                }else
                {
                    _sendArr = outputstatus1 == true ? 0x01 : 0x00;
                }
            }else if(outputname1.indexOf("A框架暖风机") != -1)
            {
                if(m_anuanfengjiStatus)
                {
                    _sendArr = (unsigned char)0x00;
                }else
                {
                    _sendArr = outputstatus1 == true ? 0x01 : 0x00;
                }
            }else{
                _sendArr = outputstatus1 == true ? 0x01 : 0x00;
            }
            _status.outputs[outputname1] = _sendArr;
        }else if(devicename.indexOf("B框架二区控制器") != -1)
        {
            if(outputname1.indexOf("B框架加热器") != -1)
            {
                if(m_bjiareqiStatus)
                {
                    _sendArr = (unsigned char)0x00;
                }else
                {
                    _sendArr = outputstatus1 == true ? 0x01 : 0x00;
                }
            }else if(outputname1.indexOf("B框架暖风机") != -1)
            {
                if(m_bnuanfengjiStatus)
                {
                    _sendArr = (unsigned char)0x00;
                }else
                {
                    _sendArr = outputstatus1 == true ? 0x01 : 0x00;
                }
            }else{
                _sendArr = outputstatus1 == true ? 0x01 : 0x00;
            }
            _status.outputs[outputname1] = _sendArr;
        }else
        {
            _status.outputs[outputname1] = outputstatus1 == true ? 0x01 : 0x00;
        }
        _arr[m_outputNameByIndex.value(devicename).value(outputname1) + 3] = _status.outputs[outputname1];
    }else if(count == 2)
    {

        _status.outputs[outputname1] = outputstatus1 == true ? 0x01 : 0x00;
        _arr[m_outputNameByIndex.value(devicename).value(outputname1) + 3] = _status.outputs[outputname1];

        _status.outputs[outputname2] = outputstatus2 == true ? 0x01 : 0x00;
        _arr[m_outputNameByIndex.value(devicename).value(outputname2) + 3] = _status.outputs[outputname2];

    }else if(count == 3)
    {
        _status.outputs[outputname1] = outputstatus1 == true ? 0x01 : 0x00;
        _arr[m_outputNameByIndex.value(devicename).value(outputname1) + 3] = _status.outputs[outputname1];

        _status.outputs[outputname2] = outputstatus2 == true ? 0x01 : 0x00;
        _arr[m_outputNameByIndex.value(devicename).value(outputname2) + 3] = _status.outputs[outputname2];

        _status.outputs[outputname3] = outputstatus3 == true ? 0x01 : 0x00;
        _arr[m_outputNameByIndex.value(devicename).value(outputname3) + 3] = _status.outputs[outputname3];

    }else if(count == 4)
    {
        _status.outputs[outputname1] = outputstatus1 == true ? 0x01 : 0x00;
        _arr[m_outputNameByIndex.value(devicename).value(outputname1) + 3] = _status.outputs[outputname1];

        _status.outputs[outputname2] = outputstatus2 == true ? 0x01 : 0x00;
        _arr[m_outputNameByIndex.value(devicename).value(outputname2) + 3] = _status.outputs[outputname2];

        _status.outputs[outputname3] = outputstatus3 == true ? 0x01 : 0x00;
        _arr[m_outputNameByIndex.value(devicename).value(outputname3) + 3] = _status.outputs[outputname3];

        _status.outputs[outputname4] = outputstatus4 == true ? 0x01 : 0x00;
        _arr[m_outputNameByIndex.value(devicename).value(outputname4) + 3] = _status.outputs[outputname4];
    }else if(count == 5)
    {
        _status.outputs[outputname1] = outputstatus1 == true ? 0x01 : 0x00;
        _arr[m_outputNameByIndex.value(devicename).value(outputname1) + 3] = _status.outputs[outputname1];

        _status.outputs[outputname2] = outputstatus2 == true ? 0x01 : 0x00;
        _arr[m_outputNameByIndex.value(devicename).value(outputname2) + 3] = _status.outputs[outputname2];

        _status.outputs[outputname3] = outputstatus3 == true ? 0x01 : 0x00;
        _arr[m_outputNameByIndex.value(devicename).value(outputname3) + 3] = _status.outputs[outputname3];

        _status.outputs[outputname4] = outputstatus4 == true ? 0x01 : 0x00;
        _arr[m_outputNameByIndex.value(devicename).value(outputname4) + 3] = _status.outputs[outputname4];

        _status.outputs[outputname5] = outputstatus5 == true ? 0x01 : 0x00;
        _arr[m_outputNameByIndex.value(devicename).value(outputname5) + 3] = _status.outputs[outputname5];

    }else if(count == 6)
    {
        _status.outputs[outputname1] = outputstatus1 == true ? 0x01 : 0x00;
        _arr[m_outputNameByIndex.value(devicename).value(outputname1) + 3] = _status.outputs[outputname1];

        _status.outputs[outputname2] = outputstatus2 == true ? 0x01 : 0x00;
        _arr[m_outputNameByIndex.value(devicename).value(outputname2) + 3] = _status.outputs[outputname2];

        _status.outputs[outputname3] = outputstatus3 == true ? 0x01 : 0x00;
        _arr[m_outputNameByIndex.value(devicename).value(outputname3) + 3] = _status.outputs[outputname3];

        _status.outputs[outputname4] = outputstatus4 == true ? 0x01 : 0x00;
        _arr[m_outputNameByIndex.value(devicename).value(outputname4) + 3] = _status.outputs[outputname4];

        _status.outputs[outputname5] = outputstatus5 == true ? 0x01 : 0x00;
        _arr[m_outputNameByIndex.value(devicename).value(outputname5) + 3] = _status.outputs[outputname5];

        _status.outputs[outputname6] = outputstatus6 == true ? 0x01 : 0x00;
        _arr[m_outputNameByIndex.value(devicename).value(outputname6) + 3] = _status.outputs[outputname6];

    }
    setOutputStatus(devicename, _status.outputs);
    //将状态设置回实时状态里
    qDebug() << _status.outputs.keys();
    qDebug() << _status.outputs.values();
    qDebug() << devicename <<":"<<_arr;
    writeDataTo(devicename, _arr);
}

void IODevice::updateIODeviceStatus(const QString devicename,
                                    const int count,
                                    const QString subname1, const bool status1,
                                    const QString subname2, const bool status2,
                                    const QString subname3, const bool status3,
                                    const QString subname4, const bool status4,
                                    const QString subname5, const bool status5,
                                    const QString subname6, const bool status6)
{
    IODeviceStatus _status = getDeviceStatus(devicename);
    if(count == 1)
    {
        if(status1 == true)
        {
            _status.outputs[subname1] = 0x01;
        }else if(status1 == false)
        {
            _status.outputs[subname1] = 0x00;
        }
    }else if(count == 2)
    {
        if(status1 == true)
        {
            _status.outputs[subname1] = 0x01;
        }else if(status1 == false)
        {
            _status.outputs[subname1] = 0x00;
        }
        if(status2 == true)
        {
            _status.outputs[subname2] = 0x01;
        }else if(status2 == false)
        {
            _status.outputs[subname2] = 0x00;
        }
    }else if(count == 3)
    {
        if(status1 == true)
        {
            _status.outputs[subname1] = 0x01;
        }else if(status1 == false)
        {
            _status.outputs[subname1] = 0x00;
        }
        if(status2 == true)
        {
            _status.outputs[subname2] = 0x01;
        }else if(status2 == false)
        {
            _status.outputs[subname2] = 0x00;
        }
        if(status3 == true)
        {
            _status.outputs[subname3] = 0x01;
        }else if(status3 == false)
        {
            _status.outputs[subname3] = 0x00;
        }
    }else if(count == 4)
    {
        if(status1 == true)
        {
            _status.outputs[subname1] = 0x01;
        }else if(status1 == false)
        {
            _status.outputs[subname1] = 0x00;
        }
        if(status2 == true)
        {
            _status.outputs[subname2] = 0x01;
        }else if(status2 == false)
        {
            _status.outputs[subname2] = 0x00;
        }
        if(status3 == true)
        {
            _status.outputs[subname3] = 0x01;
        }else if(status3 == false)
        {
            _status.outputs[subname3] = 0x00;
        }
        if(status4 == true)
        {
            _status.outputs[subname4] = 0x01;
        }else if(status4 == false)
        {
            _status.outputs[subname4] = 0x00;
        }
    }else if(count == 5)
    {
        if(status1 == true)
        {
            _status.outputs[subname1] = 0x01;
        }else if(status1 == false)
        {
            _status.outputs[subname1] = 0x00;
        }
        if(status2 == true)
        {
            _status.outputs[subname2] = 0x01;
        }else if(status2 == false)
        {
            _status.outputs[subname2] = 0x00;
        }
        if(status3 == true)
        {
            _status.outputs[subname3] = 0x01;
        }else if(status3 == false)
        {
            _status.outputs[subname3] = 0x00;
        }
        if(status4 == true)
        {
            _status.outputs[subname4] = 0x01;
        }else if(status4 == false)
        {
            _status.outputs[subname4] = 0x00;
        }
        if(status5 == true)
        {
            _status.outputs[subname5] = 0x01;
        }else if(status5 == false)
        {
            _status.outputs[subname5] = 0x00;
        }
    }else if(count == 6)
    {
        if(status1 == true)
        {
            _status.outputs[subname1] = 0x01;
        }else if(status1 == false)
        {
            _status.outputs[subname1] = 0x00;
        }
        if(status2 == true)
        {
            _status.outputs[subname2] = 0x01;
        }else if(status2 == false)
        {
            _status.outputs[subname2] = 0x00;
        }
        if(status3 == true)
        {
            _status.outputs[subname3] = 0x01;
        }else if(status3 == false)
        {
            _status.outputs[subname3] = 0x00;
        }
        if(status4 == true)
        {
            _status.outputs[subname4] = 0x01;
        }else if(status4 == false)
        {
            _status.outputs[subname4] = 0x00;
        }
        if(status5 == true)
        {
            _status.outputs[subname5] = 0x01;
        }else if(status5 == false)
        {
            _status.outputs[subname5] = 0x00;
        }
        if(status6 == true)
        {
            _status.outputs[subname6] = 0x01;
        }else if(status6 == false)
        {
            _status.outputs[subname6] = 0x00;
        }
    }
    setOutputStatus(devicename, _status.outputs);
}
bool IODevice::isOpenJiaReQiTimer(const QString devicename, QHash<QString, quint16> hash)
{
    //    qDebug() << hash.keys() << ":" << hash.values();
    bool _ret = false;
    if(devicename.indexOf("A框架二区控制器") != -1)
    {
        QList<QString> _keys = hash.keys();
        quint16 _v = 0;
        for(int i =0;i<_keys.size();i++)
        {
            if(_keys.at(i).indexOf("A框架加热器反馈") != -1)
            {
                _v = hash.value(_keys.at(i));
            }
        }
        if(_v > 2000)
        {
            _ret = true;
        }else
        {
            _ret = false;
        }
    }else if(devicename.indexOf("B框架二区控制器") != -1)
    {
        QList<QString> _keys = hash.keys();
        quint16 _v = 0;
        for(int i =0;i<_keys.size();i++)
        {
            if(_keys.at(i).indexOf("B框架加热器反馈") != -1)
            {
                _v = hash.value(_keys.at(i));
            }
        }
        if(_v > 2000)
        {
            _ret = true;
        }else
        {
            _ret = false;
        }
    }
    return _ret;
}

bool IODevice::isOpenNuanFengJiTimer(const QString devicename, QHash<QString, quint16> hash)
{
    //    qDebug() << hash.keys() << ":" << hash.values();
    bool _ret = false;
    if(devicename.indexOf("A框架二区控制器") != -1)
    {
        QList<QString> _keys = hash.keys();
        quint16 _v = 0;
        for(int i =0;i<_keys.size();i++)
        {
            if(_keys.at(i).indexOf("A框架暖风机反馈") != -1)
            {
                _v = hash.value(_keys.at(i));
            }
        }
        if(_v > 2000)
        {
            _ret = true;
        }else
        {
            _ret = false;
        }
    }else if(devicename.indexOf("B框架二区控制器") != -1)
    {
        QList<QString> _keys = hash.keys();
        quint16 _v = 0;
        for(int i =0;i<_keys.size();i++)
        {
            if(_keys.at(i).indexOf("B框架暖风机反馈") != -1)
            {
                _v = hash.value(_keys.at(i));
            }
        }
        if(_v > 2000)
        {
            _ret = true;
        }else
        {
            _ret = false;
        }
    }
    return _ret;
}

