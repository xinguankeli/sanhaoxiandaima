#ifndef GATEDEVICE_H
#define GATEDEVICE_H

#include <QObject>
#include <QHash>
#include <QThread>
#include <QMutex>
#include <QTimer>
#include "../mysocket.h"
#include <QList>

//1.获取阀门设备的状态
//2.设置阀门设备的开关
class GateDevice : public QObject
{
    Q_OBJECT
public:
    explicit GateDevice(QObject *parent = nullptr);
    //配置文件设置的阀门设备信息
    void setDeviceInfo(QHash<QString, MyGateDeviceInfo>);
    //启动所有阀门设备
    void startALLDevice();
private:
    //所有阀门设备
    QHash<QString, MySocket*> m_socketHash;

    //所有阀门设备的子线程
    QHash<QString, QThread*> m_socketThreadHash;

    //所有阀门设备信息
    QHash<QString, MyGateDeviceInfo> m_gateSensorInfoList;

    //存放所有设备的状态
    QHash<QString, GateDeviceStatus> m_deviceStatusHash;

    //写入数据的锁
    QMutex *m_mutex;

    //收集阀门状态的变量的锁
    QMutex *m_reGetGateStatusMutex;

    //阀门的未知拦截锁，在阀门开的过程中会变成未知，要在开启后将其设置为拦截状态
    QHash<QString, QHash<QString, bool> > m_unknowStatusHash;
    //阀门的拦截计数器
    QHash<QString, QHash<QString, int> > m_unknowCountHash;

    //操作阀门未知拦截锁的锁，防止次数过多，同时操作
    QMutex *m_unknowCountMutex;

    QTimer *m_timer;

    QHash<QString, QHash<QString, bool> > m_gateStatusHash;
private:
    //获取一次开机状态的标志位
    bool m_getGateStatusSign;

    bool computerGateStatus();

    //设置网络连接故障状态
    //key是设备名，value是网络状态的值
    void setNetworkStatus(const QString &, const QString);

    //设置阀门开关状态
    //key是设备名，values是开关的值
    void setOpenCloseStatus(const QString &, const QString, const QString);

    //设置阀门模块的拦截次数
    //第一个参数：模块名
    //第二个参数：子阀门名
    //第三个参数：拦截几次
    void setLanJieCount(const QString &, const QString &, const int);

    //将拦截模块的值-1
    //第一个参数设备名
    //第二个参数子阀门名
    void subLanJieCount(const QString &, const QString &);

    //获取拦截模块剩余次数
    //第一个参数设备名
    //第二个参数子阀门名
    int getLanJieCount(const QString &, const QString &);

    //获取当前阀门的状态
    //key是总设备名
    GateDeviceStatus getGateStatus(const QString &);

    //设置重新收集阀门状态变量
    void setReGetGateStatus(bool);

    //获取重新收集阀门状态变量
    bool getReGetGateStatus();
public slots:
    //处理设备的在线状态
    //设备的网络连接状态
    //第一个参数为设备名
    //第二个参数为网络连接错误或网络连接失败
    void processDeviceStatus(QString, QString);


    //处理设备的数据
    //设备发送上来的数据处理后解析成各种信号发出去
    void processDeviceData(QString, QByteArray);

    //根据key来执行对应的逻辑
    //设置火警的温度曲线
    //设置xxx等
    void writeDataTo(const QString, const QByteArray );

    //根据key来执行对应阀门的开关
    //第一个参数是设备名
    //第二个参数是左侧阀门名
    //第三个参数是左侧阀门开关状态
    //第三个参数为右侧阀门名
    //第四个参数为右侧阀门开关状态
    void setGateOpenCloseStatus(const QString , const QString , const bool , const QString , const bool );

    //外界告诉阀门类，重新收集阀门的状态
    void reGetGateStatus();
signals:
    //通过该信号将设备的状态发送出去
    //复位成功，火警状态等
    void updateDeviceStatus(const QString, GateDeviceStatus);

    //将收集的状态发给Core
    void sendMyStatus(QHash<QString, QHash<QString, bool> >);
};

#endif // GATEDEVICE_H
