#include "mysocket.h"
#include <QNetworkProxy>


MySocket::MySocket(const QString &deviceName, const QString &ip, const quint16 &port, const int &count, QObject *parent)
    :QTcpSocket(parent)
    ,    m_deivceName(deviceName),
      m_ip(ip),
      m_port(port),
      m_count(count),
      m_isCanSendNetworkErrorSign(true),
      m_gateWriteDataStatus(false),
      m_isCanSend(false),
      m_sendCount(false)

{
    connect(this, &QTcpSocket::readyRead, this, &MySocket::readMydata);//接收数据
}



void MySocket::startTcpSocket()
{
    m_timer = new QTimer(this);
    connect(m_timer, &QTimer::timeout, this, &MySocket::reconnect);
    m_timer->setInterval(2500);
    connectToHost(m_ip, m_port);
    if(!waitForConnected(3000))
    {
        qDebug() << "connectToHost error";
        emit deviceStatus(m_deivceName, NETWORKERR);
        m_timer->start();
    }else{
        qDebug() << m_deivceName << " connectToHost successful";
        m_isCanSend = true;
    }
}

void MySocket::stopTcpSocket()
{
    abort();
    close();
}

void MySocket::reconnect()
{
    abort();
    connectToHost(m_ip, m_port);
    m_isCanSend = false;
    if(!waitForConnected(1500))
    {
        emit deviceStatus(m_deivceName, NETWORKERR);
    }else
    {
        qDebug() << m_port << ":" << m_ip << "reconnect succ, stop timer";
        m_isCanSendNetworkErrorSign = true;
        m_readBuff.clear();
        emit deviceStatus(m_deivceName,  NETWORKSUCC);
        m_isCanSend = true;
        m_timer->stop();
        m_sendCount = 0;
    }
}
void MySocket::writeMyDataSlot(const QByteArray &arr)
{

    if(m_isCanSend)
    {
        m_sendCount += 1;
        //        if(m_deivceName.indexOf("感温") != -1 && arr == ":HJFW;")
        //        {
        //            qDebug() << m_deivceName << ":" << arr;
        //            qDebug() << "LINE:" << __LINE__ << ":发送长度:" << write(arr, arr.size());
        //            qDebug() << m_deivceName << ":" <<  waitForBytesWritten();

        //        }else
        //        {
        //            write(arr, arr.size());
        //            waitForBytesWritten();
        //        }
        write(arr, arr.size());
        waitForBytesWritten();
        if(m_sendCount > 15)
        {
            //设备网络故障
            if(m_isCanSendNetworkErrorSign == true)
            {
                qDebug() << "socket network down line";
                emit deviceStatus(m_deivceName, NETWORKERR);
//                m_isCanSendNetworkErrorSign = false;
            }
            //启动重连
            if(m_isCanSend == true)
            {
                qDebug() << "start reconnect timer";
                m_timer->start();
                m_isCanSendNetworkErrorSign = true;
                m_isCanSend = false;
            }
        }
    }
}

void MySocket::readMydata()
{
    m_readBuff.append(readAll());

    if(m_readBuff.size() >= m_count)
    {
        m_sendCount = 0;

        //是一个完整包的数据，处理数据
        QByteArray _arr = m_readBuff;
        emit deviceData(m_deivceName, _arr);
        m_readBuff.clear();
    }else{
        return;
    }
}
