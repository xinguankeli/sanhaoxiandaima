#ifndef MYSOCKET_H
#define MYSOCKET_H

#include <QObject>
#include <QTcpSocket>
#include <QTimer>
#include <QByteArray>
#include "global/Global.h"
class MySocket : public QTcpSocket
{
    Q_OBJECT
public:
    MySocket(const QString &deviceName,
             const QString &ip,
             const quint16 &port,
             const int &count,
             QObject *parent = nullptr);
private:
    QString m_deivceName;
    QString m_ip;
    quint16 m_port;
    int m_count;
    QTimer *m_timer;
    bool m_isCanSendNetworkErrorSign;
    QByteArray m_readBuff;
    bool m_gateWriteDataStatus;
    //用于确定是否掉线
    bool m_isCanSend;

    //用于存放发送了几次,超过三次就是没回就是故障
    int m_sendCount;
public slots:
    void readMydata();
    void writeMyDataSlot(const QByteArray &);
    void startTcpSocket();
    void stopTcpSocket();
    void reconnect();
signals:
    void deviceStatus(QString, QString);//告诉外界设备的网络状态
    void deviceData(QString, QByteArray);                 //告诉外界收到的数据
    void writeMyDataSignal(const QByteArray);//外部通过信号告知socket发送数据
};

#endif // MYSOCKET_H
