#include <QCoreApplication>
#include <QThread>
#include "core.h"
#include <QTextCodec>
#include <QFile>
void outLogMessageToFile(QtMsgType type,
                         const QMessageLogContext &context,
                         const QString &msg)
{
    if(type == QtDebugMsg)
    {
        QString message;
        message = qFormatLogMessage(type, context, msg);
        message.append("\r\n");

        QFile file("./log.txt");
        if(file.open(QIODevice::WriteOnly | QIODevice::Append))
        {
            file.write(message.toLocal8Bit());
        }
        file.close();
    }
}
int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
//    QFile file("./log.txt");
//    file.remove();
//    file.close();
//    qSetMessagePattern("[%{time yyyy-MM-dd hh:mm:ss}] %{file} %{line} %{function} %{message}");

//    qInstallMessageHandler(outLogMessageToFile);
    QThread *thread=  new QThread;
    Core *core = new Core;
    QObject::connect(thread, &QThread::started, core, &Core::startExec);
    core->moveToThread(thread);
    thread->start();
    qRegisterMetaType<FireDeviceStatus>("FireDeviceStatus");
    qRegisterMetaType<GateDeviceStatus>("GateDeviceStatus");
    qRegisterMetaType<SmogDeviceStatus>("SmogDeviceStatus");
    qRegisterMetaType<IODeviceStatus>("IODeviceStatus");
    qRegisterMetaType<MyDataBaseWord>("MyDataBaseWord");
    qRegisterMetaType<QList<MyDataBaseWord> > ("QList<MyDataBaseWord>");
    qRegisterMetaType<DataRequestResponseFrame>("DataRequestResponseFrame");
    qRegisterMetaType<MVBToCoreData>("MVBToCoreData");
    qRegisterMetaType<QHash<QString, QHash<QString, bool> >>("QHash<QString, QHash<QString, bool> >");
    return a.exec();
}
