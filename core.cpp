#include "core.h"
#include <QPair>
#include <iostream>
#include <set>
#include <QFile>
#include <QCoreApplication>
#include <QDateTime>
#define INPUT_SHIELD_VALUE 2000

//#define DOUBLERESETBUTTON
Core::Core(QObject *parent) : QObject(parent),
    m_fireDeivceStatusMutex(new QMutex),
    m_gateDeviceStatusMutex(new QMutex),
    m_smogDeviceStatusMutex(new QMutex),
    m_ioDeviceStatusMutex(new QMutex),
    m_multiLambdaMutex(new QMutex),
    m_masterOrClientMutex(new QMutex),
    m_multiLambdaTimerMutex(new QMutex),
    m_multiLambdaVarMutex(new QMutex),
    m_gateUnKnowStatusSign(false),
    m_doubleResetSign(true)
{
    m_mvbLiveSignal = 0;
    m_frontStatusByButtonClickedHash.insert("一车测试按钮", false);
    m_frontStatusByButtonClickedHash.insert("六车测试按钮", false);
    m_frontStatusByButtonClickedHash.insert("一车复位按钮", false);
    m_frontStatusByButtonClickedHash.insert("六车复位按钮", false);
    m_buttonClickedCountHash.insert("一车测试按钮", 0);
    m_buttonClickedCountHash.insert("六车测试按钮", 0);
    m_buttonClickedCountHash.insert("一车复位按钮", 0);
    m_buttonClickedCountHash.insert("六车复位按钮", 0);
    m_buttonDoubleClickedTimer = new QTimer(this);
    connect(m_buttonDoubleClickedTimer, &QTimer::timeout, this, &Core::clearButtonClickedCount);
    m_buttonDoubleClickedTimer->setInterval(1000 * 60 * 120);
    //    m_buttonDoubleClickedTimer->setInterval(3000);
    m_buttonDoubleClickedTimer->setSingleShot(true);

    m_buttonDoubleClickedYanShiTimer = new QTimer(this);
    connect(m_buttonDoubleClickedYanShiTimer, &QTimer::timeout, this, &Core::processDoubleResetButtonYanShiCopy);
    m_buttonDoubleClickedYanShiTimer->setInterval(2000);
    m_buttonDoubleClickedYanShiTimer->setSingleShot(true);

    m_checkResetButtonDownTimer = new QTimer(this);
    connect(m_checkResetButtonDownTimer, &QTimer::timeout, this, &Core::processResetButtonDownByOneMintue);
    m_checkResetButtonDownTimer->setInterval(1000);
    //    m_checkResetButtonDownTimer->start();


    m_yicheVarLanJieHash.insert("一车一到三号感温探测器集体火警", false);
    m_yicheVarLanJieHash.insert("一车四到六号感温探测器集体火警", false);
    m_yicheVarLanJieHash.insert("二车一到三号感温探测器集体火警", false);
    m_yicheVarLanJieHash.insert("二车四到六号感温探测器集体火警", false);
    m_yicheVarLanJieHash.insert("三车一到三号感温探测器集体火警", false);
    m_yicheVarLanJieHash.insert("三车四到六号感温探测器集体火警", false);

    m_liucheVarLanJieHash.insert("四车一到三号感温探测器集体火警", false);
    m_liucheVarLanJieHash.insert("四车四到六号感温探测器集体火警", false);
    m_liucheVarLanJieHash.insert("五车一到三号感温探测器集体火警", false);
    m_liucheVarLanJieHash.insert("五车四到六号感温探测器集体火警", false);
    m_liucheVarLanJieHash.insert("六车一到三号感温探测器集体火警", false);
    m_liucheVarLanJieHash.insert("六车四到六号感温探测器集体火警", false);

    insertDotByMVB();
    m_readConf = new MyReadConf;
    connect(this, &Core::setFireSheildStatusSignal, m_readConf, &MyReadConf::setFireDeviceStatusSlot, Qt::BlockingQueuedConnection);
    connect(this, &Core::setSmogShieldStatusSignal, m_readConf, &MyReadConf::setSmogDeviceStatusSlot, Qt::BlockingQueuedConnection);
    connect(this, &Core::setQXZSignal, m_readConf, &MyReadConf::setQXZSlot, Qt::QueuedConnection);
    connect(this, &Core::setLMDSignal, m_readConf, &MyReadConf::setLMDSlot, Qt::QueuedConnection);
    connect(this, &Core::startMultiLambdaTimerSignal, this, &Core::startMultiLambdaTimerSlot, Qt::QueuedConnection);
    m_eventMultiLambdaHash = m_readConf->getEventMultiLambdaList();

    DataBaseConfigWord _configWord = m_readConf->getDataBaseConfigWord();
    m_database = new MyDataBase(_configWord.hostname, _configWord.databasename, _configWord.username, _configWord.password);
    QThread *_databaseThread = new QThread;
    m_database->moveToThread(_databaseThread);
    connect(this, &Core::insertDataToDataBaseSignal, m_database, &MyDataBase::insertDataToDataBaseSlot, Qt::QueuedConnection);
    connect(_databaseThread, &QThread::started, m_database, &MyDataBase::startMyDataBase);
    connect(this, &Core::selectDataByDateToUIUsedTCPSignal, m_database, &MyDataBase::selectDataByDateToUIUsedTCP, Qt::QueuedConnection);
    connect(this, &Core::saveCsv, m_database, &MyDataBase::selectAllDataToLocal, Qt::QueuedConnection);

    _databaseThread->start();

    m_expressServer = new MyTcpServer(65432);
    //core类通过sendMessage发送数据给UI
    connect(this, &Core::sendMessage, m_expressServer, &MyTcpServer::sendMessage, Qt::QueuedConnection);
    //core通过processDataFromUI处理ui发来的控制指令
    connect(m_expressServer,&MyTcpServer::haveMessage, this, &Core::processDataFromUI, Qt::QueuedConnection);
    QThread *_expressServerThread = new QThread;
    m_expressServer->moveToThread(_expressServerThread);
    connect(_expressServerThread, &QThread::started, m_expressServer, &MyTcpServer::startExpressServer);
    _expressServerThread->start();

    m_eventUdp = new MyEventUDP;
    m_eventUdp->setEventAddressList(m_readConf->getUdpEventDeviceInfoList());
    connect(m_eventUdp, &MyEventUDP::sendUdpEvent, this, &Core::processEventFromEventUDP);
    connect(m_eventUdp, &MyEventUDP::smogShieldStatus, this, &Core::processSmogShieldStatusFromEventUDP);
    connect(m_eventUdp, &MyEventUDP::fireShieldStauts, this, &Core::processFireShieldStatusFromEventUDP);
    connect(m_eventUdp, &MyEventUDP::setQXZ, this, &Core::processQXZFromEventUDP);
    connect(m_eventUdp, &MyEventUDP::setLMD, this, &Core::processLMDFromEventUDP);
    connect(m_eventUdp, &MyEventUDP::setIOStatus, this, &Core::processIOStatusFromEventUDP);
    connect(m_eventUdp, &MyEventUDP::resetStatus, this, &Core::processResetStatusFromEventUDP);
    connect(m_eventUdp, &MyEventUDP::updateGateStatus, this, &Core::processUpdateGateStatus);
    connect(m_eventUdp, &MyEventUDP::updateFaultMessage, this, &Core::processFaultMessageToUI);
    connect(m_eventUdp, &MyEventUDP::updateEventMessage, this, &Core::processEventMessageToUI);
    connect(m_eventUdp, &MyEventUDP::updateQiMieStatus, this, &Core::processQiMieStatusToVar);
    connect(m_eventUdp, &MyEventUDP::setDebugNumber, [&](QString str){
        m_debugStr = str;
        qDebug() << m_debugStr;
    });

    if(m_readConf->getMasterSign() == "master")
    {
        m_masterOrClient = new MyMasterOrClient(m_readConf->getMasterNetworkAddress(), "client");
        connect(m_masterOrClient, &MyMasterOrClient::sendStatus, this, &Core::processMasterOrClientStatus);
        connect(m_masterOrClient, &MyMasterOrClient::updateIOStatus, this, &Core::updateIODeviceStatusFromMasterOrClient);
    }else
    {
        m_clientOrMaster = new MyClientOrMaster(m_readConf->getMasterNetworkAddress(), "master");
        connect(m_clientOrMaster, &MyClientOrMaster::sendStatus, this, &Core::processMasterOrClientStatus);
    }

    m_execMultiLambdaHash = m_readConf->getExecMultiLambdaList();


    QHash<QString, MyFireAlarmSensorInfo> _fireHash = m_readConf->getFireAlarmSensorInfoMap();
    QHash<QString, MyGateDeviceInfo> _gateHash = m_readConf->getGateDeviceInfoMap();
    QHash<QString, MySmogSensorInfo> _smogHash = m_readConf->getSmogInfoMap();
    QHash<QString, MyControllerInfo> _ioHash = m_readConf->getIOInfoMap();

    m_fireDevice = new FireDevice;
    m_fireDevice->setDeviceInfo(_fireHash);
    m_fireDevice->startALLDevice();
    connect(m_fireDevice, &FireDevice::updateDeviceStatus, this, &Core::updateFireDeviceData, Qt::QueuedConnection);
    connect(this, &Core::resetFireDeviceSignal, m_fireDevice, &FireDevice::resetFireDeviceSlot, Qt::QueuedConnection);
    connect(this, &Core::normalFireDeviceSignal, m_fireDevice, &FireDevice::normalFireDeviceSlot, Qt::QueuedConnection);
    connect(this, &Core::setFireSheildStatusSignal, m_fireDevice, &FireDevice::setSheildStatusSlot, Qt::BlockingQueuedConnection);
    connect(this, &Core::setQXZSignal, m_fireDevice, &FireDevice::setQXZ, Qt::QueuedConnection);

    QList<QString> _fireNames = m_fireDevice->getDeviceNames();
    for(int i =0;i<_fireNames.size();i++)
    {
        m_fireToIdStatusLanJieHash.insert(_fireNames.at(i), "正常");
        m_fireResetSignHash.insert(_fireNames.at(i), true);
        MyTimer *_timer = new MyTimer(this);
        QList<QVariant> _list;
        _list.append(_fireNames.at(i));
        _timer->setMyData(_list);
        connect(_timer, &MyTimer::myTimeOut, [&](QList<QVariant> list){
            QString _devicename;
            if(list.size() > 0)
            {
                _devicename = list.at(0).toString();
            }
            m_doubleResetFireDeviceFireStatusHash[_devicename] = 0;
        });
        _timer->setSingleShot(true);
        _timer->setInterval(1000 * 60 * 120);
        m_doubleResetFireDeviceFireTimerHash.insert(_fireNames.at(i), _timer);
        m_fireDeviceFireStatusHashByUpdateStatus.insert(_fireNames.at(i), "无火警");
        m_fireDeviceFireMutexHashByUpdateStatus.insert(_fireNames.at(i), true);
        m_fireDeviceResetMutexHashByUpdateStatus.insert(_fireNames.at(i), true);
        m_fireSmogDeviceStatusHash.insert(_fireNames.at(i), _fireHash.value(_fireNames.at(i)).base.shield);
    }

    m_gateDevice = new GateDevice;
    m_gateDevice->setDeviceInfo(_gateHash);
    m_gateDevice->startALLDevice();
    connect(m_gateDevice, &GateDevice::updateDeviceStatus, this, &Core::updateGateDeviceData, Qt::QueuedConnection);
    connect(this, &Core::setGateOpenCloseStatusSignal, m_gateDevice, &GateDevice::setGateOpenCloseStatus, Qt::BlockingQueuedConnection);
    connect(this, &Core::reGetGateStatus, m_gateDevice, &GateDevice::reGetGateStatus, Qt::QueuedConnection);
    connect(m_gateDevice, &GateDevice::sendMyStatus, [&](QHash<QString, QHash<QString, bool> > hash){
        m_gateUnKnowStatusHash = hash;
        m_gateUnKnowStatusSign = true;
        qDebug() << "ok";
    });
    for(int i =0;i<_gateHash.size();i++)
    {
        MyGateDeviceInfo _device = _gateHash.value(_gateHash.keys().at(i));
        m_gateUnKnowStatusLanJieHash.insert(_device.leftalias, 0);
        m_gateUnKnowStatusLanJieHash.insert(_device.rightalias, 0);
    }
    QString _smogLMD = m_readConf->getSmogDeviceArgument().value("one");
    m_smogDevice = new SmogDevice();
    m_smogDevice->setDeviceLMD(_smogLMD);
    m_smogDevice->setDeviceInfo(_smogHash);
    m_smogDevice->startALLDevice();
    connect(m_smogDevice, &SmogDevice::updateDeviceStatus, this, &Core::updateSmogDeviceData, Qt::QueuedConnection);
    connect(this, &Core::resetSmogDeviceSignal, m_smogDevice, &SmogDevice::resetDeviceSlot, Qt::QueuedConnection);
    connect(this, &Core::normalSmogDeviceSignal, m_smogDevice, &SmogDevice::normalSmogDeviceSlot, Qt::QueuedConnection);
    connect(this, &Core::setLMDSignal, m_smogDevice, &SmogDevice::setLMD, Qt::QueuedConnection);
    connect(this, &Core::setSmogShieldStatusSignal, m_smogDevice, &SmogDevice::setSheildStatusSlot, Qt::BlockingQueuedConnection);

    QList<QString> _smogNames = m_smogDevice->getDeviceNames();
    for(int i =0;i<_smogNames.size();i++)
    {
        m_smogToIdStatusLanJieHash.insert(_smogNames.at(i), "正常");

    }
    MyTimer *_asmogtimer = new MyTimer(this);
    QList<QVariant> _list;
    _list.append("一车司机室感烟探测器");
    _asmogtimer->setMyData(_list);
    connect(_asmogtimer, &MyTimer::myTimeOut, [&](QList<QVariant> list){
        QString _devicename;
        if(list.size() > 0)
        {
            _devicename = list.at(0).toString();
        }
        m_doubleResetFireDeviceFireStatusHash[_devicename] = 0;
    });
    _asmogtimer->setInterval(1000 * 60 * 120);
    _asmogtimer->setSingleShot(true);
    m_doubleResetFireDeviceFireTimerHash.insert("一车司机室感烟探测器", _asmogtimer);
    m_smogDeviceFireStatusHashByUpdateStatus.insert("一车司机室感烟探测器", "无火警");
    m_smogDeviceFireMutexHashByUpdateStatus.insert("一车司机室感烟探测器", true);
    m_smogDeviceResetMutexHashByUpdateStatus.insert("一车司机室感烟探测器", true);
    m_smogDeviceFireStatusHashByUpdateStatus.insert("六车司机室感烟探测器", "无火警");
    m_smogDeviceFireMutexHashByUpdateStatus.insert("六车司机室感烟探测器", true);
    m_smogDeviceResetMutexHashByUpdateStatus.insert("六车司机室感烟探测器", true);
    m_fireSmogDeviceStatusHash.insert("一车司机室感烟探测器", _smogHash.value("感烟传感器A区").address1shield);
    m_fireSmogDeviceStatusHash.insert("六车司机室感烟探测器", _smogHash.value("感烟传感器B区").address1shield);


    MyTimer *_bsmogtimer = new MyTimer(this);
    QList<QVariant> _list2;
    _list2.append("六车司机室感烟探测器");
    _bsmogtimer->setMyData(_list2);
    connect(_bsmogtimer, &MyTimer::myTimeOut, [&](QList<QVariant> list){
        QString _devicename;
        if(list.size() > 0)
        {
            _devicename = list.at(0).toString();
        }
        m_doubleResetFireDeviceFireStatusHash[_devicename] = 0;
    });
    _bsmogtimer->setInterval(1000 * 60 * 120);
    _bsmogtimer->setSingleShot(true);
    m_doubleResetFireDeviceFireTimerHash.insert("六车司机室感烟探测器", _bsmogtimer);



    m_ajiareqisanciCount = 0;
    m_bjiareqisanciCount = 0;
    m_anuanfengjisanciCount = 0;
    m_bnuanfengjisanciCount = 0;

    m_anuanfengjisanciTimer = new QTimer(this);
    m_bnuanfengjisanciTimer = new QTimer(this);
    m_bjiareqisanciTimer = new QTimer(this);
    m_ajiareqisanciTimer = new QTimer(this);

    m_anuanfengjisanciTimer->setInterval(1000);
    m_bnuanfengjisanciTimer->setInterval(1000);
    m_ajiareqisanciTimer->setInterval(1000);
    m_bjiareqisanciTimer->setInterval(1000);

    connect(m_anuanfengjisanciTimer, &QTimer::timeout, this, [&]{
        setMultiLambdaVariableStatus("A框架暖风机故障", true);
        setMultiLambdaVariableStatus("A框架暖风机锁", true);
        emit setIODeviceStatusSignal("A框架二区控制器", "A框架暖风机", false, "", true, "", true, "", true, "", true, "", true, 1);
        //        writeLogToLogFile("A框架暖风机多次多次发送");
        m_anuanfengjisanciCount += 1;
        if(m_anuanfengjisanciCount >= 3)
        {
            //            writeLogToLogFile("A框架暖风机发送第" + QString::number(m_anuanfengjisanciCount) + "次");
            if(m_anuanfengjisanciTimer->isActive())
            {
                //                writeLogToLogFile("A框架暖风机多次发送停止");
                m_anuanfengjisanciTimer->stop();
                m_anuanfengjisanciCount = 0;
            }
        }
    });


    connect(m_bnuanfengjisanciTimer, &QTimer::timeout, this, [&]{
        setMultiLambdaVariableStatus("B框架暖风机故障", true);
        setMultiLambdaVariableStatus("B框架暖风机锁", true);
        emit setIODeviceStatusSignal("B框架二区控制器", "B框架暖风机", false, "", true, "", true, "", true, "", true, "", true, 1);
        //        writeLogToLogFile("B框架暖风机多次多次发送");

        m_bnuanfengjisanciCount += 1;
        if(m_bnuanfengjisanciCount >= 3)
        {
            //            writeLogToLogFile("B框架暖风机发送第" + QString::number(m_bnuanfengjisanciCount) + "次");
            if(m_bnuanfengjisanciTimer->isActive())
            {
                //                writeLogToLogFile("B框架暖风机多次发送停止");
                m_bnuanfengjisanciTimer->stop();
                m_bnuanfengjisanciCount = 0;
            }
        }
    });

    connect(m_ajiareqisanciTimer, &QTimer::timeout, this, [&]{
        setMultiLambdaVariableStatus("A框架加热器故障", true);
        setMultiLambdaVariableStatus("A框架加热器锁", true);
        emit setIODeviceStatusSignal("A框架二区控制器", "A框架加热器", false, "", true, "", true, "", true, "", true, "", true, 1);
        //        writeLogToLogFile("A框架加热器多次多次发送");

        m_ajiareqisanciCount += 1;
        if(m_ajiareqisanciCount >= 3)
        {
            //            writeLogToLogFile("A框架加热器发送第" + QString::number(m_ajiareqisanciCount) + "次");
            if(m_ajiareqisanciTimer->isActive())
            {
                //                writeLogToLogFile("A框架加热器多次发送停止");
                m_ajiareqisanciTimer->stop();
                m_ajiareqisanciCount = 0;
            }
        }
    });

    connect(m_bjiareqisanciTimer, &QTimer::timeout, this, [&]{
        setMultiLambdaVariableStatus("B框架加热器故障", true);
        setMultiLambdaVariableStatus("B框架加热器锁", true);
        emit setIODeviceStatusSignal("B框架二区控制器", "B框架加热器", false, "", true, "", true, "", true, "", true, "", true, 1);
        //        writeLogToLogFile("B框架加热器多次多次发送");

        m_bjiareqisanciCount += 1;
        if(m_bjiareqisanciCount >= 3)
        {
            //            writeLogToLogFile("B框架加热器发送第" + QString::number(m_bjiareqisanciCount) + "次");

            if(m_bjiareqisanciTimer->isActive())
            {
                //                writeLogToLogFile("B框架加热器多次发送停止");
                m_bjiareqisanciTimer->stop();
                m_bjiareqisanciCount = 0;
            }
        }
    });




    MyTimer *_anuanfengjitimer = new MyTimer(this);
    QList<QVariant> _list3;
    _list3.append("A框架暖风机故障");
    _anuanfengjitimer->setMyData(_list3);
    connect(_anuanfengjitimer, &MyTimer::myTimeOut, [&](QList<QVariant> list){
        int _wendu;
        if(list.size() == 2)
        {
            _wendu = list.at(1).toUInt();
            //获取当前温度
            quint16 _awendu = getValueByIO("A框架一区控制器", "A框架内部温度");
            qint8 _akuangjiawendu = (qint16)_awendu;
            qDebug() << "A框架暖风机" << _akuangjiawendu << ":" << _wendu;
            //            writeLogToLogFile("A框架暖风机" + QString::number(_wendu) + "-" + QString::number(_akuangjiawendu));
            if((_wendu - _akuangjiawendu) < 5)
            {
                //                writeLogToLogFile("m_anuanfengjisanciTimer start");
                m_anuanfengjisanciTimer->start();
                setMultiLambdaVariableStatus("A框架暖风机故障", true);
                setMultiLambdaVariableStatus("A框架暖风机锁", true);
                emit setIODeviceStatusSignal("A框架二区控制器", "A框架暖风机", false, "", true, "", true, "", true, "", true, "", true, 1);
            }
        }
    });
    int _nuanfengjiguzhangtime = 2400000;
    _anuanfengjitimer->setInterval(_nuanfengjiguzhangtime);
    _anuanfengjitimer->setSingleShot(true);
    m_abnuanfengjijiareqiTimerHash.insert("A框架暖风机故障", _anuanfengjitimer);







    MyTimer *_bnuanfengjitimer = new MyTimer(this);
    QList<QVariant> _list4;
    _list4.append("B框架暖风机故障");
    _bnuanfengjitimer->setMyData(_list4);
    connect(_bnuanfengjitimer, &MyTimer::myTimeOut, [&](QList<QVariant> list){
        int _wendu;
        if(list.size() == 2)
        {
            _wendu = list.at(1).toUInt();
            //获取当前温度
            quint16 _awendu = getValueByIO("B框架一区控制器", "B框架内部温度");
            qint8 _akuangjiawendu = (qint16)_awendu;
            qDebug() << "B框架暖风机" << _akuangjiawendu << ":" << _wendu;
            //            writeLogToLogFile("B框架暖风机" + QString::number(_wendu) + "-" + QString::number(_akuangjiawendu));

            if((_wendu - _akuangjiawendu) < 5)
            {
                //                writeLogToLogFile("m_bnuanfengjisanciTimer start");
                m_bnuanfengjisanciTimer->start();
                setMultiLambdaVariableStatus("B框架暖风机故障", true);
                setMultiLambdaVariableStatus("B框架暖风机锁", true);
                emit setIODeviceStatusSignal("B框架二区控制器", "B框架暖风机", false, "", true, "", true, "", true, "", true, "", true, 1);

            }
        }
    });
    _bnuanfengjitimer->setInterval(_nuanfengjiguzhangtime);
    _bnuanfengjitimer->setSingleShot(true);
    m_abnuanfengjijiareqiTimerHash.insert("B框架暖风机故障", _bnuanfengjitimer);



    MyTimer *_ajiareqitimer = new MyTimer(this);
    QList<QVariant> _list5;
    _list5.append("A框架加热器故障");
    _ajiareqitimer->setMyData(_list5);
    connect(_ajiareqitimer, &MyTimer::myTimeOut, [&](QList<QVariant> list){
        int _wendu;
        if(list.size() == 2)
        {
            _wendu = list.at(1).toUInt();
            //获取当前温度
            quint16 _awendu = getValueByIO("A框架一区控制器", "A框架水温");
            qint8 _akuangjiawendu = (qint16)_awendu;
            qDebug() << "A框架加热器:" << _akuangjiawendu << ":" << _wendu;
            //            writeLogToLogFile("A框架加热器" + QString::number(_wendu) + "-" + QString::number(_akuangjiawendu));

            if((_wendu - _akuangjiawendu) < 5)
            {
                //                writeLogToLogFile("m_ajiareqisanciTimer start");
                m_ajiareqisanciTimer->start();
                setMultiLambdaVariableStatus("A框架加热器故障", true);
                setMultiLambdaVariableStatus("A框架加热器锁", true);
                emit setIODeviceStatusSignal("A框架二区控制器", "A框架加热器", false, "", true, "", true, "", true, "", true, "", true, 1);
            }
        }
    });
    int _jiareqiguzhangtime = 2370000;
    _ajiareqitimer->setInterval(_jiareqiguzhangtime);
    _ajiareqitimer->setSingleShot(true);
    m_abnuanfengjijiareqiTimerHash.insert("A框架加热器故障", _ajiareqitimer);


    MyTimer *_bjiareqitimer = new MyTimer(this);
    QList<QVariant> _list6;
    _list6.append("B框架加热器故障");
    _bjiareqitimer->setMyData(_list6);
    connect(_bjiareqitimer, &MyTimer::myTimeOut, [&](QList<QVariant> list){
        int _wendu;
        if(list.size() == 2)
        {
            _wendu = list.at(1).toUInt();
            //获取当前温度

            quint16 _awendu = getValueByIO("B框架一区控制器", "B框架水温");
            qint8 _akuangjiawendu = (qint16)_awendu;
            qDebug() << "B框架加热器:" << _akuangjiawendu << ":" << _wendu;
            //            writeLogToLogFile("B框架加热器" + QString::number(_wendu) + "-" + QString::number(_akuangjiawendu));

            if((_wendu - _akuangjiawendu) < 5)
            {
                //                writeLogToLogFile("m_bjiareqisanciTimer start");
                m_bjiareqisanciTimer->start();
                setMultiLambdaVariableStatus("B框架加热器故障", true);
                setMultiLambdaVariableStatus("B框架加热器锁", true);
                emit setIODeviceStatusSignal("B框架二区控制器", "B框架加热器", false, "", true, "", true, "", true, "", true, "", true, 1);

            }
        }
    });
    _bjiareqitimer->setInterval(_jiareqiguzhangtime);
    _bjiareqitimer->setSingleShot(true);
    m_abnuanfengjijiareqiTimerHash.insert("B框架加热器故障", _bjiareqitimer);



    m_ioDevice = new IODevice;
    m_ioDevice->setDeviceInfo(_ioHash);
    //    m_ioDevice->setCloseOutPutStatus(m_readConf->getMasterSign() == "master" ? true : false);
    m_ioDevice->setCloseMasterOrClient(m_readConf->getMasterSign() == "master" ? true : false);
    m_ioDevice->setCloseOutPutStatus(true);
    m_ioDevice->setMultiLambdaEventHash(m_eventMultiLambdaHash);
    m_ioDevice->setWenDuFuDongZhi(m_readConf->getWenDuFuDongZhi());
    m_ioDevice->setLingDianLingYiWu(m_readConf->getLingDianLingYiWu());
    m_ioDevice->setGuDingChaZhi(m_readConf->getGuDingChaZhi());
    m_ioDevice->startALLDevice();
    connect(m_ioDevice, &IODevice::updateDeviceStatus, this, &Core::updateIODeviceData, Qt::QueuedConnection);
    connect(this, &Core::setIODeviceStatusSignal, m_ioDevice, &IODevice::setIODeviceStatus, Qt::BlockingQueuedConnection);
    connect(this, &Core::setIOOutputStatusSignal, m_ioDevice, &IODevice::updateIODeviceStatus, Qt::QueuedConnection);


    for(int i = 0;i<_ioHash.size();i++)
    {
        QString _devicename = _ioHash.keys().at(i);
        QHash<QString, bool> _hash;
        _hash.insert(_ioHash.value(_devicename).output1, false);
        _hash.insert(_ioHash.value(_devicename).output2, false);
        _hash.insert(_ioHash.value(_devicename).output3, false);
        _hash.insert(_ioHash.value(_devicename).output4, false);
        _hash.insert(_ioHash.value(_devicename).output5, false);
        _hash.insert(_ioHash.value(_devicename).output6, false);
        m_ioOpenCloseLanJieHash.insert(_devicename, _hash);

    }


    if(m_readConf->getMasterSign() == "master")
    {
        m_mvbServer = new MyMVBServer(true);
        m_masterOrClientStatusForMVB = true;
        setMasterClientStatus(false);
    }else
    {
        m_mvbServer = new MyMVBServer(false);
        m_masterOrClientStatusForMVB = false;
        setMasterClientStatus(false);
    }
    connect(this, &Core::sendDataToMVB, m_mvbServer, &MyMVBServer::sendData, Qt::QueuedConnection);
    m_mvbServer->setPort(4001);
    connect(m_mvbServer, &MyMVBServer::sendMVBDataToCore, this, &Core::processDataFromMVB, Qt::QueuedConnection);
    QThread *_mvbThread = new QThread;
    m_mvbServer->moveToThread(_mvbThread);
    connect(_mvbThread, &QThread::started, m_mvbServer, &MyMVBServer::startListen);
    _mvbThread->start();
    connect(m_mvbServer, &MyMVBServer::sendMVBStatus, this, [&](QString status){
        if(status == NETWORKERR)
        {
            MyDataBaseWord _word;
            _word.type = FAULTTABLE;
            _word.fault_word.date = QDateTime::currentDateTime().toTime_t();
            _word.fault_word.devicename = "mvb";
            _word.fault_word.casue = NETWORKERR;
            _word.fault_word.isclear = ISCLEAR;
            emit insertDataToDataBaseSignal(_word);
        }else if(status == NETWORKSUCC)
        {
            MyDataBaseWord _word;
            _word.type = FAULTTABLE;
            _word.fault_word.date = QDateTime::currentDateTime().toTime_t();
            _word.fault_word.devicename = "mvb";
            _word.fault_word.casue = NETWORKSUCC;
            _word.fault_word.isclear = ISCLEAR;
            emit insertDataToDataBaseSignal(_word);
        }
    }, Qt::QueuedConnection);

    //将定时器遍历出来存放到对应的内存中
    QList<QString> _eventKeys = m_eventMultiLambdaHash.keys();
    for(int i = 0;i < _eventKeys.size();i++)
    {
        QList<MultiLambdaDeviceInfo> _list = m_eventMultiLambdaHash.value(_eventKeys.at(i));
        for(int j =0;j<_list.size();j++)
        {
            MultiLambdaDeviceInfo _struct = _list.at(j);
            if(_struct.type == "timer")
            {
                //                                qDebug() << _struct.timer.reason
                //                                         << _struct.timer.doit
                //                                         << _struct.timer.result
                //                                         << _struct.timer.minute;
                QHash<QString, MyTimer*> _hash;
                MyTimer *_timer = new MyTimer(this);
                QList<QVariant> _list;
                //第一个数据存放结果
                _list.append(_struct.timer.result);
                _list.append(_struct.timer.reason);
                _timer->setResault(_struct.timer.result);
                _timer->setMyData(_list);
                connect(_timer, &MyTimer::myTimeOut, this, [&](QList<QVariant> list){
                    //1.设置表达式中timer的标志位
                    QString _resault = list.at(0).toString();
                    QString _reason = list.at(1).toString();
                    setMultiLambdaTimerStatus(_resault, true);
                    setMultiLambdaVariableStatus(_resault, true);

                    //2.发送消息通知数据库
                    //3.发送消息通知UI
                    qDebug() << "发送通知UI:" << _resault;
                    MyDataBaseWord _word;
                    _word.type = FAULTTABLE;
                    _word.fault_word.date = QDateTime::currentDateTime().toTime_t();
                    _word.fault_word.devicename = _resault;
                    _word.fault_word.casue = "故障";
                    _word.fault_word.isclear = ISNOTCLEAR;
                    emit insertDataToDataBaseSignal(_word);

                    QJsonObject _obj;
                    _obj.insert("func", "telluifirefaultevent");
                    _obj.insert("type", FAULTTABLE);
                    _obj.insert("devicename", _resault);
                    _obj.insert("cause", "故障");
                    _obj.insert("date",  QDateTime::fromTime_t(_word.fault_word.date).toString("yyyy-MM-dd hh:mm:ss"));
                    _obj.insert("isclear", ISNOTCLEAR);
                    m_timerDeviceFaultInfoToUIHash.insert(_resault, _obj);
                    emit sendMessage(_obj);

                });
                _timer->setInterval(_struct.timer.minute * 1000 * 60);
                _timer->setSingleShot(true);
                _hash.insert(_eventKeys.at(i), _timer);
                //                qDebug() << _struct.timer.reason << __LINE__;
                m_eventMultiLambdaTimerHash.insert(_struct.timer.reason, _hash);
                QPair<QString, bool> _pair;
                _pair.first = _struct.timer.doit;
                _pair.second = false;
                m_multiLambdaTimerStatusHash.insert(_struct.timer.result, _pair);
                m_timerToIdStatusLanJieHash.insert(_struct.timer.reason, "关闭");
            }
        }
    }



    m_multiLambdaVariableHash = m_readConf->getVariableMultiLambdaList();
    //将所有变量获取到并存放
    //    qDebug() << m_eventMultiLambdaTimerHash.size();
    //    qDebug() << m_execMultiLambdaTimerHash.size();
    m_mvbTimer = new QTimer(this);
    connect(m_mvbTimer, &QTimer::timeout, this, &Core::processDataToMVB);
    m_mvbTimer->setInterval(512);
    m_mvbTimer->start();

    m_iotTimer = new QTimer(this);
    connect(m_iotTimer, &QTimer::timeout, this, &Core::processDataToIOT);
    m_iotTimer->setInterval(5000);
    m_iotTimer->start();

    m_syncGateStatusToUITimer = new QTimer(this);
    connect(m_syncGateStatusToUITimer, &QTimer::timeout, this, &Core::processGateStatusToUI);
    m_syncGateStatusToUITimer->setInterval(3000);
    m_syncGateStatusToUITimer->start();

    m_syncFaultMessageTimer =  new QTimer(this);
    connect(m_syncFaultMessageTimer, &QTimer::timeout, this, &Core::sendFaultMessageToClient);
    m_syncFaultMessageTimer->setInterval(3000);
    m_syncFaultMessageTimer->start();

    m_syncQiMieStatusToClientTimer = new QTimer(this);
    connect(m_syncQiMieStatusToClientTimer, &QTimer::timeout, this, &Core::sendQiMieStatusToClient);
    m_syncQiMieStatusToClientTimer->setInterval(2000);
    m_syncQiMieStatusToClientTimer->start();


    m_updateGateStatusToUITimer = new QTimer(this);
    connect(m_updateGateStatusToUITimer, &QTimer::timeout, this, &Core::updateGateStatusToUI);
    m_updateGateStatusToUITimer->setInterval(4000);
    m_updateGateStatusToUITimer->start();


    //感温报警拦截
    QList<QString> _fireKeys = _fireHash.keys();
    for(int i =0;i<_fireKeys.size();i++)
    {
        //        m_fireDeviceFireInfoToUIHash.insert(_fireKeys.at(i), _obj);
        m_doubleResetButtonCount.insert(_fireKeys.at(i), 0);
        m_doubleResetFireDeviceFireStatusHash.insert(_fireKeys.at(i), 0);
    }
    m_doubleResetFireDeviceFireStatusHash.insert("一车司机室感烟探测器", 0);
    m_doubleResetFireDeviceFireStatusHash.insert("六车司机室感烟探测器", 0);

    //感烟报警拦截
    QList<QString> _smogKeys = _smogHash.keys();
    for(int i =0;i<_smogKeys.size();i++)
    {
        SmogDeviceStatus _status = m_smogDeviceStatusHash.value(_smogKeys.at(i));
        QList<QString> _subSmogKeys = _status.firestatus.keys();
        for(int j =0;j<_subSmogKeys.size();j++)
        {
            QJsonObject _obj;
            MyDataBaseWord _word = getFireDataBaseWord(_subSmogKeys.at(j));
            _obj = getFireUIWord(_word);
            _obj["cause"] = "无火警";
            //            m_smogDeviceFireInfoToUIHash.insert(_subSmogKeys.at(j), _obj);
        }
    }
    //io开关拦截
    QList<QString> _ioKeys = _ioHash.keys();
    for(int i =0;i<_ioKeys.size();i++)
    {
        IODeviceStatus _status = m_ioDeviceStatusHash.value(_ioKeys.at(i));
        QList<QString> _outputKeys = _status.outputs.keys();
        for(int j = 0;j<_outputKeys.size();j++)
        {
            QJsonObject _obj;
            MyDataBaseWord _word = getEventDataBaseWord(_outputKeys.at(j), "关到位");
            _obj = getEventUIWord(_word);
            m_ioDeviceOpenCloseToUIHash.insert(_outputKeys.at(j), _obj);
        }
    }


    m_ioDeviceStatusHash["A框架一区控制器"].inputs["A框架水温"] = 35;
    m_ioDeviceStatusHash["A框架一区控制器"].inputs["A框架内部温度"] = 35;
    m_ioDeviceStatusHash["A框架一区控制器"].inputs["A框架环境温度"] = 35;
    m_ioDeviceStatusHash["B框架一区控制器"].inputs["B框架水温"] = 35;
    m_ioDeviceStatusHash["B框架一区控制器"].inputs["B框架内部温度"] = 35;
    m_ioDeviceStatusHash["B框架一区控制器"].inputs["B框架环境温度"] = 35;

    //阀门开关拦截
    QList<QString> _gateKeys = _gateHash.keys();
    for(int i =0;i<_gateKeys.size();i++)
    {
        GateDeviceStatus _status = m_gateDeviceStatusHash.value(_gateKeys.at(i));
        QString _leftName, _rightName;
        _leftName = _status.leftname;
        _rightName = _status.rightname;
        QJsonObject _obj;
        MyDataBaseWord _word;
        if(_status.leftshieldstatus == "有效")
        {
            _word = getEventDataBaseWord(_leftName, "关到位");
            _obj = getEventUIWord(_word);
            m_gateDeviceOpenCloseToUIHash.insert(_leftName, _obj);
        }
        if(_status.rightshieldstatus == "有效")
        {
            _word = getEventDataBaseWord(_rightName, "关到位");
            _obj =getEventUIWord(_word);
            m_gateDeviceOpenCloseToUIHash.insert(_rightName, _obj);
        }
    }
    //所有设备的故障信息拦截
    QList<QString> _deviceKeys;
    _deviceKeys << _fireKeys << _ioKeys << _gateKeys << _smogKeys;
    for(int i =0;i<_smogKeys.size();i++)
    {
        MySmogSensorInfo _struct = _smogHash.value(_smogKeys.at(i));
        MyDataBaseWord _word = getFaultDataBaseWord(_struct.address1);
        QJsonObject _obj;
        _obj = getFaultUIWord(_word);
        _obj["cause"] = "无故障";
        _obj["isclear"] = ISNOTCLEAR;
        m_deviceFaultToUIHash.insert(_struct.address1, _obj);
        _obj["devicename"] = _struct.address2;
        m_deviceFaultToUIHash.insert(_struct.address2, _obj);
        _obj["devicename"] = _struct.address3;
        m_deviceFaultToUIHash.insert(_struct.address3, _obj);
        _obj["devicename"] = _struct.address4;
        m_deviceFaultToUIHash.insert(_struct.address4, _obj);
        _obj["devicename"] = _struct.address5;
        m_deviceFaultToUIHash.insert(_struct.address5, _obj);
        _obj["devicename"] = _struct.address6;
        m_deviceFaultToUIHash.insert(_struct.address6, _obj);
        _obj["devicename"] = _struct.address7;
        m_deviceFaultToUIHash.insert(_struct.address7, _obj);
        _obj["devicename"] = _struct.address8;
        m_deviceFaultToUIHash.insert(_struct.address8, _obj);
        _obj["devicename"] = _struct.address9;
        m_deviceFaultToUIHash.insert(_struct.address9, _obj);
        _obj["devicename"] = _struct.address10;
        m_deviceFaultToUIHash.insert(_struct.address10, _obj);
        _obj["devicename"] = _struct.address11;
        m_deviceFaultToUIHash.insert(_struct.address11, _obj);
        _obj["devicename"] = _struct.address12;
        m_deviceFaultToUIHash.insert(_struct.address12, _obj);
        _obj["devicename"] = _struct.address13;
        m_deviceFaultToUIHash.insert(_struct.address13, _obj);
        _obj["devicename"] = _struct.address14;
        m_deviceFaultToUIHash.insert(_struct.address14, _obj);
        _obj["devicename"] = _struct.address15;
        m_deviceFaultToUIHash.insert(_struct.address15, _obj);
        _obj["devicename"] = _struct.address16;
        m_deviceFaultToUIHash.insert(_struct.address16, _obj);
        _obj["devicename"] = _struct.address17;
        m_deviceFaultToUIHash.insert(_struct.address17, _obj);
    }
    //    qDebug() << _deviceKeys;
    for(int i =0;i < _deviceKeys.size();i++)
    {
        MyDataBaseWord _word = getFaultDataBaseWord(_deviceKeys.at(i));
        QJsonObject _obj;
        _obj = getFaultUIWord(_word);
        _obj["cause"] = NETWORKSUCC;
        _obj["isclear"] = ISNOTCLEAR;
        m_deviceFaultToUIHash.insert(_deviceKeys.at(i), _obj);
    }
    for(int i = 0;i<_gateKeys.size();i++)
    {
        MyGateDeviceInfo _struct = _gateHash.value(_gateKeys.at(i));
        MyDataBaseWord _word = getFaultDataBaseWord(_struct.leftalias);
        QJsonObject _obj;
        _obj = getFaultUIWord(_word);
        _obj["cause"] = "正常";
        _obj["isclear"] = ISNOTCLEAR;
        m_deviceFaultToUIHash.insert(_struct.leftalias, _obj);

        MyGateDeviceInfo _struct2 = _gateHash.value(_gateKeys.at(i));
        MyDataBaseWord _word2 = getFaultDataBaseWord(_struct2.rightalias);
        QJsonObject _obj2;
        _obj2 = getFaultUIWord(_word2);
        _obj2["cause"] = "正常";
        _obj2["isclear"] = ISNOTCLEAR;

        m_deviceFaultToUIHash.insert(_struct.rightalias, _obj2);
    }
#ifdef DOUBLERESETBUTTON
    m_doubleResetButtonTimer = new QTimer(this);
    connect(m_doubleResetButtonTimer, &QTimer::timeout, this, &Core::processDoubleResetButton);
    m_doubleResetButtonTimer->setInterval(1000 * 60 * 120);//两小时，当有按钮按下后，触发
    m_doubleResetButtonTimer->setSingleShot(true);//只执行一次

    m_doubleResetButtonTimerYanShi = new QTimer(this);
    connect(m_doubleResetButtonTimerYanShi, &QTimer::timeout, this, &Core::processDoubleResetButtonYanShi);
    m_doubleResetButtonTimerYanShi->setInterval(1500);
    m_doubleResetButtonTimerYanShi->setSingleShot(true);
#endif

    m_iotClient = new MyIOTClient;
    m_iotClient->setCarInfomation(m_readConf->getIOTCarInfo());
    m_iotClient->setAddress("IOT设备",  8090, "39.106.188.23");
    QThread *_iotThread = new QThread;
    m_iotClient->moveToThread(_iotThread);
    connect(_iotThread, &QThread::started, m_iotClient, &MyIOTClient::startConnectIOT);
    connect(this, &Core::sendDataToIOT, m_iotClient, &MyIOTClient::sendDataToIOT, Qt::QueuedConnection);
    _iotThread->start();


    connect(m_fireDevice, &FireDevice::resetOk, this, [&]{
        //通知UI清屏
        onResetClicked();
        QJsonObject _obj;
        _obj.insert("func", "telluiresetstatus");
        emit sendMessage(_obj);
        QJsonObject obj;
        obj["type"] = "resetstatus";
        m_eventUdp->addEvent(obj);
        qDebug() << "复位执行";
    });
    MyTimer *_ayeweidiyanshiTimer = new MyTimer(this);
    QList<QVariant> _ayeweidiyanshilist;
    _ayeweidiyanshilist.append("A框架液位低");
    _ayeweidiyanshiTimer->setMyData(_ayeweidiyanshilist);
    connect(_ayeweidiyanshiTimer, &MyTimer::myTimeOut, [&](QList<QVariant> list){
        QString _var;
        qDebug() <<  "A框架液位低的定时器执行内容" <<list.size();
        if(list.size() > 0)
        {
            _var = list.at(0).toString();
            qDebug() << "设置" << _var <<"变量";
            setMultiLambdaVariableStatus(_var, true);
        }
    });
    _ayeweidiyanshiTimer->setSingleShot(true);
    _ayeweidiyanshiTimer->setInterval(1000 * 60 * 5);
    m_abyeweidiyanshiTimerHash.insert("A框架液位低", _ayeweidiyanshiTimer);


    MyTimer *_byeweidiyanshiTimer = new MyTimer(this);
    QList<QVariant> _byeweidiyanshilist;
    _byeweidiyanshilist.append("B框架液位低");
    _byeweidiyanshiTimer->setMyData(_byeweidiyanshilist);
    connect(_byeweidiyanshiTimer, &MyTimer::myTimeOut, [&](QList<QVariant> list){
        QString _var;
        qDebug() <<  "B框架液位低的定时器执行内容" <<list.size();
        if(list.size() > 0)
        {
            _var = list.at(0).toString();
            qDebug() << "设置" << _var <<"变量";
            setMultiLambdaVariableStatus(_var, true);
        }
    });
    _byeweidiyanshiTimer->setSingleShot(true);
    _byeweidiyanshiTimer->setInterval(1000 * 60 * 5);
    m_abyeweidiyanshiTimerHash.insert("B框架液位低", _byeweidiyanshiTimer);
}

void Core::setFireDeviceStatus(const QString &devicename, const FireDeviceStatus  status)
{
    //    qDebug() <<__FUNCTION__;
    m_fireDeivceStatusMutex->lock();
    if(m_fireDeviceStatusHash.contains(devicename))
    {
        m_fireDeviceStatusHash[devicename] = status;
        m_fireDeviceStatushCopyHash[devicename] = status;
    }else
    {
        m_fireDeviceStatusHash.insert(devicename, status);
        m_fireDeviceStatushCopyHash.insert(devicename, status);
    }
    m_fireDeivceStatusMutex->unlock();
    //处理隔离复位的事宜
    processGeLiFuWeiLogic(devicename, status);
    //处理火警事宜
    processFireDeviceFire(devicename);
    //处理故障事宜
    processFireDeviceFault(devicename);
}

void Core::setGateDeviceStatus(const QString &devicename, const GateDeviceStatus  status)
{
    //如果是未知状态，
    //m_gateUnKnowStatusLanJieHash + 1
    //如果是其它状态 m_gateUnKnowStatusLanJieHash = 0
    //如果m_gateUnKnowStatusLanJieHash > 5
    //确实是故障
    int _leftcount, _rightcount;
    QString _leftname = status.leftname;
    QString _rightname = status.rightname;
    QString _leftstatus = status.leftgatestatus;
    QString _rightstatus = status.rightgatestatus;


    //获取上一次的状态
    GateDeviceStatus _frontGateStatus = getGateDeviceStatus(devicename);
    if(m_gateUnKnowStatusLanJieHash.contains(_leftname))
    {
        _leftcount = m_gateUnKnowStatusLanJieHash[_leftname];
    }
    if(m_gateUnKnowStatusLanJieHash.contains(_rightname))
    {
        _rightcount = m_gateUnKnowStatusLanJieHash[_rightname];
    }
    if(_leftstatus == "未知")
    {
        _leftcount += 1;
    }else
    {
        _leftcount = 0;
    }
    if(_rightstatus == "未知")
    {
        _rightcount += 1;
    }else
    {
        _rightcount = 0;
    }
    int _counter = 12;
    m_gateUnKnowStatusLanJieHash[_leftname] = _leftcount;
    m_gateUnKnowStatusLanJieHash[_rightname] = _rightcount;
    GateDeviceStatus _tempStatus = status;
    _tempStatus.leftgatestatus = "";
    _tempStatus.rightgatestatus = "";
    //    if(_leftname == "二车一区阀门")
    //    {
    //        qDebug() << "leftname:" << _leftname <<"leftcount:" << _leftcount
    //                 << "rightname:" << _rightname <<",rightcount:" << _rightcount;
    //    }

    if(_leftcount == 0)
    {
        //将leftstatus = status.leftstatus
        //左面对，赋值左侧
        _tempStatus.leftgatestatus = status.leftgatestatus;
    }else if(_leftcount > _counter)
    {
        _tempStatus.leftgatestatus = status.leftgatestatus;
    }else
    {
        _tempStatus.leftgatestatus = _frontGateStatus.leftgatestatus;
    }
    if(_rightcount == 0)
    {
        _tempStatus.rightgatestatus = status.rightgatestatus;
    }else if(_rightcount > _counter)
    {
        _tempStatus.rightgatestatus = status.rightgatestatus;
    }else
    {
        _tempStatus.rightgatestatus = _frontGateStatus.rightgatestatus;
    }
    if(status.networkstatus == NETWORKERR)
    {
        _tempStatus.leftgatestatus = "未知";
        _tempStatus.rightgatestatus = "未知";
    }
    m_gateDeviceStatusMutex->lock();
    if(m_gateDeviceStatusHash.contains(devicename))
    {
        //        qDebug() << status.leftname << ":" << status.leftgatestatus
        //                 << status.rightname << ":" << status.rightgatestatus;
        m_gateDeviceStatusHash[devicename] = _tempStatus;
    }else
    {
        m_gateDeviceStatusHash.insert(devicename, _tempStatus);
    }
    m_gateDeviceStatusMutex->unlock();
    if(m_gateUnKnowStatusSign)
    {
        processGateDeviceFault(devicename);
        processSubGateDeviceFault(devicename);
        processSubGateDeviceEvent(devicename);
    }
}

void Core::setSmogDeviceStatus(const QString &devicename, const SmogDeviceStatus status)
{
    //    qDebug() <<__FUNCTION__;
    m_smogDeviceStatusMutex->lock();
    if(m_smogDeviceStatusHash.contains(devicename))
    {
        m_smogDeviceStatusHash[devicename] = status;
    }else
    {
        m_smogDeviceStatusHash.insert(devicename, status);
    }
    m_smogDeviceStatusMutex->unlock();
    processGeLiFuWeiLogic(devicename, status);
    processSmogDeviceFire(devicename);
    processSmogDeviceFault(devicename);
    processSubSmogDeviceFault(devicename);
}

void Core::setIODeviceStatus(const QString &devicename, const IODeviceStatus status)
{
    //    qDebug() <<__FUNCTION__;
    m_ioDeviceStatusMutex->lock();
    if(m_ioDeviceStatusHash.contains(devicename))
    {
        m_ioDeviceStatusHash[devicename] = status;
    }else
    {
        m_ioDeviceStatusHash.insert(devicename, status);
    }
    if(status.ajiareqiguzhang)
    {
        setMultiLambdaVariableStatus("A框架加热器故障", true);
        setMultiLambdaVariableStatus("A框架加热器锁", true);
        //        writeLogToLogFile("A框架加热器故障");
    }
    if(status.bjiareqiguzhang)
    {
        setMultiLambdaVariableStatus("B框架加热器故障", true);
        setMultiLambdaVariableStatus("B框架加热器锁", true);
        //        writeLogToLogFile("B框架加热器故障");
    }
    if(status.anuanfengjiguzhang)
    {
        setMultiLambdaVariableStatus("A框架暖风机故障", true);
        setMultiLambdaVariableStatus("A框架暖风机锁", true);
        //        writeLogToLogFile("A框架暖风机故障");
    }
    if(status.bnuanfengjiguzhang)
    {
        setMultiLambdaVariableStatus("B框架暖风机故障", true);
        setMultiLambdaVariableStatus("B框架暖风机锁", true);
        //        writeLogToLogFile("B框架暖风机故障");

    }

    m_ioDeviceStatusMutex->unlock();
    //处理IO设备的故障
    processIODeviceFault(devicename);
    //处理IO子设备的打开关闭事件
    processSubIODeviceEvent(devicename);
    //处理A、B框架的水温低故障和取消
    processIOInputFault();
}

FireDeviceStatus Core::getFireDeviceStatus(const QString &devicename)
{
    m_fireDeivceStatusMutex->lock();
    FireDeviceStatus _status;
    if(m_fireDeviceStatusHash.contains(devicename))
    {
        _status = m_fireDeviceStatusHash[devicename];
    }
    m_fireDeivceStatusMutex->unlock();
    return _status;
}

GateDeviceStatus Core::getGateDeviceStatus(const QString &devicename)
{
    m_gateDeviceStatusMutex->lock();
    GateDeviceStatus _status;
    if(m_gateDeviceStatusHash.contains(devicename))
    {
        _status = m_gateDeviceStatusHash[devicename];
    }
    m_gateDeviceStatusMutex->unlock();
    return _status;
}

SmogDeviceStatus Core::getSmogDeviceStatus(const QString &devicename)
{
    m_smogDeviceStatusMutex->lock();
    SmogDeviceStatus _status;
    if(m_smogDeviceStatusHash.contains(devicename))
    {
        _status = m_smogDeviceStatusHash[devicename];
    }
    m_smogDeviceStatusMutex->unlock();
    return _status;
}

IODeviceStatus Core::getIODeviceStatus(const QString &devicename)
{

    m_ioDeviceStatusMutex->lock();
    IODeviceStatus _status;
    if(m_ioDeviceStatusHash.contains(devicename))
    {
        _status = m_ioDeviceStatusHash[devicename];
    }
    m_ioDeviceStatusMutex->unlock();
    return _status;
}

bool Core::getFireInfoByFire(const QString & devicename)
{
    bool _ret = false;
    FireDeviceStatus _status = getFireDeviceStatus(devicename);
    if(_status.firestatus == "有火警")
    {
        _ret = true;
    }else
    {
        _ret = false;
    }
    return _ret;
}

bool Core::getFaultInfoByFire(const QString & devicename)
{
    bool _ret = false;
    FireDeviceStatus _status = getFireDeviceStatus(devicename);
    if(_status.networkstatus == "网络连接故障")
    {
        _ret = true;
    }else{
        _ret = false;
    }
    return  _ret;
}

bool Core::getShieldInfoByFire(const QString & devicename)
{
    bool _ret = false;
    FireDeviceStatus _status = getFireDeviceStatus(devicename);
    if(m_fireSmogDeviceStatusHash.value(devicename) == "已屏蔽")
    {
        _ret = true;
    }else
    {
        _ret = false;
    }
    return _ret;
}

quint16 Core::getHuanJingWenDuByFire(const QString &devicename)
{
    quint16 _v;
    FireDeviceStatus _status = getFireDeviceStatus(devicename);
    _v = _status.huanjingwendu * 10;
    return _v;
}

quint16 Core::getMuBiaoWenDuByFire(const QString & devicename)
{
    quint16 _v;
    FireDeviceStatus _status = getFireDeviceStatus(devicename);
    _v = _status.maxwendu * 10;
    return _v;
}

bool Core::getFireInfoBySmog(const QString & devicename, const QString & subdevicename)
{
    bool _ret = false;
    SmogDeviceStatus _status = getSmogDeviceStatus(devicename);
    if(_status.firestatus.value(subdevicename) == "有火警")
    {
        _ret = true;
    }else
    {
        _ret = false;
    }
    return _ret;
}

bool Core::getFaultInfoBySmog(const QString & devicename, const QString & subdevicename)
{
    bool _ret = false;
    SmogDeviceStatus _status = getSmogDeviceStatus(devicename);
    if(_status.faultstatus.value(subdevicename) == "有故障")
    {
        _ret = true;
    }else
    {
        _ret = false;
    }
    return _ret;
}

bool Core::getShieldInfoBySmog(const QString & devicename, const QString & subdevicename)
{
    bool _ret = false;
    SmogDeviceStatus _status = getSmogDeviceStatus(devicename);
    if(m_fireSmogDeviceStatusHash.value(subdevicename) == "已屏蔽")
    {
        _ret = true;
    }else
    {
        _ret = false;
    }
    return _ret;
}

bool Core::getFireInfoBySmogGui(const QString & var)
{
    bool _ret = false;
    _ret = getMultiLambdaVariableStatus(var);
    return _ret;
}

bool Core::getOpenedByGate(const QString & devicename, const QString & subdevicename)
{
    bool _ret = false;
    GateDeviceStatus _status = getGateDeviceStatus(devicename);
    QString _leftorright;
    if(_status.leftname == subdevicename)
    {
        _leftorright = "left";
    }
    if(_status.rightname == subdevicename)
    {
        _leftorright = "right";
    }
    if(_leftorright == "left")
    {
        if(_status.leftgatestatus == "开到位")
        {
            _ret = true;
        }else
        {
            _ret = false;
        }
    }
    if(_leftorright == "right")
    {
        if(_status.rightgatestatus == "开到位")
        {
            _ret = true;
        }else
        {
            _ret = false;
        }
    }
    return _ret;
}

bool Core::getClosedByGate(const QString & devicename, const QString & subdevicename)
{
    bool _ret = false;
    GateDeviceStatus _status = getGateDeviceStatus(devicename);
    QString _leftorright;
    if(_status.leftname == subdevicename)
    {
        _leftorright = "left";
    }
    if(_status.rightname == subdevicename)
    {
        _leftorright = "right";
    }
    if(_leftorright == "left")
    {
        if(_status.leftgatestatus == "关到位")
        {
            _ret = true;
        }else
        {
            _ret = false;
        }
    }
    if(_leftorright == "right")
    {
        if(_status.rightgatestatus == "关到位")
        {
            _ret = true;
        }else
        {
            _ret = false;
        }
    }
    return _ret;
}

bool Core::getFaultByGate(const QString & devicename, const QString & subdevicename)
{
    bool _ret = false;
    GateDeviceStatus _status = getGateDeviceStatus(devicename);
    QString _leftorright;
    if(_status.leftname == subdevicename)
    {
        _leftorright = "left";
    }
    if(_status.rightname == subdevicename)
    {
        _leftorright = "right";
    }
    if(_leftorright == "left")
    {
        if(_status.leftgatestatus == "未知")
        {
            _ret = true;
        }else
        {
            _ret = false;
        }
    }
    if(_leftorright == "right")
    {
        if(_status.rightgatestatus == "未知")
        {
            _ret = true;
        }else
        {
            _ret = false;
        }
    }
    return _ret;
}

bool Core::getYeweidiByIO(const QString & var)
{
    bool _ret = false;
    //要寻找对应变量来判断是否液位低
    _ret = getMultiLambdaVariableStatus(var);
    return _ret;
}

bool Core::getYalidiByIO(const QString & var)
{
    bool _ret = false;
    _ret = getMultiLambdaVariableStatus(var);
    return _ret;
}

bool Core::getAnniuByIO(const QString & devicename, const QString & subdevicename)
{
    bool _ret  = false;
    IODeviceStatus _status = getIODeviceStatus(devicename);
    if(_status.inputs.value(subdevicename) > 2000)
    {
        _ret = true;
    }else{
        _ret = false;
    }
    return _ret;
}

bool Core::getQiMieByIO(const QString & var)
{
    bool _ret = false;
    _ret = getMultiLambdaVariableStatus(var);
    return _ret;
}

quint16 Core::getValueByIO(const QString & devicename, const QString & subdevicename)
{
    quint16 _value = 0;
    IODeviceStatus _status = getIODeviceStatus(devicename);
    _value = _status.inputs.value(subdevicename);
    return _value;
}

bool Core::getClosedByIO(const QString & devicename, const QString & subdevicename)
{
    bool _ret = false;
    IODeviceStatus _status = getIODeviceStatus(devicename);
    if(_status.outputs.value(subdevicename) == 0x00)
    {
        _ret = true;
    }else{
        _ret = false;
    }
    return _ret;
}

bool Core::getOpenedByIO(const QString & devicename, const QString & subdevicename)
{
    bool _ret = false;
    IODeviceStatus _status = getIODeviceStatus(devicename);
    if(_status.outputs.value(subdevicename) == 0x01)
    {
        _ret = true;
    }else
    {
        _ret = false;
    }
    return _ret;
}


bool Core::canExec(const QList<MultiLambdaDeviceInfo> & list)
{
    QList<QString> _operaList;
    //    QString _test;
    for(int i =  0;i<list.size();i++)
    {
        MultiLambdaDeviceInfo _struct = list.at(i);
        //                qDebug() << _struct.type;
        if(_struct.type == "fire")
        {

            _operaList.append(_struct.fire.oper.frontopera);
            _operaList.append( _struct.fire.oper.status == true ? "true" : "false");
            _operaList.append(_struct.fire.oper.nextopera);
        }else if(_struct.type == "gate")
        {
            //            if(_struct.gate.devicename == "一车分区阀门" && _struct.gate.subdevicename == "一车一区阀门")
            //            {
            //                qDebug() << _struct.gate.devicename << ":" << _struct.gate.subdevicename << _struct.gate.oper.status;
            //                _test = _struct.gate.devicename + _struct.gate.subdevicename;
            //            }
            _operaList.append(_struct.gate.oper.frontopera);
            _operaList.append(_struct.gate.oper.status == true ? "true" : "false");
            _operaList.append(_struct.gate.oper.nextopera);
        }else if(_struct.type == "smog")
        {
            _operaList.append(_struct.smog.oper.frontopera);
            _operaList.append(_struct.smog.oper.status == true ? "true" : "false");
            _operaList.append(_struct.smog.oper.nextopera);
        }else if(_struct.type == "io")
        {
            //            if(_struct.io.devicename == "一车一区控制器" && _struct.io.subdevicename == "测试按钮")
            //            {
            //                qDebug() << _struct.io.devicename << ":" << _struct.io.subdevicename << _struct.io.oper.status;
            //                _test += _struct.io.devicename + _struct.io.subdevicename;
            //            }
            _operaList.append(_struct.io.oper.frontopera);
            _operaList.append(_struct.io.oper.status == true ? "true" : "false");
            _operaList.append(_struct.io.oper.nextopera);
        }else if(_struct.type == "timer")
        {
            _operaList.append(_struct.timer.oper.frontopera);
            _operaList.append(_struct.timer.oper.status == true ? "true" : "false");
            _operaList.append(_struct.timer.oper.nextopera);
        }else if(_struct.type == "var")
        {
            //            if(_struct.var.var == "一到六车感温探测器集体复位")
            //            {
            //                qDebug() << _struct.var.var << ":" << _struct.var.oper.status;
            //                _test += _struct.var.var;
            //            }
            _operaList.append(_struct.var.oper.frontopera);
            _operaList.append(_struct.var.oper.status == true ? "true" : "false");
            _operaList.append(_struct.var.oper.nextopera);
        }
    }
    _operaList.removeAll("");
    if(m_debugStr.toInt() >= 0 && m_debugStr.toInt() <= 500)
    {
        QString _str("lambda-multi%1");
        _str = _str
                .arg(m_debugStr);
        QString _fireName("%1车%2号感温探测器");
        int _gewei = m_debugStr.toInt() % 10;
        int _shiwei = m_debugStr.toInt() / 10;

        _fireName = _fireName
                .arg(_shiwei)
                .arg(_gewei);
        _fireName.replace("1", "一");
        _fireName.replace("2", "二");
        _fireName.replace("3", "三");
        _fireName.replace("4", "四");
        _fireName.replace("5", "五");
        _fireName.replace("6", "六");
        if(list.at(0).lambdaname == _str)
        {
            qDebug() << list.at(0).lambdaname << ":" << _operaList;
        }
        FireDeviceStatus _ssss =getFireDeviceStatus(_fireName);
        if(_ssss.shieldstatus != "")
        {
            qDebug() << _ssss.shieldstatus;
        }
    }else if(m_debugStr.toInt() == 501){
        m_debugStr = "";
    }

    //    qDebug() << _test;
    //    if(_test == "一车一区控制器测试按钮一车分区阀门一车一区阀门")
    //    {
    //        qDebug() << _operaList;

    //    }
    if(_operaList.size() == 2)
    {
        //        if(list.at(0).lambdaname == "lambda-multi382")
        //        {
        //            qDebug() << _operaList.at(0);
        //        }
        return _operaList.at(0) == "false" ? false:true;
    }
    //    bool _retaaa = parseStringByRegEx(_operaList);
    //    if(list.at(0).lambdaname == "lambda-multi19")
    //    {
    //        qDebug() << _retaaa << ":" << list.at(0).lambdaname;
    //    }
    return parseStringByRegEx(list.at(0).lambdaname, _operaList);
}

bool Core::parseStringByRegEx(const QString lambdaname, QList<QString>  _list)
{
    //    QList<QString> _list;
    //    _list << "false" << "&" << "true" << "&" << "false" << "|" << "false" << "&" << "!" << "false" << "&" << "!" << "true" << "|" << "true" << "|" << "false" << "&" << "true"
    //              << "&" << "false" << "&" << "true" << "&" << "false" << "|" << "true" << "&" << "!" << "false" << "&" << "!" << "true" << "|" << "true" << "|" << "false" << "&" << "true";
    //    if(_list.size() < 4)
    //    {
    //        qDebug() << "源列表:" << _list;
    //    }
    //先计算字符串有多少个非符号
    int _feiCount = 0;
    _feiCount = computerOperator(_list, "!");
    int _feiIndex = 0;
    while(_feiCount > 0){
        //如果当前这个是非符号，
        if(_list.at(_feiIndex) == "!"){
            //就将后面的值反转一下
            _list.replace(_feiIndex+1, _list.at(_feiIndex + 1) == "true" ? "false" : "true");
            //移除这个非符号
            _list.removeAt(_feiIndex);
            _feiCount -= 1;
        }else{
            _feiIndex += 1;
        }
    }
    //    if(_list.size() < 4)
    //    {
    //        qDebug() << "经过非符号计算:" << _list;
    //    }
    //在计算字符串有多少个与
    int _yuCount = 0;
    int _yuIndex = 0;
    _yuCount = computerOperator(_list, "&");
    while(_yuCount > 0){
        if(_list.at(_yuIndex) == "&"){

            bool _value1, _value2;
            if(_list.at(_yuIndex - 1) == "true"){
                _value1 = true;
            }else{
                _value1 = false;
            }
            if(_list.at(_yuIndex + 1) == "true"){
                _value2 = true;
            }else{
                _value2 = false;
            }
            _list.replace(_yuIndex, (_value1 && _value2) == true ? "true"  : "false");
            _list.removeAt(_yuIndex + 1);
            _list.removeAt(_yuIndex - 1);
            _yuCount -= 1;
        }else{
            _yuIndex += 1;
        }
    }
    //    if(_list.size() < 4)
    //    {
    //        qDebug() << "经过与符号计算:" << _list;
    //    }

    int _huoCount = 0;
    int _huoIndex = 0;
    _huoCount = computerOperator(_list, "|");
    //    qDebug() << _huoCount << __FUNCTION__ << __LINE__;
    while(_huoCount > 0){
        if(_list.at(_huoIndex) == "|"){
            bool _value1, _value2;
            if(_list.at(_huoIndex - 1) == "true"){
                _value1 = true;
            }else{
                _value1 = false;
            }
            if(_list.at(_huoIndex + 1) == "true"){
                _value2 = true;
            }else{
                _value2 = false;
            }
            _list.replace(_huoIndex, (_value1 || _value2) == true ? "true" : "false");
            _list.removeAt(_huoIndex + 1);
            _list.removeAt(_huoIndex - 1);
            _huoCount -= 1;
        }else{
            _huoIndex += 1;
        }
    }
    //    if(_list.size() < 4)
    //    {
    //        qDebug() << "最终版本:" << _list;
    //    }

    //    if(lambdaname == "lambda-multi377")
    //    {
    //        qDebug() << _list;
    //    }
    if(_list.size() == 1){
        return _list.at(0) == "true" ? true : false;
    }

    qDebug() << "解析程序出现问题";
    exit(0);
}

int Core::computerOperator(const QList<QString> &list, const QString &oper)
{
    //传入字符串，判断对应字符有几个
    int _count = 0;
    for(int i=0;i< list.size();i++){
        if(list.at(i) == oper){
            _count += 1;
        }
    }
    return _count;
}

void Core::execDevice(const QList<MultiLambdaDeviceInfo> &devices)
{
    //执行阀门动作
    QHash<QString, QPair<QString, QString> > _gateHash = getOpenCloseStatusFromGate(devices);
    QList<QString> _gateKeys = _gateHash.keys();
    //            qDebug() << "---------------------";
    //            qDebug() << _gateHash.keys()
    //                     << _gateHash.values();
    //            qDebug() << "===============";
    if(m_gateUnKnowStatusSign)
    {

        for(int i =0;i<_gateKeys.size();i++)
        {

            GateDeviceStatus _status = getGateDeviceStatus(_gateKeys.at(i));
            QPair<QString, QString> _pair = _gateHash.value(_gateKeys.at(i));
            QList<QString> _leftStatus;
            QList<QString> _rightStatus;
            //1.拿到要执行的阀门Hash
            QString _leftGateName, _rightGateName;
            bool _leftGateStatus, _rightGateStatus;
            //2.判断第一个是否为空，如果不为空，判断第一个是左侧阀门还是右侧阀门
            QString _sign;
            //            qDebug() << _pair.first << ":" << _pair.second;
            if(_pair.first != "" && _pair.second == "")
            {
                _leftStatus = _pair.first.split(":");//_leftStatus = (一车一区阀门,打开)
                //判断该阀门是左侧还是右侧
                if(_leftStatus.at(0) == _status.leftname)
                {
                    //是左侧阀门
                    _leftGateName = _leftStatus.at(0);
                    _leftGateStatus = _leftStatus.at(1) == "打开" ? true : false;
                    _rightGateName = "";
                    _rightGateStatus = m_gateUnKnowStatusHash.value(_gateKeys.at(i)).value(_status.rightname);
                    //                                        qDebug() << _leftGateName << ":" << _leftGateStatus << "[]" << _rightGateName << ":" << _rightGateStatus;
                }else{
                    //是右侧阀门
                    _rightGateName = _leftStatus.at(0);
                    _rightGateStatus = _leftStatus.at(1) == "打开" ? true : false;
                    _leftGateName = "";
                    _leftGateStatus = m_gateUnKnowStatusHash.value(_gateKeys.at(i)).value(_status.leftname);
                }
                _sign = "one";
            }else if(_pair.first != "" && _pair.second != "")
            {
                _leftStatus = _pair.first.split(":");
                _rightStatus = _pair.second.split(":");
                //判断该阀门是左侧还是右侧
                if(_leftStatus.at(0) == _status.leftname)
                {
                    _leftGateName = _leftStatus.at(0);
                    _leftGateStatus = _leftStatus.at(1) == "打开" ? true : false;
                    _rightGateName = _rightStatus.at(0);
                    _rightGateStatus = _rightStatus.at(1) == "打开" ? true : false;
                }else
                {
                    _rightGateName = _leftStatus.at(0);
                    _rightGateStatus = _leftStatus.at(1) == "打开" ? true :false;
                    _leftGateName  = _rightStatus.at(0);
                    _leftGateStatus = _rightStatus.at(1) == "打开" ? true : false;
                }
                _sign = "two";
            }
            //4.对阀门的上一次状态进行穿透
            //one代表就开一个阀门
            if(_sign == "one")
            {
                //如果是一个阀门
                if(_leftGateName != "")
                {
                    //将_leftGateStatus和_rightGateStatus的状态对比，是否与上一次的一样
                    bool _tempFrontLeftStatus = m_gateUnKnowStatusHash.value(_gateKeys.at(i)).value(_status.leftname);
                    //取出右侧的阀门，准备填充
                    bool _tempFrontRightStatus = m_gateUnKnowStatusHash.value(_gateKeys.at(i)).value(_status.rightname);
                    bool _ret = false;
                    if(_tempFrontLeftStatus == _leftGateStatus)
                    {
                        _ret = true;
                    }
                    //如果两个中有一个不一样，就不执行这次了
                    if(!_ret)
                    {
                        //将这一次的状态更新到上一次里
                        m_gateUnKnowStatusHash[_gateKeys.at(i)][_status.leftname] = _leftGateStatus;
                        m_gateUnKnowStatusHash[_gateKeys.at(i)][_status.rightname] = _tempFrontRightStatus;
                        //发送信号
                        //                        qDebug() << _gateKeys.at(i) << "->" << _status.leftname << ":" << _leftGateStatus << "[]" << _status.rightname << ":" << _tempFrontRightStatus << __LINE__;
                        emit setGateOpenCloseStatusSignal(_gateKeys.at(i),_status.leftname,_leftGateStatus, _status.rightname, _tempFrontRightStatus);
                    }
                }else if(_rightGateName != "")
                {
                    //取出左侧的阀门，准备填充
                    bool _tempFrontLeftStatus = m_gateUnKnowStatusHash.value(_gateKeys.at(i)).value(_status.leftname);
                    bool _tempFrontRightStatus = m_gateUnKnowStatusHash.value(_gateKeys.at(i)).value(_status.rightname);
                    bool _ret = false;
                    if(_tempFrontRightStatus == _rightGateStatus)
                    {
                        _ret = true;
                    }
                    if(!_ret)
                    {
                        //将这一次的状态更新到上一次里
                        m_gateUnKnowStatusHash[_gateKeys.at(i)][_status.leftname] = _tempFrontLeftStatus;
                        m_gateUnKnowStatusHash[_gateKeys.at(i)][_status.rightname] = _rightGateStatus;
                        //发送信号
                        //                        qDebug() << _gateKeys.at(i) << "->" << _status.leftname << ":" << _tempFrontLeftStatus << "[]" << _status.rightname << ":" << _rightGateStatus << __LINE__;
                        emit setGateOpenCloseStatusSignal(_gateKeys.at(i), _status.leftname, _tempFrontLeftStatus, _rightGateName, _rightGateStatus);
                    }
                }
                //取出上一次两个阀门的状态，对比是否一致
            }else if(_sign == "two")
            {
                //two代表是两个阀门
                //取出上一次的两个阀门的状态，与这一次的两个状态是否一致，如果一致就不做
                bool _tempFrontLeftStatus = m_gateUnKnowStatusHash.value(_gateKeys.at(i)).value(_status.leftname);
                //取出右侧的阀门，准备填充
                bool _tempFrontRightStatus = m_gateUnKnowStatusHash.value(_gateKeys.at(i)).value(_status.rightname);
                bool _ret = false;
                bool _ret2 = false;
                if(_tempFrontLeftStatus == _leftGateStatus)
                {
                    _ret = true;
                }
                if(_tempFrontRightStatus == _rightGateStatus)
                {
                    _ret2 = true;
                }
                bool _ret3 = _ret && _ret2;
                if(!_ret3)//如果两个都一样就不做操作
                {
                    //将这一次的状态更新到上一次里
                    m_gateUnKnowStatusHash[_gateKeys.at(i)][_status.leftname] = _leftGateStatus;
                    m_gateUnKnowStatusHash[_gateKeys.at(i)][_status.rightname] = _rightGateStatus;
                    //                    qDebug() << _gateKeys.at(i) << "->" << _status.leftname << ":" << _leftGateStatus << "[]" << _status.rightname << ":" << _rightGateStatus << __LINE__;
                    emit setGateOpenCloseStatusSignal(_gateKeys.at(i), _status.leftname, _leftGateStatus, _status.rightname, _rightGateStatus);
                }
            }
        }
    }


    //执行感温探测器动作
    QHash<QString, QString> _fireHash = getDongZuoFromFire(devices);
    QList<QString> _fireKeys = _fireHash.keys();
    //    qDebug() << "------------------------";
    //    qDebug() << _fireKeys.size();
    //    qDebug() << "========================";
    for(int i =0;i<_fireKeys.size();i++)
    {
        QString _doIt = _fireHash[_fireKeys.at(i)];
        if(m_fireToIdStatusLanJieHash[_fireKeys.at(i)] != _doIt)
        {
            //            qDebug() << m_fireToIdStatusLanJieHash[_fireKeys.at(i)] << ":" << _doIt;
            m_fireToIdStatusLanJieHash[_fireKeys.at(i)] = _doIt;
            //在这里进行拦截
            if(_doIt == "复位")
            {
                if(m_fireResetSignHash.value(_fireKeys.at(i)))
                {
                    m_fireResetSignHash[_fireKeys.at(i)] = false;
                    //                    qDebug() << "执行感温复位操作";
                    emit resetFireDeviceSignal(_fireKeys.at(i));

                    QJsonObject _obj;
                    _obj["type"] = "resetstatus";
                    m_eventUdp->addEvent(_obj);

                }


#ifdef DOUBLERESETBUTTON
                m_doubleResetButtonCount[_fireKeys.at(i)] += 1;
                if(!m_doubleResetButtonTimer->isActive())
                {
                    qDebug() << "启动两小时定时器";
                    m_doubleResetButtonTimer->start();
                }
                if(m_doubleResetButtonCount[_fireKeys.at(i)] > 1)
                {
                    if(m_doubleResetSign)
                    {
                        //获取所有感温的状态，是否有
                        //启动延时执行定时器，去处理数据
                        if(!m_doubleResetButtonTimerYanShi->isActive())
                        {
                            m_doubleResetButtonTimerYanShi->start();
                        }
                        m_doubleResetSign = false;
                    }
                }
#endif
                //在这里将双击计数器取出来+1
            }else if(_doIt == "正常")
            {
                if(!m_fireResetSignHash.value(_fireKeys.at(i)))
                {
                    for(int i =0;i<m_yicheVarLanJieHash.size();i++)
                    {
                        m_yicheVarLanJieHash[m_yicheVarLanJieHash.keys().at(i)] = false;
                        m_liucheVarLanJieHash[m_liucheVarLanJieHash.keys().at(i)] = false;
                    }
                    m_fireResetSignHash[_fireKeys.at(i)] = true;
                    //                    qDebug() << "执行感温正常操作";
                    emit normalFireDeviceSignal(_fireKeys.at(i));
                }
            }
        }
    }

    //执行感烟探测器动作
    QHash<QString, QString> _smogHash = getDongZuoFromSmog(devices);
    QList<QString> _smogKeys = _smogHash.keys();
    for(int i =0;i<_smogKeys.size();i++)
    {
        //有正常和复位两种操作
        QString _doIt = _smogHash[_smogKeys.at(i)];
        if(m_smogToIdStatusLanJieHash[_smogKeys.at(i)] != _doIt)
        {
            m_smogToIdStatusLanJieHash[_smogKeys.at(i)] = _doIt;
            if(_doIt == "复位")
            {
                //                qDebug() << "执行感烟复位操作";
                emit resetSmogDeviceSignal(_smogKeys.at(i));
            }else if(_doIt == "正常")
            {
                //                qDebug() << "执行感烟正常操作";
                emit normalSmogDeviceSignal(_smogKeys.at(i));
            }
        }
    }



    //执行定时器的动作
    QHash<QString, QString> _timerHash = getDongZuoFromTimer(devices);
    QList<QString> _timerKeys = _timerHash.keys();
    for(int i =0;i<_timerKeys.size();i++)
    {
        QString _doIt = _timerHash[_timerKeys.at(i)];
        //这里要拦截
        if(m_timerToIdStatusLanJieHash.value(_timerKeys.at(i)) != _doIt)
        {
            m_timerToIdStatusLanJieHash[_timerKeys.at(i)] = _doIt;
            qDebug() << _timerKeys.at(i) << _doIt;
            emit startMultiLambdaTimerSignal(_timerKeys.at(i), _doIt);
        }
    }

    //执行变量的动作
    QHash<QString, QString> _varHash = getDongZuoFromVar(devices);
    QList<QString> _varKeys = _varHash.keys();
    for(int i =0;i<_varHash.size();i++)
    {
        QString  _varName = _varKeys.at(i);
        if(m_yicheVarLanJieHash.contains(_varName))
        {
            //判断里面是否有为true的，如果有，就不执行下面的动作
            bool _ret = false;
            for(int i =0;i<m_yicheVarLanJieHash.size();i++)
            {
                _ret |= m_yicheVarLanJieHash[m_yicheVarLanJieHash.keys().at(i)];
            }
            //如果里面没有为true的，执行
            if(!_ret)
            {
                //将里面设置为true
                m_yicheVarLanJieHash[_varName] = true;
                setMultiLambdaVariableStatus(_varName, true);
            }
        }else if(m_liucheVarLanJieHash.contains(_varName))
        {
            bool _ret = false;
            for(int i =0;i<m_liucheVarLanJieHash.size();i++)
            {
                _ret |= m_liucheVarLanJieHash[m_liucheVarLanJieHash.keys().at(i)];
            }
            if(!_ret)
            {
                m_liucheVarLanJieHash[_varName] = true;
                setMultiLambdaVariableStatus(_varName, true);
            }
        }else if(m_abyeweidiyanshiTimerHash.contains(_varName))
        {
            if(!m_abyeweidiyanshiTimerHash.value(_varName)->isActive())
            {
                qDebug() << "启动" << _varName << "延时定时器";
                m_abyeweidiyanshiTimerHash.value(_varName)->start();
            }
        }else if(_varName == "A框架液位正常")
        {
            setMultiLambdaVariableStatus(_varKeys.at(i), true);
            if(m_abyeweidiyanshiTimerHash.value("A框架液位低")->isActive())
            {
                setMultiLambdaVariableStatus("A框架液位低", false);
                qDebug() << "关闭A框架液位低" << "延时定时器";
                m_abyeweidiyanshiTimerHash.value("A框架液位低")->stop();
            }
        }else if(_varName == "B框架液位正常")
        {
            setMultiLambdaVariableStatus(_varKeys.at(i), true);
            if(m_abyeweidiyanshiTimerHash.value("B框架液位低")->isActive())
            {
                setMultiLambdaVariableStatus("B框架液位低", false);
                qDebug() << "关闭B框架液位低"  << "延时定时器";
                m_abyeweidiyanshiTimerHash.value("B框架液位低")->stop();
            }
        }
        else
        {
            setMultiLambdaVariableStatus(_varKeys.at(i), true);
        }
    }


    //执行控制器动作
    QHash<QString, QList<QString> > _ioHash = getOpenCloseStatusFromIO(devices);
    QList<QString> _ioKeys = _ioHash.keys();
    //    if(_ioHash.size() > 0)
    //    {
    //                qDebug() << _ioHash.keys();
    //                qDebug() << _ioHash.values();
    //    }

    //在这里要执行拦截动作
    for(int i =0;i<_ioKeys.size();i++)
    {
        QList<QString> _dotList = _ioHash.value(_ioKeys.at(i));
        //dotlist中有几个点
        //        emit setIODeviceStatusSignal(_ioKeys.at(i), "电磁阀1", true, "电磁阀2", true, "电磁阀3", true, "电磁阀4", true, "电磁阀5", true, "电磁阀6", true);
        //        qDebug() << __FUNCTION__ << __LINE__;
        if(_dotList.size() == 1)
        {
            QString one;
            bool ones;
            one = _dotList.at(0).split(":").at(0);
            ones = _dotList.at(0).split(":").at(1) == "打开" ? true : false;
            //如果这次和上一次不一样，就把这次的值复制给上一次，并执行这次的值
            bool _ret = false;
            if(m_ioOpenCloseLanJieHash.value(_ioKeys.at(i)).value(one) != ones)
            {
                //                                qDebug() << one << ":" << ones << "[]" << _ioKeys.at(i) << "->" << m_ioOpenCloseLanJieHash.value(_ioKeys.at(i)).value(one);
                m_ioOpenCloseLanJieHash[_ioKeys.at(i)][one] = ones;
                //在这里，发送界面UI
                _ret = true;
            }
            if(_ret)
            {
                qDebug() << devices.at(0).lambdaname << "执行";

                //                                qDebug() << __LINE__ << __FUNCTION__ << "开启或关闭一个输出点";
//                if(one.indexOf("A框架暖风机") == -1 && one.indexOf("A框架加热器") == -1 && one.indexOf("B框架暖风机") == -1 && one.indexOf("B框架加热器") == -1)
//                {
                    emit setIODeviceStatusSignal(_ioKeys.at(i),
                                                 one, ones,
                                                 "", false,
                                                 "", false,
                                                 "", false,
                                                 "", false,
                                                 "", false, _dotList.size());
//                }


                processIOOutputEvent(_ioKeys.at(i), 1, one, ones);
                //如果是打开
                if(ones == true && one.indexOf("A框架加热器") != -1)
                {
                    //                    qDebug() << "A框架加热器+";
                    if(!m_abnuanfengjijiareqiTimerHash.value("A框架加热器故障")->isActive())
                    {
                        QList<QVariant> _vlist = m_abnuanfengjijiareqiTimerHash.value("A框架加热器故障")->getList();
                        quint16 _awendu = getValueByIO("A框架一区控制器", "A框架水温");
                        qint8 _akuangjiawendu = (qint16)_awendu;
                        _vlist.append(_akuangjiawendu);
                        m_abnuanfengjijiareqiTimerHash.value("A框架加热器故障")->setMyData(_vlist);
                        //                        qDebug() << "启动A框架加热器定时器";
                        //                        m_abnuanfengjijiareqiTimerHash.value("A框架加热器故障")->start();
                        //                        writeLogToLogFile("启动A框架加热器定时器");
                    }
                }else
                    if(ones == false && one.indexOf("A框架加热器") != -1)
                    {
                        //                    qDebug() << "A框架加热器-";
                        if(m_abnuanfengjijiareqiTimerHash.value("A框架加热器故障")->isActive())
                        {
                            //                        qDebug() << "关闭A框架加热器定时器";
                            //                        m_abnuanfengjijiareqiTimerHash.value("A框架加热器故障")->stop();
                            //                        writeLogToLogFile("关闭A框架加热器定时器");

                        }
                    }else
                        if(ones == true && one.indexOf("B框架加热器") != -1)
                        {
                            //                    qDebug() << "B框架加热器+";

                            if(!m_abnuanfengjijiareqiTimerHash.value("B框架加热器故障")->isActive())
                            {
                                QList<QVariant> _vlist = m_abnuanfengjijiareqiTimerHash.value("B框架加热器故障")->getList();
                                quint16 _awendu = getValueByIO("B框架一区控制器", "B框架水温");
                                qint8 _akuangjiawendu = (qint16)_awendu;
                                _vlist.append(_akuangjiawendu);
                                m_abnuanfengjijiareqiTimerHash.value("B框架加热器故障")->setMyData(_vlist);
                                //                        qDebug() << "启动B框架加热器定时器";
                                //                        m_abnuanfengjijiareqiTimerHash.value("B框架加热器故障")->start();
                                //                        writeLogToLogFile("启动B框架加热器定时器");

                            }
                        }else
                            if(ones == false && one.indexOf("B框架加热器") != -1)
                            {
                                //                    qDebug() << "B框架加热器-";
                                if(m_abnuanfengjijiareqiTimerHash.value("B框架加热器故障")->isActive())
                                {
                                    //                        qDebug() << "关闭B框架加热器定时器";
                                    //                        m_abnuanfengjijiareqiTimerHash.value("B框架加热器故障")->stop();
                                    //                        writeLogToLogFile("关闭B框架加热器定时器");

                                }
                            }else
                                if(ones == true && one.indexOf("A框架暖风机") != -1)
                                {
                                    //                    qDebug() << "A框架暖风机+";
                                    if(!m_abnuanfengjijiareqiTimerHash.value("A框架暖风机故障")->isActive())
                                    {
                                        QList<QVariant> _vlist = m_abnuanfengjijiareqiTimerHash.value("A框架暖风机故障")->getList();
                                        quint16 _awendu = getValueByIO("A框架一区控制器", "A框架内部温度");
                                        qint8 _akuangjiawendu = (qint16)_awendu;
                                        _vlist.append(_akuangjiawendu);
                                        m_abnuanfengjijiareqiTimerHash.value("A框架暖风机故障")->setMyData(_vlist);
                                        //                        qDebug() << "启动A框架暖风机定时器";
                                        //                        m_abnuanfengjijiareqiTimerHash.value("A框架暖风机故障")->start();
                                        //                        writeLogToLogFile("启动A框架暖风机定时器");

                                    }
                                }else
                                    if(ones == false && one.indexOf("A框架暖风机") != -1)
                                    {
                                        //                    qDebug() << "A框架暖风机-";
                                        if(m_abnuanfengjijiareqiTimerHash.value("A框架暖风机故障")->isActive())
                                        {
                                            //                        qDebug() << "关闭A框架暖风机定时器";
                                            //                        m_abnuanfengjijiareqiTimerHash.value("A框架暖风机故障")->stop();
                                            //                        writeLogToLogFile("关闭A框架暖风机定时器");

                                        }
                                    }else
                                        if(ones == true && one.indexOf("B框架暖风机") != -1)
                                        {
                                            //                    qDebug() << "B框架暖风机+";
                                            if(!m_abnuanfengjijiareqiTimerHash.value("B框架暖风机故障")->isActive())
                                            {
                                                QList<QVariant> _vlist = m_abnuanfengjijiareqiTimerHash.value("B框架暖风机故障")->getList();
                                                quint16 _awendu = getValueByIO("B框架一区控制器", "B框架内部温度");
                                                qint8 _akuangjiawendu = (qint16)_awendu;
                                                _vlist.append(_akuangjiawendu);
                                                m_abnuanfengjijiareqiTimerHash.value("B框架暖风机故障")->setMyData(_vlist);
                                                //                        qDebug() << "启动B框架暖风机定时器";
                                                //                        m_abnuanfengjijiareqiTimerHash.value("B框架暖风机故障")->start();
                                                //                        writeLogToLogFile("启动B框架暖风机定时器");

                                            }
                                        }else
                                            if(ones == false && one.indexOf("B框架暖风机") != -1)
                                            {
                                                //                    qDebug() << "B框架暖风机-";
                                                if(m_abnuanfengjijiareqiTimerHash.value("B框架暖风机故障")->isActive())
                                                {
                                                    //                        qDebug() << "关闭B框架暖风机定时器";
                                                    //                        m_abnuanfengjijiareqiTimerHash.value("B框架暖风机故障")->stop();
                                                    //                        writeLogToLogFile("关闭B框架暖风机定时器");

                                                }
                                            }
            }

        }else if(_dotList.size() == 2)
        {
            QString one ,two;
            bool ones, twos;
            one = _dotList.at(0).split(":").at(0);
            ones = _dotList.at(0).split(":").at(1) == "打开" ? true : false;

            two = _dotList.at(1).split(":").at(0);
            twos = _dotList.at(1).split(":").at(1) == "打开" ? true : false;
            bool _ret = false;
            //如果这一次的值不等于上一次的值
            if(m_ioOpenCloseLanJieHash.value(_ioKeys.at(i)).value(one) != ones)
            {
                _ret = true;
                m_ioOpenCloseLanJieHash[_ioKeys.at(i)][one] = ones;
            }
            if(m_ioOpenCloseLanJieHash.value(_ioKeys.at(i)).value(two) != twos)
            {
                _ret = true;
                m_ioOpenCloseLanJieHash[_ioKeys.at(i)][two] = twos;
            }
            if(_ret)
            {
                //                qDebug() << __LINE__ << __FUNCTION__ << "开启或关闭二个输出点";
                emit setIODeviceStatusSignal(_ioKeys.at(i),
                                             one, ones,
                                             two, twos,
                                             "", false,
                                             "", false,
                                             "", false,
                                             "", false, _dotList.size());
                processIOOutputEvent(_ioKeys.at(i), 2, one, ones, two, twos);

            }

        }else if(_dotList.size() == 3)
        {
            QString one ,two, three;
            bool ones, twos, threes;
            one = _dotList.at(0).split(":").at(0);
            ones = _dotList.at(0).split(":").at(1) == "打开" ? true : false;

            two = _dotList.at(1).split(":").at(0);
            twos = _dotList.at(1).split(":").at(1) == "打开" ? true : false;

            three = _dotList.at(2).split(":").at(0);
            threes = _dotList.at(2).split(":").at(1) == "打开" ? true : false;
            bool _ret = false;
            //如果这一次的值不等于上一次的值
            if(m_ioOpenCloseLanJieHash.value(_ioKeys.at(i)).value(one) != ones)
            {
                _ret = true;
                m_ioOpenCloseLanJieHash[_ioKeys.at(i)][one] = ones;
            }
            if(m_ioOpenCloseLanJieHash.value(_ioKeys.at(i)).value(two) != twos)
            {
                _ret = true;
                m_ioOpenCloseLanJieHash[_ioKeys.at(i)][two] = twos;
            }
            if(m_ioOpenCloseLanJieHash.value(_ioKeys.at(i)).value(three) != threes)
            {
                _ret = true;
                m_ioOpenCloseLanJieHash[_ioKeys.at(i)][three] = threes;
            }
            if(_ret)
            {
                //                qDebug() << __LINE__ << __FUNCTION__ << "开启或关闭三个输出点";
                emit setIODeviceStatusSignal(_ioKeys.at(i),
                                             one, ones,
                                             two, twos,
                                             three, threes,
                                             "", false,
                                             "", false,
                                             "", false, _dotList.size());
                processIOOutputEvent(_ioKeys.at(i), 3, one, ones, two, twos, three, threes);
            }

        }else if(_dotList.size() == 4)
        {
            QString one ,two, three, four;
            bool ones, twos, threes, fours;
            one = _dotList.at(0).split(":").at(0);
            ones = _dotList.at(0).split(":").at(1) == "打开" ? true : false;

            two = _dotList.at(1).split(":").at(0);
            twos = _dotList.at(1).split(":").at(1) == "打开" ? true : false;

            three = _dotList.at(2).split(":").at(0);
            threes = _dotList.at(2).split(":").at(1) == "打开" ? true : false;

            four = _dotList.at(3).split(":").at(0);
            fours = _dotList.at(3).split(":").at(1) == "打开" ? true : false;
            //            qDebug() << one << ones
            //                     << two << twos
            //                     << three << threes
            //                     << four << fours;
            bool _ret = false;
            //如果这一次的值不等于上一次的值
            if(m_ioOpenCloseLanJieHash.value(_ioKeys.at(i)).value(one) != ones)
            {
                _ret = true;
                m_ioOpenCloseLanJieHash[_ioKeys.at(i)][one] = ones;
            }
            if(m_ioOpenCloseLanJieHash.value(_ioKeys.at(i)).value(two) != twos)
            {
                _ret = true;
                m_ioOpenCloseLanJieHash[_ioKeys.at(i)][two] = twos;
            }
            if(m_ioOpenCloseLanJieHash.value(_ioKeys.at(i)).value(three) != threes)
            {
                _ret = true;
                m_ioOpenCloseLanJieHash[_ioKeys.at(i)][three] = threes;
            }
            if(m_ioOpenCloseLanJieHash.value(_ioKeys.at(i)).value(four) != fours)
            {
                _ret = true;
                m_ioOpenCloseLanJieHash[_ioKeys.at(i)][four] = fours;
            }
            if(_ret)
            {
                //                qDebug() << __LINE__ << __FUNCTION__ << "开启或关闭四个输出点";
                emit setIODeviceStatusSignal(_ioKeys.at(i),
                                             one, ones,
                                             two, twos,
                                             three, threes,
                                             four, fours,
                                             "", false,
                                             "", false, _dotList.size());
                processIOOutputEvent(_ioKeys.at(i), 4, one, ones, two, twos, three, threes, four, fours);
            }

        }else if(_dotList.size() == 5)
        {
            QString one ,two, three, four, five;
            bool ones, twos, threes, fours, fives;
            one = _dotList.at(0).split(":").at(0);
            ones = _dotList.at(0).split(":").at(1) == "打开" ? true : false;

            two = _dotList.at(1).split(":").at(0);
            twos = _dotList.at(1).split(":").at(1) == "打开" ? true : false;

            three = _dotList.at(2).split(":").at(0);
            threes = _dotList.at(2).split(":").at(1) == "打开" ? true : false;

            four = _dotList.at(3).split(":").at(0);
            fours = _dotList.at(3).split(":").at(1) == "打开" ? true : false;

            five = _dotList.at(4).split(":").at(0);
            fives = _dotList.at(4).split(":").at(1) == "打开" ? true : false;
            bool _ret = false;
            //如果这一次的值不等于上一次的值
            if(m_ioOpenCloseLanJieHash.value(_ioKeys.at(i)).value(one) != ones)
            {
                _ret = true;
                m_ioOpenCloseLanJieHash[_ioKeys.at(i)][one] = ones;
            }
            if(m_ioOpenCloseLanJieHash.value(_ioKeys.at(i)).value(two) != twos)
            {
                _ret = true;
                m_ioOpenCloseLanJieHash[_ioKeys.at(i)][two] = twos;
            }
            if(m_ioOpenCloseLanJieHash.value(_ioKeys.at(i)).value(three) != threes)
            {
                _ret = true;
                m_ioOpenCloseLanJieHash[_ioKeys.at(i)][three] = threes;
            }
            if(m_ioOpenCloseLanJieHash.value(_ioKeys.at(i)).value(four) != fours)
            {
                _ret = true;
                m_ioOpenCloseLanJieHash[_ioKeys.at(i)][four] = fours;
            }
            if(m_ioOpenCloseLanJieHash.value(_ioKeys.at(i)).value(five) != fives)
            {
                _ret = true;
                m_ioOpenCloseLanJieHash[_ioKeys.at(i)][five] = fives;
            }
            if(_ret)
            {
                //                qDebug() << __LINE__ << __FUNCTION__ << "开启或关闭五个输出点";
                emit setIODeviceStatusSignal(_ioKeys.at(i),
                                             one, ones,
                                             two, twos,
                                             three, threes,
                                             four, fours,
                                             five, fives,
                                             "", false, _dotList.size());
                processIOOutputEvent(_ioKeys.at(i), 5, one, ones, two, twos, three, threes, four, fours, five, fives);
            }

        }else if(_dotList.size() == 6)
        {
            QString one ,two, three, four, five, six;
            bool ones, twos, threes, fours, fives, sixs;
            one = _dotList.at(0).split(":").at(0);
            ones = _dotList.at(0).split(":").at(1) == "打开" ? true : false;

            two = _dotList.at(1).split(":").at(0);
            twos = _dotList.at(1).split(":").at(1) == "打开" ? true : false;

            three = _dotList.at(2).split(":").at(0);
            threes = _dotList.at(2).split(":").at(1) == "打开" ? true : false;

            four = _dotList.at(3).split(":").at(0);
            fours = _dotList.at(3).split(":").at(1) == "打开" ? true : false;

            five = _dotList.at(4).split(":").at(0);
            fives = _dotList.at(4).split(":").at(1) == "打开" ? true : false;

            six = _dotList.at(5).split(":").at(0);
            sixs = _dotList.at(5).split(":").at(1) == "打开" ? true : false;
            bool _ret = false;
            //如果这一次的值不等于上一次的值
            if(m_ioOpenCloseLanJieHash.value(_ioKeys.at(i)).value(one) != ones)
            {
                _ret = true;
                m_ioOpenCloseLanJieHash[_ioKeys.at(i)][one] = ones;
            }
            if(m_ioOpenCloseLanJieHash.value(_ioKeys.at(i)).value(two) != twos)
            {
                _ret = true;
                m_ioOpenCloseLanJieHash[_ioKeys.at(i)][two] = twos;
            }
            if(m_ioOpenCloseLanJieHash.value(_ioKeys.at(i)).value(three) != threes)
            {
                _ret = true;
                m_ioOpenCloseLanJieHash[_ioKeys.at(i)][three] = threes;
            }
            if(m_ioOpenCloseLanJieHash.value(_ioKeys.at(i)).value(four) != fours)
            {
                _ret = true;
                m_ioOpenCloseLanJieHash[_ioKeys.at(i)][four] = fours;
            }
            if(m_ioOpenCloseLanJieHash.value(_ioKeys.at(i)).value(five) != fives)
            {
                _ret = true;
                m_ioOpenCloseLanJieHash[_ioKeys.at(i)][five] = fives;
            }
            if(m_ioOpenCloseLanJieHash.value(_ioKeys.at(i)).value(six) != sixs)
            {
                _ret = true;
                m_ioOpenCloseLanJieHash[_ioKeys.at(i)][six] = sixs;
            }
            if(_ret)
            {
                //                qDebug() << __LINE__ << __FUNCTION__ << "开启或关闭六个输出点";
                emit setIODeviceStatusSignal(_ioKeys.at(i),
                                             one, ones,
                                             two, twos,
                                             three, threes,
                                             four, fours,
                                             five, fives,
                                             six, sixs, _dotList.size());
                processIOOutputEvent(_ioKeys.at(i), 6, one, ones, two, twos, three, threes, four, fours, five, fives, six, sixs);
            }

        }
    }

}

void Core::execDevice(const QHash<QString, bool> _hash)
{
    for(int i =0;i<_hash.size();i++)
    {
        QList<MultiLambdaDeviceInfo> _execDeviceList = getCanExecList(_hash.keys().at(i));
        if(_hash[_hash.keys().at(i)] == true)
        {
            execDevice(_execDeviceList);
        }else
        {
            //不成立的等于false，去遍历里面的所有表达式的右侧，如果是变量就设置为false
            changeVariableToFalse(_execDeviceList);
        }
    }
}
//只负责记录按钮按下弹起状态
void Core::buttonClickedCounter()
{
    QString _yichetest = "一车测试按钮";
    QString _liuchetest = "六车测试按钮";
    QString _yichefuwei = "一车复位按钮";
    QString _liuchefuwei = "六车复位按钮";
    QList<QString> _list;
    _list << _yichetest
          << _liuchetest
          << _yichefuwei
          << _liuchefuwei;
    for(int i =0 ;i<_list.size();i++)
    {
        QString _key = _list.at(i);
        bool _ret = getMultiLambdaVariableStatus(_key);
        if(_ret  != m_frontStatusByButtonClickedHash[_key])
        {
            m_buttonClickedCountHash[_key] += 1;
            m_frontStatusByButtonClickedHash[_key] = _ret;
        }
    }
}


void Core::changeVariableToFalse(const QList<MultiLambdaDeviceInfo> & list)
{
    for(int i =0;i<list.size();i++)
    {
        MultiLambdaDeviceInfo _struct = list.at(i);
        if(_struct.type == "var")
        {
            if(_struct.var.var == "A框架加热器故障")
            {
                IODeviceStatus _tempStatus = getIODeviceStatus("A框架二区控制器");
                if(_tempStatus.ajiareqiguzhang)
                {
                    setMultiLambdaVariableStatus(_struct.var.var, true);
                }
            }else if(_struct.var.var == "B框架加热器故障")
            {
                IODeviceStatus _tempStatus = getIODeviceStatus("B框架二区控制器");
                if(_tempStatus.bjiareqiguzhang)
                {
                    setMultiLambdaVariableStatus(_struct.var.var, true);
                }

            }else if(_struct.var.var == "A框架暖风机故障")
            {
                IODeviceStatus _tempStatus = getIODeviceStatus("A框架二区控制器");
                if(_tempStatus.anuanfengjiguzhang)
                {
                    setMultiLambdaVariableStatus(_struct.var.var, true);
                }

            }else if(_struct.var.var == "B框架暖风机故障")
            {
                IODeviceStatus _tempStatus = getIODeviceStatus("B框架二区控制器");
                if(_tempStatus.bnuanfengjiguzhang)
                {
                    setMultiLambdaVariableStatus(_struct.var.var, true);
                }
            }else
            {
                setMultiLambdaVariableStatus(_struct.var.var, false);
            }
        }
    }
}

void Core::resetMultiDevice(QList<MultiLambdaDeviceInfo> &list)
{
    for(int i = 0;i<list.size();i++)
    {
        MultiLambdaDeviceInfo _struct = list.at(i);
        if(_struct.type == "fire")
        {
            _struct.fire.oper.status = false;
        }else if(_struct.type == "gate")
        {
            _struct.gate.oper.status = false;

        }else if(_struct.type == "smog")
        {
            _struct.smog.oper.status = false;

        }else if(_struct.type == "io")
        {
            _struct.io.oper.status = false;
        }
    }
}

QHash<QString, QPair<QString, QString> > Core::getOpenCloseStatusFromGate(const QList<MultiLambdaDeviceInfo> &list)
{
    QHash<QString, QPair<QString, QString> > _hash;
    QList<MultiLambdaDeviceInfo> _list;
    for(int i =0;i<list.size();i++)
    {
        MultiLambdaDeviceInfo _struct = list.at(i);
        if(_struct.type == "gate")
        {
            _list.append(_struct);
        }
    }
    for(int i =0;i<_list.size();i++)
    {
        QPair<QString, QString> _pair;
        MultiLambdaDeviceInfo _struct = _list.at(i);
        if(_struct.gate.type == "分")
        {
            _pair.first = _struct.gate.subdevicename + ":" + _struct.gate.doit;
            for(int j = 0;j<_list.size();j++){
                MultiLambdaDeviceInfo _secondstruct = _list.at(j);
                if(_secondstruct.gate.type == "分" && _secondstruct.gate.devicename == _struct.gate.devicename && _secondstruct.gate.subdevicename != _struct.gate.subdevicename)
                {
                    _pair.second = _secondstruct.gate.subdevicename + ":" + _secondstruct.gate.doit;
                    break;
                }
            }
        }
        _hash.insert(_struct.gate.devicename, _pair);
    }
    return _hash;
}

QHash<QString, QList<QString> > Core::getOpenCloseStatusFromIO(const QList<MultiLambdaDeviceInfo> &list)
{
    QHash<QString, QList<QString> > _hash;
    QList<MultiLambdaDeviceInfo> _list;
    for(int i =0;i<list.size();i++)
    {
        MultiLambdaDeviceInfo _struct = list.at(i);
        if(_struct.type == "io")
        {
            //            if(_struct.lambdaname != "lambda-multi385")
            //            {
            //                qDebug() << _struct.lambdaname;
            //            }
            _list.append(_struct);
        }
    }
    for(int i =0;i<_list.size();i++)
    {
        QList<QString> _dotList;
        MultiLambdaDeviceInfo _struct = _list.at(i);
        if(_struct.io.type == "分")
        {
            //            qDebug() << _struct.io.type << ":" << _struct.io.ioro << ":" << _struct.io.devicename << ":" << _struct.io.subdevicename << ":" << _struct.io.doit << ":" << _struct.io.doit2;
            _dotList.append(_struct.io.subdevicename + ":" + _struct.io.doit);
            for(int j = 0;j<_list.size();j++)
            {
                MultiLambdaDeviceInfo _secondstruct = _list.at(j);
                if(_secondstruct.io.type == "分" && _secondstruct.io.devicename == _struct.io.devicename && _secondstruct.io.subdevicename != _struct.io.subdevicename)
                {
                    _dotList.append(_secondstruct.io.subdevicename + ":" + _secondstruct.io.doit);
                }
            }
        }
        QList<QString> _newList;
        std::set<QString> itemsSet(_dotList.begin(), _dotList.end());
        for(const QString &item : itemsSet)
        {
            _newList.append(item);
        }
        _hash.insert(_struct.io.devicename, _newList);
    }
    //在这里对数组里面的值去重

    return _hash;
}

QHash<QString, QString> Core::getDongZuoFromFire(const QList<MultiLambdaDeviceInfo> & list)
{
    QHash<QString, QString> _hash;
    QList<MultiLambdaDeviceInfo> _list;
    for(int i =0;i<list.size();i++)
    {
        MultiLambdaDeviceInfo _struct = list.at(i);
        if(_struct.type == "fire")
        {
            _list.append(_struct);
        }
    }
    for(int i =0;i<_list.size();i++)
    {
        QString _doIt;
        MultiLambdaDeviceInfo _struct = _list.at(i);
        _doIt = _struct.fire.doit;
        _hash.insert(_struct.fire.devicename, _doIt);
    }
    return _hash;
}

QHash<QString, QString> Core::getDongZuoFromTimer(const QList<MultiLambdaDeviceInfo> & list)
{
    QHash<QString, QString> _hash;
    QList<MultiLambdaDeviceInfo> _list;
    for(int i =0;i<list.size();i++)
    {
        MultiLambdaDeviceInfo _struct = list.at(i);
        if(_struct.type == "timer")
        {
            _list.append(_struct);
        }
    }
    for(int i =0;i<_list.size();i++)
    {
        QString _doIt;
        MultiLambdaDeviceInfo _struct = _list.at(i);
        _doIt = _struct.timer.doit;
        _hash.insert(_struct.timer.reason, _doIt);
    }
    return _hash;
}

QHash<QString, QString> Core::getDongZuoFromVar(const QList<MultiLambdaDeviceInfo> & list)
{
    QHash<QString, QString> _hash;
    QList<MultiLambdaDeviceInfo> _list;
    for(int i =0;i<list.size();i++)
    {
        MultiLambdaDeviceInfo _struct = list.at(i);
        if(_struct.type == "var")
        {
            _list.append(_struct);
        }
    }
    for(int i = 0 ;i<_list.size();i++)
    {
        QString _doIt = "触发";
        MultiLambdaDeviceInfo _struct = _list.at(i);
        _hash.insert(_struct.var.var, _doIt);
    }
    return _hash;
}

QHash<QString, QString>  Core::getDongZuoFromSmog(const QList<MultiLambdaDeviceInfo> & list)
{
    QHash<QString, QString> _hash;
    QList<MultiLambdaDeviceInfo> _list;
    for(int i =0;i<list.size();i++)
    {
        MultiLambdaDeviceInfo _struct = list.at(i);
        if(_struct.type == "smog")
        {
            _list.append(_struct);
        }
    }
    for(int i =0;i<_list.size();i++)
    {
        QString _doIt;
        MultiLambdaDeviceInfo _struct = _list.at(i);
        _doIt = _struct.smog.doit;
        _hash.insert(_struct.smog.devicename, _doIt);
    }
    return _hash;
}

void Core::changeGateStatus(MultiLambdaDeviceInfo & deviceinfo)
{
    //status是当前的实时状态，需求为:将deviceinfo中的opera.status改成status中的实时状态

    //1.获取最新的阀门状态
    GateDeviceStatus _status = getGateDeviceStatus(deviceinfo.gate.devicename);

    if(deviceinfo.gate.type == "分")
    {
        //2.判断deviceinfo中的设备名是阀门的哪个阀门
        QString _lorr;
        if(deviceinfo.gate.subdevicename == _status.leftname)
        {
            _lorr = "left";
        }else if(deviceinfo.gate.subdevicename == _status.rightname)
        {
            _lorr = "right";
        }
        //3.判断实时状态中对应侧的阀门的状态
        if(_lorr == "left")
        {
            //4.判断表达式中要求的是打开还是关闭
            if(deviceinfo.gate.doit == "打开")
            {
                //5.判断当前的状态是啥,如果当前的状态是打开，那就是为true，如果当前的状态是关闭和故障，那就是false
                if(_status.leftgatestatus == "开到位")
                {
                    //                    qDebug() << deviceinfo.gate.subdevicename << _status.leftgatestatus << __LINE__;
                    deviceinfo.gate.oper.status = true;
                }else if(_status.leftgatestatus == "关到位")
                {
                    //                    qDebug() << deviceinfo.gate.subdevicename << _status.leftgatestatus << __LINE__;
                    deviceinfo.gate.oper.status  = false;
                }
            }else if(deviceinfo.gate.doit == "关闭")
            {
                if(_status.leftgatestatus == "开到位")
                {
                    deviceinfo.gate.oper.status = false;
                }else if(_status.leftgatestatus == "关到位")
                {
                    deviceinfo.gate.oper.status  = true;
                }
            }else if(deviceinfo.gate.doit == "故障")
            {
                if(_status.leftgatestatus == "未知")
                {
                    deviceinfo.gate.oper.status = true;
                }else
                {
                    deviceinfo.gate.oper.status = false;
                }
            }
        }else if(_lorr == "right")
        {
            if(deviceinfo.gate.doit == "打开")
            {
                //5.判断当前的状态是啥,如果当前的状态是打开，那就是为true，如果当前的状态是关闭和故障，那就是false
                if(_status.rightgatestatus == "开到位")
                {
                    deviceinfo.gate.oper.status = true;
                }else if(_status.rightgatestatus == "关到位")
                {
                    deviceinfo.gate.oper.status  = false;
                }
            }else if(deviceinfo.gate.doit == "关闭")
            {
                if(_status.rightgatestatus == "开到位")
                {
                    deviceinfo.gate.oper.status = false;
                }else if(_status.rightgatestatus == "关到位")
                {
                    deviceinfo.gate.oper.status  = true;
                }
            }else if(deviceinfo.gate.doit == "故障")
            {
                if(_status.rightgatestatus == "未知")
                {
                    deviceinfo.gate.oper.status = true;
                }else
                {
                    deviceinfo.gate.oper.status = false;
                }
            }
        }
    }else if(deviceinfo.gate.type == "总")
    {
        if(_status.networkstatus == "网络连接故障")
        {
            deviceinfo.gate.oper.status = true;
        }
    }
}

void Core::changeFireStatus(MultiLambdaDeviceInfo & deviceinfo)
{
    //取出当前的实时状态
    FireDeviceStatus _status = getFireDeviceStatus(deviceinfo.fire.devicename);

    //判断配置文件中是什么状态
    if(deviceinfo.fire.doit == "火警")
    {
        if(_status.firestatus == "有火警")
        {
            deviceinfo.fire.oper.status = true;
        }
    }else if(deviceinfo.fire.doit == "故障")
    {
        if(_status.networkstatus == NETWORKERR)
        {
            deviceinfo.fire.oper.status = true;
        }
    }else if(deviceinfo.fire.doit == "复位")
    {
        //感温探测器网络故障情况下：火警="" 复位=""

        //感温探测器复位成功状态下：复位="无复位"  火警="无火警"

        //感温探测器屏蔽之前：复位=复位成功 火警=无火警 网络=网络连接成功

        //感温探测器屏蔽之后：复位= 火警=无火警 网络=

        //感温探测器默认：火警=无火警 复位=未复位 网络=网络连接成功

        if( _status.shieldstatus == "已屏蔽" || _status.firestatus == "无火警")
        {
            deviceinfo.fire.oper.status = true;
        }
    }
}

void Core::changeSmogStatus(MultiLambdaDeviceInfo & deviceinfo)
{
    //获取感烟设备的最新状态
    SmogDeviceStatus _status = getSmogDeviceStatus(deviceinfo.smog.devicename);
    if(deviceinfo.smog.type == "分")
    {
        if(deviceinfo.smog.doit == "火警")
        {
            if(_status.firestatus[deviceinfo.smog.subdevicename] == "有火警")
            {
                //                qDebug() << __LINE__ << __FUNCTION__;
                deviceinfo.smog.oper.status = true;
            }
        }else if(deviceinfo.smog.doit == "故障")
        {
            if(_status.faultstatus[deviceinfo.smog.subdevicename] == "有故障")
            {
                deviceinfo.smog.oper.status = true;
            }
        }else if(deviceinfo.smog.doit == "复位")
        {
            if(_status.firestatus[deviceinfo.smog.subdevicename] == "无火警" || _status.faultstatus[deviceinfo.smog.subdevicename] == "有故障")
            {
                deviceinfo.smog.oper.status = true;
            }
        }
    }else if(deviceinfo.smog.type == "总")
    {
        //总就有一个网络连接故障
        if(deviceinfo.smog.doit == "故障")
        {
            if(_status.networkstatus == "网络连接故障")
            {
                deviceinfo.smog.oper.status = true;
            }
        }
    }
}

void Core::changeIOStatus(MultiLambdaDeviceInfo & deviceinfo)
{
    //获取IO的实时状态
    if(!m_ioDeviceStatusHash.contains(deviceinfo.io.devicename))
    {
        return;
    }
    IODeviceStatus _status = getIODeviceStatus(deviceinfo.io.devicename);

    //1.判断该表达式的IO是什么类型：
    //IO表达式有多种类型
    //有判断一种条件，还有判断两种条件的
    //    QList<QString> _ikeys = _status.inputs.keys();
    //    for(int i =0;i<_ikeys.size();i++)
    //    {
    //        qDebug() << _ikeys.at(i) << ":" << _status.inputs.value(_ikeys.at(i));
    //    }
    //@一车一区控制器:温度>30:温度<50*
    bool _tempsign = false;
    if(
            deviceinfo.io.subdevicename.indexOf("水温") != -1
            || deviceinfo.io.subdevicename.indexOf("内部温度") != -1
            || deviceinfo.io.subdevicename.indexOf("环境温度") != -1)
    {
        _tempsign = true;
    }
    if(deviceinfo.io.type == "分")
    {
        //判断是操作Input还是output
        if(deviceinfo.io.ioro == "输入")
        {
            if(deviceinfo.io.secondstatus == "有")//有代表有一个判断值的区间值，比如》200 《 300
            {
                //判断俩点

                quint16 _value = _status.inputs[deviceinfo.io.subdevicename];
                bool _ret = true;

                if(deviceinfo.io.doit == ">")
                {
                    qint16 _v;
                    if(_tempsign)
                    {
                        _v = (qint16)_value;

                    }
                    if(_tempsign)
                    {
                        if(_v > deviceinfo.io.value)
                        {
                            _ret &= true;
                        }else
                        {
                            _ret &= false;
                        }
                    }else
                    {
                        if(_value > deviceinfo.io.value)
                        {
                            _ret = true;
                        }else
                        {
                            _ret &= false;
                        }
                    }
                }
                if(deviceinfo.io.doit == "<")
                {
                    qint16 _v;
                    if(_tempsign)
                    {
                        _v = (qint16)_value;


                    }
                    if(_tempsign)
                    {
                        if(_v < deviceinfo.io.value)
                        {
                            _ret &= true;
                        }else
                        {
                            _ret &= false;
                        }
                    }else
                    {
                        if(_value < deviceinfo.io.value)
                        {
                            _ret &= true;
                        }else
                        {
                            _ret &= false;
                        }
                    }
                }
                if(deviceinfo.io.doit == "=")
                {
                    qint16 _v;
                    if(_tempsign)
                    {
                        _v = (qint16)_value;

                    }
                    if(_tempsign)
                    {
                        if(_v == deviceinfo.io.value)
                        {
                            _ret &= true;
                        }else
                        {
                            _ret &= false;
                        }
                    }else
                    {
                        if(_value == deviceinfo.io.value)
                        {
                            _ret &= true;
                        }else
                        {
                            _ret &= false;
                        }
                    }
                }
                if(deviceinfo.io.doit2 == ">")
                {
                    qint16 _v;
                    if(_tempsign)
                    {
                        _v = (qint16)_value;

                    }
                    if(_tempsign)
                    {
                        if(_v > deviceinfo.io.value2)
                        {
                            _ret &= true;
                        }else
                        {
                            _ret &= false;
                        }
                    }else
                    {
                        if(_value > deviceinfo.io.value2)
                        {
                            _ret &= true;
                        }else
                        {
                            _ret &= false;
                        }
                    }
                }
                if(deviceinfo.io.doit2 == "<")
                {
                    qint16 _v;
                    if(_tempsign)
                    {
                        _v = (qint16)_value;

                    }
                    if(_tempsign)
                    {
                        if(_v < deviceinfo.io.value2)
                        {
                            _ret &= true;
                        }else
                        {
                            _ret &= false;
                        }
                    }else
                    {
                        if(_value < deviceinfo.io.value2)
                        {
                            _ret &= true;
                        }else
                        {
                            _ret &= false;
                        }
                    }
                }
                if(deviceinfo.io.doit2 == "=")
                {
                    qint16 _v;
                    if(_tempsign)
                    {
                        _v = (qint16)_value;

                    }
                    if(_tempsign)
                    {
                        if(_v == deviceinfo.io.value2)
                        {
                            _ret &= true;
                        }else
                        {
                            _ret &= false;
                        }
                    }else
                    {
                        if(_value == deviceinfo.io.value2)
                        {
                            _ret &= true;
                        }else
                        {
                            _ret &= false;
                        }
                    }
                }
                deviceinfo.io.oper.status = _ret;
            }else if(deviceinfo.io.secondstatus == "无")//无代表只判断一个值
            {
                //判断对应点是否可以为true
                quint16 _value = _status.inputs[deviceinfo.io.subdevicename];
                bool _ret = false;
                if(deviceinfo.io.doit == ">")
                {
                    qint16 _v;
                    if(_tempsign)
                    {
                        _v = (qint16)_value;
                    }
                    if(_tempsign)
                    {
                        if(_v > deviceinfo.io.value)
                        {
                            _ret = true;
                        }
                    }else
                    {
                        if(_value > deviceinfo.io.value)
                        {
                            _ret = true;
                        }
                    }
                }
                if(deviceinfo.io.doit == "<")
                {
                    qint16 _v;
                    if(_tempsign)
                    {
                        _v = (qint16)_value;
                    }
                    if(_tempsign)
                    {
                        if(_v < deviceinfo.io.value)
                        {
                            _ret = true;
                        }
                    }else
                    {
                        if(_value < deviceinfo.io.value)
                        {
                            _ret = true;
                        }
                    }
                }
                if(deviceinfo.io.doit == "=")
                {
                    qint16 _v;
                    if(_tempsign)
                    {
                        //                        memcpy(&_v, &_value, sizeof(quint16));
                        _v = (qint16)_value;

                    }
                    if(_tempsign)
                    {
                        if(_v == deviceinfo.io.value)
                        {
                            _ret = true;
                        }
                    }else
                    {
                        if(_value == deviceinfo.io.value)
                        {
                            _ret = true;
                        }
                    }
                }
                deviceinfo.io.oper.status = _ret;
            }
        }else if(deviceinfo.io.ioro == "输出")
        {

            //判断对应设备是否故障
            if(deviceinfo.io.doit == "故障")
            {
                if(_status.networkstatus == "网络连接故障")
                {
                    deviceinfo.io.oper.status = true;
                }
            }
            if(deviceinfo.io.doit == "打开")
            {
                if(_status.outputs.value(deviceinfo.io.subdevicename) == 0x01)
                {
                    deviceinfo.io.oper.status = true;
                }
            }
            if(deviceinfo.io.doit == "关闭")
            {
                if(_status.outputs.value(deviceinfo.io.subdevicename) == 0x00)
                {
                    deviceinfo.io.oper.status = true;
                }
            }
        }
    }else if(deviceinfo.io.type == "总")
    {
        if(deviceinfo.io.ioro == "输出")
        {
            //如果是总的输出点，就判断一个
            if(deviceinfo.io.doit == "故障")
            {
                if(_status.networkstatus == "网络连接故障")
                {
                    deviceinfo.io.oper.status = true;
                }
            }
        }
    }
}

void Core::changeVarStatus(MultiLambdaDeviceInfo & deviceinfo)
{
    bool _ret  = getMultiLambdaVariableStatus(deviceinfo.var.var);
    //    if(deviceinfo.var.var == "一车测试按钮")
    //    {
    //        qDebug() << deviceinfo.var.var << getMultiLambdaVariableStatus(deviceinfo.var.var);
    //    }
    deviceinfo.var.oper.status = _ret;
}

void Core::changeTimerStatus(MultiLambdaDeviceInfo & deviceinfo)
{
    bool _ret = getMultiLambdaTimerStatus(deviceinfo.timer.result);
    if(_ret)
    {
        deviceinfo.timer.oper.status = true;
    }
}

MyDataBaseWord Core::getFireDataBaseWord(const QString &devicename)
{
    MyDataBaseWord _word;
    _word.type = FIRETABLE;
    _word.fire_word.casue = FIRE;
    _word.fire_word.isclear = ISNOTCLEAR;
    _word.fire_word.devicename = devicename;
    _word.fire_word.date = QDateTime::currentDateTime().toTime_t();
    return _word;
}

MyDataBaseWord Core::getFaultDataBaseWord(const QString &devicename)
{
    MyDataBaseWord _word;
    _word.type = FAULTTABLE;
    _word.fault_word.casue = NETWORKERR;
    _word.fault_word.isclear = ISNOTCLEAR;
    _word.fault_word.devicename = devicename;
    _word.fault_word.date = QDateTime::currentDateTime().toTime_t();
    return _word;
}

MyDataBaseWord Core::getEventDataBaseWord(const QString &devicename, const QString &eventname)
{
    MyDataBaseWord _word;
    _word.type = EVENTTABLE;
    _word.event_word.casue = eventname;
    _word.event_word.isclear = ISNOTCLEAR;
    _word.event_word.devicename = devicename;
    _word.event_word.date = QDateTime::currentDateTime().toTime_t();
    return _word;
}

QJsonObject Core::getFireUIWord(const MyDataBaseWord &word)
{
    QJsonObject _obj;
    _obj.insert("func", "telluifirefaultevent");
    _obj.insert("type", FIRETABLE);
    _obj.insert("devicename", word.fire_word.devicename);
    _obj.insert("cause", word.fire_word.casue);
    _obj.insert("date", QDateTime::fromTime_t(word.fire_word.date).toString("yyyy-MM-dd hh:mm:ss"));
    return _obj;
}

QJsonObject Core::getFaultUIWord(const MyDataBaseWord &word)
{
    QJsonObject _obj;
    _obj.insert("func", "telluifirefaultevent");
    _obj.insert("type", FAULTTABLE);
    _obj.insert("devicename", word.fault_word.devicename);
    _obj.insert("cause", word.fault_word.casue);
    _obj.insert("date", QDateTime::fromTime_t(word.fault_word.date).toString("yyyy-MM-dd hh:mm:ss"));
    _obj.insert("isclear", ISNOTCLEAR);
    return _obj;
}

QJsonObject Core::getEventUIWord(const MyDataBaseWord &word)
{
    QJsonObject _obj;
    _obj.insert("func", "telluifirefaultevent");
    _obj.insert("type", EVENTTABLE);
    _obj.insert("devicename", word.event_word.devicename);
    _obj.insert("cause", word.event_word.casue);
    _obj.insert("date", QDateTime::fromTime_t(word.event_word.date).toString("yyyy-MM-dd hh:mm:ss"));
    return _obj;
}

void Core::processFireDeviceFire(const QString & devicename)
{
    //设备火警逻辑如下：发现有火警就报
    FireDeviceStatus _status = getFireDeviceStatus(devicename);
    if(_status.firestatus == "有火警")
    {
        //告知UI和数据库
        if(!m_fireDeviceFireInfoToUIHash.contains(devicename))
        {
            //            qDebug() << "返现火警，但是不存在，插入一次";
            MyDataBaseWord _word;
            _word.type = FIRETABLE;
            _word.fire_word.date = QDateTime::currentDateTime().toTime_t();
            _word.fire_word.devicename = devicename;
            _word.fire_word.casue = FIRE;
            _word.fire_word.isclear = ISNOTCLEAR;
            emit insertDataToDataBaseSignal(_word);


            QJsonObject _obj;
            _obj.insert("func", "telluifirefaultevent");
            _obj.insert("type", FIRETABLE);
            _obj.insert("devicename", devicename);
            _obj.insert("cause", FIRE);
            _obj.insert("date", QDateTime::fromTime_t(_word.fire_word.date).toString("yyyy-MM-dd hh:mm:ss"));
            _obj.insert("isclear", ISNOTCLEAR);


            m_fireDeviceFireInfoToUIHash.insert(devicename, _obj);
        }else
        {
            //            qDebug() << "已经存在火警，持续发送";
            QJsonObject _obj = m_fireDeviceFireInfoToUIHash.value(devicename);
            emit sendMessage(_obj);
        }
    }
}

void Core::processSmogDeviceFire(const QString & devicename)
{
    SmogDeviceStatus _status = getSmogDeviceStatus(devicename);
    QList<QString> _keys  = _status.firestatus.keys();
    for(int i =0;i<_keys.size();i++)
    {
        if(_status.firestatus.value(_keys.at(i)) == "有火警")
        {
            if(!m_smogDeviceFireInfoToUIHash.contains(_keys.at(i)))
            {
                //                qDebug() << "发现感烟火警，但是不存在，插入一次";
                MyDataBaseWord _word;
                _word.type = FIRETABLE;
                _word.fire_word.date = QDateTime::currentDateTime().toTime_t();
                _word.fire_word.devicename = _keys.at(i);
                _word.fire_word.casue = FIRE;
                _word.fire_word.isclear = ISNOTCLEAR;
                emit insertDataToDataBaseSignal(_word);

                QJsonObject _obj;
                _obj.insert("func", "telluifirefaultevent");
                _obj.insert("type", FIRETABLE);
                _obj.insert("devicename", _keys.at(i));
                _obj.insert("cause", FIRE);
                _obj.insert("date", QDateTime::fromTime_t(_word.fire_word.date).toString("yyyy-MM-dd hh:mm:ss"));

                m_smogDeviceFireInfoToUIHash.insert(_keys.at(i), _obj);
            }else
            {
                //                qDebug() << "已经存在感烟火警，持续发送";
                QJsonObject _obj = m_smogDeviceFireInfoToUIHash.value(_keys.at(i));
                emit sendMessage(_obj);
            }
        }
    }
}

void Core::processFireDeviceFault(const QString & devicename)
{
    FireDeviceStatus _status = getFireDeviceStatus(devicename);
    if(_status.networkstatus == NETWORKERR)
    {
        if(!m_fireDeviceFaultInfoToUIHash.contains(devicename))
        {
            //            qDebug() << "不存在感温设备故障,将其插入";
            //插入数据库
            MyDataBaseWord _word;
            _word.type = FAULTTABLE;
            _word.fault_word.date = QDateTime::currentDateTime().toTime_t();
            _word.fault_word.devicename = devicename;
            _word.fault_word.casue = NETWORKERR;
            _word.fault_word.isclear = ISNOTCLEAR;
            emit insertDataToDataBaseSignal(_word);

            QJsonObject _obj;
            _obj.insert("func", "telluifirefaultevent");
            _obj.insert("type", FAULTTABLE);
            _obj.insert("devicename", devicename);
            _obj.insert("cause", NETWORKERR);
            _obj.insert("date", QDateTime::fromTime_t(_word.fault_word.date).toString("yyyy-MM-dd hh:mm:ss"));
            _obj.insert("isclear", ISNOTCLEAR);
            m_fireDeviceFaultInfoToUIHash.insert(devicename, _obj);
        }else
        {
            //            qDebug() << "存在感温设备故障故障，一直发送UI";
            //如果存在就一直发送给UI
            QJsonObject _obj = m_fireDeviceFaultInfoToUIHash.value(devicename);
            emit sendMessage(_obj);
        }
    }else if(_status.networkstatus == NETWORKSUCC)
    {
        if(m_fireDeviceFaultInfoToUIHash.contains(devicename))
        {
            QJsonObject _obj = m_fireDeviceFaultInfoToUIHash.value(devicename);
            if(_obj["cause"].toString() == NETWORKERR)
            {
                //                qDebug() << "感温网络连接恢复，并且上一次是网络故障，这次发送UI清除";
                _obj["isclear"] = ISCLEAR;
                emit sendMessage(_obj);
                m_fireDeviceFaultInfoToUIHash.remove(devicename);

                //插入数据库
                MyDataBaseWord _word;
                _word.type = FAULTTABLE;
                _word.fault_word.date = QDateTime::currentDateTime().toTime_t();
                _word.fault_word.devicename = devicename;
                _word.fault_word.casue = NETWORKSUCC;
                _word.fault_word.isclear = ISCLEAR;
                emit insertDataToDataBaseSignal(_word);
            }
        }
    }
}

void Core::processSmogDeviceFault(const QString & devicename)
{
    SmogDeviceStatus _status = getSmogDeviceStatus(devicename);
    if(_status.networkstatus == NETWORKERR)
    {
        if(!m_smogDeviceFaultInfoToUIHash.contains(devicename))
        {
            //            qDebug() << "不存在感烟设备故障,将其插入";
            //插入数据库
            MyDataBaseWord _word;
            _word.type = FAULTTABLE;
            _word.fault_word.date = QDateTime::currentDateTime().toTime_t();
            _word.fault_word.devicename = devicename;
            _word.fault_word.casue = NETWORKERR;
            _word.fault_word.isclear = ISNOTCLEAR;
            emit insertDataToDataBaseSignal(_word);

            QJsonObject _obj;
            _obj.insert("func", "telluifirefaultevent");
            _obj.insert("type", FAULTTABLE);
            _obj.insert("devicename", devicename);
            _obj.insert("cause", NETWORKERR);
            _obj.insert("date", QDateTime::fromTime_t(_word.fault_word.date).toString("yyyy-MM-dd hh:mm:ss"));
            _obj.insert("isclear", ISNOTCLEAR);
            m_smogDeviceFaultInfoToUIHash.insert(devicename, _obj);
        }else{
            //            qDebug() << "存在感烟设备故障故障，一直发送UI";
            //如果存在就一直发送给UI
            QJsonObject _obj = m_smogDeviceFaultInfoToUIHash.value(devicename);
            emit sendMessage(_obj);
        }
    }else if(_status.networkstatus == NETWORKSUCC)
    {
        if(m_smogDeviceFaultInfoToUIHash.contains(devicename))
        {
            QJsonObject _obj = m_smogDeviceFaultInfoToUIHash.value(devicename);
            if(_obj["cause"].toString() == NETWORKERR)
            {
                //                qDebug() << "感烟网络连接恢复，并且上一次是网络故障，这次发送UI清除";
                _obj["isclear"] = ISCLEAR;
                emit sendMessage(_obj);
                m_smogDeviceFaultInfoToUIHash.remove(devicename);

                //插入数据库
                MyDataBaseWord _word;
                _word.type = FAULTTABLE;
                _word.fault_word.date = QDateTime::currentDateTime().toTime_t();
                _word.fault_word.devicename = devicename;
                _word.fault_word.casue = NETWORKSUCC;
                _word.fault_word.isclear = ISCLEAR;
                emit insertDataToDataBaseSignal(_word);
            }
        }
    }
}

void Core::processSubSmogDeviceFault(const QString & devicename)
{
    SmogDeviceStatus _status = getSmogDeviceStatus(devicename);
    QList<QString> _subSmogKeys;
    _subSmogKeys = _status.faultstatus.keys();
    for(int i = 0;i<_subSmogKeys.size();i++)
    {
        QString _subdevicename;
        QString _substatus;
        _subdevicename = _subSmogKeys.at(i);
        _substatus = _status.faultstatus.value(_subdevicename);
        if(_substatus == "有故障")
        {
            if(!m_smogDeviceFaultInfoToUIHash.contains(_subdevicename))
            {
                //                qDebug() << "不存在子感烟设备故障，将其插入";
                //插入数据库
                MyDataBaseWord _word;
                _word.type = FAULTTABLE;
                _word.fault_word.date = QDateTime::currentDateTime().toTime_t();
                _word.fault_word.devicename = _subdevicename;
                _word.fault_word.casue = NETWORKERR;
                _word.fault_word.isclear = ISNOTCLEAR;
                emit insertDataToDataBaseSignal(_word);
                QJsonObject _obj;
                _obj.insert("func", "telluifirefaultevent");
                _obj.insert("type", FAULTTABLE);
                _obj.insert("devicename", _subdevicename);
                _obj.insert("cause", NETWORKERR);
                _obj.insert("date", QDateTime::fromTime_t(_word.fault_word.date).toString("yyyy-MM-dd hh:mm:ss"));
                _obj.insert("isclear", ISNOTCLEAR);
                m_smogDeviceFaultInfoToUIHash.insert(_subdevicename, _obj);
            }else
            {
                //                qDebug() << "存在子感烟设备故障, 一直发送UI";
                QJsonObject _obj = m_smogDeviceFaultInfoToUIHash.value(_subdevicename);
                emit sendMessage(_obj);
            }
        }else if(_substatus == "无故障")
        {
            if(m_smogDeviceFaultInfoToUIHash.contains(_subdevicename))
            {
                QJsonObject _obj = m_smogDeviceFaultInfoToUIHash.value(_subdevicename);
                if(_obj["cause"].toString() == NETWORKERR)
                {
                    //                    qDebug() << "子阀门网络连接恢复，并且上一次是网络故障，这次发送UI清除";
                    _obj["isclear"] = ISCLEAR;
                    emit sendMessage(_obj);
                    m_smogDeviceFaultInfoToUIHash.remove(_subdevicename);

                    //插入数据库
                    MyDataBaseWord _word;
                    _word.type = FAULTTABLE;
                    _word.fault_word.date = QDateTime::currentDateTime().toTime_t();
                    _word.fault_word.devicename = _subdevicename;
                    _word.fault_word.casue = NETWORKSUCC;
                    _word.fault_word.isclear = ISCLEAR;
                    emit insertDataToDataBaseSignal(_word);
                }
            }
        }
    }
}

void Core::processIODeviceFault(const QString & devicename)
{
    IODeviceStatus _status = getIODeviceStatus(devicename);
    if(_status.networkstatus == NETWORKERR)
    {
        if(!m_ioDeviceFaultInfoToUIHash.contains(devicename))
        {
            //            qDebug() << "不存在io设备故障,将其插入";
            //插入数据库
            MyDataBaseWord _word;
            _word.type = FAULTTABLE;
            _word.fault_word.date = QDateTime::currentDateTime().toTime_t();
            _word.fault_word.devicename = devicename;
            _word.fault_word.casue = NETWORKERR;
            _word.fault_word.isclear = ISNOTCLEAR;
            emit insertDataToDataBaseSignal(_word);

            QJsonObject _obj;
            _obj.insert("func", "telluifirefaultevent");
            _obj.insert("type", FAULTTABLE);
            _obj.insert("devicename", devicename);
            _obj.insert("cause", NETWORKERR);
            _obj.insert("date", QDateTime::fromTime_t(_word.fault_word.date).toString("yyyy-MM-dd hh:mm:ss"));
            _obj.insert("isclear", ISNOTCLEAR);
            m_ioDeviceFaultInfoToUIHash.insert(devicename, _obj);
        }else{
            //            qDebug() << "存在io设备故障故障，一直发送UI";
            //如果存在就一直发送给UI
            QJsonObject _obj = m_ioDeviceFaultInfoToUIHash.value(devicename);
            emit sendMessage(_obj);
        }
    }else if(_status.networkstatus == NETWORKSUCC)
    {
        if(m_ioDeviceFaultInfoToUIHash.contains(devicename))
        {
            QJsonObject _obj = m_ioDeviceFaultInfoToUIHash.value(devicename);
            if(_obj["cause"].toString() == NETWORKERR)
            {
                //                qDebug() << "io网络连接恢复，并且上一次是网络故障，这次发送UI清除";
                _obj["isclear"] = ISCLEAR;
                emit sendMessage(_obj);
                m_ioDeviceFaultInfoToUIHash.remove(devicename);

                //插入数据库
                MyDataBaseWord _word;
                _word.type = FAULTTABLE;
                _word.fault_word.date = QDateTime::currentDateTime().toTime_t();
                _word.fault_word.devicename = devicename;
                _word.fault_word.casue = NETWORKSUCC;
                _word.fault_word.isclear = ISCLEAR;
                emit insertDataToDataBaseSignal(_word);
            }
        }
    }
}

void Core::processGateDeviceFault(const QString & devicename)
{
    GateDeviceStatus _status = getGateDeviceStatus(devicename);
    if(_status.networkstatus == NETWORKERR)
    {
        if(!m_gateDeviceFaultInfoToUIHash.contains(devicename))
        {
            //            qDebug() << "不存在阀门设备故障,将其插入";
            //插入数据库
            MyDataBaseWord _word;
            _word.type = FAULTTABLE;
            _word.fault_word.date = QDateTime::currentDateTime().toTime_t();
            _word.fault_word.devicename = devicename;
            _word.fault_word.casue = NETWORKERR;
            _word.fault_word.isclear = ISNOTCLEAR;
            emit insertDataToDataBaseSignal(_word);

            QJsonObject _obj;
            _obj.insert("func", "telluifirefaultevent");
            _obj.insert("type", FAULTTABLE);
            _obj.insert("devicename", devicename);
            _obj.insert("cause", NETWORKERR);
            _obj.insert("date", QDateTime::fromTime_t(_word.fault_word.date).toString("yyyy-MM-dd hh:mm:ss"));
            _obj.insert("isclear", ISNOTCLEAR);
            m_gateDeviceFaultInfoToUIHash.insert(devicename, _obj);
        }else{
            //            qDebug() << "存在阀门设备故障故障，一直发送UI";
            //如果存在就一直发送给UI
            QJsonObject _obj = m_gateDeviceFaultInfoToUIHash.value(devicename);
            emit sendMessage(_obj);
        }
    }else if(_status.networkstatus == NETWORKSUCC)
    {
        if(m_gateDeviceFaultInfoToUIHash.contains(devicename))
        {
            QJsonObject _obj = m_gateDeviceFaultInfoToUIHash.value(devicename);
            if(_obj["cause"].toString() == NETWORKERR)
            {
                //                qDebug() << "阀门网络连接恢复，并且上一次是网络故障，这次发送UI清除";
                _obj["isclear"] = ISCLEAR;
                emit sendMessage(_obj);
                m_gateDeviceFaultInfoToUIHash.remove(devicename);

                //插入数据库
                MyDataBaseWord _word;
                _word.type = FAULTTABLE;
                _word.fault_word.date = QDateTime::currentDateTime().toTime_t();
                _word.fault_word.devicename = devicename;
                _word.fault_word.casue = NETWORKSUCC;
                _word.fault_word.isclear = ISCLEAR;
                emit insertDataToDataBaseSignal(_word);
            }
        }
    }
}

void Core::processSubGateDeviceFault(const QString & devicename)
{
    GateDeviceStatus _status = getGateDeviceStatus(devicename);
    QList<QString> _subGateKeys;
    _subGateKeys.append(_status.leftname);
    _subGateKeys.append(_status.rightname);
    for(int j = 0;j<_subGateKeys.size();j++)
    {
        QString _sign;
        if(_subGateKeys.at(j) == _status.leftname)
        {
            _sign = "left";
        }
        if(_subGateKeys.at(j) == _status.rightname)
        {
            _sign = "right";
        }
        QString _subdevicename;
        QString _substatus;
        if(_sign == "left")
        {
            _subdevicename = _status.leftname;
            _substatus = _status.leftgatestatus;
        }else if(_sign == "right")
        {
            _subdevicename = _status.rightname;
            _substatus = _status.rightgatestatus;
        }

        if(_substatus == "未知")
        {
            if(!m_gateDeviceFaultInfoToUIHash.contains(_subdevicename))
            {
                //                qDebug() << "不存在子阀门设备故障,将其插入";
                //插入数据库
                MyDataBaseWord _word;
                _word.type = FAULTTABLE;
                _word.fault_word.date = QDateTime::currentDateTime().toTime_t();
                _word.fault_word.devicename = _subdevicename;
                _word.fault_word.casue = NETWORKERR;
                _word.fault_word.isclear = ISNOTCLEAR;
                emit insertDataToDataBaseSignal(_word);
                QJsonObject _obj;
                _obj.insert("func", "telluifirefaultevent");
                _obj.insert("type", FAULTTABLE);
                _obj.insert("devicename", _subdevicename);
                _obj.insert("cause", NETWORKERR);
                _obj.insert("date", QDateTime::fromTime_t(_word.fault_word.date).toString("yyyy-MM-dd hh:mm:ss"));
                _obj.insert("isclear", ISNOTCLEAR);
                m_gateDeviceFaultInfoToUIHash.insert(_subdevicename, _obj);
            }else{
                //                qDebug() << "存在子阀门设备故障故障，一直发送UI";
                QJsonObject _obj = m_gateDeviceFaultInfoToUIHash.value(_subdevicename);
                emit sendMessage(_obj);
            }
        }else if(_substatus == "开到位" || _substatus == "关到位")
        {
            if(m_gateDeviceFaultInfoToUIHash.contains(_subdevicename))
            {
                QJsonObject _obj = m_gateDeviceFaultInfoToUIHash.value(_subdevicename);
                if(_obj["cause"].toString() == NETWORKERR)
                {
                    //                    qDebug() << "阀门网络连接恢复，并且上一次是网络故障，这次发送UI清除";
                    _obj["isclear"] = ISCLEAR;
                    emit sendMessage(_obj);
                    m_gateDeviceFaultInfoToUIHash.remove(_subdevicename);

                    //插入数据库
                    MyDataBaseWord _word;
                    _word.type = FAULTTABLE;
                    _word.fault_word.date = QDateTime::currentDateTime().toTime_t();
                    _word.fault_word.devicename = _subdevicename;
                    _word.fault_word.casue = NETWORKSUCC;
                    _word.fault_word.isclear = ISCLEAR;
                    emit insertDataToDataBaseSignal(_word);
                }
            }
        }
    }
}

void Core::processSubGateDeviceEvent(const QString & devicename)
{
    GateDeviceStatus _status = getGateDeviceStatus(devicename);
    QList<QString> _subGateKeys;
    if(_status.leftname != "")
    {
        _subGateKeys.append(_status.leftname);
    }
    if(_status.rightname != "")
    {
        _subGateKeys.append(_status.rightname);
    }
    for(int j = 0;j<_subGateKeys.size();j++)
    {
        QString _sign;
        if(_subGateKeys.at(j) == _status.leftname)
        {
            _sign = "left";
        }
        if(_subGateKeys.at(j) == _status.rightname)
        {
            _sign = "right";
        }
        QString _subdevicename;
        QString _substatus;
        if(_sign == "left")
        {
            _subdevicename = _status.leftname;
            _substatus = _status.leftgatestatus;
        }else if(_sign == "right")
        {
            _subdevicename = _status.rightname;
            _substatus = _status.rightgatestatus;
        }
        if(_substatus == "开到位")
        {
            if(!m_gateDeviceEventInfoToUIHash.contains(_subdevicename))
            {
                //插入数据库
                MyDataBaseWord _word;
                _word.type = EVENTTABLE;
                _word.event_word.date = QDateTime::currentDateTime().toTime_t();
                _word.event_word.devicename = _subdevicename;
                _word.event_word.casue = "开到位";
                _word.event_word.isclear = ISNOTCLEAR;
                emit insertDataToDataBaseSignal(_word);
                QJsonObject _obj;
                _obj.insert("func", "telluifirefaultevent");
                _obj.insert("type", EVENTTABLE);
                _obj.insert("devicename", _subdevicename);
                _obj.insert("cause", "开到位");
                _obj.insert("date", QDateTime::fromTime_t(_word.event_word.date).toString("yyyy-MM-dd hh:mm:ss"));
                _obj.insert("isclear", ISNOTCLEAR);
                m_gateDeviceEventInfoToUIHash.insert(_subdevicename, _obj);
                //                emit sendMessage(_obj);
            }
            //            else{
            //                QJsonObject _obj = m_gateDeviceEventInfoToUIHash.value(_subdevicename);
            //                emit sendMessage(_obj);
            //            }
        }else if(_substatus == "关到位")
        {
            if(m_gateDeviceEventInfoToUIHash.contains(_subdevicename))
            {
                QJsonObject _obj = m_gateDeviceEventInfoToUIHash.value(_subdevicename);
                if(_obj["cause"].toString() == "开到位")
                {
                    _obj["isclear"] = ISCLEAR;
                    //                    emit sendMessage(_obj);
                    m_gateDeviceEventInfoToUIHash.remove(_subdevicename);

                    //插入数据库
                    MyDataBaseWord _word;
                    _word.type = EVENTTABLE;
                    _word.event_word.date = QDateTime::currentDateTime().toTime_t();
                    _word.event_word.devicename = _subdevicename;
                    _word.event_word.casue = "关到位";
                    _word.event_word.isclear = ISCLEAR;
                    emit insertDataToDataBaseSignal(_word);
                }
            }
        }
    }
}

void Core::processSubIODeviceEvent(const QString & devicename)
{
    IODeviceStatus _status = getIODeviceStatus(devicename);
    QList<QString> _subIOKeys;
    _subIOKeys = _status.outputs.keys();
    for(int i =0;i<_subIOKeys.size();i++)
    {
        QString _key = _subIOKeys.at(i);
        if(_status.outputs.value(_key) == 0x01)
        {
            if(!m_ioDeviceEventInfoToUIHash.contains(_key))
            {
                //                qDebug() << "不存在子io设备打开,将其插入";
                //插入数据库
                MyDataBaseWord _word;
                _word.type = EVENTTABLE;
                _word.event_word.date = QDateTime::currentDateTime().toTime_t();
                _word.event_word.devicename = _key;
                _word.event_word.casue = "开到位";
                _word.event_word.isclear = ISNOTCLEAR;
                emit insertDataToDataBaseSignal(_word);
                QJsonObject _obj;
                _obj.insert("func", "telluifirefaultevent");
                _obj.insert("type", EVENTTABLE);
                _obj.insert("devicename", _key);
                _obj.insert("cause", "开到位");
                _obj.insert("date", QDateTime::fromTime_t(_word.event_word.date).toString("yyyy-MM-dd hh:mm:ss"));
                _obj.insert("isclear", ISNOTCLEAR);
                m_ioDeviceEventInfoToUIHash.insert(_key, _obj);
            }
            else
            {
                //                qDebug() << "存在子io设备打开，一直发送UI";
                QJsonObject _obj = m_ioDeviceEventInfoToUIHash.value(_key);
                emit sendMessage(_obj);
            }
        }else if(_status.outputs.value(_key) == 0x00)
        {
            if(m_ioDeviceEventInfoToUIHash.contains(_key))
            {
                QJsonObject _obj = m_ioDeviceEventInfoToUIHash.value(_key);
                if(_obj["cause"].toString() == "开到位")
                {
                    //                    qDebug() << "阀门网络连接恢复，并且上一次是网络故障，这次发送UI清除";
                    _obj["isclear"] = ISCLEAR;
                    emit sendMessage(_obj);
                    m_ioDeviceEventInfoToUIHash.remove(_key);

                    //插入数据库
                    MyDataBaseWord _word;
                    _word.type = EVENTTABLE;
                    _word.event_word.date = QDateTime::fromString(_obj["date"].toString()).toTime_t();
                    _word.event_word.devicename = _key;
                    _word.event_word.casue = "开到位";
                    _word.event_word.isclear = ISCLEAR;
                    emit insertDataToDataBaseSignal(_word);
                }
            }
        }
    }
}

void Core::processIOInputFault()
{
    QString _alowstr = "A框架水温低变量";
    QString _blowstr = "B框架水温低变量";
    QString _ahightstr = "A框架水温正常变量";
    QString _bhightstr = "B框架水温正常变量";
    bool _adiret = getMultiLambdaVariableStatus(_alowstr);
    bool _bdiret = getMultiLambdaVariableStatus(_blowstr);
    bool _azhengret = getMultiLambdaVariableStatus(_ahightstr);
    bool _bzhengret = getMultiLambdaVariableStatus(_bhightstr);
    if(_adiret == true && _azhengret == false)
    {
        if(!m_ioDeviceFaultInfoToUIHash.contains(_alowstr))
        {
            //插入数据库
            MyDataBaseWord _word;
            _word.type = FAULTTABLE;
            _word.fault_word.date = QDateTime::currentDateTime().toTime_t();
            _word.fault_word.devicename = _alowstr;
            _word.fault_word.casue = NETWORKERR;
            _word.fault_word.isclear = ISNOTCLEAR;
            emit insertDataToDataBaseSignal(_word);
            QJsonObject _obj;
            _obj.insert("func", "telluifirefaultevent");
            _obj.insert("type", FAULTTABLE);
            _obj.insert("devicename", _alowstr);
            _obj.insert("cause", NETWORKERR);
            _obj.insert("date", QDateTime::fromTime_t(_word.fault_word.date).toString("yyyy-MM-dd hh:mm:ss"));
            _obj.insert("isclear", ISNOTCLEAR);
            m_ioDeviceFaultInfoToUIHash.insert(_alowstr, _obj);
        }else
        {
            QJsonObject _obj = m_ioDeviceFaultInfoToUIHash.value(_alowstr);
            emit sendMessage(_obj);
        }
    }else if(_azhengret == true && _adiret == false)
    {
        if(m_ioDeviceFaultInfoToUIHash.contains(_alowstr))
        {
            QJsonObject _obj = m_ioDeviceFaultInfoToUIHash.value(_alowstr);
            if(_obj["cause"].toString() == NETWORKERR)
            {
                //                qDebug() << "io网络连接恢复，并且上一次是网络故障，这次发送UI清除";
                _obj["isclear"] = ISCLEAR;
                emit sendMessage(_obj);
                m_ioDeviceFaultInfoToUIHash.remove(_alowstr);

                //插入数据库
                MyDataBaseWord _word;
                _word.type = FAULTTABLE;
                _word.fault_word.date = QDateTime::currentDateTime().toTime_t();
                _word.fault_word.devicename = _alowstr;
                _word.fault_word.casue = NETWORKSUCC;
                _word.fault_word.isclear = ISCLEAR;
                emit insertDataToDataBaseSignal(_word);
            }
        }
    }



    if(_bdiret == true && _bzhengret == false)
    {
        if(!m_ioDeviceFaultInfoToUIHash.contains(_blowstr))
        {
            //插入数据库
            MyDataBaseWord _word;
            _word.type = FAULTTABLE;
            _word.fault_word.date = QDateTime::currentDateTime().toTime_t();
            _word.fault_word.devicename = _blowstr;
            _word.fault_word.casue = NETWORKERR;
            _word.fault_word.isclear = ISNOTCLEAR;
            emit insertDataToDataBaseSignal(_word);
            QJsonObject _obj;
            _obj.insert("func", "telluifirefaultevent");
            _obj.insert("type", FAULTTABLE);
            _obj.insert("devicename", _blowstr);
            _obj.insert("cause", NETWORKERR);
            _obj.insert("date", QDateTime::fromTime_t(_word.fault_word.date).toString("yyyy-MM-dd hh:mm:ss"));
            _obj.insert("isclear", ISNOTCLEAR);
            m_ioDeviceFaultInfoToUIHash.insert(_blowstr, _obj);
        }else
        {
            QJsonObject _obj = m_ioDeviceFaultInfoToUIHash.value(_blowstr);
            emit sendMessage(_obj);
        }
    }else if(_bzhengret == true && _bdiret == false)
    {
        if(m_ioDeviceFaultInfoToUIHash.contains(_blowstr))
        {
            QJsonObject _obj = m_ioDeviceFaultInfoToUIHash.value(_blowstr);
            if(_obj["cause"].toString() == NETWORKERR)
            {
                //                qDebug() << "io网络连接恢复，并且上一次是网络故障，这次发送UI清除";
                _obj["isclear"] = ISCLEAR;
                emit sendMessage(_obj);
                m_ioDeviceFaultInfoToUIHash.remove(_blowstr);

                //插入数据库
                MyDataBaseWord _word;
                _word.type = FAULTTABLE;
                _word.fault_word.date = QDateTime::currentDateTime().toTime_t();
                _word.fault_word.devicename = _blowstr;
                _word.fault_word.casue = NETWORKSUCC;
                _word.fault_word.isclear = ISCLEAR;
                emit insertDataToDataBaseSignal(_word);
            }
        }
    }
}

void Core::sendUDPEvent(QString devicename, QString subdevicename, QString status)
{
    MyDataBaseWord _word;
    _word.type = "感烟屏蔽状态";
    _word.event_word.devicename = devicename;
    _word.event_word.isclear = subdevicename;
    _word.event_word.casue = status;
    m_eventUdp->addEvent(_word);
}

void Core::sendUDPEvent(QString devicename, QString subdevicename)
{
    MyDataBaseWord _word;
    _word.type = "感温屏蔽状态";
    _word.event_word.devicename = devicename;
    _word.event_word.casue = subdevicename;
    m_eventUdp->addEvent(_word);
}

void Core::insertDotByMVB()
{
    //    QHash<QString, QPair<int, QPair<int, int> > > m_fireByFireDeviceHash;

    //1.上面的数据结构手动插入，例如
    //QPair<int, int> _xyPair;
    //_xyPair.first = 第一个字节
    //_xyPair.second = 第二个字节
    //QPair<int, QPair<int, int> > _valuePair;
    //_valuePair.first = 0;
    //_valuePair.second = _xyPair;
    //_fireByFireDeviceHash.first = 一车一区感温探测器;
    //_fireByFireDeviceHash.second = _valuePair;




    //下面是插入感温探测器的数据
    QPair<int, int> pai1;
    pai1.first = 8;
    pai1.second = 1;
    QPair<int, QPair<int, int> > pai11;
    pai11.first = 0;
    pai11.second = pai1;
    m_fireByFireDeviceHash.insert("一车一号感温探测器", pai11);

    QPair<int, int> pai2;
    pai2.first = 8;
    pai2.second = 2;
    QPair<int, QPair<int, int> > pai22;
    pai22.first = 0;
    pai22.second = pai2;
    m_fireByFireDeviceHash.insert("一车二号感温探测器", pai22);

    QPair<int, int> pai3;
    pai3.first = 8;
    pai3.second = 3;
    QPair<int, QPair<int, int> > pai33;
    pai33.first = 0;
    pai33.second = pai3;
    m_fireByFireDeviceHash.insert("一车三号感温探测器", pai33);

    QPair<int, int> pai4;
    pai4.first = 8;
    pai4.second = 4;
    QPair<int, QPair<int, int> > pai44;
    pai44.first = 0;
    pai44.second = pai4;
    m_fireByFireDeviceHash.insert("一车四号感温探测器", pai44);

    QPair<int, int> pai5;
    pai5.first = 8;
    pai5.second = 5;
    QPair<int, QPair<int, int> > pai55;
    pai55.first = 0;
    pai55.second = pai5;
    m_fireByFireDeviceHash.insert("一车五号感温探测器", pai55);


    QPair<int, int> pai6;
    pai6.first = 8;
    pai6.second = 6;
    QPair<int, QPair<int, int> > pai66;
    pai66.first = 0;
    pai66.second = pai6;
    m_fireByFireDeviceHash.insert("一车六号感温探测器", pai66);


    QPair<int, int> pai7;
    pai7.first = 9;
    pai7.second = 1;
    QPair<int, QPair<int, int> > pai77;
    pai77.first = 0;
    pai77.second = pai7;
    m_fireByFireDeviceHash.insert("二车一号感温探测器", pai77);


    QPair<int, int> pai8;
    pai8.first = 9;
    pai8.second = 2;
    QPair<int, QPair<int, int> > pai88;
    pai88.first = 0;
    pai88.second = pai8;
    m_fireByFireDeviceHash.insert("二车二号感温探测器", pai88);


    QPair<int, int> pai9;
    pai9.first = 9;
    pai9.second = 3;
    QPair<int, QPair<int, int> > pai99;
    pai99.first = 0;
    pai99.second = pai9;
    m_fireByFireDeviceHash.insert("二车三号感温探测器", pai99);

    QPair<int, int> pai10;
    pai10.first = 9;
    pai10.second = 4;
    QPair<int, QPair<int, int> > pai1010;
    pai1010.first = 0;
    pai1010.second = pai10;
    m_fireByFireDeviceHash.insert("二车四号感温探测器", pai1010);

    QPair<int, int> pai111;
    pai111.first = 9;
    pai111.second = 5;
    QPair<int, QPair<int, int> > pai1111;
    pai1111.first = 0;
    pai1111.second = pai111;
    m_fireByFireDeviceHash.insert("二车五号感温探测器", pai1111);



    QPair<int, int> pai12;
    pai12.first = 9;
    pai12.second = 6;
    QPair<int, QPair<int, int> > pai1212;
    pai1212.first = 0;
    pai1212.second = pai12;
    m_fireByFireDeviceHash.insert("二车六号感温探测器", pai1212);


    QPair<int, int> pai13;
    pai13.first = 10;
    pai13.second = 1;
    QPair<int, QPair<int, int> > pai1313;
    pai1313.first = 0;
    pai1313.second = pai13;
    m_fireByFireDeviceHash.insert("三车一号感温探测器", pai1313);


    QPair<int, int> pai14;
    pai14.first = 10;
    pai14.second = 2;
    QPair<int, QPair<int, int> > pai1414;
    pai1414.first = 0;
    pai1414.second = pai14;
    m_fireByFireDeviceHash.insert("三车二号感温探测器", pai1414);



    QPair<int, int> pai15;
    pai15.first = 10;
    pai15.second = 3;
    QPair<int, QPair<int, int> > pai1515;
    pai1515.first = 0;
    pai1515.second = pai15;
    m_fireByFireDeviceHash.insert("三车三号感温探测器", pai1515);


    QPair<int, int> pai16;
    pai16.first = 10;
    pai16.second = 4;
    QPair<int, QPair<int, int> > pai1616;
    pai1616.first = 0;
    pai1616.second = pai16;
    m_fireByFireDeviceHash.insert("三车四号感温探测器", pai1616);

    QPair<int, int> pai17;
    pai17.first = 10;
    pai17.second = 5;
    QPair<int, QPair<int, int> > pai1717;
    pai1717.first = 0;
    pai1717.second = pai17;
    m_fireByFireDeviceHash.insert("三车五号感温探测器", pai1717);



    QPair<int, int> pai18;
    pai18.first = 10;
    pai18.second = 6;
    QPair<int, QPair<int, int> > pai1818;
    pai1818.first = 0;
    pai1818.second = pai18;
    m_fireByFireDeviceHash.insert("三车六号感温探测器", pai1818);



    QPair<int, int> pai19;
    pai19.first = 11;
    pai19.second = 1;
    QPair<int, QPair<int, int> > pai1919;
    pai1919.first = 0;
    pai1919.second = pai19;
    m_fireByFireDeviceHash.insert("四车一号感温探测器", pai1919);


    QPair<int, int> pai20;
    pai20.first = 11;
    pai20.second = 2;
    QPair<int, QPair<int, int> > pai2020;
    pai2020.first = 0;
    pai2020.second = pai20;
    m_fireByFireDeviceHash.insert("四车二号感温探测器", pai2020);


    QPair<int, int> pai21;
    pai21.first = 11;
    pai21.second = 3;
    QPair<int, QPair<int, int> > pai2121;
    pai2121.first = 0;
    pai2121.second = pai21;
    m_fireByFireDeviceHash.insert("四车三号感温探测器", pai2121);


    QPair<int, int> pai222;
    pai222.first = 11;
    pai222.second = 4;
    QPair<int, QPair<int, int> > pai2222;
    pai2222.first = 0;
    pai2222.second = pai222;
    m_fireByFireDeviceHash.insert("四车四号感温探测器", pai2222);


    QPair<int, int> pai23;
    pai23.first = 11;
    pai23.second = 5;
    QPair<int, QPair<int, int> > pai2323;
    pai2323.first = 0;
    pai2323.second = pai23;
    m_fireByFireDeviceHash.insert("四车五号感温探测器", pai2323);


    QPair<int, int> pai24;
    pai24.first = 11;
    pai24.second = 6;
    QPair<int, QPair<int, int> > pai2424;
    pai2424.first = 0;
    pai2424.second = pai24;
    m_fireByFireDeviceHash.insert("四车六号感温探测器", pai2424);




    QPair<int, int> pai25;
    pai25.first = 12;
    pai25.second = 1;
    QPair<int, QPair<int, int> > pai2525;
    pai2525.first = 0;
    pai2525.second = pai25;
    m_fireByFireDeviceHash.insert("五车一号感温探测器", pai2525);


    QPair<int, int> pai26;
    pai26.first = 12;
    pai26.second = 2;
    QPair<int, QPair<int, int> > pai2626;
    pai2626.first = 0;
    pai2626.second = pai26;
    m_fireByFireDeviceHash.insert("五车二号感温探测器", pai2626);


    QPair<int, int> pai27;
    pai27.first = 12;
    pai27.second = 3;
    QPair<int, QPair<int, int> > pai2727;
    pai2727.first = 0;
    pai2727.second = pai27;
    m_fireByFireDeviceHash.insert("五车三号感温探测器", pai2727);


    QPair<int, int> pai28;
    pai28.first = 12;
    pai28.second = 4;
    QPair<int, QPair<int, int> > pai2828;
    pai2828.first = 0;
    pai2828.second = pai28;
    m_fireByFireDeviceHash.insert("五车四号感温探测器", pai2828);


    QPair<int, int> pai29;
    pai29.first = 12;
    pai29.second = 5;
    QPair<int, QPair<int, int> > pai2929;
    pai2929.first = 0;
    pai2929.second = pai29;
    m_fireByFireDeviceHash.insert("五车五号感温探测器", pai2929);


    QPair<int, int> pai30;
    pai30.first = 12;
    pai30.second = 6;
    QPair<int, QPair<int, int> > pai3030;
    pai3030.first = 0;
    pai3030.second = pai30;
    m_fireByFireDeviceHash.insert("五车六号感温探测器", pai3030);

    QPair<int, int> pai31;
    pai31.first = 13;
    pai31.second = 1;
    QPair<int, QPair<int, int> > pai3131;
    pai3131.first = 0;
    pai3131.second = pai31;
    m_fireByFireDeviceHash.insert("六车一号感温探测器", pai3131);


    QPair<int, int> pai32;
    pai32.first = 13;
    pai32.second = 2;
    QPair<int, QPair<int, int> > pai3232;
    pai3232.first = 0;
    pai3232.second = pai32;
    m_fireByFireDeviceHash.insert("六车二号感温探测器", pai3232);


    QPair<int, int> pai333;
    pai333.first = 13;
    pai333.second = 3;
    QPair<int, QPair<int, int> > pai3333;
    pai3333.first = 0;
    pai3333.second = pai333;
    m_fireByFireDeviceHash.insert("六车三号感温探测器", pai3333);


    QPair<int, int> pai34;
    pai34.first = 13;
    pai34.second = 4;
    QPair<int, QPair<int, int> > pai3434;
    pai3434.first = 0;
    pai3434.second = pai34;
    m_fireByFireDeviceHash.insert("六车四号感温探测器", pai3434);


    QPair<int, int> pai35;
    pai35.first = 13;
    pai35.second = 5;
    QPair<int, QPair<int, int> > pai3535;
    pai3535.first = 0;
    pai3535.second = pai35;
    m_fireByFireDeviceHash.insert("六车五号感温探测器", pai3535);

    QPair<int, int> pai36;
    pai36.first = 13;
    pai36.second = 6;
    QPair<int, QPair<int, int> > pai3636;
    pai3636.first = 0;
    pai3636.second = pai36;
    m_fireByFireDeviceHash.insert("六车六号感温探测器", pai3636);












    QPair<int, int > _pai1;
    _pai1.first = 0;
    _pai1.second = 1;
    QPair<int, QPair<int, int > > _pai11;
    _pai11.first = 0;
    _pai11.second = _pai1;
    m_faultByFireDeviceHash.insert("一车一号感温探测器", _pai11);

    QPair<int, int > _pai2;
    _pai2.first = 0;
    _pai2.second = 2;
    QPair<int, QPair<int, int > > _pai22;
    _pai22.first = 0;
    _pai22.second = _pai2;
    m_faultByFireDeviceHash.insert("一车二号感温探测器", _pai22);

    QPair<int, int > _pai3;
    _pai3.first = 0;
    _pai3.second = 3;
    QPair<int, QPair<int, int > > _pai33;
    _pai33.first = 0;
    _pai33.second = _pai3;
    m_faultByFireDeviceHash.insert("一车三号感温探测器", _pai33);


    QPair<int, int > _pai4;
    _pai4.first = 0;
    _pai4.second = 4;
    QPair<int, QPair<int, int > > _pai44;
    _pai44.first = 0;
    _pai44.second = _pai4;
    m_faultByFireDeviceHash.insert("一车四号感温探测器", _pai44);


    QPair<int, int > _pai5;
    _pai5.first = 0;
    _pai5.second = 5;
    QPair<int, QPair<int, int > > _pai55;
    _pai55.first = 0;
    _pai55.second = _pai5;
    m_faultByFireDeviceHash.insert("一车五号感温探测器", _pai55);


    QPair<int, int > _pai6;
    _pai6.first = 0;
    _pai6.second = 6;
    QPair<int, QPair<int, int > > _pai66;
    _pai66.first = 0;
    _pai66.second = _pai6;
    m_faultByFireDeviceHash.insert("一车六号感温探测器", _pai66);


    QPair<int, int > _pai7;
    _pai7.first = 1;
    _pai7.second = 1;
    QPair<int, QPair<int, int > > _pai77;
    _pai77.first = 0;
    _pai77.second = _pai7;
    m_faultByFireDeviceHash.insert("二车一号感温探测器", _pai77);



    QPair<int, int > _pai8;
    _pai8.first = 1;
    _pai8.second = 2;
    QPair<int, QPair<int, int > > _pai88;
    _pai88.first = 0;
    _pai88.second = _pai8;
    m_faultByFireDeviceHash.insert("二车二号感温探测器", _pai88);


    QPair<int, int > _pai9;
    _pai9.first = 1;
    _pai9.second = 3;
    QPair<int, QPair<int, int > > _pai99;
    _pai99.first = 0;
    _pai99.second = _pai9;
    m_faultByFireDeviceHash.insert("二车三号感温探测器", _pai99);


    QPair<int, int > _pai10;
    _pai10.first = 1;
    _pai10.second = 4;
    QPair<int, QPair<int, int > > _pai1010;
    _pai1010.first = 0;
    _pai1010.second = _pai10;
    m_faultByFireDeviceHash.insert("二车四号感温探测器", _pai1010);

    QPair<int, int > _pai111;
    _pai111.first = 1;
    _pai111.second = 5;
    QPair<int, QPair<int, int > > _pai1111;
    _pai1111.first = 0;
    _pai1111.second = _pai111;
    m_faultByFireDeviceHash.insert("二车五号感温探测器", _pai1111);


    QPair<int, int > _pai12;
    _pai12.first = 1;
    _pai12.second = 6;
    QPair<int, QPair<int, int > > _pai1212;
    _pai1212.first = 0;
    _pai1212.second = _pai12;
    m_faultByFireDeviceHash.insert("二车六号感温探测器", _pai1212);



    QPair<int, int > _pai13;
    _pai13.first = 2;
    _pai13.second = 1;
    QPair<int, QPair<int, int > > _pai1313;
    _pai1313.first = 0;
    _pai1313.second = _pai13;
    m_faultByFireDeviceHash.insert("三车一号感温探测器", _pai1313);

    QPair<int, int > _pai14;
    _pai14.first = 2;
    _pai14.second = 2;
    QPair<int, QPair<int, int > > _pai1414;
    _pai1414.first = 0;
    _pai1414.second = _pai14;
    m_faultByFireDeviceHash.insert("三车二号感温探测器", _pai1414);


    QPair<int, int > _pai15;
    _pai15.first = 2;
    _pai15.second = 3;
    QPair<int, QPair<int, int > > _pai1515;
    _pai1515.first = 0;
    _pai1515.second = _pai15;
    m_faultByFireDeviceHash.insert("三车三号感温探测器", _pai1515);


    QPair<int, int > _pai16;
    _pai16.first = 2;
    _pai16.second = 4;
    QPair<int, QPair<int, int > > _pai1616;
    _pai1616.first = 0;
    _pai1616.second = _pai16;
    m_faultByFireDeviceHash.insert("三车四号感温探测器", _pai1616);

    QPair<int, int > _pai17;
    _pai17.first = 2;
    _pai17.second = 5;
    QPair<int, QPair<int, int > > _pai1717;
    _pai1717.first = 0;
    _pai1717.second = _pai17;
    m_faultByFireDeviceHash.insert("三车五号感温探测器", _pai1717);


    QPair<int, int > _pai18;
    _pai18.first = 2;
    _pai18.second = 6;
    QPair<int, QPair<int, int > > _pai1818;
    _pai1818.first = 0;
    _pai1818.second = _pai18;
    m_faultByFireDeviceHash.insert("三车六号感温探测器", _pai1818);

    QPair<int, int > _pai19;
    _pai19.first = 3;
    _pai19.second = 1;
    QPair<int, QPair<int, int > > _pai1919;
    _pai1919.first = 0;
    _pai1919.second = _pai19;
    m_faultByFireDeviceHash.insert("四车一号感温探测器", _pai1919);

    QPair<int, int > _pai20;
    _pai20.first = 3;
    _pai20.second = 2;
    QPair<int, QPair<int, int > > _pai2020;
    _pai2020.first = 0;
    _pai2020.second = _pai20;
    m_faultByFireDeviceHash.insert("四车二号感温探测器", _pai2020);


    QPair<int, int > _pai21;
    _pai21.first = 3;
    _pai21.second = 3;
    QPair<int, QPair<int, int > > _pai2121;
    _pai2121.first = 0;
    _pai2121.second = _pai21;
    m_faultByFireDeviceHash.insert("四车三号感温探测器", _pai2121);


    QPair<int, int > _pai222;
    _pai222.first = 3;
    _pai222.second = 4;
    QPair<int, QPair<int, int > > _pai2222;
    _pai2222.first = 0;
    _pai2222.second = _pai222;
    m_faultByFireDeviceHash.insert("四车四号感温探测器", _pai2222);

    QPair<int, int > _pai23;
    _pai23.first = 3;
    _pai23.second = 5;
    QPair<int, QPair<int, int > > _pai2323;
    _pai2323.first = 0;
    _pai2323.second = _pai23;
    m_faultByFireDeviceHash.insert("四车五号感温探测器", _pai2323);


    QPair<int, int > _pai24;
    _pai24.first = 3;
    _pai24.second = 6;
    QPair<int, QPair<int, int > > _pai2424;
    _pai2424.first = 0;
    _pai2424.second = _pai24;
    m_faultByFireDeviceHash.insert("四车六号感温探测器", _pai2424);


    QPair<int, int > _pai25;
    _pai25.first = 4;
    _pai25.second = 1;
    QPair<int, QPair<int, int > > _pai2525;
    _pai2525.first = 0;
    _pai2525.second = _pai25;
    m_faultByFireDeviceHash.insert("五车一号感温探测器", _pai2525);

    QPair<int, int > _pai26;
    _pai26.first = 4;
    _pai26.second = 2;
    QPair<int, QPair<int, int > > _pai2626;
    _pai2626.first = 0;
    _pai2626.second = _pai26;
    m_faultByFireDeviceHash.insert("五车二号感温探测器", _pai2626);


    QPair<int, int > _pai27;
    _pai27.first = 4;
    _pai27.second = 3;
    QPair<int, QPair<int, int > > _pai2727;
    _pai2727.first = 0;
    _pai2727.second = _pai27;
    m_faultByFireDeviceHash.insert("五车三号感温探测器", _pai2727);

    QPair<int, int > _pai28;
    _pai28.first = 4;
    _pai28.second = 4;
    QPair<int, QPair<int, int > > _pai2828;
    _pai2828.first = 0;
    _pai2828.second = _pai28;
    m_faultByFireDeviceHash.insert("五车四号感温探测器", _pai2828);


    QPair<int, int > _pai29;
    _pai29.first = 4;
    _pai29.second = 5;
    QPair<int, QPair<int, int > > _pai2929;
    _pai2929.first = 0;
    _pai2929.second = _pai29;
    m_faultByFireDeviceHash.insert("五车五号感温探测器", _pai2929);


    QPair<int, int > _pai30;
    _pai30.first = 4;
    _pai30.second = 6;
    QPair<int, QPair<int, int > > _pai3030;
    _pai3030.first = 0;
    _pai3030.second = _pai30;
    m_faultByFireDeviceHash.insert("五车六号感温探测器", _pai3030);


    QPair<int, int > _pai31;
    _pai31.first = 5;
    _pai31.second = 1;
    QPair<int, QPair<int, int > > _pai3131;
    _pai3131.first = 0;
    _pai3131.second = _pai31;
    m_faultByFireDeviceHash.insert("六车一号感温探测器", _pai3131);


    QPair<int, int > _pai32;
    _pai32.first = 5;
    _pai32.second = 2;
    QPair<int, QPair<int, int > > _pai3232;
    _pai3232.first = 0;
    _pai3232.second = _pai32;
    m_faultByFireDeviceHash.insert("六车二号感温探测器", _pai3232);

    QPair<int, int > _pai333;
    _pai333.first = 5;
    _pai333.second = 3;
    QPair<int, QPair<int, int > > _pai3333;
    _pai3333.first = 0;
    _pai3333.second = _pai333;
    m_faultByFireDeviceHash.insert("六车三号感温探测器", _pai3333);


    QPair<int, int > _pai34;
    _pai34.first = 5;
    _pai34.second = 4;
    QPair<int, QPair<int, int > > _pai3434;
    _pai3434.first = 0;
    _pai3434.second = _pai34;
    m_faultByFireDeviceHash.insert("六车四号感温探测器", _pai3434);


    QPair<int, int > _pai35;
    _pai35.first = 5;
    _pai35.second = 5;
    QPair<int, QPair<int, int > > _pai3535;
    _pai3535.first = 0;
    _pai3535.second = _pai35;
    m_faultByFireDeviceHash.insert("六车五号感温探测器", _pai3535);

    QPair<int, int > _pai36;
    _pai36.first = 5;
    _pai36.second = 6;
    QPair<int, QPair<int, int > > _pai3636;
    _pai3636.first = 0;
    _pai3636.second = _pai36;
    m_faultByFireDeviceHash.insert("六车六号感温探测器", _pai3636);
}


QJsonObject Core::getDeviceName(const QString &type)
{
    QJsonObject _obj;
    _obj.insert("func", "telluidevicename");
    _obj.insert("type", type);
    if(type == "fire")
    {
        QList<QString> _keys = m_readConf->getFireAlarmSensorInfoMap().keys();
        QList<MyFireAlarmSensorInfo> _values = m_readConf->getFireAlarmSensorInfoMap().values();

        for(int i =0;i<_keys.size();i++)
        {
            _obj.insert(_keys.at(i), _values.at(i).base.shield);
        }
    }else if(type == "smog")
    {
        QHash<QString, MySmogSensorInfo> _hash = m_readConf->getSmogInfoMap();
        for(int i =0;i<_hash.size();i++)
        {
            MySmogSensorInfo _struct = _hash[_hash.keys().at(i)];
            QString _str("%1@%2@%3@%4@%5@%6@%7@%8@%9@%10@%11@%12@%13@%14@%15@%16@%17");
            _str = _str
                    .arg(_struct.address1 + "^" + _struct.address1shield).arg(_struct.address2 + "^" + _struct.address2shield).arg(_struct.address3 + "^" + _struct.address3shield).arg(_struct.address4 + "^" + _struct.address4shield).arg(_struct.address5 + "^" + _struct.address5shield)
                    .arg(_struct.address6 + "^" + _struct.address6shield).arg(_struct.address7 + "^" + _struct.address7shield).arg(_struct.address8 + "^" + _struct.address8shield).arg(_struct.address9 + "^" + _struct.address9shield).arg(_struct.address10+ "^" + _struct.address10shield)
                    .arg(_struct.address11 + "^" + _struct.address11shield).arg(_struct.address12+ "^" + _struct.address12shield).arg(_struct.address13 + "^" + _struct.address13shield).arg(_struct.address14 + "^" + _struct.address14shield).arg(_struct.address15 + "^" + _struct.address15shield)
                    .arg(_struct.address16 + "^" + _struct.address16shield).arg(_struct.address17 + "^" + _struct.address17shield);

            _obj.insert(_hash.keys().at(i), _str);
        }
    }else if(type == "gate")
    {
        QHash<QString, MyGateDeviceInfo> _hash = m_readConf->getGateDeviceInfoMap();
        for(int i =0;i<_hash.size();i++)
        {
            MyGateDeviceInfo _struct = _hash[_hash.keys().at(i)];
            QString _str("%1@%2");
            _str = _str.arg(_struct.leftalias).arg(_struct.rightalias);
            //            qDebug() << _hash.keys().at(i) << ":" << _str;
            _obj.insert(_hash.keys().at(i), _str);
        }
    }
    return _obj;
}

void Core::setMasterClientStatus(bool status)
{
    //    qDebug() << "ok hahaha " << status;
    m_masterOrClientMutex->lock();
    m_masterOrClientStatus = status;
    m_masterOrClientMutex->unlock();
}

bool Core::getMasterClientStatus()
{
    bool _ret;
    m_masterOrClientMutex->lock();
    _ret = m_masterOrClientStatus;
    m_masterOrClientMutex->unlock();
    return _ret;
}

QList<MultiLambdaDeviceInfo> Core::getCanExecList(const QString &key)
{
    QList<MultiLambdaDeviceInfo> _list;
    m_multiLambdaMutex->lock();
    _list = m_execMultiLambdaHash.value(key);
    m_multiLambdaMutex->unlock();
    return _list;
}

void Core::setMultiLambdaTimerStatus(const QString &key, const bool status)
{
    m_multiLambdaTimerMutex->lock();
    m_multiLambdaTimerStatusHash[key].second = status;
    m_multiLambdaTimerMutex->unlock();
}

bool Core::getMultiLambdaTimerStatus(const QString &key)
{
    bool _ret;
    m_multiLambdaTimerMutex->lock();
    _ret = m_multiLambdaTimerStatusHash[key].second;
    m_multiLambdaTimerMutex->unlock();
    return _ret;
}

void Core::setMultiLambdaVariableStatus(const QString & key, const bool status)
{
    m_multiLambdaVarMutex->lock();
    //    qDebug() << __FUNCTION__ << __LINE__ << "->key:" << key << ":value:" << status;
    m_multiLambdaVariableHash[key] = status;
    m_multiLambdaVarMutex->unlock();
}

bool Core::getMultiLambdaVariableStatus(const QString & key)
{
    bool _ret;
    m_multiLambdaVarMutex->lock();
    _ret = m_multiLambdaVariableHash[key];
    m_multiLambdaVarMutex->unlock();
    return _ret;
}

void Core::setDataByBit(unsigned char &buf, const int index, const int value)
{
    //将某一位置为1，或上对应位数的1  即可
    if(index == 1)
    {
        if(value == 0)
        {
            buf &= TOZEROBIT1;
        }
        if(value == 1)
        {
            buf |= TOONEBIT1;
        }
    }else if(index == 2)
    {
        if(value == 0)
        {
            buf &= TOZEROBIT2;
        }
        if(value == 1)
        {
            buf |= TOONEBIT2;
        }
    }else if(index == 3)
    {
        if(value == 0)
        {
            buf &= TOZEROBIT3;
        }
        if(value == 1)
        {
            buf |= TOONEBIT3;
        }
    }else if(index == 4)
    {
        if(value == 0)
        {
            buf &= TOZEROBIT4;
        }
        if(value == 1)
        {
            buf |= TOONEBIT4;
        }
    }else if(index == 5)
    {
        if(value == 0)
        {
            buf &= TOZEROBIT5;
        }
        if(value == 1)
        {
            buf |= TOONEBIT5;
        }
    }else if(index == 6)
    {
        if(value == 0)
        {
            buf &= TOZEROBIT6;
        }
        if(value == 1)
        {
            buf |= TOONEBIT6;
        }
    }else if(index == 7)
    {
        if(value == 0)
        {
            buf &= TOZEROBIT7;
        }
        if(value == 1)
        {
            buf |= TOONEBIT7;
        }
    }else if(index == 8)
    {
        if(value == 0)
        {
            buf &= TOZEROBIT8;

        }
        if(value == 1)
        {
            buf |= TOONEBIT8;
        }
    }
}


void Core::processDeviceStatusToUI()
{
    //判断按钮状态是什么
    QString _liucheyiquceshianniu = "六车测试按钮";
    QString _yicheyiquceshianniu = "一车测试按钮";
    QString _liucheyiqushifanganniu = "六车细水雾按钮";
    QString _yicheyiqushifanganniu = "一车细水雾按钮";
    QString _yicheyiqufuweianniu = "一车复位按钮";
    QString _liucheyiqufuweianniu = "六车复位按钮";
    QList<QString> _list;
    _list << _liucheyiqushifanganniu
          << _liucheyiquceshianniu
          << _yicheyiquceshianniu
          << _yicheyiqushifanganniu
          << _yicheyiqufuweianniu
          << _liucheyiqufuweianniu;
    for(int i =0;i<_list.size();i++)
    {
        QString _devicename = _list.at(i);
        bool _ret = getMultiLambdaVariableStatus(_devicename);
        if(_ret == true)
        {
            if(!m_buttonEventInfoToUIHash.contains(_devicename))
            {
                qDebug() << _devicename << _ret;


                MyDataBaseWord _word;
                _word.type = EVENTTABLE;
                _word.event_word.date = QDateTime::currentDateTime().toTime_t();
                _word.event_word.devicename = _devicename;
                _word.event_word.casue = "按下";
                _word.event_word.isclear = ISNOTCLEAR;
                emit insertDataToDataBaseSignal(_word);

                QJsonObject _obj;
                _obj.insert("func", "telluifirefaultevent");
                _obj.insert("type", EVENTTABLE);
                _obj.insert("devicename", _devicename);
                _obj.insert("cause", "按下");
                _obj.insert("date", QDateTime::fromTime_t(_word.event_word.date).toString("yyyy-MM-dd hh:mm:ss"));
                _obj.insert("isclear", ISNOTCLEAR);
                m_buttonEventInfoToUIHash.insert(_devicename, _obj);

                QString _data("%1,%2,%3,%4,%5");
                _data = _data
                        .arg(EVENTTABLE)
                        .arg(_obj["devicename"].toString())
                        .arg(_obj["cause"].toString())
                        .arg(_obj["date"].toString())
                        .arg(_obj["isclear"].toString());
                QJsonObject _eventObj;
                _eventObj.insert("type", "updateeventmessage");
                _eventObj.insert("data", _data);
                m_eventUdp->addEvent(_eventObj);
                emit sendMessage(_obj);
            }
            //            else
            //            {
            //                QJsonObject _obj = m_buttonEventInfoToUIHash.value(_devicename);
            //                emit sendMessage(_obj);
            //            }
        }else if(_ret == false)
        {
            if(m_buttonEventInfoToUIHash.contains(_devicename))
            {
                QJsonObject _obj = m_buttonEventInfoToUIHash.value(_devicename);
                if(_obj["cause"].toString() == "按下")
                {
                    qDebug() << _devicename << _ret;
                    _obj["isclear"] = ISCLEAR;
                    _obj["date"] = QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss");
                    _obj["cause"] = "弹起";
                    emit sendMessage(_obj);

                    MyDataBaseWord _word;
                    _word.type = EVENTTABLE;
                    _word.event_word.date = QDateTime::fromString(_obj["date"].toString(), "yyyy-MM-dd hh:mm:ss").toTime_t();
                    _word.event_word.devicename = _devicename;
                    _word.event_word.casue = "弹起";
                    _word.event_word.isclear = ISCLEAR;
                    emit insertDataToDataBaseSignal(_word);

                    QString _data("%1,%2,%3,%4,%5");
                    _data = _data
                            .arg(EVENTTABLE)
                            .arg(_obj["devicename"].toString())
                            .arg(_obj["cause"].toString())
                            .arg(_obj["date"].toString())
                            .arg(_obj["isclear"].toString());
                    QJsonObject _eventObj;
                    _eventObj.insert("type", "updateeventmessage");
                    _eventObj.insert("data", _data);
                    m_eventUdp->addEvent(_eventObj);

                    m_buttonEventInfoToUIHash.remove(_devicename);
                }
            }
        }
    }
}

void Core::processIOOutputEvent(const QString devicename, const int count,
                                const QString d1, const bool d1s,
                                const QString d2, const bool d2s,
                                const QString d3, const bool d3s,
                                const QString d4, const bool d4s,
                                const QString d5, const bool d5s,
                                const QString d6, const bool d6s)
{
    //先判断几个

    //如果是打开，就ISNOTCLEAR
    //如果是关闭，就ISCLEAR

    //将上面的发送给UI
    //将上面的发送给UDPEvent
    QJsonObject _obj;
    _obj["type"] = "setiostatus";
    _obj["devicename"] = devicename;
    _obj["count"] = count;
    if(count == 1)
    {
        _obj["subdevicename1"] = d1;
        _obj["status1"] = d1s;
    }else if(count == 2)
    {
        _obj["subdevicename1"] = d1;
        _obj["status1"] = d1s;
        _obj["subdevicename2"] = d2;
        _obj["status2"] = d2s;
    }else if(count == 3)
    {
        _obj["subdevicename1"] = d1;
        _obj["status1"] = d1s;
        _obj["subdevicename2"] = d2;
        _obj["status2"] = d2s;
        _obj["subdevicename3"] = d3;
        _obj["status3"] = d3s;
    }else if(count == 4)
    {
        _obj["subdevicename1"] = d1;
        _obj["status1"] = d1s;
        _obj["subdevicename2"] = d2;
        _obj["status2"] = d2s;
        _obj["subdevicename3"] = d3;
        _obj["status3"] = d3s;
        _obj["subdevicename4"] = d4;
        _obj["status4"] = d4s;
    }else if(count == 5)
    {
        _obj["subdevicename1"] = d1;
        _obj["status1"] = d1s;
        _obj["subdevicename2"] = d2;
        _obj["status2"] = d2s;
        _obj["subdevicename3"] = d3;
        _obj["status3"] = d3s;
        _obj["subdevicename4"] = d4;
        _obj["status4"] = d4s;
        _obj["subdevicename5"] = d5;
        _obj["status5"] = d5s;
    }else if(count == 6)
    {
        _obj["subdevicename1"] = d1;
        _obj["status1"] = d1s;
        _obj["subdevicename2"] = d2;
        _obj["status2"] = d2s;
        _obj["subdevicename3"] = d3;
        _obj["status3"] = d3s;
        _obj["subdevicename4"] = d4;
        _obj["status4"] = d4s;
        _obj["subdevicename5"] = d5;
        _obj["status5"] = d5s;
        _obj["subdevicename6"] = d6;
        _obj["status6"] = d6s;
    }
    m_eventUdp->addEvent(_obj);
    if(!m_masterOrClientStatusForMVB)
    {
        m_clientOrMaster->updateIOOutPutStatus(m_ioOpenCloseLanJieHash);
    }
}

void Core::onResetClicked()
{
    //    QHash<QString, MyFireAlarmSensorInfo> _fireHash = m_readConf->getFireAlarmSensorInfoMap();
    //    QList<QString> _fireKeys = _fireHash.keys();

    //    QHash<QString, MySmogSensorInfo> _smogHash = m_readConf->getSmogInfoMap();
    //    QList<QString> _smogKeys = _smogHash.keys();

    //    QHash<QString, MyControllerInfo> _ioHash = m_readConf->getIOInfoMap();
    //    QList<QString> _ioKeys = _ioHash.keys();

    //    QHash<QString, MyGateDeviceInfo> _gateHash = m_readConf->getGateDeviceInfoMap();
    //    QList<QString> _gateKeys = _gateHash.keys();

    //    QList<QString> _deviceKeys;
    //    _deviceKeys << _fireKeys << _ioKeys << _gateKeys << _smogKeys;

    m_fireDeviceFireInfoToUIHash.clear();
    m_smogDeviceFireInfoToUIHash.clear();
}


void Core::updateFireDeviceData(const QString &devicename, FireDeviceStatus status)
{
    setFireDeviceStatus(devicename, status);
}

void Core::updateGateDeviceData(const QString &devicename, GateDeviceStatus status)
{
    setGateDeviceStatus(devicename, status);
}

void Core::updateSmogDeviceData(const QString &devicename, SmogDeviceStatus status)
{
    setSmogDeviceStatus(devicename, status);
}

void Core::updateIODeviceData(const QString &devicename, IODeviceStatus status)
{
    setIODeviceStatus(devicename, status);
}

void Core::startExec()
{

    while (1) {
        QCoreApplication::processEvents();
        processDeviceStatusToUI();
        if(getMasterClientStatus() == false)
        {
            continue;
        }
        //检测复位、测试、释放按钮按下的次数
        //        buttonClickedCounter();
        //下面是正确的方法
        QHash<QString, bool> _hash;
        QHash<QString, QList<MultiLambdaDeviceInfo> > _eventHash;
        m_multiLambdaMutex->lock();
        _eventHash = m_eventMultiLambdaHash;
        m_multiLambdaMutex->unlock();
        QList<QString> _keys = _eventHash.keys();
        for(int i =0;i<_keys.size();i++)
        {
            QList<MultiLambdaDeviceInfo> _list = _eventHash.value(_keys.at(i));
            for(int j =0;j<_list.size();j++)
            {
                MultiLambdaDeviceInfo _struct = _list.at(j);
                if(_struct.type == "fire")
                {
                    changeFireStatus(_struct);
                    _list.replace(j, _struct);
                    _eventHash[_keys.at(i)] = _list;
                }else if(_struct.type == "gate")
                {
                    changeGateStatus(_struct);
                    _list.replace(j, _struct);
                    _eventHash[_keys.at(i)] = _list;
                }else if(_struct.type == "smog")
                {
                    changeSmogStatus(_struct);
                    _list.replace(j, _struct);
                    _eventHash[_keys.at(i)] = _list;
                }else if(_struct.type == "io")
                {
                    changeIOStatus(_struct);
                    _list.replace(j, _struct);
                    _eventHash[_keys.at(i)] = _list;
                }else if(_struct.type == "var")
                {
                    changeVarStatus(_struct);
                    _list.replace(j, _struct);
                    _eventHash[_keys.at(i)] = _list;
                }else if(_struct.type == "timer")
                {
                    changeTimerStatus(_struct);
                    _list.replace(j, _struct);
                    _eventHash[_keys.at(i)] = _list;
                }
            }
            bool _ret = canExec(_list);
            _hash.insert(_keys.at(i), _ret);
        }
        //执行设备
        if(_hash.isEmpty())
        {
            continue;
        }
        execDevice(_hash);
    }
}

void Core::processDataFromUI(const QJsonObject &obj)
{
    //selectDataByDate
    //该函数就是只是负责UI告知用户操作了哪些地方，然后这里处理，通过另一条tcp传输数据库的大数据
    //    qDebug() << obj.keys();
    if(obj["func"].toString() == "database")
    {
        QString _type = obj["type"].toString();
        quint64 _start = obj["start"].toString().toUInt();
        quint64 _end = obj["end"].toString().toUInt();
        //        qDebug() << _type << _start << _end;
        emit selectDataByDateToUIUsedTCPSignal(_type, _start, _end);
    }else if(obj["func"].toString() == "savecsv")
    {
        emit saveCsv();
    }else if(obj["func"].toString() == "getdevicename")
    {
        //        qDebug() << obj["func"].toString() << ":" << obj["type"].toString();
        QString _type = obj["type"].toString();
        emit sendMessage(getDeviceName(_type));
    }else if(obj["func"].toString() == "getfiredeviceargument")
    {
        QJsonObject _obj;
        QHash<QString, QString> _hash = m_readConf->getFireDeviceArguemnt();
        _obj.insert("func", "telluifiredeviceargument");
        _obj.insert("one", _hash.value("one"));
        _obj.insert("two", _hash.value("two"));
        _obj.insert("three", _hash.value("three"));
        _obj.insert("four", _hash.value("four"));
        emit sendMessage(_obj);
    }else if(obj["func"].toString() == "getsmogdeviceargument")
    {
        QJsonObject _obj;
        QHash<QString, QString> _hash = m_readConf->getSmogDeviceArgument();
        _obj.insert("func", "telluismogdeviceargument");
        _obj.insert("one", _hash.value("one"));
        qDebug() << "one:" << _hash.value("one");
        emit sendMessage(_obj);
    }
    else if(obj["func"].toString() == "setdevicestatus")
    {
        if(obj["type"].toString() == "fire")
        {
            QString _devicename = obj["devicename"].toString();
            QString _status = obj["status"].toString();
                        qDebug() << _devicename << ":" << _status;
            emit setFireSheildStatusSignal(_devicename, _status);
            sendUDPEvent(_devicename, _status);
        }else if(obj["type"].toString() == "smog")
        {
            QString _devicename = obj["devicename"].toString();
            QString _subdevicename = obj["subdevicename"].toString();
            QString _status = obj["status"].toString();
                        qDebug() << _devicename << _subdevicename << _status;
            emit setSmogShieldStatusSignal(_devicename, _subdevicename, _status);
            sendUDPEvent(_devicename, _subdevicename, _status);
        }else if(obj["type"].toString() == "gate")
        {
            QString _devicename = obj["devicename"].toString();
            QString _subdevicename = obj["subdevicename"].toString();
            QString _status = obj["status"].toString();
            //如果是主就执行+更新
            qDebug() << _devicename << ":" << _subdevicename << ":" << _status << __FUNCTION__ << __LINE__;
            //更新阀门拦截状态
            //先判断是左是右
            QString _leftorright;
            GateDeviceStatus _st = getGateDeviceStatus(_devicename);
            if(_st.leftname == _subdevicename)
            {
                _leftorright = "left";
            }else if(_st.rightname == _subdevicename)
            {
                _leftorright = "right";
            }
            //判断是否需要被拦截
            bool _openorclose;
            if(_status == "开到位")
            {
                _openorclose= true;
            }else{
                _openorclose = false;
            }
            //如果不等于说明无论是左还是右，都要执行的
            if(m_gateUnKnowStatusHash[_devicename][_subdevicename] != _openorclose)
            {
                //如果是左面的，取出右面的上一次的状态
                //在这里进行UDP广播
                QJsonObject _obj;
                _obj.insert("type", "updategatestatus");
                _obj.insert("devicename", _devicename);
                _obj.insert("subdevicename", _subdevicename);
                _obj.insert("status", _openorclose == true ? "true" : "false");
                m_eventUdp->addEvent(_obj);
                if(_leftorright == "left")
                {
                    emit setGateOpenCloseStatusSignal(_devicename, _subdevicename, _openorclose, _st.rightname, m_gateUnKnowStatusHash[_devicename][_st.rightname]);
                }
                if(_leftorright == "right")
                {
                    emit setGateOpenCloseStatusSignal(_devicename, _st.leftname, m_gateUnKnowStatusHash[_devicename][_st.leftname], _subdevicename, _openorclose);
                }
                m_gateUnKnowStatusHash[_devicename][_subdevicename] = _openorclose;
            }
        }
    }else if(obj["func"].toString() == "setqxz")
    {
        QString _one = obj["one"].toString();
        QString _two = obj["two"].toString();
        QString _three = obj["three"].toString();
        QString _four = obj["four"].toString();
        //写入配置文件
        QJsonObject _obj;
        _obj["type"] = "setqxz";
        _obj["one"] = _one;
        _obj["two"] = _two;
        _obj["three"] = _three;
        _obj["four"] = _four;
        m_eventUdp->addEvent(_obj);
        //发送给感温探测器类设置此值
        emit setQXZSignal(_one, _two, _three, _four);
    }else if(obj["func"].toString() == "setlmd")
    {
        QString _one = obj["one"].toString();
        QJsonObject _obj;
        _obj["type"] = "setlmd";
        _obj["one"] = _one;
        m_eventUdp->addEvent(_obj);
        qDebug() << _one << __func__ << __LINE__;
        emit setLMDSignal(_one);
    }
    else if(obj["func"].toString() == "setsystemdate")
    {
        QString _date = obj["date"].toString();
        setSystemDate(_date);
    }
}
//从设备的状态
//void Core::processClientServerStatusFromMasterOrClient(MASTERORCLIENTSTATUS _status)
//{
//    qDebug() << "从设备状态:" << _status.status;
//    if(_status.status == "故障")
//    {
//        //如果不存在从设备故障
//        if(!m_masterOrClientFaultToUIHash.contains("从设备"))
//        {
//            qDebug() << "不存在从设备故障，将其插入";
//            //插入数据库
//            MyDataBaseWord _word;
//            _word.type = FAULTTABLE;
//            _word.fault_word.date = QDateTime::currentDateTime().toTime_t();
//            _word.fault_word.devicename = "从设备";
//            _word.fault_word.casue = NETWORKERR;
//            _word.fault_word.isclear = ISNOTCLEAR;
//            emit insertDataToDataBaseSignal(_word);


//            QJsonObject _obj;
//            _obj.insert("func", "telluifirefaultevent");
//            _obj.insert("type", FAULTTABLE);
//            _obj.insert("devicename", "从设备");
//            _obj.insert("cause", NETWORKERR);
//            _obj.insert("date", QDateTime::fromTime_t(_word.fault_word.date).toString("yyyy-MM-dd hh:mm:ss"));
//            _obj.insert("isclear", ISNOTCLEAR);
//            m_masterOrClientFaultToUIHash.insert("从设备", _obj);

//            //执行一次更新阀门状态
//            //2.设置主从变量
//            setMasterClientStatus(true);
//            //3.告知阀门重新收集阀门的最新状态
//            emit reGetGateStatus();
//            //将该变量设置为false，阀门暂时无法执行操作
//            m_gateUnKnowStatusSign = false;
//        }else
//        {
//            qDebug() << "存在从设备故障，一直发送UI";
//            //如果存在就一直发送给UI
//            QJsonObject _obj = m_masterOrClientFaultToUIHash.value("从设备");
//            emit sendMessage(_obj);
//        }
//    }else if(_status.status == "成功")
//    {
//        if(m_masterOrClientFaultToUIHash.contains("从设备"))
//        {

//            QJsonObject _obj = m_masterOrClientFaultToUIHash.value("从设备");
//            if(_obj["cause"].toString() == NETWORKERR)
//            {
//                qDebug() << "从设备网络连接恢复，并且上一次是网络故障，这次发送UI清除";
//                //如果上一次的是网络连接错误，就发送一次UI清除Message，并移除上一次的信息
//                _obj["isclear"] = ISCLEAR;
//                emit sendMessage(_obj);
//                m_masterOrClientFaultToUIHash.remove("从设备");

//                //插入数据库
//                MyDataBaseWord _word;
//                _word.type = FAULTTABLE;
//                _word.fault_word.date = QDateTime::fromString(_obj["date"].toString()).toTime_t();
//                _word.fault_word.devicename = "从设备";
//                _word.fault_word.casue = NETWORKSUCC;
//                _word.fault_word.isclear = ISCLEAR;
//                emit insertDataToDataBaseSignal(_word);

//                //设置主从变量
//                setMasterClientStatus(true);
//                //3.告知阀门重新收集阀门的最新状态
//                emit reGetGateStatus();
//                //将该变量设置为false，阀门暂时无法执行操作
//                m_gateUnKnowStatusSign = false;
//            }
//        }
//    }
//}
//主设备的状态
//void Core::processMasterServerStatusFromMasterOrClient(MASTERORCLIENTSTATUS _status)
//{
//    if(_status.status == "故障")
//    {
//        if(!m_masterOrClientFaultToUIHash.contains("主设备"))
//        {
//            qDebug() << "不存在主设备故障，将其插入";
//            //插入数据库
//            MyDataBaseWord _word;
//            _word.type = FAULTTABLE;
//            _word.fault_word.date = QDateTime::currentDateTime().toTime_t();
//            _word.fault_word.devicename = "主设备";
//            _word.fault_word.casue = NETWORKERR;
//            _word.fault_word.isclear = ISNOTCLEAR;
//            emit insertDataToDataBaseSignal(_word);

//            QJsonObject _obj;
//            _obj.insert("func", "telluifirefaultevent");
//            _obj.insert("type", FAULTTABLE);
//            _obj.insert("devicename", "主设备");
//            _obj.insert("cause", NETWORKERR);
//            _obj.insert("date", QDateTime::fromTime_t(_word.fault_word.date).toString("yyyy-MM-dd hh:mm:ss"));
//            _obj.insert("isclear", ISNOTCLEAR);
//            m_masterOrClientFaultToUIHash.insert("主设备", _obj);

//            //执行一次更新阀门状态
//            //2.设置主从变量
//            setMasterClientStatus(true);
//            //3.告知阀门重新收集阀门的最新状态
//            emit reGetGateStatus();
//            //将该变量设置为false，阀门暂时无法执行操作
//            m_gateUnKnowStatusSign = false;
//        }else
//        {
//            qDebug() << "存在主设备故障，一直发送UI";
//            QJsonObject _obj = m_masterOrClientFaultToUIHash.value("主设备");
//            emit sendMessage(_obj);
//        }
//    }else if(_status.status == "存活")
//    {
//        if(m_masterOrClientFaultToUIHash.contains("主设备"))
//        {
//            QJsonObject _obj = m_masterOrClientFaultToUIHash.value("主设备");
//            if(_obj["cause"].toString() == NETWORKERR)
//            {
//                qDebug() << "从设备网络连接恢复，并且上一次是网络故障，这次发送UI清除";
//                _obj["isclear"] = ISCLEAR;
//                emit sendMessage(_obj);
//                m_masterOrClientFaultToUIHash.remove("主设备");

//                //插入数据库
//                MyDataBaseWord _word;
//                _word.type = FAULTTABLE;
//                _word.fault_word.date = QDateTime::fromString(_obj["date"].toString()).toTime_t();
//                _word.fault_word.devicename = "主设备";
//                _word.fault_word.casue = NETWORKSUCC;
//                _word.fault_word.isclear = ISCLEAR;
//                emit insertDataToDataBaseSignal(_word);

//                setMasterClientStatus(false);
//                //3.告知阀门重新收集阀门的最新状态
//                emit reGetGateStatus();
//                //将该变量设置为false，阀门暂时无法执行操作
//                m_gateUnKnowStatusSign = false;
//            }
//        }

//    }
//}

void Core::processMasterOrClientStatus(QString morc, QString norf)
{
    //第一个参数是master client
    //第二个参数是正常 故障
    qDebug() << morc << ":" << norf;
    if(morc == "master")
    {
        if(norf == "normal")
        {
            setMasterClientStatus(false);
            if(m_masterOrClientFaultToUIHash.contains("主设备"))
            {
                QJsonObject _obj = m_masterOrClientFaultToUIHash.value("主设备");
                if(_obj["cause"].toString() == NETWORKERR)
                {
                    qDebug() << "主设备网络连接恢复，并且上一次是网络故障，这次发送UI清除";
                    _obj["isclear"] = ISCLEAR;
                    emit sendMessage(_obj);
                    m_masterOrClientFaultToUIHash.remove("主设备");
                    //插入数据库
                    MyDataBaseWord _word;
                    _word.type = FAULTTABLE;
                    _word.fault_word.date = QDateTime::fromString(_obj["date"].toString()).toTime_t();
                    _word.fault_word.devicename = "主设备";
                    _word.fault_word.casue = NETWORKSUCC;
                    _word.fault_word.isclear = ISCLEAR;
                    emit insertDataToDataBaseSignal(_word);
                    //3.告知阀门重新收集阀门的最新状态
                    emit reGetGateStatus();
                    //将该变量设置为false，阀门暂时无法执行操作
                    m_gateUnKnowStatusSign = false;
                }
            }
        }
        if(norf == "fault")
        {
            if(!m_masterOrClientFaultToUIHash.contains("主设备"))
            {
                qDebug() << "主设备网络连接故障";
                MyDataBaseWord _word;
                _word.type = FAULTTABLE;
                _word.fault_word.date = QDateTime::currentDateTime().toTime_t();
                _word.fault_word.devicename = "主设备";
                _word.fault_word.casue = NETWORKERR;
                _word.fault_word.isclear = ISNOTCLEAR;
                emit insertDataToDataBaseSignal(_word);

                QJsonObject _obj;
                _obj.insert("func", "telluifirefaultevent");
                _obj.insert("type", FAULTTABLE);
                _obj.insert("devicename", "主设备");
                _obj.insert("cause", NETWORKERR);
                _obj.insert("date", QDateTime::fromTime_t(_word.fault_word.date).toString("yyyy-MM-dd hh:mm:ss"));
                _obj.insert("isclear", ISNOTCLEAR);
                m_masterOrClientFaultToUIHash.insert("主设备", _obj);
                //执行一次更新阀门状态
                //2.设置主从变量
                setMasterClientStatus(true);
                //3.告知阀门重新收集阀门的最新状态
                emit reGetGateStatus();
                //将该变量设置为false，阀门暂时无法执行操作
                m_gateUnKnowStatusSign = false;
            }else
            {
                QJsonObject _obj = m_masterOrClientFaultToUIHash.value("主设备");
                emit sendMessage(_obj);
            }
        }
    }
    //下面是从设备的状态
    if(morc == "client")
    {
        if(norf == "normal")
        {
            setMasterClientStatus(true);
            if(m_masterOrClientFaultToUIHash.contains("从设备"))
            {
                QJsonObject _obj = m_masterOrClientFaultToUIHash.value("从设备");
                if(_obj["cause"].toString() == NETWORKERR)
                {
                    qDebug() << "从设备网络连接恢复，并且上一次是网络故障，这次发送UI清除";
                    _obj["isclear"] = ISCLEAR;
                    emit sendMessage(_obj);
                    m_masterOrClientFaultToUIHash.remove("从设备");

                    //插入数据库
                    MyDataBaseWord _word;
                    _word.type = FAULTTABLE;
                    _word.fault_word.date = QDateTime::fromString(_obj["date"].toString()).toTime_t();
                    _word.fault_word.devicename = "从设备";
                    _word.fault_word.casue = NETWORKSUCC;
                    _word.fault_word.isclear = ISCLEAR;
                    emit insertDataToDataBaseSignal(_word);
                    //3.告知阀门重新收集阀门的最新状态
                    emit reGetGateStatus();
                    //将该变量设置为false，阀门暂时无法执行操作
                    m_gateUnKnowStatusSign = false;
                }
            }
        }
        if(norf == "fault")
        {
            if(!m_masterOrClientFaultToUIHash.contains("从设备"))
            {
                qDebug() << "从设备网络连接故障";
                //插入数据库
                MyDataBaseWord _word;
                _word.type = FAULTTABLE;
                _word.fault_word.date = QDateTime::currentDateTime().toTime_t();
                _word.fault_word.devicename = "从设备";
                _word.fault_word.casue = NETWORKERR;
                _word.fault_word.isclear = ISNOTCLEAR;
                emit insertDataToDataBaseSignal(_word);
                QJsonObject _obj;
                _obj.insert("func", "telluifirefaultevent");
                _obj.insert("type", FAULTTABLE);
                _obj.insert("devicename", "从设备");
                _obj.insert("cause", NETWORKERR);
                _obj.insert("date", QDateTime::fromTime_t(_word.fault_word.date).toString("yyyy-MM-dd hh:mm:ss"));
                _obj.insert("isclear", ISNOTCLEAR);
                m_masterOrClientFaultToUIHash.insert("从设备", _obj);
                setMasterClientStatus(true);
                //执行一次更新阀门状态
                //3.告知阀门重新收集阀门的最新状态
                emit reGetGateStatus();
                //将该变量设置为false，阀门暂时无法执行操作
                m_gateUnKnowStatusSign = false;
            }else
            {
                //                qDebug() << "存在从设备故障，一直发送UI";
                //如果存在就一直发送给UI
                QJsonObject _obj = m_masterOrClientFaultToUIHash.value("从设备");
                emit sendMessage(_obj);
            }
        }
    }
}

void Core::processEventFromEventUDP(MyDataBaseWord word)
{
    //报告给UI发生事件
    emit sendMessage(getEventUIWord(word));
}

void Core::processFireShieldStatusFromEventUDP(QString devicename, QString status)
{
    m_fireSmogDeviceStatusHash[devicename] = status;
    //通知本地设置配置文件和更新内存等
    emit setFireSheildStatusSignal(devicename, status);
    //通过UI告知感温设备状态被更改
    QJsonObject _obj;
    _obj["func"] = "telluifireshieldstatus";
    _obj["devicename"] = devicename;
    _obj["status"] = status;
    emit sendMessage(_obj);
}

void Core::processSmogShieldStatusFromEventUDP(QString devicename, QString subdevicename, QString status)
{
    m_fireSmogDeviceStatusHash[subdevicename] = status;
    //通知本地设置配置文件和更新内存等
    emit setSmogShieldStatusSignal(devicename, subdevicename, status);
    //通过UI告知感烟设备被更改
    QJsonObject _obj;
    _obj["func"] = "telluismogshieldstatus";
    _obj["devicename"] = devicename;
    _obj["subdevicename"] = subdevicename;
    _obj["status"] = status;
    emit sendMessage(_obj);
}

void Core::processQXZFromEventUDP(QString v1, QString v2 , QString v3, QString v4)
{
    //告知UI更新曲线值
    QJsonObject _obj;
    _obj.insert("func", "telluifiredeviceargument");
    _obj.insert("one", v1);
    _obj.insert("two", v2);
    _obj.insert("three", v3);
    _obj.insert("four", v4);
    emit sendMessage(_obj);
    //告知配置文件更新QXZ
    emit setQXZSignal(v1, v2, v3, v4);
}

void Core::processLMDFromEventUDP(QString v1)
{
    QJsonObject _obj;
    QHash<QString, QString> _hash = m_readConf->getSmogDeviceArgument();
    _obj.insert("func", "telluismogdeviceargument");
    _obj.insert("one", v1);
    emit sendMessage(_obj);
    emit setLMDSignal(v1);
}

void Core::processIOStatusFromEventUDP(QJsonObject obj)
{
    //更新IO的拦截状态

    //IO的拦截也要设置
    QString _devicename = obj["devicename"].toString();
    int _count = obj["count"].toInt();
    if(_count == 1)
    {
        QString _subname1 = obj["subdevicename1"].toString();
        bool _status1 = obj["status1"].toBool();
        emit setIOOutputStatusSignal(_devicename, _count,
                                     _subname1, _status1,
                                     "", false,
                                     "", false,
                                     "", false,
                                     "", false,
                                     "", false);
        QJsonObject _obj = m_ioDeviceOpenCloseToUIHash[_subname1];
        if(_status1 == true)
        {
            _obj["cause"] = "开到位";
        }else{
            _obj["cause"] = "关到位";
        }
        m_ioDeviceOpenCloseToUIHash[_subname1] = _obj;
    }else if(_count == 2)
    {
        QString _subname1 = obj["subdevicename1"].toString();
        bool _status1 = obj["status1"].toBool();
        QString _subname2 = obj["subdevicename2"].toString();
        bool _status2 = obj["status2"].toBool();
        emit setIOOutputStatusSignal(_devicename, _count,
                                     _subname1, _status1,
                                     _subname2, _status2,
                                     "", false,
                                     "", false,
                                     "", false,
                                     "", false);
        QJsonObject _obj = m_ioDeviceOpenCloseToUIHash[_subname1];
        if(_status1 == true)
        {
            _obj["cause"] = "开到位";
        }else{
            _obj["cause"] = "关到位";
        }
        m_ioDeviceOpenCloseToUIHash[_subname1] = _obj;

        QJsonObject _obj2 = m_ioDeviceOpenCloseToUIHash[_subname2];
        if(_status2 == true)
        {
            _obj2["cause"] = "开到位";
        }else{
            _obj2["cause"] = "关到位";
        }
        m_ioDeviceOpenCloseToUIHash[_subname2] = _obj2;
    }else if(_count == 3)
    {
        QString _subname1 = obj["subdevicename1"].toString();
        bool _status1 = obj["status1"].toBool();
        QString _subname2 = obj["subdevicename2"].toString();
        bool _status2 = obj["status2"].toBool();
        QString _subname3 = obj["subdevicename3"].toString();
        bool _status3 = obj["status3"].toBool();
        emit setIOOutputStatusSignal(_devicename, _count,
                                     _subname1, _status1,
                                     _subname2, _status2,
                                     _subname3, _status3,
                                     "", false,
                                     "", false,
                                     "", false);
        QJsonObject _obj = m_ioDeviceOpenCloseToUIHash[_subname1];
        if(_status1 == true)
        {
            _obj["cause"] = "开到位";
        }else{
            _obj["cause"] = "关到位";
        }
        m_ioDeviceOpenCloseToUIHash[_subname1] = _obj;

        QJsonObject _obj2 = m_ioDeviceOpenCloseToUIHash[_subname2];
        if(_status2 == true)
        {
            _obj2["cause"] = "开到位";
        }else{
            _obj2["cause"] = "关到位";
        }
        m_ioDeviceOpenCloseToUIHash[_subname2] = _obj2;

        QJsonObject _obj3 = m_ioDeviceOpenCloseToUIHash[_subname3];
        if(_status3 == true)
        {
            _obj3["cause"] = "开到位";
        }else{
            _obj3["cause"] = "关到位";
        }
        m_ioDeviceOpenCloseToUIHash[_subname3] = _obj3;

    }else if(_count == 4)
    {
        QString _subname1 = obj["subdevicename1"].toString();
        bool _status1 = obj["status1"].toBool();
        QString _subname2 = obj["subdevicename2"].toString();
        bool _status2 = obj["status2"].toBool();
        QString _subname3 = obj["subdevicename3"].toString();
        bool _status3 = obj["status3"].toBool();
        QString _subname4 = obj["subdevicename4"].toString();
        bool _status4 = obj["status4"].toBool();
        emit setIOOutputStatusSignal(_devicename, _count,
                                     _subname1, _status1,
                                     _subname2, _status2,
                                     _subname3, _status3,
                                     _subname4, _status4,
                                     "", false,
                                     "", false);
        QJsonObject _obj = m_ioDeviceOpenCloseToUIHash[_subname1];
        if(_status1 == true)
        {
            _obj["cause"] = "开到位";
        }else{
            _obj["cause"] = "关到位";
        }
        m_ioDeviceOpenCloseToUIHash[_subname1] = _obj;

        QJsonObject _obj2 = m_ioDeviceOpenCloseToUIHash[_subname2];
        if(_status2 == true)
        {
            _obj2["cause"] = "开到位";
        }else{
            _obj2["cause"] = "关到位";
        }
        m_ioDeviceOpenCloseToUIHash[_subname2] = _obj2;

        QJsonObject _obj3 = m_ioDeviceOpenCloseToUIHash[_subname3];
        if(_status3 == true)
        {
            _obj3["cause"] = "开到位";
        }else{
            _obj3["cause"] = "关到位";
        }
        m_ioDeviceOpenCloseToUIHash[_subname3] = _obj3;

        QJsonObject _obj4 = m_ioDeviceOpenCloseToUIHash[_subname4];
        if(_status4 == true)
        {
            _obj4["cause"] = "开到位";
        }else{
            _obj4["cause"] = "关到位";
        }
        m_ioDeviceOpenCloseToUIHash[_subname4] = _obj4;
    }else if(_count == 5)
    {
        QString _subname1 = obj["subdevicename1"].toString();
        bool _status1 = obj["status1"].toBool();
        QString _subname2 = obj["subdevicename2"].toString();
        bool _status2 = obj["status2"].toBool();
        QString _subname3 = obj["subdevicename3"].toString();
        bool _status3 = obj["status3"].toBool();
        QString _subname4 = obj["subdevicename4"].toString();
        bool _status4 = obj["status4"].toBool();
        QString _subname5 = obj["subdevicename5"].toString();
        bool _status5 = obj["status5"].toBool();
        emit setIOOutputStatusSignal(_devicename, _count,
                                     _subname1, _status1,
                                     _subname2, _status2,
                                     _subname3, _status3,
                                     _subname4, _status4,
                                     _subname5, _status5,
                                     "", false);

        QJsonObject _obj = m_ioDeviceOpenCloseToUIHash[_subname1];
        if(_status1 == true)
        {
            _obj["cause"] = "开到位";
        }else{
            _obj["cause"] = "关到位";
        }
        m_ioDeviceOpenCloseToUIHash[_subname1] = _obj;

        QJsonObject _obj2 = m_ioDeviceOpenCloseToUIHash[_subname2];
        if(_status2 == true)
        {
            _obj2["cause"] = "开到位";
        }else{
            _obj2["cause"] = "关到位";
        }
        m_ioDeviceOpenCloseToUIHash[_subname2] = _obj2;

        QJsonObject _obj3 = m_ioDeviceOpenCloseToUIHash[_subname3];
        if(_status3 == true)
        {
            _obj3["cause"] = "开到位";
        }else{
            _obj3["cause"] = "关到位";
        }
        m_ioDeviceOpenCloseToUIHash[_subname3] = _obj3;

        QJsonObject _obj4 = m_ioDeviceOpenCloseToUIHash[_subname4];
        if(_status4 == true)
        {
            _obj4["cause"] = "开到位";
        }else{
            _obj4["cause"] = "关到位";
        }
        m_ioDeviceOpenCloseToUIHash[_subname4] = _obj4;

        QJsonObject _obj5 = m_ioDeviceOpenCloseToUIHash[_subname5];
        if(_status5 == true)
        {
            _obj5["cause"] = "开到位";
        }else{
            _obj5["cause"] = "关到位";
        }
        m_ioDeviceOpenCloseToUIHash[_subname5] = _obj5;
    }else if(_count == 6)
    {
        QString _subname1 = obj["subdevicename1"].toString();
        bool _status1 = obj["status1"].toBool();
        QString _subname2 = obj["subdevicename2"].toString();
        bool _status2 = obj["status2"].toBool();
        QString _subname3 = obj["subdevicename3"].toString();
        bool _status3 = obj["status3"].toBool();
        QString _subname4 = obj["subdevicename4"].toString();
        bool _status4 = obj["status4"].toBool();
        QString _subname5 = obj["subdevicename5"].toString();
        bool _status5 = obj["status5"].toBool();
        QString _subname6 = obj["subdevicename6"].toString();
        bool _status6 = obj["status6"].toBool();
        emit setIOOutputStatusSignal(_devicename, _count,
                                     _subname1, _status1,
                                     _subname2, _status2,
                                     _subname3, _status3,
                                     _subname4, _status4,
                                     _subname5, _status5,
                                     _subname6, _status6);
        QJsonObject _obj = m_ioDeviceOpenCloseToUIHash[_subname1];
        if(_status1 == true)
        {
            _obj["cause"] = "开到位";
        }else{
            _obj["cause"] = "关到位";
        }
        m_ioDeviceOpenCloseToUIHash[_subname1] = _obj;

        QJsonObject _obj2 = m_ioDeviceOpenCloseToUIHash[_subname2];
        if(_status2 == true)
        {
            _obj2["cause"] = "开到位";
        }else{
            _obj2["cause"] = "关到位";
        }
        m_ioDeviceOpenCloseToUIHash[_subname2] = _obj2;

        QJsonObject _obj3 = m_ioDeviceOpenCloseToUIHash[_subname3];
        if(_status3 == true)
        {
            _obj3["cause"] = "开到位";
        }else{
            _obj3["cause"] = "关到位";
        }
        m_ioDeviceOpenCloseToUIHash[_subname3] = _obj3;

        QJsonObject _obj4 = m_ioDeviceOpenCloseToUIHash[_subname4];
        if(_status4 == true)
        {
            _obj4["cause"] = "开到位";
        }else{
            _obj4["cause"] = "关到位";
        }
        m_ioDeviceOpenCloseToUIHash[_subname4] = _obj4;

        QJsonObject _obj5 = m_ioDeviceOpenCloseToUIHash[_subname5];
        if(_status5 == true)
        {
            _obj5["cause"] = "开到位";
        }else{
            _obj5["cause"] = "关到位";
        }
        m_ioDeviceOpenCloseToUIHash[_subname5] = _obj5;

        QJsonObject _obj6 = m_ioDeviceOpenCloseToUIHash[_subname6];
        if(_status6 == true)
        {
            _obj6["cause"] = "开到位";
        }else{
            _obj6["cause"] = "关到位";
        }
        m_ioDeviceOpenCloseToUIHash[_subname6] = _obj6;
    }
}

void Core::processResetStatusFromEventUDP()
{
    onResetClicked();
    QJsonObject _obj;
    _obj.insert("func", "telluiresetstatus");
    emit sendMessage(_obj);
}

void Core::startMultiLambdaTimerSlot(QString reason, QString doit)
{
    if(!m_eventMultiLambdaTimerHash.contains(reason))
    {
        return;
    }
    QHash<QString, MyTimer*> _hash =  m_eventMultiLambdaTimerHash[reason];

    MyTimer *_timer = _hash[_hash.keys().at(0)];
    if(doit == "打开")
    {
        if(_timer->isActive())
        {
            return;
        }
        //        qDebug() << reason << doit;
        //        qDebug() << m_eventMultiLambdaTimerHash.keys();
        //        qDebug() << _hash.keys().at(0);
        //        qDebug() << doit << reason;
        _timer->start();
    }else if(doit == "关闭")
    {
        QString _resault = _timer->getResault();
        setMultiLambdaTimerStatus(_resault, false);
        setMultiLambdaVariableStatus(_resault, false);
        qDebug() << "发送UI清除标志" << _resault;
        QJsonObject _obj = m_timerDeviceFaultInfoToUIHash.value(_resault);
        _obj["isclear"] = ISCLEAR;
        m_timerDeviceFaultInfoToUIHash[_resault] = _obj;
        emit sendMessage(_obj);
        if(_timer->isActive())
        {
            //            qDebug() << reason << doit;
            //            qDebug() << m_eventMultiLambdaTimerHash.keys();
            //            qDebug() << _hash.keys().at(0);
            //            qDebug() << doit << reason;
            //将配置文件中的变量还要取消


            //QJsonObject _obj = getUIObj(_resault)
            //emit sendMessage(_obj);
            _timer->stop();
        }
    }
}

void Core::processDataFromMVB(MVBToCoreData mcd)
{
    if(mcd.type == "设置时间")
    {

        setSystemDate(mcd.data);
    }else if(mcd.type == "隔离复位")
    {
        QList<QString> _keys = m_fireSmogDeviceStatusHash.keys();
        for(int i =0;i< _keys.size();i++)
        {
            FireDeviceStatus _status = m_fireDeviceStatusHash.value(_keys.at(i));
            if(m_fireSmogDeviceStatusHash.value(_keys.at(i)) == "已屏蔽")
            {
                qDebug() << "这里是隔离复位：" << _keys.at(i) <<"发现屏蔽,解除屏蔽";
                emit setFireSheildStatusSignal(_keys.at(i), "未屏蔽");
            }
            if(_status.shieldstatus == "已屏蔽")
            {

            }
        }
        emit setSmogShieldStatusSignal("感烟传感器B区", "六车司机室感烟探测器", "未屏蔽");
        emit setSmogShieldStatusSignal("感烟传感器A区", "一车司机室感烟探测器", "未屏蔽");
        clearButtonClickedCount();
    }
}

quint16 high_low_byte(quint16 num) {
    // 高低位转换
    quint16 high_byte = (num & 0xFF00) >> 8; // 获取高8位
    quint16 low_byte = num & 0x00FF; // 获取低8位
    return (low_byte << 8) | high_byte; // 将高低字节合并成一个16位无符号整数并返回
}
void Core::processDataToMVB()
{


    //阀门设置思路如下：
    //QHash<QString, QPair<QString, QPair<int, QPair<int, int > > > > _openedByGateDeviceHash;
    //QHash<QString, QPair<QString, QPair<int, QPair<int, int > > > > _closedByGateDeviceHash;
    //QHash<QString, QPair<QString, QPair<int, QPair<int, int > > > > _faultByGateDeviceHash;

    //感温设置思路如下：
    //QHash<QString, QPair<QString, QPair<int, QPair<int, int > > > > _fireBySmogDeviceHash;
    //QHash<QString, QPair<QString, QPair<int, QPair<int, int > > > > _faultBySmogDeviceHash;
    //QHash<QString, QPair<QString, QPair<int, QPair<int, int > > > > _shieldBySmogDeviceHash;

    //IO设置思路如下：
    //QHash<QString, QPair<QString, QPair<int, QPair<int, int > > > > _yeweidiByIODeviceHash;
    //QHash<QString, QPair<QString, QPair<int, QPair<int, int > > > > _yalidiByIODeviceHash;
    //QHash<QString, QPair<QString, QPair<int, QPair<int, int > > > > _buttonByIODeviceHash;
    DataRequestResponseFrame _struct;
    memset(&_struct, 0, sizeof(DataRequestResponseFrame));








    QList<QString> _fireKeys = m_fireByFireDeviceHash.keys();
    unsigned char buf[32];
    memset(&buf, 0, sizeof(unsigned char) * 32);
    //手动更新生命信号
    m_mvbLiveSignal += 1;
    if(m_mvbLiveSignal == 65534)
    {
        m_mvbLiveSignal = 0;
    }
    quint16 _tempLiveSiganl = high_low_byte(m_mvbLiveSignal);
    memcpy(buf, &_tempLiveSiganl, sizeof(quint16));
    quint8 _softwareversionhigh = 1;
    quint8 _softwareversionlow = 0;
    buf[2] = _softwareversionhigh;
    buf[3] = _softwareversionlow;
    quint16 _awendu = getValueByIO("A框架一区控制器", "A框架水温");
    qint8 _akuangjiawendu = (qint16)_awendu;
    quint16 _bwendu = getValueByIO("B框架一区控制器", "B框架水温");
    qint8 _bkuangjiawendu = (qint16)_bwendu;
    quint16 _akuangjianeibuwendu = getValueByIO("A框架一区控制器", "A框架内部温度");
    qint8 _akuangjianbwendu = (qint16)_akuangjianeibuwendu;
    buf[4] = _akuangjianbwendu;
    quint16 _bkuangjianeibuwendu = getValueByIO("B框架一区控制器", "B框架内部温度");
    qint8 _bkuangjianbwendu = (qint16)_bkuangjianeibuwendu;
    buf[7] = _bkuangjianbwendu;
    buf[5] = _akuangjiawendu;
    buf[6] = _bkuangjiawendu;


    if(_akuangjianbwendu > 100)
    {
        buf[4] = 110;
    }
    if(_bkuangjianbwendu > 100)
    {
        buf[7] = 110;
    }
    if(_akuangjiawendu > 100 )
    {
        buf[5] = 110;
    }else if(_akuangjiawendu < 0)
    {
        buf[5] = 0;
    }
    if(_bkuangjiawendu > 100 )
    {
        buf[6] = 110;
    }else if(_bkuangjiawendu < 0)
    {
        buf[6] = 0;
    }



    //        qDebug() << "akuangjiawendu:" << _akuangjiawendu
    //                 << "bkuangjiawendu:" << _bkuangjiawendu;
    //这里填充完感温探测器的火警信息
    for(int i =0;i<m_fireByFireDeviceHash.size();i++)
    {
        QPair<int, QPair<int, int > > _value = m_fireByFireDeviceHash[_fireKeys.at(i)];
        bool _ret = getFireInfoByFire(_fireKeys.at(i));
        quint8 _v;
        if(_ret)
        {
            _v = 1;
        }else
        {
            _v = 0;
        }
        _value.first = _v;
        setDataByBit(buf[_value.second.first], _value.second.second, _value.first);
    }
    //填充感烟探测器的火警信息


    //下面要移到其它字节

    bool _ret, _ret1, _ret2, _ret3, _ret4, _ret5;






    _ret1 = getFireInfoBySmog("感烟传感器A区", "一车F柜一号感烟探测器");
    _ret2 = getFireInfoBySmog("感烟传感器A区", "一车F柜二号感烟探测器");
    _ret3 = _ret1 & _ret2;//ret3是一车D柜一号二号感烟探测器火警
    _ret1 = getFaultInfoBySmog("感烟传感器A区", "一车F柜一号感烟探测器");
    _ret2 = getFireInfoBySmog("感烟传感器A区", "一车F柜二号感烟探测器");
    _ret4 = _ret1 & _ret2;//ret4是一车D柜一号感烟探测器故障和一车H柜二号感烟探测器火警
    _ret1 = getFireInfoBySmog("感烟传感器A区", "一车F柜一号感烟探测器");
    _ret2 = getFaultInfoBySmog("感烟传感器A区", "一车F柜二号感烟探测器");
    _ret5 = _ret1 & _ret2;//ret5是一车D柜一号感烟探测器故障和一车D柜二号感烟探测器火警
    _ret = _ret3 | _ret4 | _ret5;
    setDataByBit(buf[8], 7, _ret == true ? 1 : 0);


    _ret1 = getFireInfoBySmog("感烟传感器A区", "一车R柜一号感烟探测器");
    _ret2 = getFireInfoBySmog("感烟传感器A区", "一车R柜二号感烟探测器");
    _ret3 = _ret1 & _ret2;//ret3是一车D柜一号二号感烟探测器火警
    _ret1 = getFaultInfoBySmog("感烟传感器A区", "一车R柜一号感烟探测器");
    _ret2 = getFireInfoBySmog("感烟传感器A区", "一车R柜二号感烟探测器");
    _ret4 = _ret1 & _ret2;//ret4是一车D柜一号感烟探测器故障和一车H柜二号感烟探测器火警
    _ret1 = getFireInfoBySmog("感烟传感器A区", "一车R柜一号感烟探测器");
    _ret2 = getFaultInfoBySmog("感烟传感器A区", "一车R柜二号感烟探测器");
    _ret5 = _ret1 & _ret2;//ret5是一车D柜一号感烟探测器故障和一车D柜二号感烟探测器火警
    _ret = _ret3 | _ret4 | _ret5;
    setDataByBit(buf[8], 8, _ret == true ? 1 : 0);


    _ret1 = getFireInfoBySmog("感烟传感器A区", "二车F柜一号感烟探测器");
    _ret2 = getFireInfoBySmog("感烟传感器A区", "二车F柜二号感烟探测器");
    _ret3 = _ret1 & _ret2;
    _ret1 = getFaultInfoBySmog("感烟传感器A区", "二车F柜一号感烟探测器");
    _ret2 = getFireInfoBySmog("感烟传感器A区", "二车F柜二号感烟探测器");
    _ret4 = _ret1 & _ret2;
    _ret1 = getFireInfoBySmog("感烟传感器A区", "二车F柜一号感烟探测器");
    _ret2 = getFaultInfoBySmog("感烟传感器A区", "二车F柜二号感烟探测器");
    _ret5 = _ret1 & _ret2;
    _ret = _ret3 | _ret4 | _ret5;
    setDataByBit(buf[9], 7, _ret == true ? 1 : 0);



    _ret1 = getFireInfoBySmog("感烟传感器A区", "二车R柜一号感烟探测器");
    _ret2 = getFireInfoBySmog("感烟传感器A区", "二车R柜二号感烟探测器");
    _ret3 = _ret1 & _ret2;
    _ret1 = getFaultInfoBySmog("感烟传感器A区", "二车R柜一号感烟探测器");
    _ret2 = getFireInfoBySmog("感烟传感器A区", "二车R柜二号感烟探测器");
    _ret4 = _ret1 & _ret2;
    _ret1 = getFireInfoBySmog("感烟传感器A区", "二车R柜一号感烟探测器");
    _ret2 = getFaultInfoBySmog("感烟传感器A区", "二车R柜二号感烟探测器");
    _ret5 = _ret1 & _ret2;
    _ret = _ret3 | _ret4 | _ret5;
    setDataByBit(buf[9], 8, _ret == true ? 1 : 0);


    _ret1 = getFireInfoBySmog("感烟传感器A区", "三车F柜一号感烟探测器");
    _ret2 = getFireInfoBySmog("感烟传感器A区", "三车F柜二号感烟探测器");
    _ret3 = _ret1 & _ret2;
    _ret1 = getFaultInfoBySmog("感烟传感器A区", "三车F柜一号感烟探测器");
    _ret2 = getFireInfoBySmog("感烟传感器A区", "三车F柜二号感烟探测器");
    _ret4 = _ret1 & _ret2;
    _ret1 = getFireInfoBySmog("感烟传感器A区", "三车F柜一号感烟探测器");
    _ret2 = getFaultInfoBySmog("感烟传感器A区", "三车F柜二号感烟探测器");
    _ret5 = _ret1 & _ret2;
    _ret = _ret3 | _ret4 | _ret5;
    setDataByBit(buf[10], 7, _ret == true ? 1 : 0);



    _ret1 = getFireInfoBySmog("感烟传感器A区", "三车R柜一号感烟探测器");
    _ret2 = getFireInfoBySmog("感烟传感器A区", "三车R柜二号感烟探测器");
    _ret3 = _ret1 & _ret2;
    _ret1 = getFaultInfoBySmog("感烟传感器A区", "三车R柜一号感烟探测器");
    _ret2 = getFireInfoBySmog("感烟传感器A区", "三车R柜二号感烟探测器");
    _ret4 = _ret1 & _ret2;
    _ret1 = getFireInfoBySmog("感烟传感器A区", "三车R柜一号感烟探测器");
    _ret2 = getFaultInfoBySmog("感烟传感器A区", "三车R柜二号感烟探测器");
    _ret5 = _ret1 & _ret2;
    _ret = _ret3 | _ret4 | _ret5;
    setDataByBit(buf[10], 8, _ret == true ? 1 : 0);


    _ret1 = getFireInfoBySmog("感烟传感器B区", "四车F柜一号感烟探测器");
    _ret2 = getFireInfoBySmog("感烟传感器B区", "四车F柜二号感烟探测器");
    _ret3 = _ret1 & _ret2;
    _ret1 = getFaultInfoBySmog("感烟传感器B区", "四车F柜一号感烟探测器");
    _ret2 = getFireInfoBySmog("感烟传感器B区", "四车F柜二号感烟探测器");
    _ret4 = _ret1 & _ret2;
    _ret1 = getFireInfoBySmog("感烟传感器B区", "四车F柜一号感烟探测器");
    _ret2 = getFaultInfoBySmog("感烟传感器B区", "四车F柜二号感烟探测器");
    _ret5 = _ret1 & _ret2;
    _ret = _ret3 | _ret4 | _ret5;
    setDataByBit(buf[11], 7, _ret == true ? 1 : 0);



    _ret1 = getFireInfoBySmog("感烟传感器B区", "四车R柜一号感烟探测器");
    _ret2 = getFireInfoBySmog("感烟传感器B区", "四车R柜二号感烟探测器");
    _ret3 = _ret1 & _ret2;
    _ret1 = getFaultInfoBySmog("感烟传感器B区", "四车R柜一号感烟探测器");
    _ret2 = getFireInfoBySmog("感烟传感器B区", "四车R柜二号感烟探测器");
    _ret4 = _ret1 & _ret2;
    _ret1 = getFireInfoBySmog("感烟传感器B区", "四车R柜一号感烟探测器");
    _ret2 = getFaultInfoBySmog("感烟传感器B区", "四车R柜二号感烟探测器");
    _ret5 = _ret1 & _ret2;
    _ret = _ret3 | _ret4 | _ret5;
    setDataByBit(buf[11], 8, _ret == true ? 1 : 0);


    _ret1 = getFireInfoBySmog("感烟传感器B区", "五车F柜一号感烟探测器");
    _ret2 = getFireInfoBySmog("感烟传感器B区", "五车F柜二号感烟探测器");
    _ret3 = _ret1 & _ret2;
    _ret1 = getFaultInfoBySmog("感烟传感器B区", "五车F柜一号感烟探测器");
    _ret2 = getFireInfoBySmog("感烟传感器B区", "五车F柜二号感烟探测器");
    _ret4 = _ret1 & _ret2;
    _ret1 = getFireInfoBySmog("感烟传感器B区", "五车F柜一号感烟探测器");
    _ret2 = getFaultInfoBySmog("感烟传感器B区", "五车F柜二号感烟探测器");
    _ret5 = _ret1 & _ret2;
    _ret = _ret3 | _ret4 | _ret5;
    setDataByBit(buf[12], 7, _ret == true ? 1 : 0);


    _ret1 = getFireInfoBySmog("感烟传感器B区", "五车R柜一号感烟探测器");
    _ret2 = getFireInfoBySmog("感烟传感器B区", "五车R柜二号感烟探测器");
    _ret3 = _ret1 & _ret2;
    _ret1 = getFaultInfoBySmog("感烟传感器B区", "五车R柜一号感烟探测器");
    _ret2 = getFireInfoBySmog("感烟传感器B区", "五车R柜二号感烟探测器");
    _ret4 = _ret1 & _ret2;
    _ret1 = getFireInfoBySmog("感烟传感器B区", "五车R柜一号感烟探测器");
    _ret2 = getFaultInfoBySmog("感烟传感器B区", "五车R柜二号感烟探测器");
    _ret5 = _ret1 & _ret2;
    _ret = _ret3 | _ret4 | _ret5;
    setDataByBit(buf[12], 8, _ret == true ? 1 : 0);










    _ret = getFireInfoBySmog("感烟传感器A区", "一车司机室感烟探测器");
    setDataByBit(buf[14], 4, _ret == true ? 1 : 0);
    _ret1 = getFireInfoBySmog("感烟传感器A区", "一车D柜一号感烟探测器");
    _ret2 = getFireInfoBySmog("感烟传感器A区", "一车D柜二号感烟探测器");
    _ret3 = _ret1 & _ret2;//ret3是一车D柜一号二号感烟探测器火警
    _ret1 = getFaultInfoBySmog("感烟传感器A区", "一车D柜一号感烟探测器");
    _ret2 = getFireInfoBySmog("感烟传感器A区", "一车D柜二号感烟探测器");
    _ret4 = _ret1 & _ret2;//ret4是一车D柜一号感烟探测器故障和一车D柜二号感烟探测器火警
    _ret1 = getFireInfoBySmog("感烟传感器A区", "一车D柜一号感烟探测器");
    _ret2 = getFaultInfoBySmog("感烟传感器A区", "一车D柜二号感烟探测器");
    _ret5 = _ret1 & _ret2;//ret5是一车D柜一号感烟探测器故障和一车D柜二号感烟探测器火警
    _ret = _ret3 | _ret4 | _ret5;
    setDataByBit(buf[14], 5, _ret == true ? 1 : 0);

    _ret1 = getFireInfoBySmog("感烟传感器A区", "一车H柜一号感烟探测器");
    _ret2 = getFireInfoBySmog("感烟传感器A区", "一车H柜二号感烟探测器");
    _ret3 = _ret1 & _ret2;//ret3是一车D柜一号二号感烟探测器火警
    _ret1 = getFaultInfoBySmog("感烟传感器A区", "一车H柜一号感烟探测器");
    _ret2 = getFireInfoBySmog("感烟传感器A区", "一车H柜二号感烟探测器");
    _ret4 = _ret1 & _ret2;//ret4是一车D柜一号感烟探测器故障和一车H柜二号感烟探测器火警
    _ret1 = getFireInfoBySmog("感烟传感器A区", "一车H柜一号感烟探测器");
    _ret2 = getFaultInfoBySmog("感烟传感器A区", "一车H柜二号感烟探测器");
    _ret5 = _ret1 & _ret2;//ret5是一车D柜一号感烟探测器故障和一车D柜二号感烟探测器火警
    _ret = _ret3 | _ret4 | _ret5;
    setDataByBit(buf[14], 6, _ret == true ? 1 : 0);


    _ret = getFireInfoBySmog("感烟传感器B区", "六车司机室感烟探测器");
    setDataByBit(buf[14], 1, _ret == true ? 1 : 0);

    _ret1 = getFireInfoBySmog("感烟传感器B区", "六车D柜一号感烟探测器");
    _ret2 = getFireInfoBySmog("感烟传感器B区", "六车D柜二号感烟探测器");
    _ret3 = _ret1 & _ret2;
    _ret1 = getFaultInfoBySmog("感烟传感器B区", "六车D柜一号感烟探测器");
    _ret2 = getFireInfoBySmog("感烟传感器B区", "六车D柜二号感烟探测器");
    _ret4 = _ret1 & _ret2;
    _ret1 = getFireInfoBySmog("感烟传感器B区", "六车D柜一号感烟探测器");
    _ret2 = getFaultInfoBySmog("感烟传感器B区", "六车D柜二号感烟探测器");
    _ret5 = _ret1 & _ret2;
    _ret = _ret3 | _ret4 | _ret5;
    setDataByBit(buf[14], 2, _ret == true ? 1 : 0);


    _ret1 = getFireInfoBySmog("感烟传感器B区", "六车H柜一号感烟探测器");
    _ret2 = getFireInfoBySmog("感烟传感器B区", "六车H柜二号感烟探测器");
    _ret3 = _ret1 & _ret2;
    _ret1 = getFaultInfoBySmog("感烟传感器B区", "六车H柜一号感烟探测器");
    _ret2 = getFireInfoBySmog("感烟传感器B区", "六车H柜二号感烟探测器");
    _ret4 = _ret1 & _ret2;
    _ret1 = getFireInfoBySmog("感烟传感器B区", "六车H柜一号感烟探测器");
    _ret2 = getFaultInfoBySmog("感烟传感器B区", "六车H柜二号感烟探测器");
    _ret5 = _ret1 & _ret2;
    _ret = _ret3 | _ret4 | _ret5;
    setDataByBit(buf[14], 3, _ret == true ? 1 : 0);


    _ret1 = getFireInfoBySmog("感烟传感器B区", "六车F柜一号感烟探测器");
    _ret2 = getFireInfoBySmog("感烟传感器B区", "六车F柜二号感烟探测器");
    _ret3 = _ret1 & _ret2;
    _ret1 = getFaultInfoBySmog("感烟传感器B区", "六车F柜一号感烟探测器");
    _ret2 = getFireInfoBySmog("感烟传感器B区", "六车F柜二号感烟探测器");
    _ret4 = _ret1 & _ret2;
    _ret1 = getFireInfoBySmog("感烟传感器B区", "六车F柜一号感烟探测器");
    _ret2 = getFaultInfoBySmog("感烟传感器B区", "六车F柜二号感烟探测器");
    _ret5 = _ret1 & _ret2;
    _ret = _ret3 | _ret4 | _ret5;
    setDataByBit(buf[13], 7, _ret == true ? 1 : 0);



    _ret1 = getFireInfoBySmog("感烟传感器B区", "六车R柜一号感烟探测器");
    _ret2 = getFireInfoBySmog("感烟传感器B区", "六车R柜二号感烟探测器");
    _ret3 = _ret1 & _ret2;
    _ret1 = getFaultInfoBySmog("感烟传感器B区", "六车R柜一号感烟探测器");
    _ret2 = getFireInfoBySmog("感烟传感器B区", "六车R柜二号感烟探测器");
    _ret4 = _ret1 & _ret2;
    _ret1 = getFireInfoBySmog("感烟传感器B区", "六车R柜一号感烟探测器");
    _ret2 = getFaultInfoBySmog("感烟传感器B区", "六车R柜二号感烟探测器");
    _ret5 = _ret1 & _ret2;
    _ret = _ret3 | _ret4 | _ret5;
    setDataByBit(buf[13], 8, _ret == true ? 1 : 0);

    bool _gateRet = getOpenedByGate("一车分区阀门", "一车一区阀门");
    setDataByBit(buf[15], 1, _gateRet == true ? 1 : 0);
    _gateRet = getClosedByGate("一车分区阀门", "一车一区阀门");
    setDataByBit(buf[15], 2, _gateRet == true ? 1 : 0);
    _gateRet = getOpenedByGate("一车分区阀门", "一车二区阀门");
    setDataByBit(buf[15], 3, _gateRet == true ? 1 : 0);
    _gateRet = getClosedByGate("一车分区阀门", "一车二区阀门");
    setDataByBit(buf[15], 4, _gateRet == true ? 1 : 0);


    _gateRet = getOpenedByGate("二车分区阀门", "二车一区阀门");
    setDataByBit(buf[16], 1, _gateRet == true ? 1 : 0);
    _gateRet = getClosedByGate("二车分区阀门", "二车一区阀门");
    setDataByBit(buf[16], 2, _gateRet == true ? 1 : 0);
    _gateRet = getOpenedByGate("二车分区阀门", "二车二区阀门");
    setDataByBit(buf[16], 3, _gateRet == true ? 1 : 0);
    _gateRet = getClosedByGate("二车分区阀门", "二车二区阀门");
    setDataByBit(buf[16], 4, _gateRet == true ? 1 : 0);

    _gateRet = getOpenedByGate("二车总阀", "二车总阀门");
    setDataByBit(buf[16], 5, _gateRet == true ? 1 : 0);
    _gateRet = getClosedByGate("二车总阀", "二车总阀门");
    setDataByBit(buf[16], 6, _gateRet == true ? 1 : 0);


    _gateRet = getOpenedByGate("三车分区阀门", "三车一区阀门");
    setDataByBit(buf[17], 1, _gateRet == true ? 1 : 0);
    _gateRet = getClosedByGate("三车分区阀门", "三车一区阀门");
    setDataByBit(buf[17], 2, _gateRet == true ? 1 : 0);
    _gateRet = getOpenedByGate("三车分区阀门", "三车二区阀门");
    setDataByBit(buf[17], 3, _gateRet == true ? 1 : 0);
    _gateRet = getClosedByGate("三车分区阀门", "三车二区阀门");
    setDataByBit(buf[17], 4, _gateRet == true ? 1 : 0);

    _gateRet = getOpenedByGate("三车总阀", "三车总阀门");
    setDataByBit(buf[17], 5, _gateRet == true ? 1 : 0);
    _gateRet = getClosedByGate("三车总阀", "三车总阀门");
    setDataByBit(buf[17], 6, _gateRet == true ? 1 : 0);



    _gateRet = getOpenedByGate("四车分区阀门", "四车一区阀门");
    setDataByBit(buf[18], 1, _gateRet == true ? 1 : 0);
    _gateRet = getClosedByGate("四车分区阀门", "四车一区阀门");
    setDataByBit(buf[18], 2, _gateRet == true ? 1 : 0);
    _gateRet = getOpenedByGate("四车分区阀门", "四车二区阀门");
    setDataByBit(buf[18], 3, _gateRet == true ? 1 : 0);
    _gateRet = getClosedByGate("四车分区阀门", "四车二区阀门");
    setDataByBit(buf[18], 4, _gateRet == true ? 1 : 0);

    _gateRet = getOpenedByGate("四车总阀", "四车总阀门");
    setDataByBit(buf[18], 5, _gateRet == true ? 1 : 0);
    _gateRet = getClosedByGate("四车总阀", "四车总阀门");
    setDataByBit(buf[18], 6, _gateRet == true ? 1 : 0);


    _gateRet = getOpenedByGate("五车分区阀门", "五车一区阀门");
    setDataByBit(buf[19], 1, _gateRet == true ? 1 : 0);
    _gateRet = getClosedByGate("五车分区阀门", "五车一区阀门");
    setDataByBit(buf[19], 2, _gateRet == true ? 1 : 0);
    _gateRet = getOpenedByGate("五车分区阀门", "五车二区阀门");
    setDataByBit(buf[19], 3, _gateRet == true ? 1 : 0);
    _gateRet = getClosedByGate("五车分区阀门", "五车二区阀门");
    setDataByBit(buf[19], 4, _gateRet == true ? 1 : 0);

    _gateRet = getOpenedByGate("五车总阀", "五车总阀门");
    setDataByBit(buf[19], 5, _gateRet == true ? 1 : 0);
    _gateRet = getClosedByGate("五车总阀", "五车总阀门");
    setDataByBit(buf[19], 6, _gateRet == true ? 1 : 0);

    _gateRet = getOpenedByGate("六车分区阀门", "六车一区阀门");
    setDataByBit(buf[20], 1, _gateRet == true ? 1 : 0);
    _gateRet = getClosedByGate("六车分区阀门", "六车一区阀门");
    setDataByBit(buf[20], 2, _gateRet == true ? 1 : 0);
    _gateRet = getOpenedByGate("六车分区阀门", "六车二区阀门");
    setDataByBit(buf[20], 3, _gateRet == true ? 1 : 0);
    _gateRet = getClosedByGate("六车分区阀门", "六车二区阀门");
    setDataByBit(buf[20], 4, _gateRet == true ? 1 : 0);


    bool _iooutputRet;
    _iooutputRet = getQiMieByIO("一车D柜气灭装置MVB状态");
    setDataByBit(buf[21], 1,_iooutputRet == true ? 1 : 0);
    _iooutputRet = getQiMieByIO("一车H柜气灭装置MVB状态");
    setDataByBit(buf[21], 2,_iooutputRet == true ? 1 : 0);
    _iooutputRet = getQiMieByIO("一车F柜气灭装置MVB状态");
    setDataByBit(buf[21], 3,_iooutputRet == true ? 1 : 0);
    _iooutputRet = getQiMieByIO("一车R柜气灭装置MVB状态");
    setDataByBit(buf[21], 4,_iooutputRet == true ? 1 : 0);
    _iooutputRet = getQiMieByIO("二车F柜气灭装置MVB状态");
    setDataByBit(buf[21], 5,_iooutputRet == true ? 1 : 0);
    _iooutputRet = getQiMieByIO("二车R柜气灭装置MVB状态");
    setDataByBit(buf[21], 6,_iooutputRet == true ? 1 : 0);
    _iooutputRet = getQiMieByIO("三车F柜气灭装置MVB状态");
    setDataByBit(buf[21], 7,_iooutputRet == true ? 1 : 0);
    _iooutputRet = getQiMieByIO("三车R柜气灭装置MVB状态");
    setDataByBit(buf[21], 8,_iooutputRet == true ? 1 : 0);

    _iooutputRet = getQiMieByIO("四车F柜气灭装置MVB状态");
    setDataByBit(buf[22], 1,_iooutputRet == true ? 1 : 0);
    _iooutputRet = getQiMieByIO("四车R柜气灭装置MVB状态");
    setDataByBit(buf[22], 2,_iooutputRet == true ? 1 : 0);
    _iooutputRet = getQiMieByIO("五车F柜气灭装置MVB状态");
    setDataByBit(buf[22], 3,_iooutputRet == true ? 1 : 0);
    _iooutputRet = getQiMieByIO("五车R柜气灭装置MVB状态");
    setDataByBit(buf[22], 4,_iooutputRet == true ? 1 : 0);


    _iooutputRet = getQiMieByIO("六车D柜气灭装置MVB状态");
    setDataByBit(buf[22], 5,_iooutputRet == true ? 1 : 0);
    _iooutputRet = getQiMieByIO("六车H柜气灭装置MVB状态");
    setDataByBit(buf[22], 6,_iooutputRet == true ? 1 : 0);
    _iooutputRet = getQiMieByIO("六车F柜气灭装置MVB状态");
    setDataByBit(buf[22], 7,_iooutputRet == true ? 1 : 0);
    _iooutputRet = getQiMieByIO("六车R柜气灭装置MVB状态");
    setDataByBit(buf[22], 8,_iooutputRet == true ? 1 : 0);


    _gateRet = getOpenedByGate("A框架出水总阀门", "A框架进气阀门");
    setDataByBit(buf[23], 1, _gateRet == true ? 1 : 0);
    _gateRet = getClosedByGate("A框架出水总阀门", "A框架进气阀门");
    setDataByBit(buf[23], 2, _gateRet == true ? 1 : 0);
    _gateRet = getOpenedByGate("A框架出水总阀门", "A框架出水阀门");
    setDataByBit(buf[23], 3, _gateRet == true ? 1 : 0);
    _gateRet = getClosedByGate("A框架出水总阀门", "A框架出水阀门");
    setDataByBit(buf[23], 4, _gateRet == true ? 1 : 0);

    _iooutputRet = getMultiLambdaVariableStatus("A框架瓶口阀开启");
    setDataByBit(buf[23], 5,_iooutputRet == true ? 1 : 0);
    _iooutputRet = getMultiLambdaVariableStatus("A框架加热器开启");
    setDataByBit(buf[23], 6,_iooutputRet == true ? 1 : 0);
    _iooutputRet = getMultiLambdaVariableStatus("A框架暖风机开启");
    setDataByBit(buf[23], 7,_iooutputRet == true ? 1 : 0);


    _gateRet = getOpenedByGate("B框架出水总阀门", "B框架进气阀门");
    setDataByBit(buf[24], 1, _gateRet == true ? 1 : 0);
    _gateRet = getClosedByGate("B框架出水总阀门", "B框架进气阀门");
    setDataByBit(buf[24], 2, _gateRet == true ? 1 : 0);
    _gateRet = getOpenedByGate("B框架出水总阀门", "B框架出水阀门");
    setDataByBit(buf[24], 3, _gateRet == true ? 1 : 0);
    _gateRet = getClosedByGate("B框架出水总阀门", "B框架出水阀门");
    setDataByBit(buf[24], 4, _gateRet == true ? 1 : 0);

    _iooutputRet = getMultiLambdaVariableStatus("B框架瓶口阀开启");
    setDataByBit(buf[24], 5,_iooutputRet == true ? 1 : 0);
    _iooutputRet = getMultiLambdaVariableStatus("B框架加热器开启");
    setDataByBit(buf[24], 6,_iooutputRet == true ? 1 : 0);
    _iooutputRet = getMultiLambdaVariableStatus("B框架暖风机开启");
    setDataByBit(buf[24], 7,_iooutputRet == true ? 1 : 0);

    bool _buttonRet;
    _buttonRet =  getAnniuByIO("一车一区控制器", "细水雾启动按钮");
    setDataByBit(buf[25], 1, _buttonRet == true ? 1 : 0);
    _buttonRet =  getAnniuByIO("一车一区控制器", "测试按钮");
    setDataByBit(buf[25], 2, _buttonRet == true ? 1 : 0);
    _buttonRet =  getAnniuByIO("一车一区控制器", "复位按钮");
    setDataByBit(buf[25], 3, _buttonRet == true ? 1 : 0);
    _buttonRet =  getAnniuByIO("六车一区控制器", "细水雾启动按钮");
    setDataByBit(buf[25], 4, _buttonRet == true ? 1 : 0);
    _buttonRet =  getAnniuByIO("六车一区控制器", "测试按钮");
    setDataByBit(buf[25], 5, _buttonRet == true ? 1 : 0);
    _buttonRet =  getAnniuByIO("六车一区控制器", "复位按钮");
    setDataByBit(buf[25], 6, _buttonRet == true ? 1 : 0);

    quint8 _akuangjiayewei = getValueByIO("A框架一区控制器", "A框架液位");
    buf[26] = _akuangjiayewei;

    quint8 _bkuangjiayewei = getValueByIO("B框架一区控制器", "B框架液位");
    buf[27] = _bkuangjiayewei;

    qint8 _akuangjiadanqinpingyali1 = getValueByIO("A框架一区控制器", "A框架氮气瓶一压力");
    //    qDebug() << _akuangjiadanqinpingyali1;
    buf[28] = _akuangjiadanqinpingyali1;
    qint8 _akuangjiadanqinpingyali2 = getValueByIO("A框架一区控制器", "A框架氮气瓶二压力");
    buf[29] = _akuangjiadanqinpingyali2;
    //    qDebug() << _akuangjiadanqinpingyali2;

    qint8 _bkuangjiadanqinpingyali1 = getValueByIO("B框架一区控制器", "B框架氮气瓶一压力");
    buf[30] = _bkuangjiadanqinpingyali1;
    qint8 _bkuangjiadanqinpingyali2 = getValueByIO("B框架一区控制器", "B框架氮气瓶二压力");
    buf[31] = _bkuangjiadanqinpingyali2;
    //    qDebug() << _bkuangjiadanqinpingyali1;

    //    qDebug() << _bkuangjiadanqinpingyali2;

    //            qDebug() << "A框架液位：" << _akuangjiayewei
    //                     << "B框架液位: " << _bkuangjiayewei
    //                     << "A框架氮气瓶压力1：" << _akuangjiadanqinpingyali1
    //                     << "A框架氮气瓶压力2：" << _akuangjiadanqinpingyali2
    //                     << "B框架氮气瓶压力1：" << _bkuangjiadanqinpingyali1
    //                     << "B框架氮气瓶压力2：" << _bkuangjiadanqinpingyali2
    //                     << "A框架温度：" << (qint16)_akuangjiawendu
    //                     << "B框架温度: "<< (qint16)_bkuangjiawendu;

    //    qDebug() << "akuangjiayewei:" << _akuangjiayewei
    //             << "bkuangjiayewei: " << _bkuangjiayewei
    //             << "ayali1：" << _akuangjiadanqinpingyali1
    //             << "ayali2：" << _akuangjiadanqinpingyali2
    //             << "byali1：" << _bkuangjiadanqinpingyali1
    //             << "byali2：" << _bkuangjiadanqinpingyali2
    //             << "awendu：" << (qint16)_akuangjiawendu
    //             << "bwendu: "<< (qint16)_bkuangjiawendu;

    memcpy(&_struct.data, buf, sizeof(unsigned char) * 32);

    //    qDebug() << "[";
    //    for(int i =0;i<32;i++)
    //    {
    //        qDebug() << _struct.data[i];
    //    }
    //    qDebug() << buf[4] << ":" << buf[7];
    //    qDebug()<< _struct.data[4] << ":" << _struct.data[7];
    //    qDebug() << "]";

    DataRequestResponseFrame _struct2;
    memset(&_struct2, 0, sizeof(DataRequestResponseFrame));
    unsigned char buf2[32];
    memset(buf2, 0, sizeof(unsigned char) * 32);

    QList<QString> _faultKeys = m_faultByFireDeviceHash.keys();
    for(int i =0;i<m_faultByFireDeviceHash.size();i++)
    {
        QPair<int, QPair<int, int > > _value = m_faultByFireDeviceHash[_faultKeys.at(i)];
        bool _ret = getFaultInfoByFire(_faultKeys.at(i));
        quint8 _v;
        if(_ret)
        {
            _v = 1;
        }else
        {
            _v = 0;
        }
        _value.first = _v;
        setDataByBit(buf2[_value.second.first], _value.second.second, _value.first);
    }
    bool _smogRet;
    _smogRet = getFaultInfoBySmog("感烟传感器A区", "一车司机室感烟探测器");
    setDataByBit(buf2[0], 7, _smogRet == true ? 1 : 0);
    _smogRet = getFaultInfoBySmog("感烟传感器B区", "六车司机室感烟探测器");
    setDataByBit(buf2[5], 7, _smogRet == true ? 1 : 0);


    _smogRet = getFaultInfoBySmog("感烟传感器A区", "一车F柜一号感烟探测器");
    setDataByBit(buf2[6], 1, _smogRet == true ? 1 : 0);
    _smogRet = getFaultInfoBySmog("感烟传感器A区", "一车F柜二号感烟探测器");
    setDataByBit(buf2[6], 2, _smogRet == true ? 1 : 0);

    _smogRet = getFaultInfoBySmog("感烟传感器A区", "一车R柜一号感烟探测器");
    setDataByBit(buf2[6], 3, _smogRet == true ? 1 : 0);
    _smogRet = getFaultInfoBySmog("感烟传感器A区", "一车R柜二号感烟探测器");
    setDataByBit(buf2[6], 4, _smogRet == true ? 1 : 0);

    _smogRet = getFaultInfoBySmog("感烟传感器A区", "一车D柜一号感烟探测器");
    setDataByBit(buf2[6], 5, _smogRet == true ? 1 : 0);
    _smogRet = getFaultInfoBySmog("感烟传感器A区", "一车D柜二号感烟探测器");
    setDataByBit(buf2[6], 6, _smogRet == true ? 1 : 0);

    _smogRet = getFaultInfoBySmog("感烟传感器A区", "一车H柜一号感烟探测器");
    setDataByBit(buf2[6], 7, _smogRet == true ? 1 : 0);
    _smogRet = getFaultInfoBySmog("感烟传感器A区", "一车H柜二号感烟探测器");
    setDataByBit(buf2[6], 8, _smogRet == true ? 1 : 0);


    _smogRet = getFaultInfoBySmog("感烟传感器A区", "二车F柜一号感烟探测器");
    setDataByBit(buf2[7], 1, _smogRet == true ? 1 : 0);
    _smogRet = getFaultInfoBySmog("感烟传感器A区", "二车F柜二号感烟探测器");
    setDataByBit(buf2[7], 2, _smogRet == true ? 1 : 0);

    _smogRet = getFaultInfoBySmog("感烟传感器A区", "二车R柜一号感烟探测器");
    setDataByBit(buf2[7], 3, _smogRet == true ? 1 : 0);
    _smogRet = getFaultInfoBySmog("感烟传感器A区", "二车R柜二号感烟探测器");
    setDataByBit(buf2[7], 4, _smogRet == true ? 1 : 0);

    _smogRet = getFaultInfoBySmog("感烟传感器A区", "三车F柜一号感烟探测器");
    setDataByBit(buf2[8], 1, _smogRet == true ? 1 : 0);
    _smogRet = getFaultInfoBySmog("感烟传感器A区", "三车F柜二号感烟探测器");
    setDataByBit(buf2[8], 2, _smogRet == true ? 1 : 0);

    _smogRet = getFaultInfoBySmog("感烟传感器A区", "三车R柜一号感烟探测器");
    setDataByBit(buf2[8], 3, _smogRet == true ? 1 : 0);
    _smogRet = getFaultInfoBySmog("感烟传感器A区", "三车R柜二号感烟探测器");
    setDataByBit(buf2[8], 4, _smogRet == true ? 1 : 0);

    _smogRet = getFaultInfoBySmog("感烟传感器B区", "四车F柜一号感烟探测器");
    setDataByBit(buf2[9], 1, _smogRet == true ? 1 : 0);
    _smogRet = getFaultInfoBySmog("感烟传感器B区", "四车F柜二号感烟探测器");
    setDataByBit(buf2[9], 2, _smogRet == true ? 1 : 0);

    _smogRet = getFaultInfoBySmog("感烟传感器B区", "四车R柜一号感烟探测器");
    setDataByBit(buf2[9], 3, _smogRet == true ? 1 : 0);
    _smogRet = getFaultInfoBySmog("感烟传感器B区", "四车R柜二号感烟探测器");
    setDataByBit(buf2[9], 4, _smogRet == true ? 1 : 0);


    _smogRet = getFaultInfoBySmog("感烟传感器B区", "五车F柜一号感烟探测器");
    setDataByBit(buf2[10], 1, _smogRet == true ? 1 : 0);
    _smogRet = getFaultInfoBySmog("感烟传感器B区", "五车F柜二号感烟探测器");
    setDataByBit(buf2[10], 2, _smogRet == true ? 1 : 0);

    _smogRet = getFaultInfoBySmog("感烟传感器B区", "五车R柜一号感烟探测器");
    setDataByBit(buf2[10], 3, _smogRet == true ? 1 : 0);
    _smogRet = getFaultInfoBySmog("感烟传感器B区", "五车R柜二号感烟探测器");
    setDataByBit(buf2[10], 4, _smogRet == true ? 1 : 0);

    _smogRet = getFaultInfoBySmog("感烟传感器B区", "六车F柜一号感烟探测器");
    setDataByBit(buf2[11], 1, _smogRet == true ? 1 : 0);
    _smogRet = getFaultInfoBySmog("感烟传感器B区", "六车F柜二号感烟探测器");
    setDataByBit(buf2[11], 2, _smogRet == true ? 1 : 0);

    _smogRet = getFaultInfoBySmog("感烟传感器B区", "六车R柜一号感烟探测器");
    setDataByBit(buf2[11], 3, _smogRet == true ? 1 : 0);
    _smogRet = getFaultInfoBySmog("感烟传感器B区", "六车R柜二号感烟探测器");
    setDataByBit(buf2[11], 4, _smogRet == true ? 1 : 0);

    _smogRet = getFaultInfoBySmog("感烟传感器B区", "六车D柜一号感烟探测器");
    setDataByBit(buf2[11], 5, _smogRet == true ? 1 : 0);
    _smogRet = getFaultInfoBySmog("感烟传感器B区", "六车D柜二号感烟探测器");
    setDataByBit(buf2[11], 6, _smogRet == true ? 1 : 0);

    _smogRet = getFaultInfoBySmog("感烟传感器B区", "六车H柜一号感烟探测器");
    setDataByBit(buf2[11], 7, _smogRet == true ? 1 : 0);
    _smogRet = getFaultInfoBySmog("感烟传感器B区", "六车H柜二号感烟探测器");
    setDataByBit(buf2[11], 8, _smogRet == true ? 1 : 0);

    bool _ioinputRet;
    _ioinputRet = getYeweidiByIO("一车D柜气灭瓶液位低");
    setDataByBit(buf2[12], 1, _ioinputRet == true ? 1 :0);
    _ioinputRet = getYeweidiByIO("一车H柜气灭瓶液位低");
    setDataByBit(buf2[12], 2, _ioinputRet == true ? 1 :0);
    _ioinputRet = getYeweidiByIO("一车F柜气灭瓶液位低");
    setDataByBit(buf2[12], 3, _ioinputRet == true ? 1 :0);
    _ioinputRet = getYeweidiByIO("一车R柜气灭瓶液位低");
    setDataByBit(buf2[12], 4, _ioinputRet == true ? 1 :0);
    _ioinputRet = getYeweidiByIO("二车F柜气灭瓶液位低");
    setDataByBit(buf2[12], 5, _ioinputRet == true ? 1 :0);
    _ioinputRet = getYeweidiByIO("二车R柜气灭瓶液位低");
    setDataByBit(buf2[12], 6, _ioinputRet == true ? 1 :0);
    _ioinputRet = getYeweidiByIO("三车F柜气灭瓶液位低");
    setDataByBit(buf2[12], 7, _ioinputRet == true ? 1 :0);
    _ioinputRet = getYeweidiByIO("三车R柜气灭瓶液位低");
    setDataByBit(buf2[12], 8, _ioinputRet == true ? 1 :0);
    _ioinputRet = getYeweidiByIO("四车F柜气灭瓶液位低");
    setDataByBit(buf2[13], 1, _ioinputRet == true ? 1 :0);
    _ioinputRet = getYeweidiByIO("四车R柜气灭瓶液位低");
    setDataByBit(buf2[13], 2, _ioinputRet == true ? 1 :0);
    _ioinputRet = getYeweidiByIO("五车F柜气灭瓶液位低");
    setDataByBit(buf2[13], 3, _ioinputRet == true ? 1 :0);
    _ioinputRet = getYeweidiByIO("五车R柜气灭瓶液位低");
    setDataByBit(buf2[13], 4, _ioinputRet == true ? 1 :0);

    _ioinputRet = getYeweidiByIO("六车D柜气灭瓶液位低");
    setDataByBit(buf2[13], 5, _ioinputRet == true ? 1 :0);
    _ioinputRet = getYeweidiByIO("六车H柜气灭瓶液位低");
    setDataByBit(buf2[13], 6, _ioinputRet == true ? 1 :0);
    _ioinputRet = getYeweidiByIO("六车F柜气灭瓶液位低");
    setDataByBit(buf2[13], 7, _ioinputRet == true ? 1 :0);
    _ioinputRet = getYeweidiByIO("六车R柜气灭瓶液位低");
    setDataByBit(buf2[13], 8, _ioinputRet == true ? 1 :0);

    _ioinputRet = getYeweidiByIO("A框架液位低");
    setDataByBit(buf2[14], 1, _ioinputRet == true ? 1 :0);
    _ioinputRet = getYeweidiByIO("A框架氮气瓶一压力低");
    setDataByBit(buf2[14], 2, _ioinputRet == true ? 1 :0);
    _ioinputRet = getYeweidiByIO("A框架氮气瓶二压力低");
    setDataByBit(buf2[14], 3, _ioinputRet == true ? 1 :0);
    _ioinputRet = getYeweidiByIO("A框架加热器故障");
    //    qDebug() << "A框架加热器故障" << _ioinputRet;
    setDataByBit(buf2[14], 4, _ioinputRet == true ? 1 :0);
    _ioinputRet = getYeweidiByIO("A框架暖风机故障");
    setDataByBit(buf2[14], 5, _ioinputRet == true ? 1 : 0);
//    _ioinputRet = getYeweidiByIO("A框架温度低");
//    setDataByBit(buf2[14], 6, _ioinputRet == true ? 1 : 0);



    _ioinputRet = getYeweidiByIO("B框架液位低");
    setDataByBit(buf2[15], 1, _ioinputRet == true ? 1 :0);
    _ioinputRet = getYeweidiByIO("B框架氮气瓶一压力低");
    setDataByBit(buf2[15], 2, _ioinputRet == true ? 1 :0);
    _ioinputRet = getYeweidiByIO("B框架氮气瓶二压力低");
    setDataByBit(buf2[15], 3, _ioinputRet == true ? 1 :0);
    _ioinputRet = getYeweidiByIO("B框架加热器故障");
    setDataByBit(buf2[15], 4, _ioinputRet == true ? 1 :0);
    _ioinputRet = getYeweidiByIO("B框架暖风机故障");
    setDataByBit(buf2[15], 5, _ioinputRet == true ? 1 : 0);
//    _ioinputRet = getYeweidiByIO("B框架温度低");
//    setDataByBit(buf2[15], 6, _ioinputRet == true ? 1 : 0);
    //    qDebug() << "A框架液位低:" << getYeweidiByIO("A框架液位低")
    //             << "A框架氮气瓶一压力低:" << getYeweidiByIO("A框架氮气瓶一压力低")
    //             << "A框架氮气瓶二压力低:" << getYeweidiByIO("A框架氮气瓶二压力低")
    //             << "B框架液位低:" << getYeweidiByIO("B框架液位低")
    //             << "B框架氮气瓶一压力低:" << getYeweidiByIO("B框架氮气瓶一压力低")
    //             << "B框架氮气瓶二压力低:" << getYeweidiByIO("B框架氮气瓶二压力低");

    bool _gateFaultRet;
    _gateFaultRet = getFaultByGate("一车分区阀门", "一车一区阀门");
    setDataByBit(buf2[16], 1, _gateFaultRet == true ? 1 : 0);
    _gateFaultRet = getFaultByGate("一车分区阀门", "一车二区阀门");
    setDataByBit(buf2[16], 2, _gateFaultRet == true ? 1 : 0);
    _gateFaultRet = getFaultByGate("二车分区阀门", "二车一区阀门");
    setDataByBit(buf2[16], 3, _gateFaultRet == true ? 1 : 0);
    _gateFaultRet = getFaultByGate("二车分区阀门", "二车二区阀门");
    setDataByBit(buf2[16], 4, _gateFaultRet == true ? 1 : 0);
    _gateFaultRet = getFaultByGate("二车总阀", "二车总阀门");
    setDataByBit(buf2[16], 5, _gateFaultRet == true ? 1 : 0);


    _gateFaultRet = getFaultByGate("三车分区阀门", "三车一区阀门");
    setDataByBit(buf2[16], 6, _gateFaultRet == true ? 1 : 0);
    _gateFaultRet = getFaultByGate("三车分区阀门", "三车二区阀门");
    setDataByBit(buf2[16], 7, _gateFaultRet == true ? 1 : 0);
    _gateFaultRet = getFaultByGate("三车总阀", "三车总阀门");
    setDataByBit(buf2[16], 8, _gateFaultRet == true ? 1 : 0);

    _gateFaultRet = getFaultByGate("四车分区阀门", "四车一区阀门");
    setDataByBit(buf2[17], 1, _gateFaultRet == true ? 1 : 0);
    _gateFaultRet = getFaultByGate("四车分区阀门", "四车二区阀门");
    setDataByBit(buf2[17], 2, _gateFaultRet == true ? 1 : 0);
    _gateFaultRet = getFaultByGate("四车总阀", "四车总阀门");
    setDataByBit(buf2[17], 3, _gateFaultRet == true ? 1 : 0);
    _gateFaultRet = getFaultByGate("五车分区阀门", "五车一区阀门");
    setDataByBit(buf2[17], 4, _gateFaultRet == true ? 1 : 0);
    _gateFaultRet = getFaultByGate("五车分区阀门", "五车二区阀门");
    setDataByBit(buf2[17], 5, _gateFaultRet == true ? 1 : 0);
    _gateFaultRet = getFaultByGate("五车总阀", "五车总阀门");
    setDataByBit(buf2[17], 6, _gateFaultRet == true ? 1 : 0);

    _gateFaultRet = getFaultByGate("六车分区阀门", "六车一区阀门");
    setDataByBit(buf2[17], 7, _gateFaultRet == true ? 1: 0);
    _gateFaultRet = getFaultByGate("六车分区阀门", "六车二区阀门");
    setDataByBit(buf2[17], 8, _gateFaultRet == true ? 1 : 0);

    _gateFaultRet = getFaultByGate("A框架出水总阀门", "A框架进气阀门");
    setDataByBit(buf2[18], 1, _gateFaultRet == true ? 1: 0);
    _gateFaultRet = getFaultByGate("A框架出水总阀门", "A框架出水阀门");
    setDataByBit(buf2[18], 2, _gateFaultRet == true ? 1 : 0);
    _gateFaultRet = getFaultByGate("B框架出水总阀门", "B框架进气阀门");
    setDataByBit(buf2[18], 3, _gateFaultRet == true ? 1: 0);
    _gateFaultRet = getFaultByGate("B框架出水总阀门", "B框架出水阀门");
    setDataByBit(buf2[18], 4, _gateFaultRet == true ? 1 : 0);

    bool _smogShieldRet;
    _smogShieldRet = getShieldInfoBySmog("感烟传感器A区", "一车司机室感烟探测器");
    setDataByBit(buf2[18], 7, _smogShieldRet == true ? 1 : 0);
    _smogShieldRet = getShieldInfoBySmog("感烟传感器B区", "六车司机室感烟探测器");
    setDataByBit(buf2[18], 8, _smogShieldRet == true ? 1 : 0);


    bool _fireShieldRet;
    _fireShieldRet = getShieldInfoByFire("一车一号感温探测器");
    setDataByBit(buf2[23], 1, _fireShieldRet == true ? 1 : 0);
    _fireShieldRet = getShieldInfoByFire("一车二号感温探测器");
    setDataByBit(buf2[23], 2, _fireShieldRet == true ? 1 : 0);
    _fireShieldRet = getShieldInfoByFire("一车三号感温探测器");
    setDataByBit(buf2[23], 3, _fireShieldRet == true ? 1 : 0);
    _fireShieldRet = getShieldInfoByFire("一车四号感温探测器");
    setDataByBit(buf2[23], 4, _fireShieldRet == true ? 1 : 0);
    _fireShieldRet = getShieldInfoByFire("一车五号感温探测器");
    setDataByBit(buf2[23], 5, _fireShieldRet == true ? 1 : 0);
    _fireShieldRet = getShieldInfoByFire("一车六号感温探测器");
    setDataByBit(buf2[23], 6, _fireShieldRet == true ? 1 : 0);

    _fireShieldRet = getShieldInfoByFire("二车一号感温探测器");
    setDataByBit(buf2[24], 1, _fireShieldRet == true ? 1 : 0);
    _fireShieldRet = getShieldInfoByFire("二车二号感温探测器");
    setDataByBit(buf2[24], 2, _fireShieldRet == true ? 1 : 0);
    _fireShieldRet = getShieldInfoByFire("二车三号感温探测器");
    setDataByBit(buf2[24], 3, _fireShieldRet == true ? 1 : 0);
    _fireShieldRet = getShieldInfoByFire("二车四号感温探测器");
    setDataByBit(buf2[24], 4, _fireShieldRet == true ? 1 : 0);
    _fireShieldRet = getShieldInfoByFire("二车五号感温探测器");
    setDataByBit(buf2[24], 5, _fireShieldRet == true ? 1 : 0);
    _fireShieldRet = getShieldInfoByFire("二车六号感温探测器");
    setDataByBit(buf2[24], 6, _fireShieldRet == true ? 1 : 0);


    _fireShieldRet = getShieldInfoByFire("三车一号感温探测器");
    setDataByBit(buf2[25], 1, _fireShieldRet == true ? 1 : 0);
    _fireShieldRet = getShieldInfoByFire("三车二号感温探测器");
    setDataByBit(buf2[25], 2, _fireShieldRet == true ? 1 : 0);
    _fireShieldRet = getShieldInfoByFire("三车三号感温探测器");
    setDataByBit(buf2[25], 3, _fireShieldRet == true ? 1 : 0);
    _fireShieldRet = getShieldInfoByFire("三车四号感温探测器");
    setDataByBit(buf2[25], 4, _fireShieldRet == true ? 1 : 0);
    _fireShieldRet = getShieldInfoByFire("三车五号感温探测器");
    setDataByBit(buf2[25], 5, _fireShieldRet == true ? 1 : 0);
    _fireShieldRet = getShieldInfoByFire("三车六号感温探测器");
    setDataByBit(buf2[25], 6, _fireShieldRet == true ? 1 : 0);


    _fireShieldRet = getShieldInfoByFire("四车一号感温探测器");
    setDataByBit(buf2[26], 1, _fireShieldRet == true ? 1 : 0);
    _fireShieldRet = getShieldInfoByFire("四车二号感温探测器");
    setDataByBit(buf2[26], 2, _fireShieldRet == true ? 1 : 0);
    _fireShieldRet = getShieldInfoByFire("四车三号感温探测器");
    setDataByBit(buf2[26], 3, _fireShieldRet == true ? 1 : 0);
    _fireShieldRet = getShieldInfoByFire("四车四号感温探测器");
    setDataByBit(buf2[26], 4, _fireShieldRet == true ? 1 : 0);
    _fireShieldRet = getShieldInfoByFire("四车五号感温探测器");
    setDataByBit(buf2[26], 5, _fireShieldRet == true ? 1 : 0);
    _fireShieldRet = getShieldInfoByFire("四车六号感温探测器");
    setDataByBit(buf2[26], 6, _fireShieldRet == true ? 1 : 0);


    _fireShieldRet = getShieldInfoByFire("五车一号感温探测器");
    setDataByBit(buf2[27], 1, _fireShieldRet == true ? 1 : 0);
    _fireShieldRet = getShieldInfoByFire("五车二号感温探测器");
    setDataByBit(buf2[27], 2, _fireShieldRet == true ? 1 : 0);
    _fireShieldRet = getShieldInfoByFire("五车三号感温探测器");
    setDataByBit(buf2[27], 3, _fireShieldRet == true ? 1 : 0);
    _fireShieldRet = getShieldInfoByFire("五车四号感温探测器");
    setDataByBit(buf2[27], 4, _fireShieldRet == true ? 1 : 0);
    _fireShieldRet = getShieldInfoByFire("五车五号感温探测器");
    setDataByBit(buf2[27], 5, _fireShieldRet == true ? 1 : 0);
    _fireShieldRet = getShieldInfoByFire("五车六号感温探测器");
    setDataByBit(buf2[27], 6, _fireShieldRet == true ? 1 : 0);


    _fireShieldRet = getShieldInfoByFire("六车一号感温探测器");
    setDataByBit(buf2[28], 1, _fireShieldRet == true ? 1 : 0);
    _fireShieldRet = getShieldInfoByFire("六车二号感温探测器");
    setDataByBit(buf2[28], 2, _fireShieldRet == true ? 1 : 0);
    _fireShieldRet = getShieldInfoByFire("六车三号感温探测器");
    setDataByBit(buf2[28], 3, _fireShieldRet == true ? 1 : 0);
    _fireShieldRet = getShieldInfoByFire("六车四号感温探测器");
    setDataByBit(buf2[28], 4, _fireShieldRet == true ? 1 : 0);
    _fireShieldRet = getShieldInfoByFire("六车五号感温探测器");
    setDataByBit(buf2[28], 5, _fireShieldRet == true ? 1 : 0);
    _fireShieldRet = getShieldInfoByFire("六车六号感温探测器");
    setDataByBit(buf2[28], 6, _fireShieldRet == true ? 1 : 0);

    memcpy(&_struct2.data, buf2, sizeof(unsigned char) * 32);
    //    for(int i =0;i<32;i++)
    //    {
    //        qDebug() << _struct2.data[i];
    //    }
    //判断主从
    if(m_masterOrClientStatusForMVB)
    {
        //如果是主，发送端口为 填充0xb10、0xb11结构体并发送
        emit sendDataToMVB(_struct, 0x0b, 0x10, false);
        emit sendDataToMVB(_struct2, 0x0b, 0x11, false);

    }else
    {
        //如果是从，发送端口为 填充0xb60、oxb61结构体并发送
        emit sendDataToMVB(_struct, 0x0b, 0x60, false);
        emit sendDataToMVB(_struct2, 0x0b, 0x61, false);

    }

}

void Core::processDataToIOT()
{
    QList<quint16> data;
    //手动插入655个数据
    data.append(getFireInfoByFire("一车一号感温探测器") == true ? 1 : 0);
    data.append(getFaultInfoByFire("一车一号感温探测器") == true ? 1 : 0);
    data.append(getMuBiaoWenDuByFire("一车一号感温探测器"));
    data.append(getMuBiaoWenDuByFire("一车一号感温探测器"));
    data.append(getMuBiaoWenDuByFire("一车一号感温探测器"));
    data.append(getHuanJingWenDuByFire("一车一号感温探测器"));
    data.append(getHuanJingWenDuByFire("一车一号感温探测器"));
    data.append(getHuanJingWenDuByFire("一车一号感温探测器"));
    data.append(getFireInfoByFire("一车二号感温探测器") == true ? 1 : 0);
    data.append(getFaultInfoByFire("一车二号感温探测器") == true ? 1 : 0);
    data.append(getMuBiaoWenDuByFire("一车二号感温探测器"));
    data.append(getMuBiaoWenDuByFire("一车二号感温探测器"));
    data.append(getMuBiaoWenDuByFire("一车二号感温探测器"));
    data.append(getHuanJingWenDuByFire("一车二号感温探测器"));
    data.append(getHuanJingWenDuByFire("一车二号感温探测器"));
    data.append(getHuanJingWenDuByFire("一车二号感温探测器"));
    data.append(getFireInfoByFire("一车三号感温探测器") == true ? 1 : 0);
    data.append(getFaultInfoByFire("一车三号感温探测器") == true ? 1 : 0);
    data.append(getMuBiaoWenDuByFire("一车三号感温探测器"));
    data.append(getMuBiaoWenDuByFire("一车三号感温探测器"));
    data.append(getMuBiaoWenDuByFire("一车三号感温探测器"));
    data.append(getHuanJingWenDuByFire("一车三号感温探测器"));
    data.append(getHuanJingWenDuByFire("一车三号感温探测器"));
    data.append(getHuanJingWenDuByFire("一车三号感温探测器"));
    data.append(getFireInfoByFire("一车四号感温探测器") == true ? 1 : 0);
    data.append(getFaultInfoByFire("一车四号感温探测器") == true ? 1 : 0);
    data.append(getMuBiaoWenDuByFire("一车四号感温探测器"));
    data.append(getMuBiaoWenDuByFire("一车四号感温探测器"));
    data.append(getMuBiaoWenDuByFire("一车四号感温探测器"));
    data.append(getHuanJingWenDuByFire("一车四号感温探测器"));
    data.append(getHuanJingWenDuByFire("一车四号感温探测器"));
    data.append(getHuanJingWenDuByFire("一车四号感温探测器"));
    data.append(getFireInfoByFire("一车五号感温探测器") == true ? 1 : 0);
    data.append(getFaultInfoByFire("一车五号感温探测器") == true ? 1 : 0);
    data.append(getMuBiaoWenDuByFire("一车五号感温探测器"));
    data.append(getMuBiaoWenDuByFire("一车五号感温探测器"));
    data.append(getMuBiaoWenDuByFire("一车五号感温探测器"));
    data.append(getHuanJingWenDuByFire("一车五号感温探测器"));
    data.append(getHuanJingWenDuByFire("一车五号感温探测器"));
    data.append(getHuanJingWenDuByFire("一车五号感温探测器"));
    data.append(getFireInfoByFire("一车六号感温探测器") == true ? 1 : 0);
    data.append(getFaultInfoByFire("一车六号感温探测器") == true ? 1 : 0);
    data.append(getMuBiaoWenDuByFire("一车六号感温探测器"));
    data.append(getMuBiaoWenDuByFire("一车六号感温探测器"));
    data.append(getMuBiaoWenDuByFire("一车六号感温探测器"));
    data.append(getHuanJingWenDuByFire("一车六号感温探测器"));
    data.append(getHuanJingWenDuByFire("一车六号感温探测器"));
    data.append(getHuanJingWenDuByFire("一车六号感温探测器"));
    data.append(getFireInfoByFire("二车一号感温探测器") == true ? 1 : 0);
    data.append(getFaultInfoByFire("二车一号感温探测器") == true ? 1 : 0);
    data.append(getMuBiaoWenDuByFire("二车一号感温探测器"));
    data.append(getMuBiaoWenDuByFire("二车一号感温探测器"));
    data.append(getMuBiaoWenDuByFire("二车一号感温探测器"));
    data.append(getHuanJingWenDuByFire("二车一号感温探测器"));
    data.append(getHuanJingWenDuByFire("二车一号感温探测器"));
    data.append(getHuanJingWenDuByFire("二车一号感温探测器"));
    data.append(getFireInfoByFire("二车二号感温探测器") == true ? 1 : 0);
    data.append(getFaultInfoByFire("二车二号感温探测器") == true ? 1 : 0);
    data.append(getMuBiaoWenDuByFire("二车二号感温探测器"));
    data.append(getMuBiaoWenDuByFire("二车二号感温探测器"));
    data.append(getMuBiaoWenDuByFire("二车二号感温探测器"));
    data.append(getHuanJingWenDuByFire("二车二号感温探测器"));
    data.append(getHuanJingWenDuByFire("二车二号感温探测器"));
    data.append(getHuanJingWenDuByFire("二车二号感温探测器"));
    data.append(getFireInfoByFire("二车三号感温探测器") == true ? 1 : 0);
    data.append(getFaultInfoByFire("二车三号感温探测器") == true ? 1 : 0);
    data.append(getMuBiaoWenDuByFire("二车三号感温探测器"));
    data.append(getMuBiaoWenDuByFire("二车三号感温探测器"));
    data.append(getMuBiaoWenDuByFire("二车三号感温探测器"));
    data.append(getHuanJingWenDuByFire("二车三号感温探测器"));
    data.append(getHuanJingWenDuByFire("二车三号感温探测器"));
    data.append(getHuanJingWenDuByFire("二车三号感温探测器"));
    data.append(getFireInfoByFire("二车四号感温探测器") == true ? 1 : 0);
    data.append(getFaultInfoByFire("二车四号感温探测器") == true ? 1 : 0);
    data.append(getMuBiaoWenDuByFire("二车四号感温探测器"));
    data.append(getMuBiaoWenDuByFire("二车四号感温探测器"));
    data.append(getMuBiaoWenDuByFire("二车四号感温探测器"));
    data.append(getHuanJingWenDuByFire("二车四号感温探测器"));
    data.append(getHuanJingWenDuByFire("二车四号感温探测器"));
    data.append(getHuanJingWenDuByFire("二车四号感温探测器"));
    data.append(getFireInfoByFire("二车五号感温探测器") == true ? 1 : 0);
    data.append(getFaultInfoByFire("二车五号感温探测器") == true ? 1 : 0);
    data.append(getMuBiaoWenDuByFire("二车五号感温探测器"));
    data.append(getMuBiaoWenDuByFire("二车五号感温探测器"));
    data.append(getMuBiaoWenDuByFire("二车五号感温探测器"));
    data.append(getHuanJingWenDuByFire("二车五号感温探测器"));
    data.append(getHuanJingWenDuByFire("二车五号感温探测器"));
    data.append(getHuanJingWenDuByFire("二车五号感温探测器"));
    data.append(getFireInfoByFire("二车六号感温探测器") == true ? 1 : 0);
    data.append(getFaultInfoByFire("二车六号感温探测器") == true ? 1 : 0);
    data.append(getMuBiaoWenDuByFire("二车六号感温探测器"));
    data.append(getMuBiaoWenDuByFire("二车六号感温探测器"));
    data.append(getMuBiaoWenDuByFire("二车六号感温探测器"));
    data.append(getHuanJingWenDuByFire("二车六号感温探测器"));
    data.append(getHuanJingWenDuByFire("二车六号感温探测器"));
    data.append(getHuanJingWenDuByFire("二车六号感温探测器"));
    data.append(getFireInfoByFire("三车一号感温探测器") == true ? 1 : 0);
    data.append(getFaultInfoByFire("三车一号感温探测器") == true ? 1 : 0);
    data.append(getMuBiaoWenDuByFire("三车一号感温探测器"));
    data.append(getMuBiaoWenDuByFire("三车一号感温探测器"));
    data.append(getMuBiaoWenDuByFire("三车一号感温探测器"));
    data.append(getHuanJingWenDuByFire("三车一号感温探测器"));
    data.append(getHuanJingWenDuByFire("三车一号感温探测器"));
    data.append(getHuanJingWenDuByFire("三车一号感温探测器"));
    data.append(getFireInfoByFire("三车二号感温探测器") == true ? 1 : 0);
    data.append(getFaultInfoByFire("三车二号感温探测器") == true ? 1 : 0);
    data.append(getMuBiaoWenDuByFire("三车二号感温探测器"));
    data.append(getMuBiaoWenDuByFire("三车二号感温探测器"));
    data.append(getMuBiaoWenDuByFire("三车二号感温探测器"));
    data.append(getHuanJingWenDuByFire("三车二号感温探测器"));
    data.append(getHuanJingWenDuByFire("三车二号感温探测器"));
    data.append(getHuanJingWenDuByFire("三车二号感温探测器"));
    data.append(getFireInfoByFire("三车三号感温探测器") == true ? 1 : 0);
    data.append(getFaultInfoByFire("三车三号感温探测器") == true ? 1 : 0);
    data.append(getMuBiaoWenDuByFire("三车三号感温探测器"));
    data.append(getMuBiaoWenDuByFire("三车三号感温探测器"));
    data.append(getMuBiaoWenDuByFire("三车三号感温探测器"));
    data.append(getHuanJingWenDuByFire("三车三号感温探测器"));
    data.append(getHuanJingWenDuByFire("三车三号感温探测器"));
    data.append(getHuanJingWenDuByFire("三车三号感温探测器"));
    data.append(getFireInfoByFire("三车四号感温探测器") == true ? 1 : 0);
    data.append(getFaultInfoByFire("三车四号感温探测器") == true ? 1 : 0);
    data.append(getMuBiaoWenDuByFire("三车四号感温探测器"));
    data.append(getMuBiaoWenDuByFire("三车四号感温探测器"));
    data.append(getMuBiaoWenDuByFire("三车四号感温探测器"));
    data.append(getHuanJingWenDuByFire("三车四号感温探测器"));
    data.append(getHuanJingWenDuByFire("三车四号感温探测器"));
    data.append(getHuanJingWenDuByFire("三车四号感温探测器"));
    data.append(getFireInfoByFire("三车五号感温探测器") == true ? 1 : 0);
    data.append(getFaultInfoByFire("三车五号感温探测器") == true ? 1 : 0);
    data.append(getMuBiaoWenDuByFire("三车五号感温探测器"));
    data.append(getMuBiaoWenDuByFire("三车五号感温探测器"));
    data.append(getMuBiaoWenDuByFire("三车五号感温探测器"));
    data.append(getHuanJingWenDuByFire("三车五号感温探测器"));
    data.append(getHuanJingWenDuByFire("三车五号感温探测器"));
    data.append(getHuanJingWenDuByFire("三车五号感温探测器"));
    data.append(getFireInfoByFire("三车六号感温探测器") == true ? 1 : 0);
    data.append(getFaultInfoByFire("三车六号感温探测器") == true ? 1 : 0);
    data.append(getMuBiaoWenDuByFire("三车六号感温探测器"));
    data.append(getMuBiaoWenDuByFire("三车六号感温探测器"));
    data.append(getMuBiaoWenDuByFire("三车六号感温探测器"));
    data.append(getHuanJingWenDuByFire("三车六号感温探测器"));
    data.append(getHuanJingWenDuByFire("三车六号感温探测器"));
    data.append(getHuanJingWenDuByFire("三车六号感温探测器"));
    data.append(getFireInfoByFire("四车一号感温探测器") == true ? 1 : 0);
    data.append(getFaultInfoByFire("四车一号感温探测器") == true ? 1 : 0);
    data.append(getMuBiaoWenDuByFire("四车一号感温探测器"));
    data.append(getMuBiaoWenDuByFire("四车一号感温探测器"));
    data.append(getMuBiaoWenDuByFire("四车一号感温探测器"));
    data.append(getHuanJingWenDuByFire("四车一号感温探测器"));
    data.append(getHuanJingWenDuByFire("四车一号感温探测器"));
    data.append(getHuanJingWenDuByFire("四车一号感温探测器"));
    data.append(getFireInfoByFire("四车二号感温探测器") == true ? 1 : 0);
    data.append(getFaultInfoByFire("四车二号感温探测器") == true ? 1 : 0);
    data.append(getMuBiaoWenDuByFire("四车二号感温探测器"));
    data.append(getMuBiaoWenDuByFire("四车二号感温探测器"));
    data.append(getMuBiaoWenDuByFire("四车二号感温探测器"));
    data.append(getHuanJingWenDuByFire("四车二号感温探测器"));
    data.append(getHuanJingWenDuByFire("四车二号感温探测器"));
    data.append(getHuanJingWenDuByFire("四车二号感温探测器"));
    data.append(getFireInfoByFire("四车三号感温探测器") == true ? 1 : 0);
    data.append(getFaultInfoByFire("四车三号感温探测器") == true ? 1 : 0);
    data.append(getMuBiaoWenDuByFire("四车三号感温探测器"));
    data.append(getMuBiaoWenDuByFire("四车三号感温探测器"));
    data.append(getMuBiaoWenDuByFire("四车三号感温探测器"));
    data.append(getHuanJingWenDuByFire("四车三号感温探测器"));
    data.append(getHuanJingWenDuByFire("四车三号感温探测器"));
    data.append(getHuanJingWenDuByFire("四车三号感温探测器"));
    data.append(getFireInfoByFire("四车四号感温探测器") == true ? 1 : 0);
    data.append(getFaultInfoByFire("四车四号感温探测器") == true ? 1 : 0);
    data.append(getMuBiaoWenDuByFire("四车四号感温探测器"));
    data.append(getMuBiaoWenDuByFire("四车四号感温探测器"));
    data.append(getMuBiaoWenDuByFire("四车四号感温探测器"));
    data.append(getHuanJingWenDuByFire("四车四号感温探测器"));
    data.append(getHuanJingWenDuByFire("四车四号感温探测器"));
    data.append(getHuanJingWenDuByFire("四车四号感温探测器"));
    data.append(getFireInfoByFire("四车五号感温探测器") == true ? 1 : 0);
    data.append(getFaultInfoByFire("四车五号感温探测器") == true ? 1 : 0);
    data.append(getMuBiaoWenDuByFire("四车五号感温探测器"));
    data.append(getMuBiaoWenDuByFire("四车五号感温探测器"));
    data.append(getMuBiaoWenDuByFire("四车五号感温探测器"));
    data.append(getHuanJingWenDuByFire("四车五号感温探测器"));
    data.append(getHuanJingWenDuByFire("四车五号感温探测器"));
    data.append(getHuanJingWenDuByFire("四车五号感温探测器"));
    data.append(getFireInfoByFire("四车六号感温探测器") == true ? 1 : 0);
    data.append(getFaultInfoByFire("四车六号感温探测器") == true ? 1 : 0);
    data.append(getMuBiaoWenDuByFire("四车六号感温探测器"));
    data.append(getMuBiaoWenDuByFire("四车六号感温探测器"));
    data.append(getMuBiaoWenDuByFire("四车六号感温探测器"));
    data.append(getHuanJingWenDuByFire("四车六号感温探测器"));
    data.append(getHuanJingWenDuByFire("四车六号感温探测器"));
    data.append(getHuanJingWenDuByFire("四车六号感温探测器"));
    data.append(getFireInfoByFire("五车一号感温探测器") == true ? 1 : 0);
    data.append(getFaultInfoByFire("五车一号感温探测器") == true ? 1 : 0);
    data.append(getMuBiaoWenDuByFire("五车一号感温探测器"));
    data.append(getMuBiaoWenDuByFire("五车一号感温探测器"));
    data.append(getMuBiaoWenDuByFire("五车一号感温探测器"));
    data.append(getHuanJingWenDuByFire("五车一号感温探测器"));
    data.append(getHuanJingWenDuByFire("五车一号感温探测器"));
    data.append(getHuanJingWenDuByFire("五车一号感温探测器"));
    data.append(getFireInfoByFire("五车二号感温探测器") == true ? 1 : 0);
    data.append(getFaultInfoByFire("五车二号感温探测器") == true ? 1 : 0);
    data.append(getMuBiaoWenDuByFire("五车二号感温探测器"));
    data.append(getMuBiaoWenDuByFire("五车二号感温探测器"));
    data.append(getMuBiaoWenDuByFire("五车二号感温探测器"));
    data.append(getHuanJingWenDuByFire("五车二号感温探测器"));
    data.append(getHuanJingWenDuByFire("五车二号感温探测器"));
    data.append(getHuanJingWenDuByFire("五车二号感温探测器"));
    data.append(getFireInfoByFire("五车三号感温探测器") == true ? 1 : 0);
    data.append(getFaultInfoByFire("五车三号感温探测器") == true ? 1 : 0);
    data.append(getMuBiaoWenDuByFire("五车三号感温探测器"));
    data.append(getMuBiaoWenDuByFire("五车三号感温探测器"));
    data.append(getMuBiaoWenDuByFire("五车三号感温探测器"));
    data.append(getHuanJingWenDuByFire("五车三号感温探测器"));
    data.append(getHuanJingWenDuByFire("五车三号感温探测器"));
    data.append(getHuanJingWenDuByFire("五车三号感温探测器"));
    data.append(getFireInfoByFire("五车四号感温探测器") == true ? 1 : 0);
    data.append(getFaultInfoByFire("五车四号感温探测器") == true ? 1 : 0);
    data.append(getMuBiaoWenDuByFire("五车四号感温探测器"));
    data.append(getMuBiaoWenDuByFire("五车四号感温探测器"));
    data.append(getMuBiaoWenDuByFire("五车四号感温探测器"));
    data.append(getHuanJingWenDuByFire("五车四号感温探测器"));
    data.append(getHuanJingWenDuByFire("五车四号感温探测器"));
    data.append(getHuanJingWenDuByFire("五车四号感温探测器"));
    data.append(getFireInfoByFire("五车五号感温探测器") == true ? 1 : 0);
    data.append(getFaultInfoByFire("五车五号感温探测器") == true ? 1 : 0);
    data.append(getMuBiaoWenDuByFire("五车五号感温探测器"));
    data.append(getMuBiaoWenDuByFire("五车五号感温探测器"));
    data.append(getMuBiaoWenDuByFire("五车五号感温探测器"));
    data.append(getHuanJingWenDuByFire("五车五号感温探测器"));
    data.append(getHuanJingWenDuByFire("五车五号感温探测器"));
    data.append(getHuanJingWenDuByFire("五车五号感温探测器"));
    data.append(getFireInfoByFire("五车六号感温探测器") == true ? 1 : 0);
    data.append(getFaultInfoByFire("五车六号感温探测器") == true ? 1 : 0);
    data.append(getMuBiaoWenDuByFire("五车六号感温探测器"));
    data.append(getMuBiaoWenDuByFire("五车六号感温探测器"));
    data.append(getMuBiaoWenDuByFire("五车六号感温探测器"));
    data.append(getHuanJingWenDuByFire("五车六号感温探测器"));
    data.append(getHuanJingWenDuByFire("五车六号感温探测器"));
    data.append(getHuanJingWenDuByFire("五车六号感温探测器"));
    data.append(getFireInfoByFire("六车一号感温探测器") == true ? 1 : 0);
    data.append(getFaultInfoByFire("六车一号感温探测器") == true ? 1 : 0);
    data.append(getMuBiaoWenDuByFire("六车一号感温探测器"));
    data.append(getMuBiaoWenDuByFire("六车一号感温探测器"));
    data.append(getMuBiaoWenDuByFire("六车一号感温探测器"));
    data.append(getHuanJingWenDuByFire("六车一号感温探测器"));
    data.append(getHuanJingWenDuByFire("六车一号感温探测器"));
    data.append(getHuanJingWenDuByFire("六车一号感温探测器"));
    data.append(getFireInfoByFire("六车二号感温探测器") == true ? 1 : 0);
    data.append(getFaultInfoByFire("六车二号感温探测器") == true ? 1 : 0);
    data.append(getMuBiaoWenDuByFire("六车二号感温探测器"));
    data.append(getMuBiaoWenDuByFire("六车二号感温探测器"));
    data.append(getMuBiaoWenDuByFire("六车二号感温探测器"));
    data.append(getHuanJingWenDuByFire("六车二号感温探测器"));
    data.append(getHuanJingWenDuByFire("六车二号感温探测器"));
    data.append(getHuanJingWenDuByFire("六车二号感温探测器"));
    data.append(getFireInfoByFire("六车三号感温探测器") == true ? 1 : 0);
    data.append(getFaultInfoByFire("六车三号感温探测器") == true ? 1 : 0);
    data.append(getMuBiaoWenDuByFire("六车三号感温探测器"));
    data.append(getMuBiaoWenDuByFire("六车三号感温探测器"));
    data.append(getMuBiaoWenDuByFire("六车三号感温探测器"));
    data.append(getHuanJingWenDuByFire("六车三号感温探测器"));
    data.append(getHuanJingWenDuByFire("六车三号感温探测器"));
    data.append(getHuanJingWenDuByFire("六车三号感温探测器"));
    data.append(getFireInfoByFire("六车四号感温探测器") == true ? 1 : 0);
    data.append(getFaultInfoByFire("六车四号感温探测器") == true ? 1 : 0);
    data.append(getMuBiaoWenDuByFire("六车四号感温探测器"));
    data.append(getMuBiaoWenDuByFire("六车四号感温探测器"));
    data.append(getMuBiaoWenDuByFire("六车四号感温探测器"));
    data.append(getHuanJingWenDuByFire("六车四号感温探测器"));
    data.append(getHuanJingWenDuByFire("六车四号感温探测器"));
    data.append(getHuanJingWenDuByFire("六车四号感温探测器"));
    data.append(getFireInfoByFire("六车五号感温探测器") == true ? 1 : 0);
    data.append(getFaultInfoByFire("六车五号感温探测器") == true ? 1 : 0);
    data.append(getMuBiaoWenDuByFire("六车五号感温探测器"));
    data.append(getMuBiaoWenDuByFire("六车五号感温探测器"));
    data.append(getMuBiaoWenDuByFire("六车五号感温探测器"));
    data.append(getHuanJingWenDuByFire("六车五号感温探测器"));
    data.append(getHuanJingWenDuByFire("六车五号感温探测器"));
    data.append(getHuanJingWenDuByFire("六车五号感温探测器"));
    data.append(getFireInfoByFire("六车六号感温探测器") == true ? 1 : 0);
    data.append(getFaultInfoByFire("六车六号感温探测器") == true ? 1 : 0);
    data.append(getMuBiaoWenDuByFire("六车六号感温探测器"));
    data.append(getMuBiaoWenDuByFire("六车六号感温探测器"));
    data.append(getMuBiaoWenDuByFire("六车六号感温探测器"));
    data.append(getHuanJingWenDuByFire("六车六号感温探测器"));
    data.append(getHuanJingWenDuByFire("六车六号感温探测器"));
    data.append(getHuanJingWenDuByFire("六车六号感温探测器"));


    bool _st = false;
    _st = getMultiLambdaVariableStatus("一车司机室感烟探测器火警");
    data.append(_st == true ? 1 :0);
    _st = getMultiLambdaVariableStatus("一车D柜一号二号感烟探测器火警")
            || getMultiLambdaVariableStatus("一车D柜一号感烟探测器故障和一车D柜二号感烟探测器火警")
            || getMultiLambdaVariableStatus("一车D柜一号感烟探测器火警和一车D柜二号感烟探测器故障");
    data.append(_st == true ? 1 : 0);
    _st = getMultiLambdaVariableStatus("一车H柜一号二号感烟探测器火警")
            || getMultiLambdaVariableStatus("一车H柜一号感烟探测器故障和一车H柜二号感烟探测器火警")
            || getMultiLambdaVariableStatus("一车H柜一号感烟探测器火警和一车H柜二号感烟探测器故障");
    data.append(_st == true ? 1 : 0);
    _st = getMultiLambdaVariableStatus("一车F柜一号二号感烟探测器火警")
            || getMultiLambdaVariableStatus("一车F柜一号感烟探测器故障和一车F柜二号感烟探测器火警")
            || getMultiLambdaVariableStatus("一车F柜一号感烟探测器火警和一车F柜二号感烟探测器故障");
    data.append(_st == true ? 1 : 0);
    _st = getMultiLambdaVariableStatus("一车R柜一号二号感烟探测器火警")
            || getMultiLambdaVariableStatus("一车R柜一号感烟探测器故障和一车R柜二号感烟探测器火警")
            || getMultiLambdaVariableStatus("一车R柜一号感烟探测器火警和一车R柜二号感烟探测器故障");
    data.append(_st == true ? 1 : 0);
    _st = getMultiLambdaVariableStatus("二车F柜一号二号感烟探测器火警")
            || getMultiLambdaVariableStatus("二车F柜一号感烟探测器故障和二车F柜二号感烟探测器火警")
            || getMultiLambdaVariableStatus("二车F柜一号感烟探测器火警和二车F柜二号感烟探测器故障");
    data.append(_st == true ? 1 : 0);
    _st = getMultiLambdaVariableStatus("二车R柜一号二号感烟探测器火警")
            || getMultiLambdaVariableStatus("二车R柜一号感烟探测器故障和二车R柜二号感烟探测器火警")
            || getMultiLambdaVariableStatus("二车R柜一号感烟探测器火警和二车R柜二号感烟探测器故障");
    data.append(_st == true ? 1 : 0);
    _st = getMultiLambdaVariableStatus("三车F柜一号二号感烟探测器火警")
            || getMultiLambdaVariableStatus("三车F柜一号感烟探测器故障和三车F柜二号感烟探测器火警")
            || getMultiLambdaVariableStatus("三车F柜一号感烟探测器火警和三车F柜二号感烟探测器故障");
    data.append(_st == true ? 1 : 0);
    _st = getMultiLambdaVariableStatus("三车R柜一号二号感烟探测器火警")
            || getMultiLambdaVariableStatus("三车R柜一号感烟探测器故障和三车R柜二号感烟探测器火警")
            || getMultiLambdaVariableStatus("三车R柜一号感烟探测器火警和三车R柜二号感烟探测器故障");
    data.append(_st == true ? 1 : 0);
    _st = getMultiLambdaVariableStatus("四车F柜一号二号感烟探测器火警")
            || getMultiLambdaVariableStatus("四车F柜一号感烟探测器故障和四车F柜一号感烟探测器火警")
            || getMultiLambdaVariableStatus("四车F柜一号感烟探测器火警和四车F柜一号感烟探测器故障");
    data.append(_st == true ? 1 : 0);
    _st = getMultiLambdaVariableStatus("四车R柜一号二号感烟探测器火警")
            || getMultiLambdaVariableStatus("四车R柜一号感烟探测器故障和四车R柜一号感烟探测器火警")
            || getMultiLambdaVariableStatus("四车R柜一号感烟探测器火警和四车R柜一号感烟探测器故障");
    data.append(_st == true ? 1 : 0);
    _st = getMultiLambdaVariableStatus("五车F柜一号二号感烟探测器火警")
            || getMultiLambdaVariableStatus("五车F柜一号感烟探测器故障和五车F柜一号感烟探测器火警")
            || getMultiLambdaVariableStatus("五车F柜一号感烟探测器火警和五车F柜一号感烟探测器故障");
    data.append(_st == true ? 1 : 0);
    _st = getMultiLambdaVariableStatus("五车R柜一号二号感烟探测器火警")
            || getMultiLambdaVariableStatus("五车R柜一号感烟探测器故障和五车R柜一号感烟探测器火警")
            || getMultiLambdaVariableStatus("五车R柜一号感烟探测器火警和五车R柜一号感烟探测器故障");
    data.append(_st == true ? 1 : 0);
    _st = getMultiLambdaVariableStatus("六车D柜一号二号感烟探测器火警")
            || getMultiLambdaVariableStatus("六车D柜一号感烟探测器故障和六车D柜二号感烟探测器火警")
            || getMultiLambdaVariableStatus("六车D柜一号感烟探测器火警和六车D柜二号感烟探测器故障");
    data.append(_st == true ? 1 : 0);
    _st = getMultiLambdaVariableStatus("六车H柜一号二号感烟探测器火警")
            || getMultiLambdaVariableStatus("六车H柜一号感烟探测器故障和六车H柜二号感烟探测器火警")
            || getMultiLambdaVariableStatus("六车H柜一号感烟探测器火警和六车H柜二号感烟探测器故障");
    data.append(_st == true ? 1 : 0);
    _st = getMultiLambdaVariableStatus("六车F柜一号二号感烟探测器火警")
            || getMultiLambdaVariableStatus("六车F柜一号感烟探测器故障和六车F柜二号感烟探测器火警")
            || getMultiLambdaVariableStatus("六车F柜一号感烟探测器火警和六车F柜二号感烟探测器故障");
    data.append(_st == true ? 1 : 0);
    _st = getMultiLambdaVariableStatus("六车R柜一号二号感烟探测器火警")
            || getMultiLambdaVariableStatus("六车R柜一号感烟探测器故障和六车R柜二号感烟探测器火警")
            || getMultiLambdaVariableStatus("六车R柜一号感烟探测器火警和六车R柜二号感烟探测器故障");
    data.append(_st == true ? 1 : 0);
    _st = getMultiLambdaVariableStatus("六车司机室感烟探测器火警");
    data.append(_st == true ? 1 : 0);


    data.append(getFaultInfoBySmog("感烟传感器A区", "一车司机室感烟探测器") == true ? 1 : 0);
    data.append(getFaultInfoBySmog("感烟传感器A区", "一车D柜一号感烟探测器") == true ? 1 : 0);
    data.append(getFaultInfoBySmog("感烟传感器A区", "一车D柜二号感烟探测器") == true ? 1 : 0);
    data.append(getFaultInfoBySmog("感烟传感器A区", "一车H柜一号感烟探测器") == true ? 1 : 0);
    data.append(getFaultInfoBySmog("感烟传感器A区", "一车H柜二号感烟探测器") == true ? 1 : 0);
    data.append(getFaultInfoBySmog("感烟传感器A区", "一车F柜一号感烟探测器") == true ? 1 : 0);
    data.append(getFaultInfoBySmog("感烟传感器A区", "一车F柜二号感烟探测器") == true ? 1 : 0);
    data.append(getFaultInfoBySmog("感烟传感器A区", "一车R柜一号感烟探测器") == true ? 1 : 0);
    data.append(getFaultInfoBySmog("感烟传感器A区", "一车R柜二号感烟探测器") == true ? 1 : 0);
    data.append(getFaultInfoBySmog("感烟传感器A区", "二车F柜一号感烟探测器") == true ? 1 : 0);
    data.append(getFaultInfoBySmog("感烟传感器A区", "二车F柜二号感烟探测器") == true ? 1 : 0);
    data.append(getFaultInfoBySmog("感烟传感器A区", "二车R柜一号感烟探测器") == true ? 1 : 0);
    data.append(getFaultInfoBySmog("感烟传感器A区", "二车R柜二号感烟探测器") == true ? 1 : 0);
    data.append(getFaultInfoBySmog("感烟传感器A区", "三车F柜一号感烟探测器") == true ? 1 : 0);
    data.append(getFaultInfoBySmog("感烟传感器A区", "三车F柜二号感烟探测器") == true ? 1 : 0);
    data.append(getFaultInfoBySmog("感烟传感器A区", "三车R柜一号感烟探测器") == true ? 1 : 0);
    data.append(getFaultInfoBySmog("感烟传感器A区", "三车R柜二号感烟探测器") == true ? 1 : 0);
    data.append(getFaultInfoBySmog("感烟传感器B区", "六车D柜一号感烟探测器") == true ? 1 : 0);
    data.append(getFaultInfoBySmog("感烟传感器B区", "六车D柜二号感烟探测器") == true ? 1 : 0);
    data.append(getFaultInfoBySmog("感烟传感器B区", "六车H柜一号感烟探测器") == true ? 1 : 0);
    data.append(getFaultInfoBySmog("感烟传感器B区", "六车H柜二号感烟探测器") == true ? 1 : 0);
    data.append(getFaultInfoBySmog("感烟传感器B区", "六车F柜一号感烟探测器") == true ? 1 : 0);
    data.append(getFaultInfoBySmog("感烟传感器B区", "六车F柜二号感烟探测器") == true ? 1 : 0);
    data.append(getFaultInfoBySmog("感烟传感器B区", "六车R柜一号感烟探测器") == true ? 1 : 0);
    data.append(getFaultInfoBySmog("感烟传感器B区", "六车R柜二号感烟探测器") == true ? 1 : 0);
    data.append(getFaultInfoBySmog("感烟传感器B区", "五车F柜一号感烟探测器") == true ? 1 : 0);
    data.append(getFaultInfoBySmog("感烟传感器B区", "五车F柜二号感烟探测器") == true ? 1 : 0);
    data.append(getFaultInfoBySmog("感烟传感器B区", "五车R柜一号感烟探测器") == true ? 1 : 0);
    data.append(getFaultInfoBySmog("感烟传感器B区", "五车R柜二号感烟探测器") == true ? 1 : 0);
    data.append(getFaultInfoBySmog("感烟传感器B区", "四车F柜一号感烟探测器") == true ? 1 : 0);
    data.append(getFaultInfoBySmog("感烟传感器B区", "四车F柜二号感烟探测器") == true ? 1 : 0);
    data.append(getFaultInfoBySmog("感烟传感器B区", "四车R柜一号感烟探测器") == true ? 1 : 0);
    data.append(getFaultInfoBySmog("感烟传感器B区", "四车R柜二号感烟探测器") == true ? 1 : 0);
    data.append(getFaultInfoBySmog("感烟传感器B区", "六车司机室感烟探测器") == true ? 1 : 0);
    data.append(getOpenedByGate("一车分区阀门", "一车一区阀门") == true ? 1 : 0);
    data.append(getClosedByGate("一车分区阀门", "一车一区阀门") == true ? 1 : 0);
    data.append(getOpenedByGate("一车分区阀门", "一车二区阀门") == true ? 1 : 0);
    data.append(getClosedByGate("一车分区阀门", "一车二区阀门") == true ? 1 : 0);
    data.append(getOpenedByGate("二车总阀", "二车总阀门") == true ? 1 : 0);
    data.append(getClosedByGate("二车总阀", "二车总阀门") == true ? 1 : 0);
    data.append(getOpenedByGate("二车分区阀门", "二车一区阀门") == true ? 1 : 0);
    data.append(getClosedByGate("二车分区阀门", "二车一区阀门") == true ? 1 : 0);
    data.append(getOpenedByGate("二车分区阀门", "二车二区阀门") == true ? 1 : 0);
    data.append(getClosedByGate("二车分区阀门", "二车二区阀门") == true ? 1 : 0);
    data.append(getOpenedByGate("三车总阀", "三车总阀门") == true ? 1 : 0);
    data.append(getClosedByGate("三车总阀", "三车总阀门") == true ? 1 : 0);
    data.append(getOpenedByGate("三车分区阀门", "三车一区阀门") == true ? 1 : 0);
    data.append(getClosedByGate("三车分区阀门", "三车一区阀门") == true ? 1 : 0);
    data.append(getOpenedByGate("三车分区阀门", "三车二区阀门") == true ? 1 : 0);
    data.append(getClosedByGate("三车分区阀门", "三车二区阀门") == true ? 1 : 0);
    data.append(getOpenedByGate("四车分区阀门", "四车一区阀门") == true ? 1 : 0);
    data.append(getClosedByGate("四车分区阀门", "四车一区阀门") == true ? 1 : 0);
    data.append(getOpenedByGate("四车分区阀门", "四车二区阀门") == true ? 1 : 0);
    data.append(getClosedByGate("四车分区阀门", "四车二区阀门") == true ? 1 : 0);
    data.append(getOpenedByGate("四车总阀", "四车总阀门") == true ? 1 : 0);
    data.append(getClosedByGate("四车总阀", "四车总阀门") == true ? 1 : 0);
    data.append(getOpenedByGate("五车分区阀门", "五车一区阀门") == true ? 1 : 0);
    data.append(getClosedByGate("五车分区阀门", "五车一区阀门") == true ? 1 : 0);
    data.append(getOpenedByGate("五车分区阀门", "五车二区阀门") == true ? 1 : 0);
    data.append(getClosedByGate("五车分区阀门", "五车二区阀门") == true ? 1 : 0);
    data.append(getOpenedByGate("五车总阀", "五车总阀门") == true ? 1 : 0);
    data.append(getClosedByGate("五车总阀", "五车总阀门") == true ? 1 : 0);
    data.append(getOpenedByGate("六车分区阀门", "六车一区阀门") == true ? 1 : 0);
    data.append(getClosedByGate("六车分区阀门", "六车一区阀门") == true ? 1 : 0);
    data.append(getOpenedByGate("六车分区阀门", "六车二区阀门") == true ? 1 : 0);
    data.append(getClosedByGate("六车分区阀门", "六车二区阀门") == true ? 1 : 0);
    data.append(getOpenedByGate("A框架出水总阀门", "A框架进气阀门") == true ? 1 : 0);
    data.append(getClosedByGate("A框架出水总阀门", "A框架进气阀门") == true ? 1 : 0);
    data.append(getOpenedByGate("A框架出水总阀门", "A框架出水阀门") == true ? 1 : 0);
    data.append(getClosedByGate("A框架出水总阀门", "A框架出水阀门") == true ? 1 : 0);
    data.append(getOpenedByGate("B框架出水总阀门", "B框架进气阀门") == true ? 1 : 0);
    data.append(getClosedByGate("B框架出水总阀门", "B框架进气阀门") == true ? 1 : 0);
    data.append(getOpenedByGate("B框架出水总阀门", "B框架出水阀门") == true ? 1 : 0);
    data.append(getClosedByGate("B框架出水总阀门", "B框架出水阀门") == true ? 1 : 0);
    data.append(getOpenedByIO("一车二区控制器", "一车D柜气灭瓶液位") == true ? 1 : 0);
    data.append(getOpenedByIO("一车二区控制器", "一车H柜气灭瓶液位") == true ? 1 : 0);
    data.append(getOpenedByIO("一车二区控制器", "一车F柜气灭瓶液位") == true ? 1 : 0);
    data.append(getOpenedByIO("一车二区控制器", "一车R柜气灭瓶液位") == true ? 1 : 0);
    data.append(getOpenedByIO("二车一区控制器", "二车F柜气灭瓶液位") == true ? 1 : 0);
    data.append(getOpenedByIO("二车一区控制器", "二车R柜气灭瓶液位") == true ? 1 : 0);
    data.append(getOpenedByIO("三车一区控制器", "三车F柜气灭瓶液位") == true ? 1 : 0);
    data.append(getOpenedByIO("三车一区控制器", "三车R柜气灭瓶液位") == true ? 1 : 0);
    data.append(getOpenedByIO("四车一区控制器", "四车F柜气灭瓶液位") == true ? 1 : 0);
    data.append(getOpenedByIO("四车一区控制器", "四车R柜气灭瓶液位") == true ? 1 : 0);
    data.append(getOpenedByIO("五车一区控制器", "五车F柜气灭瓶液位") == true ? 1 : 0);
    data.append(getOpenedByIO("五车一区控制器", "五车R柜气灭瓶液位") == true ? 1 : 0);
    data.append(getOpenedByIO("六车二区控制器", "六车D柜气灭瓶液位") == true ? 1 : 0);
    data.append(getOpenedByIO("六车二区控制器", "六车H柜气灭瓶液位") == true ? 1 : 0);
    data.append(getOpenedByIO("六车二区控制器", "六车F柜气灭瓶液位") == true ? 1 : 0);
    data.append(getOpenedByIO("六车二区控制器", "六车R柜气灭瓶液位") == true ? 1 : 0);
    data.append(getMultiLambdaVariableStatus("一车D柜气灭瓶液位低") == true ? 1 : 0);
    data.append(getMultiLambdaVariableStatus("一车H柜气灭瓶液位低") == true ? 1 : 0);
    data.append(getMultiLambdaVariableStatus("一车F柜气灭瓶液位低") == true ? 1 : 0);
    data.append(getMultiLambdaVariableStatus("一车R柜气灭瓶液位低") == true ? 1 : 0);
    data.append(getMultiLambdaVariableStatus("二车F柜气灭瓶液位低") == true ? 1 : 0);
    data.append(getMultiLambdaVariableStatus("二车R柜气灭瓶液位低") == true ? 1 : 0);
    data.append(getMultiLambdaVariableStatus("三车F柜气灭瓶液位低") == true ? 1 : 0);
    data.append(getMultiLambdaVariableStatus("三车R柜气灭瓶液位低") == true ? 1 : 0);
    data.append(getMultiLambdaVariableStatus("四车F柜气灭瓶液位低") == true ? 1 : 0);
    data.append(getMultiLambdaVariableStatus("四车R柜气灭瓶液位低") == true ? 1 : 0);
    data.append(getMultiLambdaVariableStatus("五车F柜气灭瓶液位低") == true ? 1 : 0);
    data.append(getMultiLambdaVariableStatus("五车R柜气灭瓶液位低") == true ? 1 : 0);
    data.append(getMultiLambdaVariableStatus("六车D柜气灭瓶液位低") == true ? 1 : 0);
    data.append(getMultiLambdaVariableStatus("六车H柜气灭瓶液位低") == true ? 1 : 0);
    data.append(getMultiLambdaVariableStatus("六车F柜气灭瓶液位低") == true ? 1 : 0);
    data.append(getMultiLambdaVariableStatus("六车R柜气灭瓶液位低") == true ? 1 : 0);
    data.append(getOpenedByIO("A框架二区控制器", "A框架瓶口阀") == true ? 1 : 0);
    data.append(getOpenedByIO("A框架二区控制器", "A框架加热器") == true ? 1 : 0);
    data.append(getMultiLambdaVariableStatus("A框架液位低") == true ? 1 : 0);
    data.append(getMultiLambdaVariableStatus("A框架氮气瓶一压力低") == true ? 1 : 0);
    data.append(getMultiLambdaVariableStatus("A框架氮气瓶二压力低") == true ? 1 : 0);
    data.append(getMultiLambdaVariableStatus("A框架加热器故障") == true ? 1 : 0);
    data.append(getOpenedByIO("B框架二区控制器", "B框架瓶口阀") == true ? 1 : 0);
    data.append(getOpenedByIO("B框架二区控制器", "B框架加热器") == true ? 1 : 0);
    data.append(getMultiLambdaVariableStatus("B框架液位低") == true ? 1 : 0);
    data.append(getMultiLambdaVariableStatus("B框架氮气瓶一压力低") == true ? 1 : 0);
    data.append(getMultiLambdaVariableStatus("B框架氮气瓶二压力低") == true ? 1 : 0);
    data.append(getMultiLambdaVariableStatus("B框架加热器故障") == true ? 1 : 0);
    data.append(getAnniuByIO("一车一区控制器", "细水雾启动按钮") == true ? 1 : 0);
    data.append(getAnniuByIO("一车一区控制器", "测试按钮") == true ? 1 : 0);
    data.append(getAnniuByIO("一车一区控制器", "复位按钮") == true ? 1 : 0);
    data.append(getAnniuByIO("六车一区控制器", "细水雾启动按钮") == true ? 1 : 0);
    data.append(getAnniuByIO("六车一区控制器", "测试按钮") == true ? 1 : 0);
    data.append(getAnniuByIO("六车一区控制器", "复位按钮") == true ? 1 : 0);
    data.append(getValueByIO("A框架一区控制器", "A框架水温"));
    data.append(getValueByIO("B框架一区控制器", "B框架水温"));
    data.append(getValueByIO("A框架一区控制器", "A框架液位"));
    data.append(getValueByIO("B框架一区控制器", "B框架液位"));
    data.append(getValueByIO("A框架一区控制器", "A框架氮气瓶一压力") * 10);
    data.append(getValueByIO("A框架一区控制器", "A框架氮气瓶二压力") * 10);
    data.append(getValueByIO("B框架一区控制器", "B框架氮气瓶一压力") * 10);
    data.append(getValueByIO("B框架一区控制器", "B框架氮气瓶二压力") * 10);
    //数据copy
    emit sendDataToIOT(data);
}

void Core::setSystemDate(const QString date)
{
    if(date.indexOf(" ") == -1)
    {
        qDebug() << "mvb date isn't right";
        return;
    }
    QString _setdatecmd = QString("date -s ") + date.split(" ").at(0) + QString(" && hwclock --systohc");
    QString _settimecmd = QString("date -s ") + date.split(" ").at(1) + QString(" && hwclock --systohc");
    qDebug() << "strsettimecommand : " << _setdatecmd.replace("\"", "").toStdString().c_str();
    qDebug() << "strsettimecommand : " << _settimecmd.replace("\"", "").toStdString().c_str();
    system(_setdatecmd.toStdString().c_str());
    system(_settimecmd.toStdString().c_str());
}

void Core::processDoubleResetButton()
{
#ifdef DOUBLERESETBUTTON
    //1.到时间了就将其计数器清零
    for(int i =0;i<m_doubleResetButtonCount.size();i++)
    {
        m_doubleResetButtonCount[m_doubleResetButtonCount.keys().at(i)] = 0;
    }
    m_doubleResetSign = true;
#endif
}

void Core::processDoubleResetButtonYanShi()
{
#ifdef DOUBLERESETBUTTON
    if(m_doubleResetButtonTimer->isActive())
    {
        m_doubleResetButtonTimer->stop();
    }
    //获取所有的感温探测器的状态
    for(int i =0;i<m_fireDeviceStatusHash.size();i++)
    {
        FireDeviceStatus _status = getFireDeviceStatus(m_fireDeviceStatusHash.keys().at(i));
        if(_status.firestatus == "有火警")
        {
            qDebug() << m_fireDeviceStatusHash.keys().at(i) << "对其进行屏蔽";
            QString _devicename = m_fireDeviceStatusHash.keys().at(i);
            emit setFireSheildStatusSignal(_devicename, "已屏蔽");
            sendUDPEvent(_devicename, "已屏蔽");
        }
    }
#endif
}

void Core::processGeLiFuWeiLogic(const QString devicename, const FireDeviceStatus status)
{
    if(status.firestatus == "有火警")
    {
        if(m_fireDeviceFireMutexHashByUpdateStatus.value(devicename))
        {
            //            qDebug() << "发现有火警，存一份";
            m_fireDeviceFireStatusHashByUpdateStatus[devicename] = status.firestatus;
            m_fireDeviceFireMutexHashByUpdateStatus[devicename] = false;
        }
    }
    if(status.resetstatus == "复位成功" && m_fireDeviceFireStatusHashByUpdateStatus[devicename] == "有火警")
    {
        if(m_fireDeviceResetMutexHashByUpdateStatus.value(devicename))
        {
            //            qDebug() << "存一份有火警，并且复位成功,次数+1，并将其清空位无火警和解锁";
            m_doubleResetFireDeviceFireStatusHash[devicename] += 1;
            m_fireDeviceFireMutexHashByUpdateStatus[devicename] = true;
            m_fireDeviceFireStatusHashByUpdateStatus[devicename] = "无火警";
            m_fireDeviceResetMutexHashByUpdateStatus[devicename] = false;
        }
    }
    if(m_doubleResetFireDeviceFireStatusHash[devicename] == 1)
    {
        //启动两小时定时器
        if(!m_doubleResetFireDeviceFireTimerHash[devicename]->isActive())
        {
            //            qDebug() << "启动:" << devicename << "两小时定时器";
            m_doubleResetFireDeviceFireTimerHash[devicename]->start();
            m_fireDeviceResetMutexHashByUpdateStatus[devicename] = true;
        }
    }else if(m_doubleResetFireDeviceFireStatusHash[devicename] >= 2)
    {
        //执行隔离
        m_fireSmogDeviceStatusHash[devicename] = "已屏蔽";
        emit setFireSheildStatusSignal(devicename, "已屏蔽");
        sendUDPEvent(devicename, "已屏蔽");
        clearDeviceButtonClickedCount(devicename);
        //关闭定时器
        if(m_doubleResetFireDeviceFireTimerHash[devicename]->isActive())
        {
            //            qDebug() << "关闭:" << devicename << "两小时定时器";
            m_doubleResetFireDeviceFireTimerHash[devicename]->stop();
        }
        //计数器清0
    }
}

void Core::processGeLiFuWeiLogic(const QString devicename, const SmogDeviceStatus status)
{
    QString _yiche = "一车司机室感烟探测器";
    QString _liuche = "六车司机室感烟探测器";
    QString _name;
    if(status.firestatus.keys().contains(_yiche))
    {
        _name = _yiche;
    }else if(status.firestatus.keys().contains(_liuche))
    {
        _name = _liuche;
    }
    if(status.firestatus.value(_name) == "有火警")
    {
        if(m_smogDeviceFireMutexHashByUpdateStatus.value(_name))
        {
            //            qDebug() << "发现有感烟火警，存一份";
            m_smogDeviceFireStatusHashByUpdateStatus[_name] = status.firestatus.value(_name);
            m_smogDeviceFireMutexHashByUpdateStatus[_name] = false;
        }
    }
    //    qDebug() << _name << "火警状态：" << status.firestatus.value(_name);
    //    qDebug() << _name << "复位状态：" << status.resetstatus.value(_name);
    if(status.resetstatus.value(_name) == "复位成功" && m_smogDeviceFireStatusHashByUpdateStatus[_name] == "有火警")
    {
        if(m_smogDeviceResetMutexHashByUpdateStatus.value(_name))
        {
            //            qDebug() << "存一份有火警，并且复位成功,次数+1，并将其清空位感烟无火警和解锁";
            m_doubleResetFireDeviceFireStatusHash[_name] += 1;
            m_smogDeviceFireMutexHashByUpdateStatus[_name] = true;
            m_smogDeviceFireStatusHashByUpdateStatus[_name] = "无火警";
            m_smogDeviceResetMutexHashByUpdateStatus[_name] = false;
        }
    }
    if(m_doubleResetFireDeviceFireStatusHash[_name] == 1)
    {
        //启动两小时定时器
        if(!m_doubleResetFireDeviceFireTimerHash[_name]->isActive())
        {
            //            qDebug() << "启动:" << devicename << "两小时定时器";
            m_doubleResetFireDeviceFireTimerHash[_name]->start();
            m_smogDeviceResetMutexHashByUpdateStatus[_name] = true;
        }
    }else if(m_doubleResetFireDeviceFireStatusHash[_name] >= 2)
    {
        m_fireSmogDeviceStatusHash[_name] = "已屏蔽";
        emit setSmogShieldStatusSignal(devicename, _name, "已屏蔽");
        //执行隔离
        sendUDPEvent(devicename, _name, "已屏蔽");
        clearDeviceButtonClickedCount(_name);
        //关闭定时器
        if(m_doubleResetFireDeviceFireTimerHash[_name]->isActive())
        {
            //            qDebug() << "关闭:" << devicename << "两小时定时器";
            m_doubleResetFireDeviceFireTimerHash[_name]->stop();
        }
        //计数器清0
    }
}

void Core::clearButtonClickedCount()
{
    for(int i =0;i<m_fireSmogDeviceStatusHash.size();i++)
    {
        m_fireSmogDeviceStatusHash[m_fireSmogDeviceStatusHash.keys().at(i)] = "未屏蔽";
    }
    //执行清空按钮按压次数
    qDebug() << "两小时已到，清空按钮按压次数";
    QList<QString> _keys = m_buttonClickedCountHash.keys();
    for(int i =0;i<_keys.size();i++)
    {
        m_buttonClickedCountHash[_keys.at(i)] = 0;
    }
    //清空复位前的感温探测器状态
    for(int i =0;i<m_doubleResetFireDeviceFireStatusHash.keys().size();i++)
    {
        m_doubleResetFireDeviceFireStatusHash[m_doubleResetFireDeviceFireStatusHash.keys().at(i)] = 0;
    }
    for(int i =0;i<m_smogDeviceFireMutexHashByUpdateStatus.size();i++)
    {
        m_smogDeviceFireMutexHashByUpdateStatus[m_smogDeviceFireMutexHashByUpdateStatus.keys().at(i)] = true;
    }
    for(int i =0;i<m_smogDeviceFireStatusHashByUpdateStatus.size();i++)
    {
        m_smogDeviceFireStatusHashByUpdateStatus[m_smogDeviceFireStatusHashByUpdateStatus.keys().at(i)] = "无火警";
    }
    for(int i =0;i<m_smogDeviceResetMutexHashByUpdateStatus.size();i++)
    {
        m_smogDeviceResetMutexHashByUpdateStatus[m_smogDeviceResetMutexHashByUpdateStatus.keys().at(i)] = true;
    }

    for(int i =0;i<m_fireDeviceFireMutexHashByUpdateStatus.size();i++)
    {
        m_fireDeviceFireMutexHashByUpdateStatus[m_fireDeviceFireMutexHashByUpdateStatus.keys().at(i)] = true;
    }

    for(int i =0;i<m_fireDeviceFireStatusHashByUpdateStatus.size();i++)
    {
        m_fireDeviceFireStatusHashByUpdateStatus[m_fireDeviceFireStatusHashByUpdateStatus.keys().at(i)] = "无火警";
    }
    for(int i =0;i<m_fireDeviceResetMutexHashByUpdateStatus.size();i++)
    {
        m_fireDeviceResetMutexHashByUpdateStatus[m_fireDeviceResetMutexHashByUpdateStatus.keys().at(i)] = true;
    }
    for(int i =0;i<m_doubleResetFireDeviceFireTimerHash.size();i++)
    {
        if(m_doubleResetFireDeviceFireTimerHash[m_doubleResetFireDeviceFireTimerHash.keys().at(i)]->isActive())
        {
            m_doubleResetFireDeviceFireTimerHash[m_doubleResetFireDeviceFireTimerHash.keys().at(i)]->stop();
        }
    }

}

void Core::clearDeviceButtonClickedCount(const QString devicename)
{
    m_doubleResetFireDeviceFireStatusHash[devicename] = 0;
    m_smogDeviceFireMutexHashByUpdateStatus[devicename] = true;
    m_smogDeviceFireStatusHashByUpdateStatus[devicename] = "无火警";
    m_smogDeviceResetMutexHashByUpdateStatus[devicename] = true;

    m_fireDeviceFireMutexHashByUpdateStatus[devicename] = true;
    m_fireDeviceFireStatusHashByUpdateStatus[devicename] = "无火警";
    m_fireDeviceResetMutexHashByUpdateStatus[devicename] = true;

}

void Core::processDoubleResetButtonYanShiCopy()
{
    //    qDebug() << "执行隔离复位!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!";
    QList<QString> _keys = m_doubleResetFireDeviceFireStatusHash.keys();
    for(int i =0;i<_keys.size();i++)
    {
        qDebug() << m_doubleResetFireDeviceFireStatusHash.values();
        if(m_doubleResetFireDeviceFireStatusHash.value(_keys.at(i)) >= 2)
        {
            //发送隔离消息
            qDebug() << _keys << "对其进行屏蔽";
            if(_keys.at(i).indexOf("感温") != -1)
            {
                emit setFireSheildStatusSignal(_keys.at(i), "已屏蔽");
                sendUDPEvent(_keys.at(i), "已屏蔽");
                clearDeviceButtonClickedCount(_keys.at(i));
            }
            if(_keys.at(i).indexOf("感烟") != -1)
            {
                if(_keys.at(i) == "六车司机室感烟探测器")
                {
                    emit setSmogShieldStatusSignal("感烟传感器B区", "六车司机室感烟探测器", "已屏蔽");
                    sendUDPEvent("感烟传感器B区", "六车司机室感烟探测器", "已屏蔽");
                    clearDeviceButtonClickedCount("六车司机室感烟探测器");
                }
                if(_keys.at(i) == "一车司机室感烟探测器")
                {
                    emit setSmogShieldStatusSignal("感烟传感器A区", "一车司机室感烟探测器", "已屏蔽");
                    sendUDPEvent("感烟传感器A区", "一车司机室感烟探测器", "已屏蔽");
                    clearDeviceButtonClickedCount("一车司机室感烟探测器");

                }
            }
        }
    }
}

void Core::processResetButtonDownByOneMintue()
{
    QString _yichetest = "一车测试按钮";
    QString _liuchetest = "六车测试按钮";
    QString _yichefuwei = "一车复位按钮";
    QString _liuchefuwei = "六车复位按钮";
    //一车或六车复位按钮按下
    bool _retfuwei1ci = m_buttonClickedCountHash[_yichefuwei] == 1 ||m_buttonClickedCountHash[_liuchefuwei] == 1;
    if(_retfuwei1ci)
    {
        //如果检测到六车或一车复位按钮按下
        //直接拿到所有的感温探测器的状态
        QList<QString> _fireKeys = m_fireDevice->getDeviceNames();
        for(int i =0;i<_fireKeys.size();i++)
        {
            FireDeviceStatus _tempFireDeviceStatus = m_fireDeviceStatushCopyHash.value(_fireKeys.at(i));
            if(_tempFireDeviceStatus.firestatus == "有火警")
            {
                qDebug() << _fireKeys.at(i) << "有火警" << __FUNCTION__ << __LINE__;
                m_doubleResetFireDeviceFireStatusHash[_fireKeys.at(i)] += 1;
                if(m_doubleResetFireDeviceFireStatusHash[_fireKeys.at(i)] == 1)
                {
                    //启动两小时定时器
                    if(!m_doubleResetFireDeviceFireTimerHash[_fireKeys.at(i)]->isActive())
                    {
                        qDebug() << "启动:" << _fireKeys.at(i) << "两小时定时器";
                        m_doubleResetFireDeviceFireTimerHash[_fireKeys.at(i)]->start();
                    }
                }else if(m_doubleResetFireDeviceFireStatusHash[_fireKeys.at(i)] >= 2)
                {
                    //执行隔离
                    emit setFireSheildStatusSignal(_fireKeys.at(i), "已屏蔽");
                    sendUDPEvent(_fireKeys.at(i), "已屏蔽");
                    clearDeviceButtonClickedCount(_fireKeys.at(i));
                    //关闭定时器
                    if(m_doubleResetFireDeviceFireTimerHash[_fireKeys.at(i)]->isActive())
                    {
                        qDebug() << "关闭:" << _fireKeys.at(i) << "两小时定时器";
                        m_doubleResetFireDeviceFireTimerHash[_fireKeys.at(i)]->stop();
                    }
                    //计数器清0
                }
            }
        }
    }

}

void Core::processGateStatusToUI()
{
    QJsonObject _obj;
    QList<QString> _keys = m_gateDeviceStatusHash.keys();
    QString _data;
    QHash<QString, MyGateDeviceInfo> _deviceInfo =  m_readConf->getGateDeviceInfoMap();
    for(int i = 0;i<m_gateDeviceStatusHash.size();i++)
    {
        GateDeviceStatus _status = getGateDeviceStatus(_keys.at(i));
        //总阀门名字-子阀门名字-子阀门状态-子阀门名字-子阀门状态
        MyGateDeviceInfo _dinfo = _deviceInfo.value(_keys.at(i));
        QString _Str;
        if(_dinfo.leftstatus == "有效")
        {
            _Str += _keys.at(i) + "-" + _status.leftname + "-" + _status.leftgatestatus + "#";
        }else
        {
        }
        if(_dinfo.rightstatus == "有效")
        {
            _Str += _keys.at(i) + "-" + _status.rightname + "-" + _status.rightgatestatus + "#";
        }else
        {

        }
        //获取哪些阀门是有效的
        _data += _Str;
    }
    _obj.insert("func", "updategatestatus");
    _obj.insert("data", _data);
    emit sendMessage(_obj);
}

void Core::processUpdateGateStatus(QString devicename, QString subdevicename, bool status)
{
    if(m_gateUnKnowStatusHash[devicename][subdevicename] != status)
    {
        m_gateUnKnowStatusHash[devicename][subdevicename] = status;
    }
}
//发送消息给从设备，告知从设备状态
void Core::sendFaultMessageToClient()
{
    if(getMasterClientStatus())
    {
        QList<QString> _keys = m_timerDeviceFaultInfoToUIHash.keys();
        QString _data;
        for(int i = 0;i < _keys.size();i++)
        {
            QJsonObject _obj = m_timerDeviceFaultInfoToUIHash.value(_keys.at(i));
            QString _type = _obj["type"].toString();
            QString _devicename = _obj["devicename"].toString();
            QString _cause = _obj["cause"].toString();
            QString _date = QString::number(QDateTime::fromString(_obj["date"].toString(), "yyyy-MM-dd hh:mm:ss").toTime_t());
            //            qDebug() << "_date:" << _obj["date"].toString() << ":" << QDateTime::fromString(_obj["date"].toString(), "yyyy-MM-dd hh:mm:ss").toTime_t();
            QString _isclear = _obj["isclear"].toString();
            QString _str("%1,%2,%3,%4,%5&");
            _str = _str
                    .arg(_type)
                    .arg(_devicename)
                    .arg(_cause)
                    .arg(_date)
                    .arg(_isclear);
            _data += _str;
            //将上面的数据发出去
        }
        QJsonObject _obj1;
        QString _type1 = FAULTTABLE;
        QString _devicename1 = "A框架加热器故障";
        QString _cause1 = "故障";
        QString _date1 = QString::number(QDateTime::fromString(QDateTime::currentDateTime().toString(), "yyyy-MM-dd hh:mm:ss").toTime_t());
        QString _isclear1 = getMultiLambdaVariableStatus("A框架加热器故障") ? ISNOTCLEAR : ISCLEAR;
        QString _str1("%1,%2,%3,%4,%5&");
        _str1 = _str1
                .arg(_type1)
                .arg(_devicename1)
                .arg(_cause1)
                .arg(_date1)
                .arg(_isclear1);
        _data += _str1;

        QJsonObject _obj2;
        QString _type2 = FAULTTABLE;
        QString _devicename2 = "B框架加热器故障";
        QString _cause2 = "故障";
        QString _date2 = QString::number(QDateTime::fromString(QDateTime::currentDateTime().toString(), "yyyy-MM-dd hh:mm:ss").toTime_t());
        QString _isclear2 = getMultiLambdaVariableStatus("B框架加热器故障") ? ISNOTCLEAR : ISCLEAR;
        QString _str2("%1,%2,%3,%4,%5&");
        _str2 = _str2
                .arg(_type2)
                .arg(_devicename2)
                .arg(_cause2)
                .arg(_date2)
                .arg(_isclear2);
        _data += _str2;

        QJsonObject _obj3;
        QString _type3 = FAULTTABLE;
        QString _devicename3 = "A框架暖风机故障";
        QString _cause3 = "故障";
        QString _date3 = QString::number(QDateTime::fromString(QDateTime::currentDateTime().toString(), "yyyy-MM-dd hh:mm:ss").toTime_t());
        QString _isclear3 = getMultiLambdaVariableStatus("A框架暖风机故障") ? ISNOTCLEAR : ISCLEAR;
        QString _str3("%1,%2,%3,%4,%5&");
        _str3 = _str3
                .arg(_type3)
                .arg(_devicename3)
                .arg(_cause3)
                .arg(_date3)
                .arg(_isclear3);
        _data += _str3;

        QJsonObject _obj4;
        QString _type4 = FAULTTABLE;
        QString _devicename4 = "B框架暖风机故障";
        QString _cause4 = "故障";
        QString _date4 = QString::number(QDateTime::fromString(QDateTime::currentDateTime().toString(), "yyyy-MM-dd hh:mm:ss").toTime_t());
        QString _isclear4 = getMultiLambdaVariableStatus("B框架暖风机故障") ? ISNOTCLEAR : ISCLEAR;
        QString _str4("%1,%2,%3,%4,%5&");
        _str4 = _str4
                .arg(_type4)
                .arg(_devicename4)
                .arg(_cause4)
                .arg(_date4)
                .arg(_isclear4);
        _data += _str4;

        QString _newData = _data.mid(0, _data.size() - 1);
        QJsonObject _obj;
        _obj.insert("type", "updatefaultmessage");
        _obj.insert("data", _newData);
        m_eventUdp->addEvent(_obj);
    }
}

void Core::sendQiMieStatusToClient()
{
    if(getMasterClientStatus())
    {
        QJsonObject _obj;
        bool _iooutputRet;
        _iooutputRet = getQiMieByIO("一车D柜气灭装置MVB状态");
        _obj.insert("一车D柜气灭装置MVB状态", _iooutputRet == true ? "true" : "false");
        _iooutputRet = getQiMieByIO("一车H柜气灭装置MVB状态");
        _obj.insert("一车H柜气灭装置MVB状态", _iooutputRet == true ? "true" : "false");
        _iooutputRet = getQiMieByIO("一车F柜气灭装置MVB状态");
        _obj.insert("一车F柜气灭装置MVB状态", _iooutputRet == true ? "true" : "false");
        _iooutputRet = getQiMieByIO("一车R柜气灭装置MVB状态");
        _obj.insert("一车R柜气灭装置MVB状态", _iooutputRet == true ? "true" : "false");
        _iooutputRet = getQiMieByIO("二车F柜气灭装置MVB状态");
        _obj.insert("二车F柜气灭装置MVB状态", _iooutputRet == true ? "true" : "false");
        _iooutputRet = getQiMieByIO("二车R柜气灭装置MVB状态");
        _obj.insert("二车R柜气灭装置MVB状态", _iooutputRet == true ? "true" : "false");
        _iooutputRet = getQiMieByIO("三车F柜气灭装置MVB状态");
        _obj.insert("三车F柜气灭装置MVB状态", _iooutputRet == true ? "true" : "false");
        _iooutputRet = getQiMieByIO("三车R柜气灭装置MVB状态");
        _obj.insert("三车R柜气灭装置MVB状态", _iooutputRet == true ? "true" : "false");
        _iooutputRet = getQiMieByIO("四车F柜气灭装置MVB状态");
        _obj.insert("四车F柜气灭装置MVB状态", _iooutputRet == true ? "true" : "false");
        _iooutputRet = getQiMieByIO("四车R柜气灭装置MVB状态");
        _obj.insert("四车R柜气灭装置MVB状态", _iooutputRet == true ? "true" : "false");
        _iooutputRet = getQiMieByIO("五车F柜气灭装置MVB状态");
        _obj.insert("五车F柜气灭装置MVB状态", _iooutputRet == true ? "true" : "false");
        _iooutputRet = getQiMieByIO("五车R柜气灭装置MVB状态");
        _obj.insert("五车R柜气灭装置MVB状态", _iooutputRet == true ? "true" : "false");
        _iooutputRet = getQiMieByIO("六车D柜气灭装置MVB状态");
        _obj.insert("六车D柜气灭装置MVB状态", _iooutputRet == true ? "true" : "false");
        _iooutputRet = getQiMieByIO("六车H柜气灭装置MVB状态");
        _obj.insert("六车H柜气灭装置MVB状态", _iooutputRet == true ? "true" : "false");
        _iooutputRet = getQiMieByIO("六车F柜气灭装置MVB状态");
        _obj.insert("六车F柜气灭装置MVB状态", _iooutputRet == true ? "true" : "false");
        _iooutputRet = getQiMieByIO("六车R柜气灭装置MVB状态");
        _obj.insert("六车R柜气灭装置MVB状态", _iooutputRet == true ? "true" : "false");
        _iooutputRet = getQiMieByIO("A框架瓶口阀开启");
        _obj.insert("A框架瓶口阀开启", _iooutputRet == true ? "true" : "false");
        _iooutputRet = getQiMieByIO("A框架加热器开启");
        _obj.insert("A框架加热器开启", _iooutputRet == true ? "true" : "false");
        _iooutputRet = getQiMieByIO("A框架暖风机开启");
        _obj.insert("A框架暖风机开启", _iooutputRet == true ? "true" : "false");
        _iooutputRet = getQiMieByIO("B框架瓶口阀开启");
        _obj.insert("B框架瓶口阀开启", _iooutputRet == true ? "true" : "false");
        _iooutputRet = getQiMieByIO("B框架加热器开启");
        _obj.insert("B框架加热器开启", _iooutputRet == true ? "true" : "false");
        _iooutputRet = getQiMieByIO("B框架暖风机开启");
        _obj.insert("B框架暖风机开启", _iooutputRet == true ? "true" : "false");


        _iooutputRet = getQiMieByIO("A框架温度低");
        _obj.insert("A框架温度低", _iooutputRet == true ? "true" : "false");
        _iooutputRet = getQiMieByIO("B框架温度低");
        _obj.insert("B框架温度低", _iooutputRet == true ? "true" : "false");


        _obj.insert("type", "updateqimiestatus");
        m_eventUdp->addEvent(_obj);
    }
}

void Core::processFaultMessageToUI(QString data)
{
    if(data.indexOf("&") != -1)
    {
        QList<QString> _list = data.split("&");
        //现在每个list里
        for(int i =0;i<_list.size();i++)
        {
            if(_list.at(i).indexOf(",") != -1)
            {
                QList<QString> _subList = _list.at(i).split(",");
                QString _type = _subList.at(0);
                QString _devicename = _subList.at(1);
                QString _cause = _subList.at(2);
                QString _date = _subList.at(3);
                QString _isclear = _subList.at(4);
                QJsonObject _obj;
                _obj.insert("type", _type);
                _obj.insert("devicename", _devicename);
                _obj.insert("cause", _cause);
                _obj.insert("date", QDateTime::fromTime_t(_date.toUInt()).toString("yyyy-MM-dd hh:mm:ss"));
                _obj.insert("isclear", _isclear);
                _obj.insert("func", "telluifirefaultevent");
                if(_isclear == ISNOTCLEAR)
                {
                    if(!m_faultMessageIntoToUIHash.contains(_devicename))
                    {
                        MyDataBaseWord _word;
                        _word.type = FAULTTABLE;
                        _word.fault_word.devicename = _devicename;
                        _word.fault_word.casue = _cause;
                        _word.fault_word.isclear = _isclear;
                        _word.fault_word.date = _date.toUInt();
                        emit insertDataToDataBaseSignal(_word);
                        m_faultMessageIntoToUIHash.insert(_devicename, _obj);
                    }else
                    {
                        QJsonObject _obj = m_faultMessageIntoToUIHash.value(_devicename);

                        //                                                    qDebug() << "type:" << _type
                        //                                                             << "devicename:" << _devicename
                        //                                                             << "cause:" << _cause
                        //                                                             << "date:" << _date
                        //                                                             << "isclear:" << _isclear;
                        if(_devicename.indexOf("A框架液位低故障") != -1)
                        {
                            setMultiLambdaVariableStatus("A框架液位低", true);
                        }else if(_devicename.indexOf("B框架液位低故障") != -1)
                        {
                            setMultiLambdaVariableStatus("B框架液位低", true);
                        }
                        else
                        {
                            setMultiLambdaVariableStatus(_devicename, true);
                        }

                        emit sendMessage(_obj);
                    }
                }else if(_isclear == ISCLEAR)
                {
                    if(m_faultMessageIntoToUIHash.contains(_devicename))
                    {
                        QJsonObject obj = m_faultMessageIntoToUIHash.value(_devicename);
                        if(obj["isclear"].toString() == ISNOTCLEAR)
                        {

                            //                                                            qDebug() << "type:" << _type
                            //                                                                     << "devicename:" << _devicename
                            //                                                                     << "cause:" << _cause
                            //                                                                     << "date:" << _date
                            //                                                                     << "isclear:" << _isclear;

                            if(_devicename.indexOf("A框架液位低故障") != -1)
                            {
                                setMultiLambdaVariableStatus("A框架液位低", false);
                            }else if(_devicename.indexOf("B框架液位低故障") != -1)
                            {
                                setMultiLambdaVariableStatus("B框架液位低", false);
                            }else
                            {
                                setMultiLambdaVariableStatus(_devicename, false);
                            }
                            obj["isclear"] = _isclear;
                            emit sendMessage(obj);
                            m_faultMessageIntoToUIHash.remove(_devicename);

                            MyDataBaseWord _word;
                            _word.type = FAULTTABLE;
                            _word.fault_word.devicename = obj["devicename"].toString();
                            _word.fault_word.casue = obj["cause"].toString();
                            _word.fault_word.isclear = obj["isclear"].toString();
                            _word.fault_word.date = QDateTime::fromString(_obj["date"].toString(), "yyyy-MM-dd hh:mm:ss").toTime_t();
                            //                            emit insertDataToDataBaseSignal(_word);
                        }
                    }
                }
            }
        }
    }
}

void Core::processEventMessageToUI(QString data)
{
    //设备名：xx按钮
    //原因：按下
    //日期：2022
    //清除：未清除
    //    qDebug() << data;
    if(data.indexOf(",") != -1)
    {
        QList<QString> _list = data.split(",");
        QString _type = _list.at(0);
        QString _devicename = _list.at(1);
        QString _cause = _list.at(2);
        QString _date = _list.at(3);
        QString _isclear = _list.at(4);
        QJsonObject _obj;
        _obj.insert("type", _type);
        _obj.insert("devicename", _devicename);
        _obj.insert("cause", _cause);
        _obj.insert("date", _date);
        _obj.insert("isclear", _isclear);
        _obj.insert("func", "telluifirefaultevent");
        qDebug() << _type
                 << _devicename
                 << _cause
                 << _date
                 << _isclear;
        bool _status;
        if(_cause == "按下")
        {
            _status = true;
        }else if(_cause == "弹起")
        {
            _status = false;
        }
        setMultiLambdaVariableStatus(_devicename, _status);
    }
}

void Core::processQiMieStatusToVar(QJsonObject obj)
{
    QList<QString> _keys = obj.keys();
    for(int i =0;i<_keys.size();i++)
    {
        bool _ret;
        if(obj.value(_keys.at(i)).toString() == "true")
        {
            _ret = true;
        }else if(obj.value(_keys.at(i)).toString() == "false")
        {
            _ret = false;
        }
        setMultiLambdaVariableStatus(_keys.at(i), _ret);
    }
}

void Core::updateGateStatusToUI()
{
    if(m_gateUnKnowStatusSign)
    {
        QList<QString> _keys = m_gateDeviceStatusHash.keys();
        QString _data;
        for(int i =0;i<_keys.size();i++)
        {
            QString _devicename = _keys.at(i);
            GateDeviceStatus _status = getGateDeviceStatus(_devicename);
            //阀门名、开到位、日期
            if(_status.leftshieldstatus == "有效")
            {
                if(_status.leftgatestatus != "未知")
                {
                    QString _leftstr("%1,%2,%3");
                    _leftstr = _leftstr
                            .arg(_status.leftname)
                            .arg(_status.leftgatestatus)
                            .arg(QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss"));
                    _data += _leftstr + "#";
                }
            }
            if(_status.rightshieldstatus == "有效")
            {
                if(_status.rightgatestatus != "未知")
                {
                    QString _rightstr("%1,%2,%3");
                    _rightstr = _rightstr
                            .arg(_status.rightname)
                            .arg(_status.rightgatestatus)
                            .arg(QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss"));
                    _data += _rightstr + "#";
                }
            }
        }
        QJsonObject _obj;
        _obj.insert("func", "telluifirefaultevent");
        _obj.insert("type", EVENTTABLE);
        _obj.insert("class", "gate");
        _obj.insert("data", _data.mid(0, _data.size() - 1));
        //        qDebug() << _data.mid(0, _data.size() - 1);
        emit sendMessage(_obj);
    }
}

void Core::updateIODeviceStatusFromMasterOrClient(QHash<QString, QHash<QString, bool> > hash)
{
    QList<QString> _keys = hash.keys();
    for(int i =0;i<_keys.size();i++)
    {
        QHash<QString, bool> _hash = hash[_keys.at(i)];
        QList<QString> _subKeys = _hash.keys();
        for(int j =0;j<_subKeys.size();j++)
        {
            qDebug() << "devicename:" <<_keys.at(i) << ",subdevicename:" << _subKeys.at(j) << ",status:" << hash[_keys.at(i)][_subKeys.at(j)];
            m_ioOpenCloseLanJieHash[_keys.at(i)][_subKeys.at(j)] = hash[_keys.at(i)][_subKeys.at(j)];
        }
    }
}


void Core::writeLogToLogFile(QString text)
{
    QFile file("./log.txt");
    if(!file.open(QIODevice::WriteOnly | QIODevice::Append))
    {
        qDebug() << "log file create error";
    }
    QString _data("[%1]:[%2]\r\n");
    _data = _data
            .arg(QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss"))
            .arg(text);
    file.write(_data.toLocal8Bit());
    file.flush();
    file.close();
}





