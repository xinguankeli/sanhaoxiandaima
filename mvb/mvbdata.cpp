#include "mvbdata.h"
#include <QHash>
#ifndef MVBDATA_COUNT
#define MVBDATA_COUNT 30
#endif
#include <QDebug>
const unsigned short crc_table[] = {
    0x0000, 0x1021, 0x2042, 0x3063, 0x4084, 0x50a5, 0x60c6, 0x70e7,
    0x8108, 0x9129, 0xa14a, 0xb16b, 0xc18c, 0xd1ad, 0xe1ce, 0xf1ef,
    0x1231, 0x0210, 0x3273, 0x2252, 0x52b5, 0x4294, 0x72f7, 0x62d6,
    0x9339, 0x8318, 0xb37b, 0xa35a, 0xd3bd, 0xc39c, 0xf3ff, 0xe3de,
    0x2462, 0x3443, 0x0420, 0x1401, 0x64e6, 0x74c7, 0x44a4, 0x5485,
    0xa56a, 0xb54b, 0x8528, 0x9509, 0xe5ee, 0xf5cf, 0xc5ac, 0xd58d,
    0x3653, 0x2672, 0x1611, 0x0630, 0x76d7, 0x66f6, 0x5695, 0x46b4,
    0xb75b, 0xa77a, 0x9719, 0x8738, 0xf7df, 0xe7fe, 0xd79d, 0xc7bc,
    0x48c4, 0x58e5, 0x6886, 0x78a7, 0x0840, 0x1861, 0x2802, 0x3823,
    0xc9cc, 0xd9ed, 0xe98e, 0xf9af, 0x8948, 0x9969, 0xa90a, 0xb92b,
    0x5af5, 0x4ad4, 0x7ab7, 0x6a96, 0x1a71, 0x0a50, 0x3a33, 0x2a12,
    0xdbfd, 0xcbdc, 0xfbbf, 0xeb9e, 0x9b79, 0x8b58, 0xbb3b, 0xab1a,
    0x6ca6, 0x7c87, 0x4ce4, 0x5cc5, 0x2c22, 0x3c03, 0x0c60, 0x1c41,
    0xedae, 0xfd8f, 0xcdec, 0xddcd, 0xad2a, 0xbd0b, 0x8d68, 0x9d49,
    0x7e97, 0x6eb6, 0x5ed5, 0x4ef4, 0x3e13, 0x2e32, 0x1e51, 0x0e70,
    0xff9f, 0xefbe, 0xdfdd, 0xcffc, 0xbf1b, 0xaf3a, 0x9f59, 0x8f78,
    0x9188, 0x81a9, 0xb1ca, 0xa1eb, 0xd10c, 0xc12d, 0xf14e, 0xe16f,
    0x1080, 0x00a1, 0x30c2, 0x20e3, 0x5004, 0x4025, 0x7046, 0x6067,
    0x83b9, 0x9398, 0xa3fb, 0xb3da, 0xc33d, 0xd31c, 0xe37f, 0xf35e,
    0x02b1, 0x1290, 0x22f3, 0x32d2, 0x4235, 0x5214, 0x6277, 0x7256,
    0xb5ea, 0xa5cb, 0x95a8, 0x8589, 0xf56e, 0xe54f, 0xd52c, 0xc50d,
    0x34e2, 0x24c3, 0x14a0, 0x0481, 0x7466, 0x6447, 0x5424, 0x4405,
    0xa7db, 0xb7fa, 0x8799, 0x97b8, 0xe75f, 0xf77e, 0xc71d, 0xd73c,
    0x26d3, 0x36f2, 0x0691, 0x16b0, 0x6657, 0x7676, 0x4615, 0x5634,
    0xd94c, 0xc96d, 0xf90e, 0xe92f, 0x99c8, 0x89e9, 0xb98a, 0xa9ab,
    0x5844, 0x4865, 0x7806, 0x6827, 0x18c0, 0x08e1, 0x3882, 0x28a3,
    0xcb7d, 0xdb5c, 0xeb3f, 0xfb1e, 0x8bf9, 0x9bd8, 0xabbb, 0xbb9a,
    0x4a75, 0x5a54, 0x6a37, 0x7a16, 0x0af1, 0x1ad0, 0x2ab3, 0x3a92,
    0xfd2e, 0xed0f, 0xdd6c, 0xcd4d, 0xbdaa, 0xad8b, 0x9de8, 0x8dc9,
    0x7c26, 0x6c07, 0x5c64, 0x4c45, 0x3ca2, 0x2c83, 0x1ce0, 0x0cc1,
    0xef1f, 0xff3e, 0xcf5d, 0xdf7c, 0xaf9b, 0xbfba, 0x8fd9, 0x9ff8,
    0x6e17, 0x7e36, 0x4e55, 0x5e74, 0x2e93, 0x3eb2, 0x0ed1, 0x1ef0
};
MVBData::MVBData(const bool morc,QObject *parent) : QObject(parent),m_masterOrClient(morc)
{
    m_configPortSignStruct.crcbytelow = false;
    m_configPortSignStruct.crcbytehigh = false;
    m_configPortSignStruct.deviceaddresslowsign = false;
    m_configPortSignStruct.deviceaddresshgihsign = false;
    m_configPortSignStruct.mvbtomcuhostportcountsign = false;
    m_configPortSignStruct.mcutomvbsourceportcountsign = false;

    //头部三个字节默认命令
    m_configPortStruct.startbyte = 0xFE;
    m_configPortStruct.framelength = 0xFA;
    m_configPortStruct.configcommand = 0x05;
    //结束字节
    m_configPortStruct.endbyte = 0xFF;
    //默认填充多个空白端口
    for(int i =0;i<MVBDATA_COUNT;i++)
    {
        MCUToMVBPortAddress _struct;
        memset(&_struct,0 ,sizeof(MCUToMVBPortAddress));
        _struct.portaddresslow = 0x00;
        _struct.portaddresshigh = 0x00;
        _struct.portdatalength = 0x00;
        _struct.portcommunicationcycle = 0x00;
        m_configPortStruct.dist[i] = _struct;
        m_configPortStruct.source[i] = _struct;
    }
    //设备我的控制器的地址高低位，比如控制器地址是192.168.0.113

    setMCUToMVBSourcePortCount(2);
    if(m_masterOrClient == true)
    {
        setDeviceAddressHigh(0x00);
        setDeviceAddressLow(0x73);
        //设置主端口
        MCUToMVBPortAddress _struct;
        _struct.portaddresslow = 0x10;
        _struct.portaddresshigh = 0x0B;
        _struct.portdatalength = 0x04;
        _struct.portcommunicationcycle = 0x00;
        addMCUToMVB(_struct);
        MCUToMVBPortAddress _struct2;
        _struct2.portaddresslow = 0x11;
        _struct2.portaddresshigh = 0x0B;
        _struct2.portdatalength = 0x04;
        _struct2.portcommunicationcycle = 0x00;
        addMCUToMVB(_struct2);
    }else
    {
        setDeviceAddressHigh(0x00);
        setDeviceAddressLow(0x75);
        //设置从端口
        MCUToMVBPortAddress _struct;
        _struct.portaddresslow = 0x60;
        _struct.portaddresshigh = 0x0B;
        _struct.portdatalength = 0x04;
        _struct.portcommunicationcycle = 0x00;
        addMCUToMVB(_struct);
        MCUToMVBPortAddress _struct2;
        _struct2.portaddresslow = 0x61;
        _struct2.portaddresshigh = 0x0B;
        _struct2.portdatalength = 0x04;
        _struct2.portcommunicationcycle = 0x00;
        addMCUToMVB(_struct2);
    }


    setMVBToMCUHostPortCount(2);
    MCUToMVBPortAddress _struct3;
    _struct3.portaddresslow = 0xFF;
    _struct3.portaddresshigh = 0x00;
    _struct3.portdatalength = 0x03;
    _struct3.portcommunicationcycle = 0x00;
    addMVBToMCU(_struct3);

    MCUToMVBPortAddress _struct4;
    _struct4.portaddresslow = 0x08;
    _struct4.portaddresshigh = 0x0B;
    _struct4.portdatalength = 0x01;
    _struct4.portcommunicationcycle = 0x00;
    addMVBToMCU(_struct4);

    //下面是增加了

}

void MVBData::setDeviceAddressHigh(const quint8 byte)
{
    m_configPortStruct.deviceaddresshigh = byte;
    m_configPortSignStruct.deviceaddresshgihsign = true;

}

void MVBData::setDeviceAddressLow(const quint8 byte)
{
    m_configPortStruct.deviceaddresslow = byte;
    m_configPortSignStruct.deviceaddresslowsign = true;
}

void MVBData::setMCUToMVBSourcePortCount(const quint8 byte)
{
    m_configPortStruct.mcutomvbsourceportcount = byte;
    m_configPortSignStruct.mcutomvbsourceportcountsign = true;
}

void MVBData::setMVBToMCUHostPortCount(const quint8 byte)
{
    m_configPortStruct.mvbtomcuhostportcount = byte;
    m_configPortSignStruct.mvbtomcuhostportcountsign = true;
}

void MVBData::setCRCByteHigh(const quint8 byte)
{
    m_configPortStruct.crcbytehigh = byte;
    m_configPortSignStruct.crcbytehigh = true;
}

void MVBData::setCRCByteLow(const quint8 byte)
{
    m_configPortStruct.crcbytelow = byte;
    m_configPortSignStruct.crcbytelow = true;
}

void MVBData::addMVBToMCU(const MCUToMVBPortAddress address)
{
    if(m_mvbToMcuCount > MVBDATA_COUNT)
    {
        return;
    }
    m_configPortStruct.dist[m_mvbToMcuCount] = address;
    m_mvbToMcuCount += 1;
}

void MVBData::addMCUToMVB(const MCUToMVBPortAddress address)
{
    if(m_mcuToMvbCount > MVBDATA_COUNT)
    {
        return;
    }
    m_configPortStruct.source[m_mcuToMvbCount] = address;
    m_mcuToMvbCount += 1;
}

bool MVBData::canSend()
{
    return
            m_configPortSignStruct.crcbytelow
            && m_configPortSignStruct.crcbytehigh
            && m_configPortSignStruct.deviceaddresslowsign
            && m_configPortSignStruct.deviceaddresshgihsign
            && m_configPortSignStruct.mvbtomcuhostportcountsign
            && m_configPortSignStruct.mcutomvbsourceportcountsign;
}

uint16_t MVBData::my_crc(unsigned char *buf, int len)
{
    int counter;
        unsigned char *p=buf;
        unsigned short crc = 0x0000;
        for( counter = 0; counter < len; counter++)
            crc = (crc<<8) ^ crc_table[((crc>>8) ^ *(char *)p++)&0x00FF];
        return crc;
}

QMap<QString, QVariant> MVBData::getCrc(unsigned char *buf, int len)
{
    MAP _map;
    uint16_t value = my_crc(buf, len);
    uint8_t _left;
    memset(&_left, 0, sizeof(uint8_t));
    uint8_t _right;
    memset(&_right, 0, sizeof(uint8_t));
    uint8_t value3 = value >> 8;
    memcpy(&_left, &value3, sizeof(uint8_t));
    memcpy(&_right, &value, sizeof(uint8_t));
    _map.insert("high", _left);
    _map.insert("low", _right);
    return _map;
}

QList<quint8> MVBData::getStruct()
{
    QList<quint8> _list;
    unsigned char p[250] = {};
    p[0] = m_configPortStruct.startbyte;
    p[1] = m_configPortStruct.framelength;
    p[2] = m_configPortStruct.configcommand;
    p[3] = m_configPortStruct.deviceaddresshigh;
    p[4] = m_configPortStruct.deviceaddresslow;
    p[5] = m_configPortStruct.mcutomvbsourceportcount;
    int count = 6;
    for(int i =0;i<MVBDATA_COUNT;i++)
    {
        p[count + 0] = m_configPortStruct.source[i].portaddresshigh;
        p[count + 1] = m_configPortStruct.source[i].portaddresslow;
        p[count + 2] = m_configPortStruct.source[i].portdatalength;
        p[count + 3] = m_configPortStruct.source[i].portcommunicationcycle;
        count += 4;
    }
    p[126] = m_configPortStruct.mvbtomcuhostportcount;
    count = 127;
    for(int i =0;i<MVBDATA_COUNT;i++)
    {
        p[count + 0] = m_configPortStruct.dist[i].portaddresshigh;
        p[count + 1] = m_configPortStruct.dist[i].portaddresslow;
        p[count + 2] = m_configPortStruct.dist[i].portdatalength;
        p[count + 3] = m_configPortStruct.dist[i].portcommunicationcycle;
        count += 4;
    }
    MAP _map = getCrc(p, 247);
    setCRCByteHigh(_map["high"].toUInt());
    setCRCByteLow(_map["low"].toUInt());
    p[247] = m_configPortStruct.crcbytehigh;
    p[248] = m_configPortStruct.crcbytelow;
    p[249] = m_configPortStruct.endbyte;
    //设置crc函数
    for(int i = 0;i<250;i++)
    {
        _list.append(p[i]);
    }
    return _list;
}

QMap<QString, QVariant> MVBData::processStruct0xff(QByteArray arr)
{
    MAP _map;
    struct0xff _struct;
    memset(&_struct, 0, sizeof(struct0xff));
    memcpy(&_struct, arr.data(), sizeof(struct0xff));

    return _map;
}

QMap<QString, QVariant> MVBData::processStruct0xb10(QByteArray arr)
{
    MAP _map;
    struct0xb10Andb60 _struct;
    memset(&_struct, 0, sizeof(struct0xb10Andb60));
    memcpy(&_struct, arr.data(), sizeof(struct0xb10Andb60));

    return _map;
}

QMap<QString, QVariant> MVBData::processStruct0xb11(QByteArray arr)
{
    MAP _map;
    struct0xb11Andb61 _struct;
    memset(&_struct, 0, sizeof(struct0xb11Andb61));
    memcpy(&_struct, arr.data(), sizeof(struct0xb11Andb61   ));

    return _map;
}
