#ifndef MVBDATA_H
#define MVBDATA_H

#include <QObject>
#include "global/MVBGlobal.h"
#include "global/Global.h"
#include <QList>
#include <QVariant>
#define MAP QMap<QString, QVariant>
//该类用和mvb交互
//1.提供信号，比如设置时间、屏蔽设备等通过signals发出去
//2.提供和mvb交互的方法，比如发送数据的slot等
class MVBData : public QObject
{
    Q_OBJECT
public:
    explicit MVBData(const bool , QObject *parent = nullptr);
public:
    //设置设备地址的高位字节
    void setDeviceAddressHigh(const q8);
    //设置设备地址的低位字节
    void setDeviceAddressLow(const q8);
    //设置源端口数量
    void setMCUToMVBSourcePortCount(const q8);
    //设置宿端口数量
    void setMVBToMCUHostPortCount(const q8);
    //设置crc校验高字节
    void setCRCByteHigh(const q8);
    //设置crc校验低字节
    void setCRCByteLow(const q8);
    //添加一个mvb to mcu的地址
    void addMVBToMCU(const MCUToMVBPortAddress);
    //添加一个mcu to mvb的地址
    void addMCUToMVB(const MCUToMVBPortAddress);
    //如果结构体中关键字段没设置返回false，终止发送
    bool canSend();
    uint16_t my_crc(unsigned char *buf, int len);
    MAP getCrc(unsigned char *buf, int len);
    QList<q8> getStruct();
private:
    ConfigPort m_configPortStruct;
    ConfigPortSign m_configPortSignStruct;
    int m_mcuToMvbCount;//最多存放30个
    int m_mvbToMcuCount;//最多存放30个
    bool m_masterOrClient;
signals:
private slots:
    MAP processStruct0xff(QByteArray);
    MAP processStruct0xb10(QByteArray);
    MAP processStruct0xb11(QByteArray);
};

#endif // MVBDATA_H
