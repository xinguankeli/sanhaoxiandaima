#ifndef MYMVBSERVER_H
#define MYMVBSERVER_H

#include <QObject>
#include <QTcpServer>
#include "mvbdata.h"
#include <QTimer>
#include <QTcpSocket>
#include <QDateTime>
#include <QDate>
#include <QTime>
#include <QMutex>
#include <QHash>
#include <QReadLocker>
#include <QWriteLocker>
#include <QReadWriteLock>
class MyMVBServer : public QTcpServer
{
    Q_OBJECT
public:
    MyMVBServer(const bool, QObject *parent = nullptr);
    ~MyMVBServer();
    //设置端口
    void setPort(const q16);
    //通知mvb，停止获取消息
    void stopGetMessage();
    //通知mvb，开始获取消息
    void startGetMessage();

    uint16_t my_crc(unsigned char *buf, int len);
    MAP getCrc(unsigned char *buf, int len);
    //FE 2A 09 80 00 07 10 01 02 03 04 05 06 07 08 09 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 E2 21 FF
    //FE 2A 09 EA 00 07 10 01 02 03 04 05 06 07 08 09 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 63 05 FF
    quint16 m_autoadd;
public slots:
    void startListen();//子线程启动这个函数
    void sendData(DataRequestResponseFrame, q8, q8,bool);

private slots:
    void whenNewConnection();
    void whenClientConnected();
    void whenClientDisconnected();
    void whenSocketReadyRead();
    //设置250个字节的函数
    void setConfig();
    //于MVB发送来的数据对比
    void compareTime();
private:
    QTcpSocket * m_tcpSocket;
    quint16 m_port;
    MVBData *m_mvbData;
    //判断mvb是否通过网络连接到控制器
    bool m_sign;
    //是主还是从
    bool m_masterOrClient;
    //重新配置定时器
    QTimer * m_reSetConfigTimer;
    //判断mvb配置成功与否
    bool m_setConfigOk;
    //0xff时间有效标志位是否有效，只执行一次
    bool m_timeValidSign;

    //时间强制同步指令的下降沿
    bool m_xiajiangyan;
    //隔离复位的下降沿
    bool m_gelifuweixiajiangyan;
    //定时来于MVB对时
    QTimer *m_compareTimeTimer;
    //存放MVB发来的日期
    QHash<QString, q8> m_mvbToMeTimeHash;
    //频繁写入mvb的锁
    QMutex *m_mvbToMeTimeMutex;

    mutable QReadWriteLock m_lock;
signals:
    void sendMVBDataToCore(MVBToCoreData);
    void sendMVBStatus(QString);
private:
    bool panduanjiaoyanhe(const q8, const q8, const q8, const q8);
    //传入一个字符，根据索引返回对应的位是1还是0
    bool getStatusByBit(const char, const int);
    //传入一个字节，将对应位设置为对应值
    void setDataByBit(unsigned char &buf, const int index, const int value);
    //将对应字节bit位从1到8反转
    void fanzhuan(unsigned char * data, int size);
    //第一个参数为数据,第二个、三个就是配置文件中的地址，比如07 10 07 20 07 30第四个参数为是否自增
    bool computerCrc(const enableResponseFrame);
    bool computerCrc2(const enableResponseFrame2);
};

#endif // MYMVBSERVER_H
