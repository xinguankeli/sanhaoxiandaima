#include "mymvbserver.h"
#include <QTimer>
#ifndef BITFANZHAUN
#define BITFANZHUAN
#endif
const unsigned short crc_table[] = {
    0x0000, 0x1021, 0x2042, 0x3063, 0x4084, 0x50a5, 0x60c6, 0x70e7,
    0x8108, 0x9129, 0xa14a, 0xb16b, 0xc18c, 0xd1ad, 0xe1ce, 0xf1ef,
    0x1231, 0x0210, 0x3273, 0x2252, 0x52b5, 0x4294, 0x72f7, 0x62d6,
    0x9339, 0x8318, 0xb37b, 0xa35a, 0xd3bd, 0xc39c, 0xf3ff, 0xe3de,
    0x2462, 0x3443, 0x0420, 0x1401, 0x64e6, 0x74c7, 0x44a4, 0x5485,
    0xa56a, 0xb54b, 0x8528, 0x9509, 0xe5ee, 0xf5cf, 0xc5ac, 0xd58d,
    0x3653, 0x2672, 0x1611, 0x0630, 0x76d7, 0x66f6, 0x5695, 0x46b4,
    0xb75b, 0xa77a, 0x9719, 0x8738, 0xf7df, 0xe7fe, 0xd79d, 0xc7bc,
    0x48c4, 0x58e5, 0x6886, 0x78a7, 0x0840, 0x1861, 0x2802, 0x3823,
    0xc9cc, 0xd9ed, 0xe98e, 0xf9af, 0x8948, 0x9969, 0xa90a, 0xb92b,
    0x5af5, 0x4ad4, 0x7ab7, 0x6a96, 0x1a71, 0x0a50, 0x3a33, 0x2a12,
    0xdbfd, 0xcbdc, 0xfbbf, 0xeb9e, 0x9b79, 0x8b58, 0xbb3b, 0xab1a,
    0x6ca6, 0x7c87, 0x4ce4, 0x5cc5, 0x2c22, 0x3c03, 0x0c60, 0x1c41,
    0xedae, 0xfd8f, 0xcdec, 0xddcd, 0xad2a, 0xbd0b, 0x8d68, 0x9d49,
    0x7e97, 0x6eb6, 0x5ed5, 0x4ef4, 0x3e13, 0x2e32, 0x1e51, 0x0e70,
    0xff9f, 0xefbe, 0xdfdd, 0xcffc, 0xbf1b, 0xaf3a, 0x9f59, 0x8f78,
    0x9188, 0x81a9, 0xb1ca, 0xa1eb, 0xd10c, 0xc12d, 0xf14e, 0xe16f,
    0x1080, 0x00a1, 0x30c2, 0x20e3, 0x5004, 0x4025, 0x7046, 0x6067,
    0x83b9, 0x9398, 0xa3fb, 0xb3da, 0xc33d, 0xd31c, 0xe37f, 0xf35e,
    0x02b1, 0x1290, 0x22f3, 0x32d2, 0x4235, 0x5214, 0x6277, 0x7256,
    0xb5ea, 0xa5cb, 0x95a8, 0x8589, 0xf56e, 0xe54f, 0xd52c, 0xc50d,
    0x34e2, 0x24c3, 0x14a0, 0x0481, 0x7466, 0x6447, 0x5424, 0x4405,
    0xa7db, 0xb7fa, 0x8799, 0x97b8, 0xe75f, 0xf77e, 0xc71d, 0xd73c,
    0x26d3, 0x36f2, 0x0691, 0x16b0, 0x6657, 0x7676, 0x4615, 0x5634,
    0xd94c, 0xc96d, 0xf90e, 0xe92f, 0x99c8, 0x89e9, 0xb98a, 0xa9ab,
    0x5844, 0x4865, 0x7806, 0x6827, 0x18c0, 0x08e1, 0x3882, 0x28a3,
    0xcb7d, 0xdb5c, 0xeb3f, 0xfb1e, 0x8bf9, 0x9bd8, 0xabbb, 0xbb9a,
    0x4a75, 0x5a54, 0x6a37, 0x7a16, 0x0af1, 0x1ad0, 0x2ab3, 0x3a92,
    0xfd2e, 0xed0f, 0xdd6c, 0xcd4d, 0xbdaa, 0xad8b, 0x9de8, 0x8dc9,
    0x7c26, 0x6c07, 0x5c64, 0x4c45, 0x3ca2, 0x2c83, 0x1ce0, 0x0cc1,
    0xef1f, 0xff3e, 0xcf5d, 0xdf7c, 0xaf9b, 0xbfba, 0x8fd9, 0x9ff8,
    0x6e17, 0x7e36, 0x4e55, 0x5e74, 0x2e93, 0x3eb2, 0x0ed1, 0x1ef0
};
MyMVBServer::MyMVBServer(const bool morc, QObject *parent):QTcpServer(parent),m_masterOrClient(morc),m_setConfigOk(false),m_timeValidSign(true),m_xiajiangyan(false),m_gelifuweixiajiangyan(false)
{
    m_mvbData = new MVBData(m_masterOrClient);
    connect(this, &QTcpServer::newConnection, this, &MyMVBServer::whenNewConnection);
    m_reSetConfigTimer = new QTimer(this);
    connect(m_reSetConfigTimer, &QTimer::timeout, this, &MyMVBServer::setConfig);
    m_reSetConfigTimer->setInterval(2000);
    m_compareTimeTimer = new QTimer(this);
    connect(m_compareTimeTimer, &QTimer::timeout, this, &MyMVBServer::compareTime);
    m_compareTimeTimer->setInterval(1000 * 60);
//    m_compareTimeTimer->start();
    m_mvbToMeTimeHash.insert("nian", 0);
    m_mvbToMeTimeHash.insert("yue", 0);
    m_mvbToMeTimeHash.insert("ri", 0);
    m_mvbToMeTimeHash.insert("shi", 0);
    m_mvbToMeTimeHash.insert("fen", 0);
    m_mvbToMeTimeHash.insert("miao", 0);
}

MyMVBServer::~MyMVBServer()
{
    m_tcpSocket->close();
    delete m_tcpSocket;
}

void MyMVBServer::setPort(const quint16 port)
{
    m_port = port;
}

void MyMVBServer::startListen()
{
    if(!listen(QHostAddress::AnyIPv4, m_port))
    {
        qDebug() << "mvb server listen error";
    }else
    {
        qDebug() << "mvb server listen succ";
    }
}

void MyMVBServer::stopGetMessage()
{
    GetMessage _struct;
    _struct.startbyte = 0xFE;
    _struct.framelength = 0x08;
    _struct.commandbyte = 0x07;
    _struct.uploadcycle = 0x10;
    _struct.requestcommand = (char)0x00;
    unsigned char p[] = {0xFE, 0x08, 0x07, 0x00, 0x00};
    MAP _map = getCrc(p, 5);
    _struct.crchigh = _map["high"].toUInt();
    _struct.crclow = _map["low"].toUInt();
    _struct.endbyte = 0xFF;
    QByteArray _arr;
    _arr.resize(sizeof(GetMessage));
    memcpy(_arr.data(), &_struct, sizeof(GetMessage));
    m_tcpSocket->write(_arr);
    m_tcpSocket->flush();
}

void MyMVBServer::startGetMessage()
{
    GetMessage _struct;
    _struct.startbyte = 0xFE;
    _struct.framelength = 0x08;
    _struct.commandbyte = 0x07;
    _struct.uploadcycle = 0x10;
    _struct.requestcommand = 0x01;
    unsigned char p[] = {0xFE, 0x08, 0x07, 0x00, 0x01, 0xe0, 0x8c, 0xff};
    MAP _map = getCrc(p, 5);
    _struct.crchigh = _map["high"].toUInt();
    _struct.crclow = _map["low"].toUInt();
    _struct.endbyte = 0xFF;
    QByteArray _arr;
    q8 _a = 0xfe;
    q8 _b = 0x08;
    q8 _c = 0x07;
    q8 _d = 0x00;
    q8 _e = 0x01;
    q8 _f = 0xe0;
    q8 _g = 0x8c;
    q8 _h = 0xff;
    _arr.append(_a);
    _arr.append(_b);
    _arr.append(_c);
    _arr.append(_d);
    _arr.append(_e);
    _arr.append(_f);
    _arr.append(_g);
    _arr.append(_h);
    if(m_sign)
    {
        m_tcpSocket->write(_arr);
        m_tcpSocket->flush();
    }
}

void MyMVBServer::setConfig()
{
    QList<quint8> _arr = m_mvbData->getStruct();
    //    qDebug() << _arr;
    if(m_mvbData->canSend() == false)
    {
        return;
    }
    QList<QString> _a;
    for(int i =0;i<_arr.size();i++)
    {
        QString s;
        s = s.sprintf("%02X", _arr[i]);
        _a.append(s);
    }
    //    qDebug() << _a;
    //    qDebug() << _a.size();
    QByteArray _b;
    for(int i= 0 ;i<_a.size();i++)
    {
        _b.append(_a[i].toUInt(nullptr, 16));
    }
    if(m_sign)
    {
        m_tcpSocket->write(_b);
        m_tcpSocket->flush();
    }
}

void MyMVBServer::compareTime()
{
    bool _ret = true;
    //取出自己的时间
    int _year, _month, _day, _hour, _mintu, _second;
    _year = QDate::currentDate().year();
    _month = QDate::currentDate().month();
    _day = QDate::currentDate().day();
    _hour = QTime::currentTime().hour();
    _mintu = QTime::currentTime().minute();
    _second = QTime::currentTime().second();
    //取出mvb发来的时间
    q8 _nian, _yue, _ri, _shi, _fen, _miao;
    qDebug() << "槽函数尝试加锁";
    qDebug() << "槽函数加锁";
    QReadLocker locker(&m_lock);
    _nian = m_mvbToMeTimeHash.value("nian");
    _yue = m_mvbToMeTimeHash.value("yue");
    _ri = m_mvbToMeTimeHash.value("ri");
    _shi = m_mvbToMeTimeHash.value("shi");
    _fen = m_mvbToMeTimeHash.value("fen");
    _miao = m_mvbToMeTimeHash.value("miao");
    //判断年月日日分秒是否有不同
    if(_year != _nian)
    {
        _ret &= false;
    }
    if(_month != _yue)
    {
        _ret &= false;
    }
    if(_day != _ri)
    {
        _ret &= false;
    }
    if(_hour != _shi)
    {
        _ret &= false;
    }
    if(_mintu != _fen)
    {
        _ret &= false;
    }
    int _sum = _second - _miao;
    if(_sum < -3 || _sum > 3 )
    {
        _ret &= false;
    }
    //发送信号告知Core设置时间
    if(!_ret)
    {
        MVBToCoreData _mcd;
        QString _date("20%1-%2-%3 %4:%5:%6");

        _date = _date
                .arg(QString("%1").arg(_nian, 2, 10, QChar('0')))
                .arg(QString("%1").arg(_yue, 2, 10, QChar('0')))
                .arg(QString("%1").arg(_ri, 2, 10, QChar('0')))
                .arg(QString("%1").arg(_shi, 2, 10, QChar('0')))
                .arg(QString("%1").arg(_fen, 2, 10, QChar('0')))
                .arg(QString("%1").arg(_miao, 2, 10, QChar('0')));
        _mcd.type = "设置时间";
        _mcd.data = _date;
//        m_timeValidSign = false;
    //                        qDebug() << "设置时间为:" << _date;
        emit sendMVBDataToCore(_mcd);
    }


}

uint16_t MyMVBServer::my_crc(unsigned char *buf, int len)
{
    int counter;
    unsigned char *p=buf;
    unsigned short crc = 0x0000;
    for( counter = 0; counter < len; counter++)
        crc = (crc<<8) ^ crc_table[((crc>>8) ^ *(char *)p++)&0x00FF];
    return crc;
}

QMap<QString, QVariant> MyMVBServer::getCrc(unsigned char *buf, int len)
{
    MAP _map;
    uint16_t value = my_crc(buf, len);
    uint8_t _left;
    uint8_t _right;
    uint8_t value3 = value >> 8;
    memset(&_left, 0, sizeof(uint8_t));
    memset(&_right, 0, sizeof(uint8_t));
    memcpy(&_left, &value3, sizeof(uint8_t));
    memcpy(&_right, &value, sizeof(uint8_t));
    _map.insert("high", _left);
    _map.insert("low", _right);
    return _map;
}

void MyMVBServer::whenNewConnection()
{
    qDebug() << "mvb server new connected";
    m_sign = true;
    m_tcpSocket = nextPendingConnection();
    emit sendMVBStatus(NETWORKSUCC);

    connect(m_tcpSocket, &QTcpSocket::connected, this, &MyMVBServer::whenClientConnected);
    connect(m_tcpSocket, &QTcpSocket::disconnected, this, &MyMVBServer::whenClientDisconnected);
    connect(m_tcpSocket, &QTcpSocket::readyRead, this, &MyMVBServer::whenSocketReadyRead);
    setConfig();
}

void MyMVBServer::whenClientConnected()
{
    qDebug() << "client in";
}

void MyMVBServer::whenClientDisconnected()
{
    m_sign = false;
    m_tcpSocket->deleteLater();
    qDebug() << "client out";
    emit sendMVBStatus(NETWORKERR);
}

void MyMVBServer::whenSocketReadyRead()
{

    //    DataRequestResponseFrame _dadada;
    //    for(int i =0;i<32;i++)
    //    {
    //        _dadada.data[i] = 1;
    //    }
    //    sendData(_dadada, 0x0B, 0x10, true);
    QByteArray _arr = m_tcpSocket->readAll();
    //    "0xfe"
    //    "0x06"
    //    "0x06"
    //    "0x32"
    //    "0x33"
    //    "0xff"
//        QList<QString> _list;
    //    for(int i =0;i<_arr.size();i++)
    //    {
    //        QString s;
    //        s = s.sprintf("%02X", _arr.at(i));
    //        _list.append(s);
    //    }
    //    qDebug() << "["<<_list << "]";
//    if(_arr.size() == 25)
//    {
//        for(int i =0;i<_arr.size();i++)
//        {
//            quint8 _data = _arr.at(i);
//            printf("%x", _data);
//        }
//        printf("\n");
//    }
    if(_arr.size() == 6)
    {
        ConfigResponseFrame _struct;
        memset(&_struct, 0, sizeof(ConfigResponseFrame));
        memcpy(&_struct, _arr.data(), sizeof(ConfigResponseFrame));
        //计算校验和
        unsigned char q[3] = {
            _struct.startbyte,
            _struct.framelength,
            _struct.configresponsecommand
        };//因为长度为6的只需要计算三个字节的校验和
        MAP _map = getCrc(q, 3);
        _struct.crchigh = _map["high"].toUInt();
        _struct.crclow = _map["low"].toUInt();
        bool _ret = panduanjiaoyanhe(_struct.crchigh, _map["high"].toUInt()
                ,_struct.crclow, _map["low"].toUInt());
        if(!_ret)
        {
            //如果校验帧不同，不处理此帧，启动重发定时器继续重发setConfig
            if(!m_reSetConfigTimer->isActive()){
                m_reSetConfigTimer->start();
            }
            m_setConfigOk = false;
            qDebug() << "校验帧不同,重新配置";
        }else{
            //如果校验帧相同，判断对应帧的属性
            if(_struct.configresponsecommand == 0x06)
            {
                //如果重发定时器启动状态，将其停止
                if(m_reSetConfigTimer->isActive())
                {
                    m_reSetConfigTimer->stop();
                }
                qDebug() << "校验帧和值没问题，开始处理数据";
                m_setConfigOk = true;
                startGetMessage();
            }else if(_struct.configresponsecommand == 0x05)
            {
                //校验帧没问题但值有问题，启动重发定时器依然重发
                if(!m_reSetConfigTimer->isActive())
                {
                    m_reSetConfigTimer->start();
                }
                m_setConfigOk = false;
                qDebug() << "校验和没问题但值有问题，重新配置";
            }
        }
    }else if(_arr.size() == 25)
    {
        enableResponseFrame _struct;
        memset(&_struct, 0, sizeof(enableResponseFrame));
        memcpy(&_struct, _arr.data(), sizeof(enableResponseFrame));
        bool _ret = getStatusByBit(_struct.datastatusinfo, 8);//判断是否是有效数据，这里其实
        if(_ret && _struct.startbytex == 0xFE)//如果数据有效，并且头部是0xFE
        {
            //计算校验和
            bool _crcRet = computerCrc(_struct);
            //如果校验和也正确
            if(_crcRet)
            {
                //取出16个字节的数据
                quint8 _realData[16];
                memset(_realData, 0, sizeof(quint8) * 16);
                struct0xff _structData;
                memset(&_structData, 0, sizeof(struct0xff));
                for(int i =0;i<16;i++)
                {
                    _realData[i] = _struct.data[i];
                }
                memcpy(&_structData, _realData, sizeof(struct0xff));

//                qDebug() << "mvb to cpu, port is FF:"
//                         << (_structData.cpulivesignalhigh)
//                         << (_structData.cpulivesignallow)
//                         << (_structData.year)
//                         << (_structData.month)
//                         << (_structData.day)
//                         << (_structData.hour)
//                         << (_structData.minute)
//                         << (_structData.second)
//                         << (_structData.timevalid)
//                         << (_structData.gelifuwei)
//                         << (_structData.yuliu1)
//                         << (_structData.yuliu2)
//                         << (_structData.linenumber)
//                         << (_structData.trainnumber)
//                         << (_structData.trainspeedhigh)
//                         << (_structData.trainspeedlow)
                            ;
                //1.判断第九个字节是否有效，如果有效发送信号告知Core设置系统时间，只执行一次
                if(m_timeValidSign)
                {
                    if(getStatusByBit(_structData.timevalid, 8) == true)
                    {
                        QWriteLocker locker(&m_lock);
                        m_mvbToMeTimeHash.insert("nian", _structData.year);
                        m_mvbToMeTimeHash.insert("yue", _structData.month);
                        m_mvbToMeTimeHash.insert("ri", _structData.day);
                        m_mvbToMeTimeHash.insert("shi", _structData.hour);
                        m_mvbToMeTimeHash.insert("fen", _structData.minute);
                        m_mvbToMeTimeHash.insert("miao", _structData.second);
                    }
                }
                //处理时间使能位
                bool _tempRet = getStatusByBit(_structData.timevalid, 7);
                if(_tempRet != m_xiajiangyan)
                {
                    if(_tempRet)
                    {
                        //发送信号告知Core设置时间
                        MVBToCoreData _mcd;
                        QString _date("20%1-%2-%3 %4:%5:%6");
                        _date = _date
                                .arg(QString("%1").arg(_structData.year, 2, 10, QChar('0')))
                                .arg(QString("%1").arg(_structData.month, 2, 10, QChar('0')))
                                .arg(QString("%1").arg(_structData.day, 2, 10, QChar('0')))
                                .arg(QString("%1").arg(_structData.hour, 2, 10, QChar('0')))
                                .arg(QString("%1").arg(_structData.minute, 2, 10, QChar('0')))
                                .arg(QString("%1").arg(_structData.second, 2, 10, QChar('0')));

                        _mcd.type = "设置时间";
                        _mcd.data = _date;
                        emit sendMVBDataToCore(_mcd);

                    }
                    m_xiajiangyan = _tempRet;
                }
            }
        }
    }else if(_arr.size() == 13)
    {
        enableResponseFrame2 _struct;
        memset(&_struct, 0, sizeof(enableResponseFrame2));
        memcpy(&_struct, _arr.data(), sizeof(enableResponseFrame2));
        bool _ret = getStatusByBit(_struct.datastatusinfo, 8);//判断是否是有效数据，这里其实
        if(_ret && _struct.startbytex == 0xFE)
        {
            //计算校验和
            bool _crcRet = computerCrc2(_struct);
            //如果校验和也正确
            if(_crcRet)
            {
                //取出4个字节
                quint8 _realData[4];
                memset(_realData, 0, sizeof(quint8) * 4);
                struct0xb08 _structData;
                memset(&_structData, 0, sizeof(struct0xb08));
                for(int i =0;i<4;i++)
                {
                    _realData[i] = _struct.data[i];
                }
                memcpy(&_structData, _realData, sizeof(struct0xb08));
                //1.判断第九个字节是否有效，如果有效发送信号告知Core设置系统时间，只执行一次
                //处理隔离复位使能位
                bool _tempGeLiRet = getStatusByBit(_structData.gelifuwei, 8);
                if(_tempGeLiRet != m_gelifuweixiajiangyan)
                {
                    m_gelifuweixiajiangyan = _tempGeLiRet;
                    if(_tempGeLiRet)
                    {
//                        qDebug() << "收到隔离复位信号";
                        //发送信号告知Core取消屏蔽所有感温探测器的屏蔽状态
                        MVBToCoreData _mcd;
                        _mcd.type = "隔离复位";
                        _mcd.data = "";
                        emit sendMVBDataToCore(_mcd);
                    }
                }
            }
        }
    }
}

bool MyMVBServer::panduanjiaoyanhe(const quint8 dhigh, const quint8 shigh, const quint8 dlow, const quint8 slow)
{
    return (dhigh == shigh) && (dlow == slow);
}

bool MyMVBServer::getStatusByBit(const char data, const int index)
{
    int c = ((data >> (index - 1)) & 1);
//    qDebug() << "第"<< index<<"位的状态为：" << c;
    return c == 1 ?true:false;
}
void MyMVBServer::setDataByBit(unsigned char &buf, const int index, const int value)
{
    //将某一位置为1，或上对应位数的1  即可
    if(index == 1)
    {
        if(value == 0)
        {
            buf &= TOZEROBIT1;
        }
        if(value == 1)
        {
            buf |= TOONEBIT1;
        }
    }else if(index == 2)
    {
        if(value == 0)
        {
            buf &= TOZEROBIT2;
        }
        if(value == 1)
        {
            buf |= TOONEBIT2;
        }
    }else if(index == 3)
    {
        if(value == 0)
        {
            buf &= TOZEROBIT3;
        }
        if(value == 1)
        {
            buf |= TOONEBIT3;
        }
    }else if(index == 4)
    {
        if(value == 0)
        {
            buf &= TOZEROBIT4;
        }
        if(value == 1)
        {
            buf |= TOONEBIT4;
        }
    }else if(index == 5)
    {
        if(value == 0)
        {
            buf &= TOZEROBIT5;
        }
        if(value == 1)
        {
            buf |= TOONEBIT5;
        }
    }else if(index == 6)
    {
        if(value == 0)
        {
            buf &= TOZEROBIT6;
        }
        if(value == 1)
        {
            buf |= TOONEBIT6;
        }
    }else if(index == 7)
    {
        if(value == 0)
        {
            buf &= TOZEROBIT7;
        }
        if(value == 1)
        {
            buf |= TOONEBIT7;
        }
    }else if(index == 8)
    {
        if(value == 0)
        {
            buf &= TOZEROBIT8;

        }
        if(value == 1)
        {
            buf |= TOONEBIT8;
        }
    }
}
void MyMVBServer::fanzhuan(unsigned char *data, int size)
{
    unsigned char buf[size];

    for(int i =0;i<size;i++)
    {
        unsigned char tmepdata = 0;
        int index = 8;
//        printf("source:%d\n", data[i]);
        for(int j = 1;j <= 8;j++)
        {
            //获取每一位bit
            int _ret = getStatusByBit(data[i], j);
//            qDebug() << "获取到的第"<< j <<"位bit:" << _ret;
            setDataByBit(tmepdata, index, _ret);
//            qDebug() << "将值" << _ret << "设置到第" << index << "位";
            index -= 1;
        }
        buf[i] = tmepdata;
//        printf("change to:%d\n", buf[i]);
    }
    for(int i =0;i<size;i++)
    {
        data[i] = buf[i];
    }
}
bool MyMVBServer::computerCrc(const enableResponseFrame frame)
{
    quint8 _tempData[22];
    memset(_tempData, 0, sizeof(quint8) * 22);
    _tempData[0] = frame.startbytex;
    _tempData[1] = frame.framelength;
    _tempData[2] = frame.datareturncommand;
    _tempData[3] = frame.datastatusinfo;
    _tempData[4] = frame.returnportaddresshigh;
    _tempData[5] = frame.returnportaddresslow;
    _tempData[6] = frame.data[0];
    _tempData[7] = frame.data[1];
    _tempData[8] = frame.data[2];
    _tempData[9] = frame.data[3];
    _tempData[10] = frame.data[4];
    _tempData[11] = frame.data[5];
    _tempData[12] = frame.data[6];
    _tempData[13] = frame.data[7];
    _tempData[14] = frame.data[8];
    _tempData[15] = frame.data[9];
    _tempData[16] = frame.data[10];
    _tempData[17] = frame.data[11];
    _tempData[18] = frame.data[12];
    _tempData[19] = frame.data[13];
    _tempData[20] = frame.data[14];
    _tempData[21] = frame.data[15];
    MAP _map = getCrc(_tempData, 22);
    if(frame.crchigh == _map["high"].toUInt() && frame.crclow == _map["low"].toUInt())
    {
        return true;
    }
    return false;
}

bool MyMVBServer::computerCrc2(const enableResponseFrame2 frame)
{
    quint8 _tempData[10];
    memset(_tempData, 0, sizeof(quint8) * 10);
    _tempData[0] = frame.startbytex;
    _tempData[1] = frame.framelength;
    _tempData[2] = frame.datareturncommand;
    _tempData[3] = frame.datastatusinfo;
    _tempData[4] = frame.returnportaddresshigh;
    _tempData[5] = frame.returnportaddresslow;
    _tempData[6] = frame.data[0];
    _tempData[7] = frame.data[1];
    _tempData[8] = frame.data[2];
    _tempData[9] = frame.data[3];
    MAP _map = getCrc(_tempData, 10);
    if(frame.crchigh == _map["high"].toUInt() && frame.crclow == _map["low"].toUInt())
    {
        return true;
    }
    return false;
}


void MyMVBServer::sendData(DataRequestResponseFrame data, quint8 portaddresshigh, quint8 portaddresslow,bool autoadd)
{
    //    if(portaddresshigh == 0x0b && portaddresslow == 0x10)
    //    {
    //        qDebug() << "0xb10:" << data.data[7];
    //    }else
    //    {
    //        qDebug() << "0xb60:" << data.data[7];
    //    }
    if(m_setConfigOk == false)
    {
        return;
    }
    sendFrame _struct;
    _struct.startbyte = 0xfe;
    _struct.framelength = 0x2a;
    _struct.senddatacommand = 0x09;
    quint8 _add;
    if(autoadd)
    {
        _add = 0xE0;
    }else{
        _add = 0x80;
    }
    _struct.superinfo = _add;
    _struct.autoaddaddress = 0x00;
    _struct.returnportaddresshigh = portaddresshigh;
    _struct.returnportaddresslow = portaddresslow;
#ifdef BITFANZHUAN
    if(portaddresshigh == 0x0b && portaddresslow == 0x10)
    {
        q8 tempdata[19];
        int tempindex = 0;
        for(int i =8;i<26;i++)
        {
            tempdata[tempindex] = data.data[i];
            tempindex += 1;
        }
        fanzhuan(tempdata, 19);
        tempindex = 0;
        for(int i =8;i<26;i++)
        {
            data.data[i] = tempdata[tempindex];
            tempindex += 1;
        }
    }else if(portaddresshigh == 0x0b && portaddresslow == 0x60)
    {
        q8 tempdata[19];
        int tempindex = 0;
        for(int i =8;i<26;i++)
        {
            tempdata[tempindex] = data.data[i];
            tempindex += 1;
        }
        fanzhuan(tempdata, 19);
        tempindex = 0;
        for(int i =8;i<26;i++)
        {
            data.data[i] = tempdata[tempindex];
            tempindex += 1;
        }
    }else if(portaddresshigh == 0x0b && portaddresslow == 0x11)
    {
        q8 tempdata[29];
        int tempindex = 0;
        for(int i =0;i<29;i++)
        {
            tempdata[tempindex] = data.data[i];
            tempindex += 1;
        }
        fanzhuan(tempdata, 29);
        tempindex = 0;
        for(int i =0;i<29;i++)
        {
            data.data[i] = tempdata[tempindex];
            tempindex += 1;
        }
    }else if(portaddresshigh == 0x0b && portaddresslow == 0x61)
    {
        q8 tempdata[29];
        int tempindex = 0;
        for(int i =0;i<29;i++)
        {
            tempdata[tempindex] = data.data[i];
            tempindex += 1;
        }
        fanzhuan(tempdata, 29);
        tempindex = 0;
        for(int i =0;i<29;i++)
        {
            data.data[i] = tempdata[tempindex];
            tempindex += 1;
        }
    }
#endif
    _struct.senddata = data;
    //获取校验和
    unsigned char q[39] ={_struct.startbyte, _struct.framelength, _struct.senddatacommand, _struct.superinfo,
                          _struct.autoaddaddress, _struct.returnportaddresshigh, _struct.returnportaddresslow};//因为长度为6的只需要计算三个字节的校验和
    int _count = 0;
    for(int i = 7;i<39;i++)
    {
        q[i] =data.data[_count];
        _count += 1;
    }
    MAP _map = getCrc(q, 39);
    _struct.crchig = _map["high"].toUInt();
    _struct.crclow = _map["low"].toUInt();
    _struct.endbyte = 0xff;
    QByteArray _arr;
    _arr.resize(sizeof(sendFrame));
    memcpy(_arr.data(), &_struct, sizeof(sendFrame));
    //    QList<QString> _testlist;
    //    for(int i =0;i<39;i++){
    //        QString s;
    //        s.sprintf("0x%02x", q[i]);
    //        _testlist.append(s);
    //    }
    //    qDebug() << "[" << _testlist << "]";
    //    QString s1, s2;
    //    s1.sprintf("0x%02x", _struct.crchig);
    //    s2.sprintf("0x%02x", _struct.crclow);
    //    qDebug() << s1 << ":" << s2;
    if(m_sign)
    {
        m_tcpSocket->write(_arr);
        m_tcpSocket->flush();
    }
}
