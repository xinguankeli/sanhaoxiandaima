QT -= gui

QT += network sql
CONFIG += c++11 console
CONFIG -= app_bundle

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        conf/myreadconf.cpp \
        core.cpp \
        database/mydatabase.cpp \
        express/myclientormaster.cpp \
        express/mydatabaseserver.cpp \
        express/myeventudp.cpp \
        express/mytcpserver.cpp \
        express/mymasterorclient.cpp \
        main.cpp \
        mvb/mymvbserver.cpp \
        mywidget/mytimer.cpp \
        socket/device/firedevice.cpp \
        socket/device/gatedevice.cpp \
        socket/device/iodevice.cpp \
        socket/device/smogdevice.cpp \
        socket/mysocket.cpp \
        mvb/mvbdata.cpp \
        iot/myiotclient.cpp

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

HEADERS += \
    conf/myreadconf.h \
    core.h \
    database/mydatabase.h \
    express/myclientormaster.h \
    express/mydatabaseserver.h \
    express/myeventudp.h \
    express/mytcpserver.h \
    express/mymasterorclient.h \
    global/Global.h \
    global/IOTGlobal.h \
    global/MVBGlobal.h \
    mvb/mymvbserver.h \
    mywidget/mytimer.h \
    socket/device/firedevice.h \
    socket/device/gatedevice.h \
    socket/device/iodevice.h \
    socket/device/smogdevice.h \
    socket/mysocket.h \
    mvb/mvbdata.h \
    iot/myiotclient.h
win32:LIBS += -lws2_32
