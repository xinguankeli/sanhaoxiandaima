#include "myreadconf.h"
#include <QCoreApplication>
#include <QFileInfo>
#include <QDebug>
#include <QTextCodec>
MyReadConf::MyReadConf(const QString filePath, QObject *parent) : QObject(parent)
  ,m_configFilePath(filePath)
{
    init();
}

MyReadConf::MyReadConf(QObject *parent)
{
    m_configFilePath = QCoreApplication::applicationDirPath() + "/config.ini";
    init();
}

DataBaseConfigWord MyReadConf::getDataBaseConfigWord()
{
    return m_dataBaseConfigWord;
}

QHash<QString, MyFireAlarmSensorInfo> MyReadConf::getFireAlarmSensorInfoMap()
{
    return m_fireAlarmSensorInfoMap;
}

QHash<QString, MyGateDeviceInfo> MyReadConf::getGateDeviceInfoMap()
{
    return m_gateDeviceInfoMap;
}

QHash<QString, MyControllerInfo> MyReadConf::getIOInfoMap()
{
    return m_IOMap;
}

QHash<QString, MySmogSensorInfo> MyReadConf::getSmogInfoMap()
{
    return m_smogInfoMap;
}

QHash<QString, QList<MultiLambdaDeviceInfo> > MyReadConf::getEventMultiLambdaList()
{
    return m_eventMultiLambdaList;
}

QHash<QString, QList<MultiLambdaDeviceInfo> > MyReadConf::getExecMultiLambdaList()
{
    return m_execMultiLambdaList;
}

QHash<QString, bool> MyReadConf::getVariableMultiLambdaList()
{
    return m_variableMultiLambdaList;
}

QList<MyDeviceBaseInfo> MyReadConf::getUdpEventDeviceInfoList()
{
    return m_udpEventDeviceInfoList;
}

QString MyReadConf::getMasterSign()
{
    return m_masterSign;
}

QMap<QString, QPair<QString, quint16> > MyReadConf::getMasterNetworkAddress()
{
    return m_masterNetworkAddress;
}

QHash<QString, QString> MyReadConf::getIOTCarInfo()
{
    return m_IOTCarInfoHash;
}

QHash<QString, QString> MyReadConf::getFireDeviceArguemnt()
{
    return m_fireDeviceArgumentHash;
}

QHash<QString, QString> MyReadConf::getSmogDeviceArgument()
{
    return m_smogDeviceArgumentHash;
}

double MyReadConf::getWenDuFuDongZhi()
{
    return m_wenduFudongZhi;
}

double MyReadConf::getLingDianLingYiWu()
{
    return m_lingDianLingYiWuZhi;
}

QHash<QString, int> MyReadConf::getGuDingChaZhi()
{
    return m_guDingChaZhiHash;
}

void MyReadConf::init()
{
    QFileInfo _fileInfo(m_configFilePath);
    if(!_fileInfo.isFile()){
        qDebug() << "config file not exists";
        exit(0);//配置文件没读取到，程序活着没用处
    }
    qDebug() <<"[read config]";
    set = new QSettings(m_configFilePath, QSettings::IniFormat);
    set->setIniCodec(QTextCodec::codecForName("UTF-8"));
    QStringList temp_list = set->childGroups();
    for(int i =0;i<temp_list.size();i++)
    {
        if(temp_list.at(i).indexOf("fire-device") != -1){
            MyFireAlarmSensorInfo _sensorInfo;
            _sensorInfo.base.alias = set->value(QString("%1/%2").arg(temp_list.at(i)).arg("alias")).toString();
            _sensorInfo.base.ip = set->value(QString("%1/%2").arg(temp_list.at(i)).arg("ip")).toString();
            _sensorInfo.base.port = set->value(QString("%1/%2").arg(temp_list.at(i)).arg("port")).toUInt();
            _sensorInfo.base.shield = set->value(QString("%1/%2").arg(temp_list.at(i)).arg("shield")).toString();
            _sensorInfo.base.recv_length = set->value(QString("%1/%2").arg(temp_list.at(i)).arg("recv_length")).toInt();
            _sensorInfo.base.englishname = temp_list.at(i);
            m_fireAlarmSensorInfoMap.insert(_sensorInfo.base.alias, _sensorInfo);
        }
        else if(temp_list.at(i).indexOf("gate-device") != -1)
        {
            MyGateDeviceInfo _gateInfo;
            _gateInfo.base.ip = set->value(QString("%1/%2").arg(temp_list.at(i)).arg("ip")).toString();
            _gateInfo.base.alias = set->value(QString("%1/%2").arg(temp_list.at(i)).arg("alias")).toString();
            _gateInfo.base.port = set->value(QString("%1/%2").arg(temp_list.at(i)).arg("port")).toUInt();
            _gateInfo.base.recv_length = set->value(QString("%1/%2").arg(temp_list.at(i)).arg("recv_length")).toInt();
            _gateInfo.base.englishname = temp_list.at(i);
            _gateInfo.leftalias = set->value(QString("%1/%2").arg(temp_list.at(i)).arg("leftalias")).toString();
            _gateInfo.rightalias = set->value(QString("%1/%2").arg(temp_list.at(i)).arg("rightalias")).toString();
            _gateInfo.leftstatus = set->value(QString("%1/%2").arg(temp_list.at(i)).arg("leftstatus")).toString();
            _gateInfo.rightstatus = set->value(QString("%1/%2").arg(temp_list.at(i)).arg("rightstatus")).toString();
            m_gateDeviceInfoMap.insert(_gateInfo.base.alias, _gateInfo);
        }
        else if(temp_list.at(i).indexOf("io-device") != -1)
        {
            MyControllerInfo _conInfo;
            _conInfo.base.alias = set->value(QString("%1/%2").arg(temp_list.at(i)).arg("alias")).toString();
            _conInfo.base.ip = set->value(QString("%1/%2").arg(temp_list.at(i)).arg("ip")).toString();
            _conInfo.base.port = set->value(QString("%1/%2").arg(temp_list.at(i)).arg("port")).toUInt();
            _conInfo.base.recv_length = set->value(QString("%1/%2").arg(temp_list.at(i)).arg("recv_length")).toInt();
            _conInfo.base.englishname = temp_list.at(i);
            _conInfo.input1 = set->value(QString("%1/%2").arg(temp_list.at(i)).arg("input1")).toString();
            _conInfo.input2 = set->value(QString("%1/%2").arg(temp_list.at(i)).arg("input2")).toString();
            _conInfo.input3 = set->value(QString("%1/%2").arg(temp_list.at(i)).arg("input3")).toString();
            _conInfo.input4 = set->value(QString("%1/%2").arg(temp_list.at(i)).arg("input4")).toString();
            _conInfo.input5 = set->value(QString("%1/%2").arg(temp_list.at(i)).arg("input5")).toString();
            _conInfo.input6 = set->value(QString("%1/%2").arg(temp_list.at(i)).arg("input6")).toString();

            _conInfo.output1 = set->value(QString("%1/%2").arg(temp_list.at(i)).arg("output1")).toString();
            _conInfo.output2 = set->value(QString("%1/%2").arg(temp_list.at(i)).arg("output2")).toString();
            _conInfo.output3 = set->value(QString("%1/%2").arg(temp_list.at(i)).arg("output3")).toString();
            _conInfo.output4 = set->value(QString("%1/%2").arg(temp_list.at(i)).arg("output4")).toString();
            _conInfo.output5 = set->value(QString("%1/%2").arg(temp_list.at(i)).arg("output5")).toString();
            _conInfo.output6 = set->value(QString("%1/%2").arg(temp_list.at(i)).arg("output6")).toString();
            m_IOMap.insert(_conInfo.base.alias, _conInfo);

        }
        else if(temp_list.at(i).indexOf("smog-device") != - 1)
        {
            MySmogSensorInfo _smogInfo;//感烟模块要能被屏蔽,在内存里屏蔽即可,但也要预留出接口
            _smogInfo.base.alias = set->value(QString("%1/%2").arg(temp_list.at(i)).arg("alias")).toString();
            _smogInfo.base.ip = set->value(QString("%1/%2").arg(temp_list.at(i)).arg("ip")).toString();
            _smogInfo.base.port = set->value(QString("%1/%2").arg(temp_list.at(i)).arg("port")).toUInt();
            _smogInfo.base.recv_length = set->value(QString("%1/%2").arg(temp_list.at(i)).arg("recv_length")).toInt();
            _smogInfo.base.englishname = temp_list.at(i);
            _smogInfo.address1 = set->value(QString("%1/%2").arg(temp_list.at(i)).arg("address1")).toString();
            _smogInfo.address2 = set->value(QString("%1/%2").arg(temp_list.at(i)).arg("address2")).toString();
            _smogInfo.address3 = set->value(QString("%1/%2").arg(temp_list.at(i)).arg("address3")).toString();
            _smogInfo.address4 = set->value(QString("%1/%2").arg(temp_list.at(i)).arg("address4")).toString();
            _smogInfo.address5 = set->value(QString("%1/%2").arg(temp_list.at(i)).arg("address5")).toString();
            _smogInfo.address6 = set->value(QString("%1/%2").arg(temp_list.at(i)).arg("address6")).toString();
            _smogInfo.address7 = set->value(QString("%1/%2").arg(temp_list.at(i)).arg("address7")).toString();
            _smogInfo.address8 = set->value(QString("%1/%2").arg(temp_list.at(i)).arg("address8")).toString();
            _smogInfo.address9 = set->value(QString("%1/%2").arg(temp_list.at(i)).arg("address9")).toString();
            _smogInfo.address10 = set->value(QString("%1/%2").arg(temp_list.at(i)).arg("address10")).toString();
            _smogInfo.address11 = set->value(QString("%1/%2").arg(temp_list.at(i)).arg("address11")).toString();
            _smogInfo.address12 = set->value(QString("%1/%2").arg(temp_list.at(i)).arg("address12")).toString();
            _smogInfo.address13 = set->value(QString("%1/%2").arg(temp_list.at(i)).arg("address13")).toString();
            _smogInfo.address14 = set->value(QString("%1/%2").arg(temp_list.at(i)).arg("address14")).toString();
            _smogInfo.address15 = set->value(QString("%1/%2").arg(temp_list.at(i)).arg("address15")).toString();
            _smogInfo.address16= set->value(QString("%1/%2").arg(temp_list.at(i)).arg("address16")).toString();
            _smogInfo.address17 = set->value(QString("%1/%2").arg(temp_list.at(i)).arg("address17")).toString();

            _smogInfo.address1shield = set->value(QString("%1/%2").arg(temp_list.at(i)).arg("address1shield")).toString();
            _smogInfo.address2shield = set->value(QString("%1/%2").arg(temp_list.at(i)).arg("address2shield")).toString();
            _smogInfo.address3shield = set->value(QString("%1/%2").arg(temp_list.at(i)).arg("address3shield")).toString();
            _smogInfo.address4shield = set->value(QString("%1/%2").arg(temp_list.at(i)).arg("address4shield")).toString();
            _smogInfo.address5shield = set->value(QString("%1/%2").arg(temp_list.at(i)).arg("address5shield")).toString();
            _smogInfo.address6shield = set->value(QString("%1/%2").arg(temp_list.at(i)).arg("address6shield")).toString();
            _smogInfo.address7shield = set->value(QString("%1/%2").arg(temp_list.at(i)).arg("address7shield")).toString();
            _smogInfo.address8shield = set->value(QString("%1/%2").arg(temp_list.at(i)).arg("address8shield")).toString();
            _smogInfo.address9shield = set->value(QString("%1/%2").arg(temp_list.at(i)).arg("address9shield")).toString();
            _smogInfo.address10shield = set->value(QString("%1/%2").arg(temp_list.at(i)).arg("address10shield")).toString();
            _smogInfo.address11shield = set->value(QString("%1/%2").arg(temp_list.at(i)).arg("address11shield")).toString();
            _smogInfo.address12shield = set->value(QString("%1/%2").arg(temp_list.at(i)).arg("address12shield")).toString();
            _smogInfo.address13shield = set->value(QString("%1/%2").arg(temp_list.at(i)).arg("address13shield")).toString();
            _smogInfo.address14shield = set->value(QString("%1/%2").arg(temp_list.at(i)).arg("address14shield")).toString();
            _smogInfo.address15shield = set->value(QString("%1/%2").arg(temp_list.at(i)).arg("address15shield")).toString();
            _smogInfo.address16shield = set->value(QString("%1/%2").arg(temp_list.at(i)).arg("address16shield")).toString();
            _smogInfo.address17shield = set->value(QString("%1/%2").arg(temp_list.at(i)).arg("address17shield")).toString();
            m_smogInfoMap.insert(_smogInfo.base.alias, _smogInfo);
        }else if(temp_list.at(i).indexOf("lambda-multi") != -1)
        {
            QString _str = set->value(QString("%1/%2").arg(temp_list.at(i)).arg("my-lambda")).toString();
            //对==号切割
            QString _left, _right;
            _left = _str.split("==").at(0);
            _right = _str.split("==").at(1);
            QList<QString> _operaList = getMultiLambdaOperator(_left);
            QList<MultiLambdaDeviceInfo> _eventDeviceList = processMultiLambdaLeft(_left);
            for(int j =0;j<_eventDeviceList.size();j++)
            {
                MultiLambdaDeviceInfo _struct = _eventDeviceList.at(j);
                _struct.lambdaname = temp_list.at(i);
                _eventDeviceList[j] = _struct;
            }
            insertOperatorToDevice(_eventDeviceList, _operaList);
            m_eventMultiLambdaList.insert(temp_list.at(i), _eventDeviceList);

            QList<MultiLambdaDeviceInfo> _execDeviceList = processMultiLambdaLeft(_right);
            for(int j =0;j<_execDeviceList.size();j++)
            {
                MultiLambdaDeviceInfo _struct = _execDeviceList.at(j);
                _struct.lambdaname = temp_list.at(i);
                _execDeviceList[j] = _struct;
            }
            m_execMultiLambdaList.insert(temp_list.at(i), _execDeviceList);
        }else if(temp_list.at(i).indexOf("lambda-var") != -1)
        {
            QString _str = set->value(QString("%1/%2").arg(temp_list.at(i)).arg("my-lambda")).toString();
            m_variableMultiLambdaList.insert(_str, false);
        }
        else if(temp_list.at(i).indexOf("event-udp") != -1)
        {
            MyDeviceBaseInfo _struct;
            _struct.ip = set->value(QString("%1/%2").arg(temp_list.at(i)).arg("ip")).toString();
            _struct.port = set->value(QString("%1/%2").arg(temp_list.at(i)).arg("port")).toUInt();
            m_udpEventDeviceInfoList.append(_struct);
        }else if(temp_list.at(i).indexOf("master") != -1){
            m_masterSign = set->value(QString("%1/%2").arg(temp_list.at(i)).arg("status")).toString();
            m_masterNetworkAddress["client"].first = set->value(QString("%1/%2").arg(temp_list.at(i)).arg("client-ip")).toString();
            m_masterNetworkAddress["client"].second = set->value(QString("%1/%2").arg(temp_list.at(i)).arg("client-port")).toUInt();
        }else if(temp_list.at(i).indexOf("iot-carinfo") != -1)
        {

            QString _citycode = set->value(QString("%1/%2").arg(temp_list.at(i)).arg("citycode")).toString();
            QString _linecode = set->value(QString("%1/%2").arg(temp_list.at(i)).arg("linecode")).toString();
            QString _carcode = set->value(QString("%1/%2").arg(temp_list.at(i)).arg("carcode")).toString();
            m_IOTCarInfoHash.insert("citycode", _citycode);
            m_IOTCarInfoHash.insert("linecode", _linecode);
            m_IOTCarInfoHash.insert("carcode", _carcode);
        }else if(temp_list.at(i).indexOf("fire-argument") != -1)
        {
            m_fireDeviceArgumentHash.insert("one", set->value(QString("%1/%2").arg(temp_list.at(i)).arg("one")).toString());
            m_fireDeviceArgumentHash.insert("two", set->value(QString("%1/%2").arg(temp_list.at(i)).arg("two")).toString());
            m_fireDeviceArgumentHash.insert("three", set->value(QString("%1/%2").arg(temp_list.at(i)).arg("three")).toString());
            m_fireDeviceArgumentHash.insert("four", set->value(QString("%1/%2").arg(temp_list.at(i)).arg("four")).toString());
        }else if(temp_list.at(i).indexOf("smog-argument") != -1)
        {
            m_smogDeviceArgumentHash.insert("one", set->value(QString("%1/%2").arg(temp_list.at(i)).arg("one")).toString());
        }
        else if(temp_list.at(i).indexOf("wendufudongzhi") != -1)
        {
            m_wenduFudongZhi = set->value(QString("%1/%2").arg(temp_list.at(i)).arg("fudongzhi")).toDouble();
            m_lingDianLingYiWuZhi = set->value(QString("%1/%2").arg(temp_list.at(i)).arg("lingdianlingyiwu")).toDouble();
            m_guDingChaZhiHash.insert("A框架水温", set->value(QString("%1/%2").arg(temp_list.at(i)).arg("akuangjiashuiwen")).toInt());
            m_guDingChaZhiHash.insert("A框架内部温度", set->value(QString("%1/%2").arg(temp_list.at(i)).arg("akuangjianeibuwendu")).toInt());
            m_guDingChaZhiHash.insert("A框架环境温度", set->value(QString("%1/%2").arg(temp_list.at(i)).arg("akuangjiahuanjingwendu")).toInt());
            m_guDingChaZhiHash.insert("B框架水温", set->value(QString("%1/%2").arg(temp_list.at(i)).arg("bkuangjiashuiwen")).toInt());
            m_guDingChaZhiHash.insert("B框架内部温度", set->value(QString("%1/%2").arg(temp_list.at(i)).arg("bkuangjianeibuwendu")).toInt());
            m_guDingChaZhiHash.insert("B框架环境温度", set->value(QString("%1/%2").arg(temp_list.at(i)).arg("bkuangjiahuanjingwendu")).toInt());

        }

    }
    if(readDataBaseConfigWord() == false)
    {
        qDebug() << "read database config error";
        exit(0);
    }

}

bool MyReadConf::readDataBaseConfigWord()
{
    bool _ret = true;
    QString _hostname = set->value(QString("database/hostname")).toString();
    QString _databasename = set->value(QString("database/databasename")).toString();
    QString _username = set->value(QString("database/username")).toString();
    QString _password = set->value(QString("database/password")).toString();
    if(_hostname == "" || _password == "" || _databasename == "" || _username == ""){
        _ret = false;
    }
    m_dataBaseConfigWord.hostname = _hostname;
    m_dataBaseConfigWord.password = _password;
    m_dataBaseConfigWord.username = _username;
    m_dataBaseConfigWord.databasename = _databasename;
    return _ret;
}

QList<MultiLambdaDeviceInfo> MyReadConf::processMultiLambdaLeft(QString lambda)
{
    //lambda是[lambda-multi1]旗下的my-lambda字符串，我们做如下处理
    QList<MultiLambdaDeviceInfo> _list;

    //1.从头遍历整个字符串
    int _count =0;
    while (lambda.size() > 0)
    {
        //2.判断第一个字符是[、@、(、%、^

        if(lambda.at(_count) == "[")
        {
            //提取第一个字符串
            QPair<QString, int> _pair = getStrStartToEnd("[", "]", lambda);
            QList<QString> _tempList;
            _tempList << _pair.first;
//                        qDebug() << _pair.first;
            _list.append(parseMultiDeviceInfo("gate", _tempList));
            QString _newStr =  lambda.mid(_pair.second, lambda.size());
            lambda = _newStr;
            _count = 0;
        }else if(lambda.at(_count) == "@")
        {
            //提取第一个字符串
            QPair<QString, int> _pair = getStrStartToEnd("@", "*", lambda);
            QList<QString> _tempList;
            _tempList << _pair.first;
            //            qDebug() << _pair.first;
            _list.append(parseMultiDeviceInfo("io", _tempList));
            QString _newStr =  lambda.mid(_pair.second, lambda.size());
            lambda = _newStr;
            _count = 0;
        }else if(lambda.at(_count) == "(")
        {
            //提取第一个字符串
            QPair<QString, int> _pair = getStrStartToEnd("(", ")", lambda);
            QList<QString> _tempList;
            _tempList << _pair.first;
            //            qDebug() << _pair.first;
            _list.append(parseMultiDeviceInfo("fire", _tempList));
            QString _newStr =  lambda.mid(_pair.second, lambda.size());
            lambda = _newStr;
            _count = 0;
        }else if(lambda.at(_count) == "%")
        {
            //提取第一个字符串
            QPair<QString, int> _pair = getStrStartToEnd("%", "$", lambda);
            QList<QString> _tempList;
            _tempList << _pair.first;
            //            qDebug() << _pair.first;
            _list.append(parseMultiDeviceInfo("smog", _tempList));
            QString _newStr =  lambda.mid(_pair.second, lambda.size());
            lambda = _newStr;
            _count = 0;
        }else if(lambda.at(_count) == "^")
        {
            //提取第一个字符串
            QPair<QString, int> _pair = getStrStartToEnd("^", "~", lambda);
            QList<QString> _tempList;
            _tempList << _pair.first;
            //            qDebug() << _pair.first;
            _list.append(parseMultiDeviceInfo("timer", _tempList));
            QString _newStr =  lambda.mid(_pair.second, lambda.size());
            lambda = _newStr;
            _count = 0;
        }else if(lambda.at(_count) == "+")
        {
            //提取第一个字符串
            QPair<QString, int> _pair = getStrStartToEnd("+", "o", lambda);
            QList<QString> _tempList;
            _tempList << _pair.first;
            //            qDebug() << _pair.first;
            _list.append(parseMultiDeviceInfo("var", _tempList));
            QString _newStr =  lambda.mid(_pair.second, lambda.size());
            lambda = _newStr;
            _count = 0;
        }
        else
        {
            _count += 1;
        }
    }



    //    bool _gateType = false;
    //    bool _fireType = false;
    //    bool _smogType = false;
    //    bool _ioType = false;
    //    bool _timerType = false;
    //    bool _varType = false;
    //    if(lambda.indexOf("[") != -1 && lambda.indexOf("]") != -1)
    //    {
    //        _gateType = true;
    //    }
    //    if(lambda.indexOf("@") != -1 && lambda.indexOf("*") != -1)
    //    {
    //        _ioType = true;
    //    }
    //    if(lambda.indexOf("(") != -1 && lambda.indexOf(")") != -1)
    //    {
    //        _fireType = true;
    //    }
    //    if(lambda.indexOf("%") != -1 && lambda.indexOf("$") != -1)
    //    {
    //        _smogType = true;
    //    }
    //    if(lambda.indexOf("^") != -1 && lambda.indexOf("~") != -1)
    //    {
    //        _timerType = true;
    //    }
    //    if(lambda.indexOf("+") != -1 && lambda.indexOf("o") != -1)
    //    {
    //        _varType = true;
    //    }

    //1.先判断标识符在表达式中出现的位置，对其索引进行排序，从最小的开始读取


    //分别处理对应解析
    //    if(_gateType)
    //    {
    //        _list.append(parseMultiDeviceInfo("gate", betweenStartAndEnd("[", "]", lambda)));
    //        //                qDebug() << betweenStartAndEnd("[", "]", lambda);
    //    }
    //    if(_fireType)
    //    {
    //        _list.append(parseMultiDeviceInfo("fire", betweenStartAndEnd("(", ")", lambda)));
    //        //                qDebug() << betweenStartAndEnd("(", ")", lambda);
    //    }
    //    if(_smogType)
    //    {
    //        _list.append(parseMultiDeviceInfo("smog", betweenStartAndEnd("%", "$", lambda)));
    //        //                qDebug() << betweenStartAndEnd("%", "$", lambda);
    //    }
    //    if(_ioType)
    //    {
    //        _list.append(parseMultiDeviceInfo("io", betweenStartAndEnd("@", "*", lambda)));
    //        //                qDebug() << betweenStartAndEnd("@", "*", lambda);
    //    }
    //    if(_timerType)
    //    {
    //        _list.append(parseMultiDeviceInfo("timer", betweenStartAndEnd("^", "~", lambda)));
    ////                        qDebug() << betweenStartAndEnd("^", "~", lambda);
    //    }
    //    if(_varType)
    //    {
    //        _list.append(parseMultiDeviceInfo("timer", betweenStartAndEnd("+", "o", lambda)));
    ////                        qDebug() << betweenStartAndEnd("+", "o", lambda);
    //    }
    return _list;
}

QList<QString> MyReadConf::betweenStartAndEnd(const QString start, const QString end, QString str)
{
    QList<QString> result;

    int startIndex = str.indexOf(start);

    int endIndex = str.indexOf(end);

    while (startIndex != -1 && endIndex != -1) {

        QString _tempStr = str.mid(startIndex + start.length(), endIndex - startIndex);
        result << _tempStr.mid(0, _tempStr.size() - 1);
        startIndex = str.indexOf(start, endIndex + 1);

        endIndex = str.indexOf(end, startIndex + 1);

    }
    return result;
}

QPair<QString, int> MyReadConf::getStrStartToEnd(const QString start, const QString end, QString str)
{
    QPair<QString, int> _pair;
    QString _newStr;
    for(int i =0;i<str.size();i++)
    {
        if(str.at(i) == start)//如果是第一个字符就跳过
        {
            _newStr = start;
            continue;
        }
        if(str.at(i) == end)
        {
            _pair.first = _newStr.mid(1, _newStr.size());
            _pair.second = i + 1;
            break;
        }
        _newStr += str.at(i);
    }
    return _pair;
}

QList<MultiLambdaDeviceInfo> MyReadConf::parseMultiDeviceInfo(QString type, QList<QString> list)
{
    QList<MultiLambdaDeviceInfo> _list;
    for(int i =0;i<list.size();i++)
    {
        MultiLambdaDeviceInfo _dinfo;
        QString _str = list.at(i);
        if(type == "fire")
        {
            //一车感温探测器1:故障
            //一车感温探测器1:火警
            //一车感温探测器1:复位
            //fire就是一个冒号
            MultiFireDeviceLambdaInfo _info;
            if(_str.indexOf(":") != -1)
            {
                QList<QString> _tempList = _str.split(":");
                _info.devicename = _tempList.at(0);
                _info.doit = _tempList.at(1);
            }
            DeviceOperator _opera;
            _opera.status = false;
            _opera.nextopera = "";
            _opera.frontopera = "";
            _info.oper = _opera;
            _dinfo.type = type;
            _dinfo.fire = _info;
        }else if(type == "smog")
        {
            //感烟传感器A区:一车驾驶室感烟探测器-火警
            //感烟传感器A区:故障
            //感烟传感器A区:一车驾驶室感烟探测器-故障
            MultiSmogDeviceLambdaInfo _info;
            if(_str.indexOf("-") != -1)//如果有-号，有子设备
            {
                _info.type = "分";
                if(_str.indexOf(":") != -1)
                {
                    QList<QString> _firstList = _str.split(":");
                    QList<QString> _secondList;
                    if(_firstList.at(1).indexOf("-") != -1)
                    {
                        _secondList = _firstList.at(1).split("-");
                        _info.devicename = _firstList.at(0);
                        _info.subdevicename = _secondList.at(0);
                        _info.doit = _secondList.at(1);
                    }
                }

            }else
            {
                _info.type = "总";
                if(_str.indexOf(":") != -1)
                {
                    QList<QString> _firstList = _str.split(":");
                    _info.devicename = _firstList.at(0);
                    _info.doit = _firstList.at(1);
                }
            }
            DeviceOperator _opera;
            _opera.status = false;
            _opera.nextopera = "";
            _opera.frontopera = "";
            _info.oper = _opera;
            _dinfo.type = type;
            _dinfo.smog = _info;

        }else if(type == "gate")
        {
            //五车总阀门:五车一区阀门-打开
            //五车总阀门:五车二区阀门-关闭
            //五车总阀门:故障
            //五车总阀门:五车二区阀门-故障
            MultiGateDeviceLambdaInfo _info;
            if(_str.indexOf("-") != -1)//如果有-号，有子设备
            {
                _info.type = "分";
                if(_str.indexOf(":") != -1)
                {
                    QList<QString> _firstList = _str.split(":");
                    QList<QString> _secondList;
                    if(_firstList.at(1).indexOf("-") != -1)
                    {
                        _secondList = _firstList.at(1).split("-");
                        _info.devicename = _firstList.at(0);
                        _info.subdevicename = _secondList.at(0);
                        _info.doit = _secondList.at(1);
                    }
                }

            }else
            {
                _info.type = "总";
                if(_str.indexOf(":") != -1)
                {
                    QList<QString> _firstList = _str.split(":");
                    _info.devicename = _firstList.at(0);
                    _info.doit = _firstList.at(1);
                }
            }
            DeviceOperator _opera;
            _opera.status = false;
            _opera.nextopera = "";
            _opera.frontopera = "";
            _info.oper = _opera;
            _dinfo.type = type;
            _dinfo.gate  = _info;
        }else if(type == "io")
        {
            //B框架二区控制器:电磁阀1-打开
            //B框架二区控制器:电磁阀2-关闭
            //B框架二区控制器:电磁阀2-打开
            //B框架二区控制器:故障
            //B框架二区控制器:温度>30:温度<50
            //B框架二区控制器:液位<2000


            //            "分" "B框架二区控制器" "电磁阀1" "打开" 0
            //            "分" "B框架二区控制器" "电磁阀2" "关闭" 0
            //            "分" "B框架二区控制器" "电磁阀2" "打开" 0
            //            "总" "B框架二区控制器" "" "故障" 0
            //            "分" "B框架二区控制器" "液位" "<" 2000

            MultiIODeviceLambdaInfo _info;
            //如果有-，但是没有< > =号，就算是IO的输出点
            if(_str.indexOf("-") != -1 && _str.indexOf("<") == -1 && _str.indexOf(">") == -1 && _str.indexOf("=") == -1)
            {
                _info.type = "分";
                _info.ioro = "输出";
                if(_str.indexOf(":") != -1)
                {
                    QList<QString> _firstList = _str.split(":");
                    QList<QString> _secondList = _firstList.at(1).split("-");
                    _info.devicename = _firstList.at(0);
                    _info.subdevicename = _secondList.at(0);
                    _info.doit = _secondList.at(1);
                    _info.value = 0;
                }
                //如果没有-，但是有< > =
            }else if(_str.indexOf("-") == -1 || _str.indexOf("<") != -1 || _str.indexOf(">") != -1 || _str.indexOf("=") != -1)
            {
                _info.ioro = "输入";
                if(_str.indexOf(">") == -1 && _str.indexOf("<") == -1 && _str.indexOf("=") == -1 && _str.indexOf(":") != -1)
                {
                    _info.type = "总";
                    QList<QString> _firstList = _str.split(":");
                    _info.devicename = _firstList.at(0);
                    _info.doit = _firstList.at(1);
                    _info.value = 0;
                }
                if(_str.indexOf(">") !=  -1)//进入到这里不一定只有>,还有<号
                {
                    _info.type = "分";
                    if(_str.indexOf(":") != -1)
                    {
                        QList<QString> _firstList = _str.split(":");
                        if(_firstList.size() == 3)//就是两条规则
                        {
                            _info.secondstatus = "有";
                            if(_firstList.at(1).indexOf(">") != -1)
                            {
                                QList<QString> _secondList = _firstList.at(1).split(">");
                                _info.devicename = _firstList.at(0);
                                _info.subdevicename = _secondList.at(0);
                                _info.doit = ">";
                                _info.value = _secondList.at(1).toInt();
                            }else if(_firstList.at(1).indexOf("<") != -1)
                            {
                                QList<QString> _secondList = _firstList.at(1).split("<");
                                _info.devicename = _firstList.at(0);
                                _info.subdevicename = _secondList.at(0);
                                _info.doit = "<";
                                _info.value = _secondList.at(1).toInt();
                            }else if(_firstList.at(1).indexOf("=") != -1)
                            {
                                QList<QString> _secondList = _firstList.at(1).split("=");
                                _info.devicename = _firstList.at(0);
                                _info.subdevicename = _secondList.at(0);
                                _info.doit = "=";
                                _info.value = _secondList.at(1).toInt();
                            }
                            if(_firstList.at(2).indexOf(">") != -1)
                            {
                                QList<QString> _secondList = _firstList.at(2).split(">");
                                _info.doit2 = ">";
                                _info.value2 = _secondList.at(1).toInt();
                            }
                            if(_firstList.at(2).indexOf("<") != -1)
                            {
                                QList<QString> _secondList = _firstList.at(2).split("<");
                                _info.doit2 = "<";
                                _info.value2 = _secondList.at(1).toInt();
                            }
                            if(_firstList.at(2).indexOf("=") != -1)
                            {
                                QList<QString> _secondList = _firstList.at(2).split("=");
                                _info.doit2 = "=";
                                _info.value2 = _secondList.at(1).toInt();
                            }
                        }else if(_firstList.size() == 2)//一条规则
                        {
                            QList<QString> _secondList = _firstList.at(1).split(">");
                            _info.devicename = _firstList.at(0);
                            _info.subdevicename = _secondList.at(0);
                            _info.doit = ">";
                            _info.value = _secondList.at(1).toInt();
                            _info.secondstatus = "无";
                        }
                    }
                }
                if(_str.indexOf("<") != -1)
                {
                    _info.type = "分";
                    if(_str.indexOf(":") != -1)
                    {

                        QList<QString> _firstList = _str.split(":");
                        if(_firstList.size() == 3)
                        {
                            _info.secondstatus = "有";
                            if(_firstList.at(1).indexOf(">") != -1)
                            {
                                QList<QString> _secondList = _firstList.at(1).split(">");
                                _info.devicename = _firstList.at(0);
                                _info.subdevicename = _secondList.at(0);
                                _info.doit = ">";
                                _info.value = _secondList.at(1).toInt();
                            }else if(_firstList.at(1).indexOf("<") != -1)
                            {
                                QList<QString> _secondList = _firstList.at(1).split("<");
                                _info.devicename = _firstList.at(0);
                                _info.subdevicename = _secondList.at(0);
                                _info.doit = "<";
                                _info.value = _secondList.at(1).toInt();
                            }else if(_firstList.at(1).indexOf("=") != -1)
                            {
                                QList<QString> _secondList = _firstList.at(1).split("=");
                                _info.devicename = _firstList.at(0);
                                _info.subdevicename = _secondList.at(0);
                                _info.doit = "=";
                                _info.value = _secondList.at(1).toInt();
                            }
                            if(_firstList.at(2).indexOf(">") != -1)
                            {
                                QList<QString> _secondList = _firstList.at(2).split(">");
                                _info.doit2 = ">";
                                _info.value2 = _secondList.at(1).toInt();
                            }
                            if(_firstList.at(2).indexOf("<") != -1)
                            {
                                QList<QString> _secondList = _firstList.at(2).split("<");
                                _info.doit2 = "<";
                                _info.value2 = _secondList.at(1).toInt();
                            }
                            if(_firstList.at(2).indexOf("=") != -1)
                            {
                                QList<QString> _secondList = _firstList.at(2).split("=");
                                _info.doit2 = "=";
                                _info.value2 = _secondList.at(1).toInt();
                            }
                        }else if(_firstList.size() == 2)
                        {
                            QList<QString> _secondList = _firstList.at(1).split("<");
                            _info.devicename = _firstList.at(0);
                            _info.subdevicename = _secondList.at(0);
                            _info.doit = "<";
                            _info.value = _secondList.at(1).toInt();
                            _info.secondstatus = "无";
                        }
                    }
                }
                if(_str.indexOf("=") != -1)
                {
                    _info.type = "分";

                    if(_str.indexOf(":") != -1)
                    {
                        QList<QString> _firstList = _str.split(":");
                        if(_firstList.size() == 3)
                        {
                            _info.secondstatus = "有";
                            if(_firstList.at(1).indexOf(">") != -1)
                            {
                                QList<QString> _secondList = _firstList.at(1).split(">");
                                _info.devicename = _firstList.at(0);
                                _info.subdevicename = _secondList.at(0);
                                _info.doit = ">";
                                _info.value = _secondList.at(1).toInt();
                            }else if(_firstList.at(1).indexOf("<") != -1)
                            {
                                QList<QString> _secondList = _firstList.at(1).split("<");
                                _info.devicename = _firstList.at(0);
                                _info.subdevicename = _secondList.at(0);
                                _info.doit = "<";
                                _info.value = _secondList.at(1).toInt();
                            }else if(_firstList.at(1).indexOf("=") != -1)
                            {
                                QList<QString> _secondList = _firstList.at(1).split("=");
                                _info.devicename = _firstList.at(0);
                                _info.subdevicename = _secondList.at(0);
                                _info.doit = "=";
                                _info.value = _secondList.at(1).toInt();
                            }
                            if(_firstList.at(2).indexOf(">") != -1)
                            {
                                QList<QString> _secondList = _firstList.at(2).split(">");
                                _info.doit2 = ">";
                                _info.value2 = _secondList.at(1).toInt();
                            }
                            if(_firstList.at(2).indexOf("<") != -1)
                            {
                                QList<QString> _secondList = _firstList.at(2).split("<");
                                _info.doit2 = "<";
                                _info.value2 = _secondList.at(1).toInt();
                            }
                            if(_firstList.at(2).indexOf("=") != -1)
                            {
                                QList<QString> _secondList = _firstList.at(2).split("=");
                                _info.doit2 = "=";
                                _info.value2 = _secondList.at(1).toInt();
                            }
                        }else if(_firstList.size() == 2)
                        {
                            QList<QString> _secondList = _firstList.at(1).split("=");
                            _info.devicename = _firstList.at(0);
                            _info.subdevicename = _secondList.at(0);
                            _info.doit = "=";
                            _info.value = _secondList.at(1).toInt();
                            _info.secondstatus = "无";
                        }
                    }
                }
            }
//                        qDebug() << _info.devicename
//                                 << _info.subdevicename
//                                 << _info.type
//                                 << _info.secondstatus
//                                 << _info.doit
//                                 << _info.value
//                                 << _info.doit2
//                                 << _info.value2
//                                 << _info.ioro;
            DeviceOperator _opera;
            _opera.status = false;
            _opera.nextopera = "";
            _opera.frontopera = "";
            _info.oper = _opera;
            _dinfo.type = type;
            _dinfo.io = _info;
        }else if(type == "timer")
        {
            MultiTimerLambdaInfo _info;
            _info.type = "分";
            if(_str.indexOf(":") != -1 && _str.indexOf("-") != -1)
            {
                QList<QString> _firstList = _str.split(":");//(A框架加热器,关闭-5-A框架故障)
                QList<QString> _secondList = _firstList.at(1).split("-");//(关闭,5,A框架故障)
                _info.reason = _firstList.at(0);
                _info.doit = _secondList.at(0);
                _info.minute = _secondList.at(1).toUInt();
                _info.result = _secondList.at(2);
            }
            DeviceOperator _opera;
            _opera.status = false;
            _opera.nextopera = "";
            _opera.frontopera = "";
            _info.oper = _opera;
            _dinfo.type = type;
            _dinfo.timer = _info;
        }else if(type == "var")
        {
            MultiVarLambdaInfo _info;
            _info.type = "总";
            _info.var = _str;
            DeviceOperator _opera;
            _opera.status = false;
            _opera.nextopera = "";
            _opera.frontopera = "";
            _info.oper = _opera;
            _dinfo.type = type;
            _dinfo.var = _info;
        }
        _list.append(_dinfo);
    }
    return _list;
}

QList<QString> MyReadConf::getMultiLambdaOperator(QString lambda)
{
    QList<QString> _list;
    for(int i =0;i<lambda.size();i++)
    {
        if(lambda.at(i) == "&" || lambda.at(i) == "|" || lambda.at(i) == "!")
        {
            _list.append(lambda.at(i));

        }
    }
    return _list;
}

void MyReadConf::insertOperatorToDevice(QList<MultiLambdaDeviceInfo> &devices, const QList<QString> &opertors)
{
    int _count = 0;
    for(int i =0;i<opertors.size();i++)
    {
        if(opertors.at(i) == "&" || opertors.at(i) == "|")
        {
            MultiLambdaDeviceInfo _struct = devices.at(_count);
            if(_struct.type == "fire")
            {
                _struct.fire.oper.nextopera = opertors.at(i);
            }
            if(_struct.type == "gate")
            {
                _struct.gate.oper.nextopera = opertors.at(i);
            }
            if(_struct.type == "io")
            {
                _struct.io.oper.nextopera = opertors.at(i);
            }
            if(_struct.type == "smog")
            {
                _struct.smog.oper.nextopera = opertors.at(i);
            }
            if(_struct.type == "timer")
            {
                _struct.timer.oper.nextopera = opertors.at(i);
            }
            if(_struct.type == "var")
            {
                _struct.var.oper.nextopera = opertors.at(i);
            }
            devices.replace(_count, _struct);
            _count += 1;
        }
        if(opertors.at(i) == "!")
        {
            MultiLambdaDeviceInfo _struct = devices.at(_count);
            if(_struct.type == "fire")
            {
                _struct.fire.oper.frontopera = opertors.at(i);
            }
            if(_struct.type == "gate")
            {
                _struct.gate.oper.frontopera = opertors.at(i);
            }
            if(_struct.type == "io")
            {
                _struct.io.oper.frontopera = opertors.at(i);
            }
            if(_struct.type == "smog")
            {
                _struct.smog.oper.frontopera = opertors.at(i);
            }
            if(_struct.type == "timer")
            {
                _struct.timer.oper.frontopera = opertors.at(i);
            }
            if(_struct.type == "var")
            {
                _struct.var.oper.frontopera = opertors.at(i);
            }
            devices.replace(_count, _struct);
        }
    }

    if(opertors.size() == 0 && devices.size() == 1)
    {
        //        qDebug() << devices.size() << opertors.size() << __LINE__;

        MultiLambdaDeviceInfo _struct = devices.at(0);
        if(_struct.type == "fire")
        {
            _struct.fire.oper.nextopera = "|";
        }
        if(_struct.type == "gate")
        {
            _struct.gate.oper.nextopera = "|";
        }
        if(_struct.type == "io")
        {
            _struct.io.oper.nextopera = "|";
        }
        if(_struct.type == "smog")
        {
            _struct.smog.oper.nextopera = "|";
        }
        if(_struct.type == "timer")
        {
            _struct.timer.oper.nextopera = "|";
        }
        if(_struct.type == "var")
        {
            _struct.var.oper.nextopera = "|";
        }
        devices.replace(0, _struct);
    }
}

void MyReadConf::setFireDeviceStatusSlot(QString devicename, QString status)
{
    qDebug() << devicename << status;
    //获取英文设备名
    QString _englishname;
    for(int i =0;i<m_fireAlarmSensorInfoMap.size();i++)
    {
        MyFireAlarmSensorInfo _struct = m_fireAlarmSensorInfoMap[m_fireAlarmSensorInfoMap.keys().at(i)];
        if(_struct.base.alias == devicename)
        {
            _englishname = _struct.base.englishname;
        }
    }
    set->setValue(QString("%1/%2").arg(_englishname).arg("shield"), status);
    //更新以下m_fireAlarmSensorInfoMap
    m_fireAlarmSensorInfoMap[devicename].base.shield = status;
}

void MyReadConf::setSmogDeviceStatusSlot(QString devicename, QString subdevicename, QString status)
{
    qDebug() << devicename << subdevicename << status;
    QString _englishname;
    for(int i =0;i<m_smogInfoMap.size(); i++)
    {
        MySmogSensorInfo _struct = m_smogInfoMap[m_smogInfoMap.keys().at(i)];
        if(_struct.base.alias == devicename)
        {
            _englishname = _struct.base.englishname;
        }
    }
    //获取
    QString _str("%1/%2");
    if(m_smogInfoMap[devicename].address1 == subdevicename)
    {
        m_smogInfoMap[devicename].address1shield = status;
        _str = _str.arg(_englishname).arg("address1shield");
    }else if(m_smogInfoMap[devicename].address2 == subdevicename)
    {
        m_smogInfoMap[devicename].address2shield = status;
        _str = _str.arg(_englishname).arg("address2shield");
    }else if(m_smogInfoMap[devicename].address3 == subdevicename)
    {
        m_smogInfoMap[devicename].address3shield = status;
        _str = _str.arg(_englishname).arg("address3shield");

    }else if(m_smogInfoMap[devicename].address4 == subdevicename)
    {
        m_smogInfoMap[devicename].address4shield = status;
        _str = _str.arg(_englishname).arg("address4shield");

    }else if(m_smogInfoMap[devicename].address5 == subdevicename)
    {
        m_smogInfoMap[devicename].address5shield = status;
        _str = _str.arg(_englishname).arg("address5shield");

    }else if(m_smogInfoMap[devicename].address6 == subdevicename)
    {
        m_smogInfoMap[devicename].address6shield = status;
        _str = _str.arg(_englishname).arg("address6shield");

    }else if(m_smogInfoMap[devicename].address7 == subdevicename)
    {
        m_smogInfoMap[devicename].address7shield = status;
        _str = _str.arg(_englishname).arg("address7shield");

    }else if(m_smogInfoMap[devicename].address8 == subdevicename)
    {
        m_smogInfoMap[devicename].address8shield = status;
        _str = _str.arg(_englishname).arg("address8shield");

    }else if(m_smogInfoMap[devicename].address9 == subdevicename)
    {
        m_smogInfoMap[devicename].address9shield = status;
        _str = _str.arg(_englishname).arg("address9shield");

    }else if(m_smogInfoMap[devicename].address10 == subdevicename)
    {
        m_smogInfoMap[devicename].address10shield = status;
        _str = _str.arg(_englishname).arg("address10shield");

    }else if(m_smogInfoMap[devicename].address11 == subdevicename)
    {
        m_smogInfoMap[devicename].address11shield = status;
        _str = _str.arg(_englishname).arg("address11shield");

    }else if(m_smogInfoMap[devicename].address12 == subdevicename)
    {
        m_smogInfoMap[devicename].address12shield = status;
        _str = _str.arg(_englishname).arg("address12shield");

    }else if(m_smogInfoMap[devicename].address13 == subdevicename)
    {
        m_smogInfoMap[devicename].address13shield = status;
        _str = _str.arg(_englishname).arg("address13shield");

    }else if(m_smogInfoMap[devicename].address14 == subdevicename)
    {
        m_smogInfoMap[devicename].address14shield = status;
        _str = _str.arg(_englishname).arg("address14shield");

    }else if(m_smogInfoMap[devicename].address15 == subdevicename)
    {
        m_smogInfoMap[devicename].address15shield = status;
        _str = _str.arg(_englishname).arg("address15shield");

    }else if(m_smogInfoMap[devicename].address16 == subdevicename)
    {
        m_smogInfoMap[devicename].address16shield = status;
        _str = _str.arg(_englishname).arg("address16shield");

    }else if(m_smogInfoMap[devicename].address17 == subdevicename)
    {
        m_smogInfoMap[devicename].address17shield = status;
        _str = _str.arg(_englishname).arg("address17shield");
    }
    set->setValue(_str, status);
}

void MyReadConf::setQXZSlot(QString one, QString two, QString three, QString four)
{
    m_fireDeviceArgumentHash["one"] = one;
    m_fireDeviceArgumentHash["two"] = two;
    m_fireDeviceArgumentHash["three"] = three;
    m_fireDeviceArgumentHash["four"] = four;
    set->setValue(QString("%1/%2").arg("fire-argument").arg("one"), one);
    set->setValue(QString("%1/%2").arg("fire-argument").arg("two"), two);
    set->setValue(QString("%1/%2").arg("fire-argument").arg("three"), three);
    set->setValue(QString("%1/%2").arg("fire-argument").arg("four"), four);
}

void MyReadConf::setLMDSlot(QString one)
{
    m_smogDeviceArgumentHash["one"] = one;
    set->setValue(QString("%1/%2").arg("smog-argument").arg("one"), one);
}
