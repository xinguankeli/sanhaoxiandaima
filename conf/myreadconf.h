#ifndef MYREADCONF_H
#define MYREADCONF_H

#include <QObject>
#include <global/Global.h>
#include <QSettings>
#include <QHash>
class MyReadConf : public QObject
{
    Q_OBJECT
public:
    explicit MyReadConf(const QString filePath, QObject *parent = nullptr);
    MyReadConf(QObject *parent = nullptr);
    DataBaseConfigWord getDataBaseConfigWord();//对外提供获取数据接口
    QHash<QString, MyFireAlarmSensorInfo> getFireAlarmSensorInfoMap();
    QHash<QString, MyGateDeviceInfo> getGateDeviceInfoMap();
    QHash<QString, MyControllerInfo> getIOInfoMap();
    QHash<QString, MySmogSensorInfo> getSmogInfoMap();
    QHash<QString, QList<MultiLambdaDeviceInfo> > getEventMultiLambdaList();
    QHash<QString, QList<MultiLambdaDeviceInfo> > getExecMultiLambdaList();
    QHash<QString, bool> getVariableMultiLambdaList();



    QList<MyDeviceBaseInfo> getUdpEventDeviceInfoList();
    QString getMasterSign();
    QMap<QString, QPair<QString, quint16> > getMasterNetworkAddress();
    QHash<QString, QString> getIOTCarInfo();
    QHash<QString, QString> getFireDeviceArguemnt();
    QHash<QString, QString> getSmogDeviceArgument();
    double getWenDuFuDongZhi();
    double getLingDianLingYiWu();

    QHash<QString, int> getGuDingChaZhi();
signals:
private:
    QString m_configFilePath;
    QHash<QString, MyFireAlarmSensorInfo> m_fireAlarmSensorInfoMap;//存放火灾报警探测器的所有设备信息，ip端口设备名等
    QHash<QString, MyGateDeviceInfo> m_gateDeviceInfoMap;          //存放阀门的所有设备信息，ip端口设备名等
    QHash<QString, MyControllerInfo> m_IOMap;              //存放控制器的所有设备信息,ip端口设备名等
    QHash<QString, MySmogSensorInfo> m_smogInfoMap;          //存放感烟探测器的所有设备信息,ip端口设备名等
    QHash<QString, QList<MultiLambdaDeviceInfo> > m_eventMultiLambdaList;         //存放配置文件中解析出来的设备信息
    QHash<QString, QList<MultiLambdaDeviceInfo> > m_execMultiLambdaList;     //存放配置文件中解析出来的执行信息
    QHash<QString, bool> m_variableMultiLambdaList;//存放配置文件中的变量名

    DataBaseConfigWord m_dataBaseConfigWord;//从配置文件中读取出来的数据库配置信息
    QSettings *set;
    QList<MyDeviceBaseInfo> m_udpEventDeviceInfoList;
    QMap<QString, QPair<QString, quint16> > m_masterNetworkAddress;
    QString m_masterSign;//主从字符串
    QHash<QString, QString> m_IOTCarInfoHash;
    QHash<QString, QString> m_fireDeviceArgumentHash;
    //感烟探测器的灵敏度
    QHash<QString, QString> m_smogDeviceArgumentHash;
    //温度算法中的根据电压的浮动值
    double m_wenduFudongZhi;
    //温度算法中的0.015000值
    double m_lingDianLingYiWuZhi;
    //固定插值
    QHash<QString, int> m_guDingChaZhiHash;
private:
    void init();
    bool readDataBaseConfigWord();

    //参数为在配置文件中完整的my-lambda
    //返回值为解析完成的设备结构体list
    QList<MultiLambdaDeviceInfo> processMultiLambdaLeft(QString);

    //第一个参数为开始字符
    //第二个参数为结束字符
    //第三个参数为总字符串
    //返回这个字符串中两个符号中间的所有字符串
    QList<QString> betweenStartAndEnd(const QString, const QString, QString);

    //返回第一个字符和第二个字符之间的字符串
    //第一个参数[
    //第二个参数]
    //第三个参数[你好啊，bro]2323
    //返回值 [你好啊，bro, index]
    QPair<QString, int> getStrStartToEnd(const QString, const QString, QString);


    //第一个参数为type:fire smog io gate
    //第一个参数为QList<QString>
    //返回值为QList<MultiLambdaDeviceInfo>
    QList<MultiLambdaDeviceInfo> parseMultiDeviceInfo(QString, QList<QString>);


    //第一个参数为QString
    //返回值为QList<QString> 里面是操作符
    QList<QString> getMultiLambdaOperator(QString);



    //第一个参数为解析==号左侧的设备列表，第二个参数为解析的操作符
    void insertOperatorToDevice(QList<MultiLambdaDeviceInfo> &, const QList<QString> &);
public slots:
    //在配置文件中设置感温设备的屏蔽状态
    //第一个参数是设备名
    //第二个参数是已屏蔽、未屏蔽
    void setFireDeviceStatusSlot(QString, QString);

    //在配置文件中设置感烟设备的屏蔽状态
    //第一个参数是设备名
    //第二个参数是子设备名
    //第三个参数是已屏蔽、未屏蔽
    void setSmogDeviceStatusSlot(QString, QString, QString);

    //在配置文件中设置感温探测器的QXZ
    void setQXZSlot(QString, QString, QString, QString);

    //在配置文件中设置感烟探测器的灵敏度
    void setLMDSlot(QString);
};

#endif // MYREADCONF_H
