#ifndef MYFIREDEVICEDATACOPY_H
#define MYFIREDEVICEDATACOPY_H

#include <QObject>
#include <QMutex>
class MyFireDeviceDataCopy : public QObject
{
    Q_OBJECT
public:
    explicit MyFireDeviceDataCopy(QObject *parent = nullptr);
    static MyFireDeviceDataCopy *GetInstance();

    static void deleteInstance();
private:
    static MyFireDeviceDataCopy *m_instance;
signals:

};

#endif // MYFIREDEVICEDATACOPY_H
