#include "mytimer.h"

MyTimer::MyTimer(QObject *parent):QTimer(parent)
{
    connect(this, &QTimer::timeout, this, [&]{
        emit myTimeOut(m_data);
    });
}

void MyTimer::setMyData(const QList<QVariant> data)
{
    m_data = data;
}

void MyTimer::setResault(const QString resault)
{
    m_resault = resault;
}

QString MyTimer::getResault()
{
    return m_resault;
}

QList<QVariant> MyTimer::getList()
{
    return m_data;
}
