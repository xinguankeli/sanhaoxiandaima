#ifndef MYTIMER_H
#define MYTIMER_H

#include <QObject>
#include <QTimer>
#include <QVariant>
class MyTimer : public QTimer
{
    Q_OBJECT
public:
    MyTimer(QObject *parent = nullptr);
    void setMyData(const QList<QVariant>);
    void setResault(const QString);
    QString getResault();
    QList<QVariant> getList();
private:
    QList<QVariant> m_data;
    QString m_resault;
signals:
    //信号的参数是存放在该定时器里的数据
    void myTimeOut(QList<QVariant>);
};

#endif // MYTIMER_H
