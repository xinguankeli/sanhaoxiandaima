
#ifndef GLOBAL_H
#define GLOBAL_H

#define TOONEBIT1 0x01
#define TOONEBIT2 0x02
#define TOONEBIT3 0x04
#define TOONEBIT4 0x08
#define TOONEBIT5 0x10
#define TOONEBIT6 0x20
#define TOONEBIT7 0x40
#define TOONEBIT8 0x80

#define TOZEROBIT1 0xFE
#define TOZEROBIT2 0xFD
#define TOZEROBIT3 0xFB
#define TOZEROBIT4 0xF7
#define TOZEROBIT5 0xEF
#define TOZEROBIT6 0xDF
#define TOZEROBIT7 0xBF
#define TOZEROBIT8 0x7F

#include <QString>
#include <QHash>


#define FIRETABLE "fireTable"
#define FAULTTABLE "faultTable"
#define EVENTTABLE "eventTable"
#define SMOGTABLE "smogTable"
#define ALLTABLE "allTable"
#define NETWORKERR "网络连接故障"
#define NETWORKSUCC "网络连接成功"
#define FIRE "火警"
#define ISNOTCLEAR "未清除"
#define ISCLEAR "已清除"
struct DataBaseConfigWord
{
    QString hostname;
    QString databasename;
    QString username;
    QString password;
};

struct MyDeviceBaseInfo
{
    QString englishname;
    QString alias;
    QString ip;
    quint16 port;
    QString shield;
    int recv_length;
};
struct MyFireAlarmSensorInfo
{
    MyDeviceBaseInfo base;
};
struct MyGateDeviceInfo
{
    MyDeviceBaseInfo base;
    QString leftalias;
    QString rightalias;
    QString leftstatus;
    QString rightstatus;
};
struct MyControllerInfo
{
    MyDeviceBaseInfo base;
    QString output1,output2,output3,output4,output5,output6;
    QString input1, input2, input3, input4, input5, input6;
};
struct MySmogSensorInfo
{
    MyDeviceBaseInfo base;
    QString address1,address2, address3, address4, address5,
    address6, address7, address8, address9, address10, address11,
    address12, address13,address14, address15, address16, address17;

    QString address1shield,address2shield, address3shield, address4shield, address5shield,
    address6shield, address7shield, address8shield, address9shield, address10shield, address11shield,
    address12shield, address13shield,address14shield, address15shield, address16shield, address17shield;
};

//下面是表达式各项结构体
//阀门设备的表达式解析格式
struct DeviceOperator
{
    //存放该设备后面的操作符有& | !
    QString nextopera;

    //存放自己当前的状态
    bool status;

    //存放是否需要取反操作,设备前面的！符号
    QString frontopera;
};

struct MultiGateDeviceLambdaInfo
{
    //type类型有两种：总、分
    //总代表总模块的属性
    //分代表子阀门的属性
    QString type;

    //总模块名字
    QString devicename;

    //子模块名字
    QString subdevicename;

    //动作有以下几个动作
    //打开、关闭、故障
    QString doit;

    //存放表达式中的操作符
    DeviceOperator oper;

};

//感温设备的表达式解析格式
struct MultiFireDeviceLambdaInfo
{
    //总模块名字
    QString devicename;

    //动作有以下几个动作
    //故障、火警
    QString doit;

    //存放表达式中的操作符
    DeviceOperator oper;
};

//感烟设备的表达式解析格式
struct MultiSmogDeviceLambdaInfo
{
    //type类型有两种：总、分
    //总代表总模块的属性
    //分代表输出点的属性
    QString type;

    //总模块名字
    QString devicename;

    //子模块名字
    QString subdevicename;

    //动作有以下几个动作
    //打开、关闭、故障
    QString doit;

    //存放表达式中的操作符
    DeviceOperator oper;
};

//IO设备的表达式解析格式
struct MultiIODeviceLambdaInfo
{
    //type总类有两种：总、分
    //总代表总模块的属性
    //分代表输出点的属性
    QString type;

    //该字段描述是input值还是output值
    //表达式==号左侧写的是温度>300 or 复位按钮>2000 还是 电磁阀1-打开
    QString ioro;

    //总模块名字
    QString devicename;

    //子模块名字
    QString subdevicename;

    //是否有第二个规则
    //有 无
    QString secondstatus;

    //动作有以下几个动作
    //打开、关闭、故障、小于、大于
    QString doit;
    QString doit2;
    //值
    qint16 value;
    qint16 value2;
    //存放表达式中的操作符
    DeviceOperator oper;
};
//Timer表达式
struct MultiTimerLambdaInfo
{
    //type总类分两种：总、分
    //总代表总模块的属性
    //分暂无代表
    QString type;

    //定时器名字
    QString reason;

    //定时器参数：分钟
    quint16 minute;

    //执行的动作
    QString doit;

    //结果
    QString result;

    //存放表达式中的操作符
    DeviceOperator oper;
};
//变量
struct MultiVarLambdaInfo
{
    //type总类分两种：总、分
    //总代表总模块的属性
    //分暂无代表
    QString type;

    //变量名
    QString var;

    //存放表达式中的操作符
    DeviceOperator oper;

};

//集合四种设备的表达式解析字符串
struct MultiLambdaDeviceInfo
{
    //分别是四种设备
    //fire、fault、smog、io
    QString type;
    QString lambdaname;
    MultiFireDeviceLambdaInfo fire;
    MultiSmogDeviceLambdaInfo smog;
    MultiGateDeviceLambdaInfo gate;
    MultiIODeviceLambdaInfo io;
    MultiTimerLambdaInfo timer;
    MultiVarLambdaInfo var;
};



//下面是故障、报警、日志表的结构体
struct TableBaseWord
{
    QString casue;//原因
    QString devicename;//设备名
    quint64 date;//日期
    QString isclear;//是否清除
};
//数据库故障表
struct MyDataBaseWord
{
    QString type;
    TableBaseWord fault_word;
    TableBaseWord fire_word;
    TableBaseWord event_word;
    TableBaseWord smog_word;
};

//整个火警设备的状态
struct FireDeviceStatus
{
    //网络状态，有以下两种状态
    //网络连接故障、网络连接成功
    QString networkstatus;

    //设备复位状态，有以下两种状态
    //未复位、复位成功
    QString resetstatus;

    //设备火警状态，有以下两种状态
    //无火警、有火警
    QString firestatus;

    //设备屏蔽状态，有以下两种状态
    //未屏蔽、已屏蔽
    QString shieldstatus;

    //最高温度
    quint16 maxwendu;
    //环境温度
    quint16 huanjingwendu;

    //四个值
    QString onevalue;
    QString twovalue;
    QString threeValue;
    QString fourValue;

};
//整个阀门设备的状态
struct GateDeviceStatus
{
    //网络状态，有以下两种状态
    //网络连接故障、网络连接成功
    QString networkstatus;

    //左侧阀门开关状态，有以下两种状态
    //开、关、未知
    QString leftgatestatus;

    //右侧阀门状态，有以下两种状态
    //开、关、未知
    QString rightgatestatus;

    //左侧阀门名字
    QString leftname;

    //右侧阀门名字
    QString rightname;

    //左侧阀门的屏蔽状态
    QString leftshieldstatus;

    //右侧阀门的屏蔽状态
    QString rightshieldstatus;
};
//整个感温设备的状态
struct SmogDeviceStatus
{
    //网络状态，有以下两种状态
    //网络连接故障、网络连接成功
    QString networkstatus;

    //设备火警状态，有以下两种状态
    //无火警、有火警
    QHash<QString, QString> firestatus;

    //设备故障状态，有以下两种状态
    //有故障、无故障
    QHash<QString, QString> faultstatus;

    //复位状态，有以下两种状态
    //未复位、复位成功
    QHash<QString, QString> resetstatus;

    //设备屏蔽状态，有以下两种状态
    //未屏蔽、已屏蔽
    QHash<QString, QString> shieldstatus;
};
//整个IO设备的状态
struct IODeviceStatus
{
    //网络状态，有以下两种状态
    //网络连接故障，网路连接成功
    QString networkstatus;

    //六个输入点的值
    QHash<QString, quint16> inputs;

    //六个输出点的开关
    QHash<QString, quint8> outputs;

    bool ajiareqiguzhang;
    bool bjiareqiguzhang;
    bool anuanfengjiguzhang;
    bool bnuanfengjiguzhang;

};
//MVB类给Core类发送的设置收到的数据的结构体
struct MVBToCoreData
{
    //数据的类型
    //目前有 设置时间、隔离复位
    QString type;

    //数据类型
    QString data;
};

#endif // GLOBAL_H


