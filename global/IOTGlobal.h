#ifndef IOTGLOBAL_H
#define IOTGLOBAL_H
#include <QObject>
#define IOTDATACOUNT 436
//IOT数据结构体

//定义IOT的发送数据结构体
struct IOTData
{
    //请求编号
    quint8 requestcode[2];
    //车辆编号
    quint8 carnumber[3];
    //设备类型
    quint8 devicenumber;
    //功能码
    quint8 functioncode;
    //数据组数量
    quint16 datagroupnumber;
    //所有数据
    quint16 data[IOTDATACOUNT];
    //感温探测器总共504个

    //感烟探测器总共51个

    //阀门总共43个

    //气灭总共31个

    //A框架设备状态总共6个

    //B框架设备状态总共6个

    //1车按钮总共3个

    //6车按钮总共3个

    //水罐温度总共2个

    //液位总共2个

    //压力总共4个

    //以上计算总共655个
};

#endif // IOTGLOBAL_H
