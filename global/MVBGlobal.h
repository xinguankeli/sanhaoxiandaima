#ifndef MVBGLOBAL_H
#define MVBGLOBAL_H
//存放mvb的结构体
#include <QObject>
#define q8 quint8
#define q16 quint16
#define qb QByteArray
//0xff port
struct struct0xff
{
    q8 cpulivesignalhigh;//cpu生命信号，高位字节
    q8 cpulivesignallow;//cpu生命信号，低位字节
    q8 year;//年
    q8 month;//月
    q8 day;//日
    q8 hour;//时
    q8 minute;//分
    q8 second;//秒
    q8 timevalid;//时间有效
    q8 gelifuwei;//隔离复位
    q8 yuliu1;//预留
    q8 yuliu2;//预留
    q8 linenumber;//线路号
    q8 trainnumber;//列车号
    q8 trainspeedhigh;//列车参考速度高位
    q8 trainspeedlow;//列车参考速度低位
};
//0xb08
struct struct0xb08
{
    q8 gelifuwei;
    q8 yuliu1;
    q8 yuliu2;
    q8 yuliu3;
};

//0xb10 port
struct struct0xb10Andb60
{
    q8 cpulivesignalhigh;
    q8 cpulivesignallow;
    q8 softwareversionhigh;
    q8 softwareversionlow;
    q8 yuliu1;
    q8 aframewendu;
    q8 bframewendu;
    q8 data[19];
    q8 aframeyewei;
    q8 bframeyewei;
    q8 aframedanqiping1yali;
    q8 aframedanqiping2yali;
    q8 bframedanqiping1yali;
    q8 bframedanqiping2yali;
};
//0xb11 port
struct struct0xb11Andb61
{
    q8 data[32];
};

//0xb61 port
//配置mvb源端口和宿端口的结构体
struct MCUToMVBPortAddress
{
    q8 portaddresshigh;
    q8 portaddresslow;
    q8 portdatalength;
    q8 portcommunicationcycle;
};

struct ConfigPort
{
    q8 startbyte;//起始字节
    q8 framelength;//帧长度
    q8 configcommand;//配置命令
    q8 deviceaddresshigh;//设备地址高位
    q8 deviceaddresslow;//设备地址低位
    q8 mcutomvbsourceportcount;//MCU源端口数量
    MCUToMVBPortAddress source[30];//MCU to MVB端口
    q8 mvbtomcuhostportcount;//MCU宿端口数量
    MCUToMVBPortAddress dist[30];//MVB to MCU端口
    q8 crcbytehigh;//crc校验低
    q8 crcbytelow;//crc校验高
    q8 endbyte;//终止字节
};
struct ConfigPortSign//用来标志哪些东西被设置过
{
    bool deviceaddresshgihsign;
    bool deviceaddresslowsign;
    bool mcutomvbsourceportcountsign;
    bool mvbtomcuhostportcountsign;
    bool crcbytehigh;
    bool crcbytelow;
};
struct GetMessage{
    q8 startbyte;
    q8 framelength;
    q8 commandbyte;
    q8 uploadcycle;
    q8 requestcommand;
    q8 crchigh;
    q8 crclow;
    q8 endbyte;
};
//配置应答帧,该结构体是设置完250个字节后返回的数据应答帧
struct ConfigResponseFrame
{
    q8 startbyte;
    q8 framelength;
    q8 configresponsecommand;
    q8 crchigh;
    q8 crclow;
    q8 endbyte;
};
//返回的每个端口的内容
struct DataRequestResponseFrame
{
    q8 data[32];
};
//该结构体是设置完使能，返回的配置应答帧
//=======================================接收数据用这个结构体
struct enableResponseFrame
{
    q8 startbytex;
    q8 framelength;
    q8 datareturncommand;
    q8 datastatusinfo;
    q8 returnportaddresshigh;
    q8 returnportaddresslow;
    q8 data[16];
    q8 crchigh;
    q8 crclow;
    q8 endbyte;
};
struct enableResponseFrame2
{
    q8 startbytex;
    q8 framelength;
    q8 datareturncommand;
    q8 datastatusinfo;
    q8 returnportaddresshigh;
    q8 returnportaddresslow;
    q8 data[4];
    q8 crchigh;
    q8 crclow;
    q8 endbyte;
};

//========================================发送数据用这个结构体
struct sendFrame
{
    q8 startbyte;
    q8 framelength;
    q8 senddatacommand;
    q8 superinfo;
    q8 autoaddaddress;
    q8 returnportaddresshigh;
    q8 returnportaddresslow;
    DataRequestResponseFrame senddata;
    q8 crchig;
    q8 crclow;
    q8 endbyte;
};
#endif // MVBGLOBAL_H
