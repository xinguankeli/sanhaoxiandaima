#ifndef CORE_H
#define CORE_H
#include <QHash>
#include <QObject>
#include "socket/device/firedevice.h"
#include "socket/device/gatedevice.h"
#include "socket/device/smogdevice.h"
#include "socket/device/iodevice.h"
#include "global/Global.h"
#include <QMutex>
#include <QJsonObject>
#include "conf/myreadconf.h"
#include "database/mydatabase.h"
#include "express/mytcpserver.h"
#include "express/myeventudp.h"
#include "express/mymasterorclient.h"
#include "express/myclientormaster.h"
#include "mywidget/mytimer.h"
#include "mvb/mymvbserver.h"
#include "iot/myiotclient.h"
class Core : public QObject
{
    Q_OBJECT
public:
    explicit Core(QObject *parent = nullptr);
private:
    FireDevice *m_fireDevice;
    GateDevice *m_gateDevice;
    SmogDevice *m_smogDevice;
    IODevice *m_ioDevice;
    MyDataBase *m_database;
    MyTcpServer *m_expressServer;
    MyEventUDP *m_eventUdp;
    MyMasterOrClient *m_masterOrClient;
    MyMVBServer *m_mvbServer;
    MyIOTClient *m_iotClient;
    MyClientOrMaster *m_clientOrMaster;
    //火警设备的数据结构
    //key是设备名 value是实时的状态值
    QHash<QString, FireDeviceStatus> m_fireDeviceStatusHash;
    //用作隔离服用的感温探测器副本
    QHash<QString, FireDeviceStatus> m_fireDeviceStatushCopyHash;
    //用来存放接收有火警的桶
    QHash<QString, QString> m_fireDeviceFireStatusHashByUpdateStatus;
    //用来存放接收火警的桶的锁
    QHash<QString, bool> m_fireDeviceFireMutexHashByUpdateStatus;
    //用来存放接收复位成功的锁
    QHash<QString, bool> m_fireDeviceResetMutexHashByUpdateStatus;
    //用来存放接收有感烟火警的桶
    QHash<QString, QString> m_smogDeviceFireStatusHashByUpdateStatus;
    //用来存放接收感烟的桶的锁
    QHash<QString, bool> m_smogDeviceFireMutexHashByUpdateStatus;
    //用来存放接收复位成功的锁
    QHash<QString, bool> m_smogDeviceResetMutexHashByUpdateStatus;
    //用来拦截阀门的未知问题
    //key是所有左右阀门，value是每个阀门的未知次数
    QHash<QString, int> m_gateUnKnowStatusLanJieHash;

    //阀门设备的数据结构
    //key是设备名，value是实时的状态值
    QHash<QString, GateDeviceStatus> m_gateDeviceStatusHash;

    //感烟设备的数据结构
    //key是设备名，value是实时的状态值
    QHash<QString, SmogDeviceStatus> m_smogDeviceStatusHash;

    //IO设备的数据结构
    //key是设备名，value是实时的状态值
    QHash<QString, IODeviceStatus> m_ioDeviceStatusHash;

    //设置火警数据结构的锁
    QMutex *m_fireDeivceStatusMutex;

    //设置阀门数据结构的锁
    QMutex *m_gateDeviceStatusMutex;

    //设置阀门数据结构的锁
    QMutex *m_smogDeviceStatusMutex;

    //设置IO数据结构的锁
    QMutex *m_ioDeviceStatusMutex;

    //主从变量的锁
    QMutex *m_masterOrClientMutex;

    //读取本地配置文件类
    MyReadConf *m_readConf;

    //存放从配置文件解析出来的表达式中的事件触发设备结构体列表
    QHash<QString, QList<MultiLambdaDeviceInfo> > m_eventMultiLambdaHash;
    //存放从配置文件中解析出来的表达式中的被触发设备结构体列表
    QHash<QString, QList<MultiLambdaDeviceInfo> > m_execMultiLambdaHash;

    //存放配置文件中四个预留定时器的列表
    //key是定时器名字(原因) value:[key是表达式名字,values是定时器指针]
    QHash<QString, QHash<QString, MyTimer*> > m_eventMultiLambdaTimerHash;

    //存放表达式中的Timer执行标志位,key为：表达式中的event value的key为doit, value的value为bool类型：true可执行，false不可执行
    QHash<QString, QPair<QString, bool> > m_multiLambdaTimerStatusHash;

    //存放配置文件中表达式的Hash,key是变量名字，value是变量是否触发
    QHash<QString, bool> m_multiLambdaVariableHash;

    //主从变量，该变量用于主从之间的通信，如果主掉线了，那么从接替做一些事情，属于不稳定变量
    bool m_masterOrClientStatus;

    //主从变量，该变量在配置文件中读取出来，属于稳定的主从变量，用于给mvb发送端口数据
    bool m_masterOrClientStatusForMVB;
    //专门处理表达式的锁
    QMutex *m_multiLambdaMutex;

    //处理表达式中的timer的锁
    QMutex *m_multiLambdaTimerMutex;

    //处理表达式中的var的锁
    QMutex *m_multiLambdaVarMutex;

    //阀门的未知拦截锁，在阀门开的过程中会变成未知，要在开启后将其设置为拦截状态
    QHash<QString, QHash<QString, bool> > m_gateUnKnowStatusHash;

    //这个标志不为true，execDevice阀门不执行
    bool m_gateUnKnowStatusSign;

    //感温的执行动作的状态拦截
    QHash<QString, QString> m_fireToIdStatusLanJieHash;

    //感烟的执行动作的状态拦截
    QHash<QString, QString> m_smogToIdStatusLanJieHash;

    //表达式中定时器的动作的拦截
    QHash<QString, QString> m_timerToIdStatusLanJieHash;

    //IO开启或关闭的拦截锁
    QHash<QString, QHash<QString, bool> > m_ioOpenCloseLanJieHash;

    //发送mvb的定时器
    QTimer *m_mvbTimer;


    //发送iot的定时器
    QTimer *m_iotTimer;


    //发送表达式中var的变量的液位低、氮气瓶压力低等故障信息给从设备
    QTimer *m_syncFaultMessageTimer;

    //发送阀门的所有状态给UI，三秒发送一次
    QTimer *m_updateGateStatusToUITimer;

    //发送气灭启动状态的变量值
    QTimer *m_syncQiMieStatusToClientTimer;

    //补丁：如果加热器故障因为信号没发出去，导致无法关闭，连续发三次
    QTimer *m_ajiareqisanciTimer;
    QTimer *m_bjiareqisanciTimer;
    QTimer *m_anuanfengjisanciTimer;
    QTimer *m_bnuanfengjisanciTimer;
    int m_ajiareqisanciCount;
    int m_bjiareqisanciCount;
    int m_anuanfengjisanciCount;
    int m_bnuanfengjisanciCount;


    //下面是填充到对应的设备故障、火警、隔离、液位低、压力低、温度低的数据结构=========================================
    //感温设备的火警信息
    QHash<QString, QPair<int, QPair<int, int> > > m_fireByFireDeviceHash;
    //感温设备的故障信息
    QHash<QString, QPair<int, QPair<int, int> > > m_faultByFireDeviceHash;
    //感温设备的隔离信息
    QHash<QString, QPair<int, QPair<int, int> > > m_shieldByFireDeviceHash;

    //两次按钮隔离复位存储在第二次复位之前的所有设备是否是火警状态
    QHash<QString, int> m_doubleResetFireDeviceFireStatusHash;
    //每个设备都有两小时定时器
    QHash<QString, QTimer*> m_doubleResetFireDeviceFireTimerHash;

    //下面是发往UI的Message的拦截数据结构,感温和感烟的火警拦截
    QHash<QString, QJsonObject> m_fireDeviceFireInfoToUIHash;
    QHash<QString, QJsonObject> m_smogDeviceFireInfoToUIHash;
    //下面是发往UI的Message的拦截数据结构，感温和感烟的故障拦截
    QHash<QString, QJsonObject> m_fireDeviceFaultInfoToUIHash;
    QHash<QString, QJsonObject> m_smogDeviceFaultInfoToUIHash;
    QHash<QString, QJsonObject> m_ioDeviceFaultInfoToUIHash;
    QHash<QString, QJsonObject> m_gateDeviceFaultInfoToUIHash;
    QHash<QString, QJsonObject> m_timerDeviceFaultInfoToUIHash;
    //下面是发往UI的Message的拦截数据结构，阀门和IO的事件拦截
    QHash<QString, QJsonObject> m_gateDeviceEventInfoToUIHash;
    QHash<QString, QJsonObject> m_ioDeviceEventInfoToUIHash;
    //下面是Lambda里的定时器的拦截数据结构，只拦截定时器的故障和清除
    QHash<QString, QJsonObject> m_faultMessageIntoToUIHash;
    //下面是按钮按下的拦截数据结构，只拦截按钮事件
    QHash<QString, QJsonObject> m_buttonEventInfoToUIHash;

    //1-3车拦截变量的Hash
    QHash<QString, bool> m_yicheVarLanJieHash;
    //4-6车拦截变量的Hash
    QHash<QString, bool> m_liucheVarLanJieHash;


    //IO和GATE的开关事件拦截
    QHash<QString, QJsonObject> m_ioDeviceOpenCloseToUIHash;
    QHash<QString, QJsonObject> m_gateDeviceOpenCloseToUIHash;
    //所有设备的故障信息拦截
    QHash<QString, QJsonObject> m_deviceFaultToUIHash;
    //从设备的故障拦截
    QHash<QString, QJsonObject> m_masterOrClientFaultToUIHash;

    //记录两小时内按下两次复位键后，还有未复位掉的感温探测器，将其屏蔽，关闭阀门
    QHash<QString, int> m_doubleResetButtonCount;
    //记录两次按钮按下后的时间,默认两小时
    QTimer *m_doubleResetButtonTimer;
    //拦截不让执行多次
    bool m_doubleResetSign;
    //两次按钮后的延时执行定时器，等待全部复位完成
    QTimer *m_doubleResetButtonTimerYanShi;

    //给UI发送同步阀门的状态定时器，3秒发一次
    QTimer *m_syncGateStatusToUITimer;


    //感温复位标志，只使用一次，在正常情况下给设置归为，让感温和感烟设备自己去处理自己的复位
    QHash<QString, bool> m_fireResetSignHash;

    //生命信号的16位无符号变量
    quint8 m_mvbLiveSignal;

    //一、六车测试按钮、复位按钮上一次的变量状态
    QHash<QString, bool> m_frontStatusByButtonClickedHash;

    //一、六车测试按钮、复位按钮的按下次数
    QHash<QString, int> m_buttonClickedCountHash;

    //两次按钮按下的两小时定时器
    QTimer *m_buttonDoubleClickedTimer;

    //执行隔离复位的延时定时器
    QTimer *m_buttonDoubleClickedYanShiTimer;


    //外部调试的文本标志
    QString m_debugStr;
    //1秒刷新一次来检测复位按钮是否按下
    QTimer *m_checkResetButtonDownTimer;

    //A框架液位和B框架液位低的定时器
    QHash<QString, MyTimer*> m_abyeweidiyanshiTimerHash;
    //A框架加热器和暖风机和B框架加热器和暖风机的定时器
    QHash<QString, MyTimer*> m_abnuanfengjijiareqiTimerHash;
    //在core类中拦截火警探测器和感烟探测器的隔离状态，防止信号发送不出去
    QHash<QString, QString> m_fireSmogDeviceStatusHash;
private:
    //设置火警实时状态的函数
    void setFireDeviceStatus(const QString&, const FireDeviceStatus);

    //设置阀门实时状态的函数
    void setGateDeviceStatus(const QString&, const GateDeviceStatus);

    //设置感烟实时状态的函数
    void setSmogDeviceStatus(const QString&, const SmogDeviceStatus);

    //设置IO实时状态的函数
    void setIODeviceStatus(const QString&, const IODeviceStatus);

    //获取火警实时状态的函数
    FireDeviceStatus getFireDeviceStatus(const QString &);

    //获取阀门实时状态的函数
    GateDeviceStatus getGateDeviceStatus(const QString &);

    //获取感烟实时状态的函数
    SmogDeviceStatus getSmogDeviceStatus(const QString &);

    //获取IO实时状态的函数
    IODeviceStatus getIODeviceStatus(const QString &);
private:
    //获取感温设备的火警信息
    bool getFireInfoByFire(const QString &);
    //获取感温设备的故障信息
    bool getFaultInfoByFire(const QString &);
    //获取感温设备的隔离信息
    bool getShieldInfoByFire(const QString &);
    //获取感温探测器的目标温度
    quint16 getHuanJingWenDuByFire(const QString &);
    //获取感温探测器的环境温度
    quint16 getMuBiaoWenDuByFire(const QString &);


    //获取感烟设备的火警信息
    bool getFireInfoBySmog(const QString &, const QString &);
    //获取感烟设备的故障信息
    bool getFaultInfoBySmog(const QString &, const QString &);
    //获取感烟设备的隔离信息
    bool getShieldInfoBySmog(const QString &, const QString &);
    //获取感烟探测器所在柜的火警信息
    //参数是配置文件的变量名:一车D柜火警
    bool getFireInfoBySmogGui(const QString &);


    //获取阀门设备的开到位信息
    bool getOpenedByGate(const QString &, const QString &);
    //获取阀门设备的关到位信息
    bool getClosedByGate(const QString &, const QString &);
    //获取阀门设备的故障信息
    bool getFaultByGate(const QString &, const QString &);

    //获取IO设备的液位低
    //参数是配置文件的变量名：一车D柜液位低
    bool getYeweidiByIO(const QString &);
    //获取IO设备的压力低
    //参数是配置文件的变量名：一车D柜压力低
    bool getYalidiByIO(const QString &);
    //获取IO设备的按钮状态
    bool getAnniuByIO(const QString &, const QString &);
    //获取气灭打开的状态
    //参数是配置文件中的变量名：一车D柜气灭装置MVB状态
    bool getQiMieByIO(const QString &);

    //获取IO设备的值
    quint16 getValueByIO(const QString &, const QString &);
    //获取IO设备的输出点打开信息
    bool getClosedByIO(const QString &, const QString &);
    //获取IO设备的输出点的关闭信息
    bool getOpenedByIO(const QString &, const QString &);
private:

    //判断整个表达式是否可以被触发
    //参数为：表达式列表
    //返回值为bool
    bool canExec(const QList<MultiLambdaDeviceInfo> &);


    //参数是list(true & false & false | true)
    //返回值是最终执行后的结果
    bool parseStringByRegEx(const QString, QList<QString>);


    //第一个参数为字符列表
    //第二个参数为单个字符
    //返回第一个参数里有多少个第二个参数
    int computerOperator(const QList<QString> &, const QString&);

    //执行对应设备
    //参数为设备数据结构体
    void execDevice(const QList<MultiLambdaDeviceInfo> &);

    //参数为哪些表达式可以被执行
    void execDevice(const QHash<QString, bool> );

    //判断按钮是否按下并执行隔离
    void buttonClickedCounter();

    //执行对应设备，将所有变量设置为false
    //参数为设备数据结构体
    void changeVariableToFalse(const QList<MultiLambdaDeviceInfo> &);

    //重置list中所有标志位
    //参数为可变的list
    void resetMultiDevice(QList<MultiLambdaDeviceInfo> &);

    //参数为一个QList<Gate>
    //返回值为QHash<QString, QPair<QString, QString> > key为阀门别名，value为[子阀门名字:动作]
    QHash<QString, QPair<QString, QString> > getOpenCloseStatusFromGate(const QList<MultiLambdaDeviceInfo> &);


    //参数为一个QList<IO>
    //返回值QHash<QString, QList<QString> > key为IO设备别名，value为(点1:打开，点2:关闭，点3:打开)
    QHash<QString, QList<QString> > getOpenCloseStatusFromIO(const QList<MultiLambdaDeviceInfo> &);


    //参数为一个QList<Fire>
    //返回值为QHash<QString, QString> key为Fire设备别名，value为复位字符串
    QHash<QString, QString> getDongZuoFromFire(const QList<MultiLambdaDeviceInfo> &);

    //参数为一个QList<Timer>
    //返回值为QHash<QString, QString> key作为timer设备的别名，value为打开或关闭字符串
    QHash<QString, QString> getDongZuoFromTimer(const QList<MultiLambdaDeviceInfo> &);

    //参数为QList<Var>
    //返回值为QHash<QString, QString> key作为变量名，value为执行的动作，目前为空
    QHash<QString, QString> getDongZuoFromVar(const QList<MultiLambdaDeviceInfo> &);

    //参数为QList<Smog>
    //返回值为QHash<QString, QPair<QString, QString> > key为感烟设备别名，value为[子设备名字:动作]
    QHash<QString, QString> getDongZuoFromSmog(const QList<MultiLambdaDeviceInfo> &);


    //改变表达式中阀门的状态
    //第一个参数为表达式中阀门的状态
    //无返回值
    void changeGateStatus(MultiLambdaDeviceInfo &);

    //改变表达式中的感温探测器的状态
    //第一个参数为表达式中感温的状态
    //无返回值
    void changeFireStatus(MultiLambdaDeviceInfo &);

    //该表表达式中的感烟探测器的状态
    //第一个参数为表达式中的感烟的状态
    //无返回值
    void changeSmogStatus(MultiLambdaDeviceInfo &);

    //改变该表达式中的IO的状态
    //第一个参数为表达式的IO状态
    //无返回值
    void changeIOStatus(MultiLambdaDeviceInfo &);

    //改变表达式中的var的状态
    //第一个参数为表达式中的变量名
    //无返回值
    void changeVarStatus(MultiLambdaDeviceInfo &);

    //改变表达式中的timer的状态
    //第一个参数为表达式中的定时器名
    //无返回值
    void changeTimerStatus(MultiLambdaDeviceInfo &);

private:
    //获取一个完整的数据库火警消息结构体
    //参数为设备名
    //返回值为填充完毕的结构体
    MyDataBaseWord getFireDataBaseWord(const QString &);

    //获取一个完整的数据库故障消息结构体
    //参数为设备名
    //返回值为填充完毕的结构体
    MyDataBaseWord getFaultDataBaseWord(const QString &);

    //获取一个完整的数据库事件消息结构体
    //参数为设备名,事件名
    //返回值为填充完毕的结构体
    MyDataBaseWord getEventDataBaseWord(const QString &, const QString &);


    //获取一个完整的UI火警消息JsonObject
    QJsonObject getFireUIWord(const MyDataBaseWord &);

    //获取一个完整的UI故障消息JsonObject
    QJsonObject getFaultUIWord(const MyDataBaseWord &);

    //获取一个完整的UI事件消息JsonObject
    QJsonObject getEventUIWord(const MyDataBaseWord &);


    //处理感温设备火警
    void processFireDeviceFire(const QString &);
    //处理感烟设备火警
    void processSmogDeviceFire(const QString &);

    //处理感温设备故障
    void processFireDeviceFault(const QString &);
    //处理感烟设备故障
    void processSmogDeviceFault(const QString &);
    //处理感烟子设备故障
    void processSubSmogDeviceFault(const QString &);
    //处理IO设备故障
    void processIODeviceFault(const QString &);
    //处理阀门设备故障
    void processGateDeviceFault(const QString &);
    //处理子阀门故障
    void processSubGateDeviceFault(const QString &);

    //处理阀门的事件
    void processSubGateDeviceEvent(const QString &);
    //处理IO的事件
    void processSubIODeviceEvent(const QString &);
    //处理IO的A、B框架水温低故障
    //该函数只判断俩变量，就是A框架水温低、B框架水温低、A框架水温正常、B框架水温正常
    void processIOInputFault();

    //传入一个数据信息，发送发送udp event事件
    //发送感烟设备的事件，所以三个参数
    void sendUDPEvent(QString, QString, QString);

    //发送感温设备的参数，俩参数
    void sendUDPEvent(QString, QString);

    //====================================临时测试函数
    void printtest(QHash<QString, QList<MultiLambdaDeviceInfo> > hash)
    {
        qDebug() << "--------------------------------------";
        QList<QString> _keys = hash.keys();
        for(int i =0;i<_keys.size();i++)
        {
            QList<MultiLambdaDeviceInfo> _list = hash[_keys.at(i)];
            for(int j =0;j<_list.size();j++)
            {
                MultiLambdaDeviceInfo _str = _list.at(j);
                if(_str.type == "io")
                {
                    qDebug() << _str.io.devicename << ":" << _str.io.oper.status;
                }else if(_str.type == "gate")
                {
                    qDebug() << _str.gate.devicename << ":" << _str.gate.oper.status;

                }else if(_str.type == "smog")
                {
                    qDebug() << _str.smog.devicename << ":" << _str.smog.oper.status;

                }else if(_str.type == "fire")
                {
                    qDebug() << _str.fire.devicename << ":" << _str.fire.oper.status;
                }
            }
        }
        qDebug() << "==========================================";
    }
private:

    //只执行一次的插入mvb对应点位的函数
    void insertDotByMVB();

    //专门处理UI发送过来的请求设备名的函数
    //参数是type  ：fire smog gate
    QJsonObject getDeviceName(const QString &);

    //设置主从变量的函数，参数是主从的状态
    void setMasterClientStatus(bool);

    //获取主从变量的函数
    bool getMasterClientStatus();

    //在唯一的表达式列表中获取要执行的文件
    QList<MultiLambdaDeviceInfo> getCanExecList(const QString &);

    //设置表达式中的timer的标志位
    //第一个参数为A框架加热器故障(就是其它表达式的结果)
    //第二个参数为bool
    void setMultiLambdaTimerStatus(const QString &, const bool);

    //获取表达式中的timer的标志位
    //参数为A框架加热器故障(就是其它表达式的结果)
    bool getMultiLambdaTimerStatus(const QString &);

    //设置表达式中的var的标志位
    //第一个参数为感温探测器集体火警变量
    //第二个参数bool值
    void setMultiLambdaVariableStatus(const QString &, const bool);

    //获取表达式中的var的标志位
    //参数为感温探测器集体火警变量
    bool getMultiLambdaVariableStatus(const QString &);

    //将字节的某一位设置为0 或1
    //第一个参数是单字节的地址引用
    //第二个参数是第几位，从做开始是第一位
    //第三个参数是0 还是 1，0就置为0  1就置为1
    void setDataByBit(unsigned char &, const int , const int );


    //在while里轮询设备的状态，如果符合就通过sendMessage发送到UI
    void processDeviceStatusToUI();


    //处理IO的输出点的事件
    //第一个参数是总设别名
    //第二个参数是数量
    //第三个参数是子设备名
    //第四个参数是子设备名的状态
    void processIOOutputEvent(const QString, const int,
                              const QString = "", const bool = false,
                              const QString = "", const bool = false,
                              const QString = "",const bool = false,
                              const QString = "",const bool = false,
                              const QString = "", const bool = false,
                              const QString = "", const bool = false);

    //复位按钮按下后，要将所有UI拦截的都恢复到默认状态
    //UI有火警、事件、故障
    void onResetClicked();

    //将日志写入本地文件
    void writeLogToLogFile(QString text);
public slots:
    //更新火警设备的实时数据槽函数
    void updateFireDeviceData(const QString &, FireDeviceStatus);

    //更新阀门设备的实时数据槽函数
    void updateGateDeviceData(const QString &, GateDeviceStatus);

    //更新感烟设备的实时数据槽函数
    void updateSmogDeviceData(const QString &, SmogDeviceStatus);

    //更新IO设备的实时数据槽函数
    void updateIODeviceData(const QString &, IODeviceStatus);

    //开始执行事件循环
    void startExec();

    //处理UI发送过来的控制指令
    void processDataFromUI(const QJsonObject &);

    //处理主从发出来的状态
    //第一个参数是主还是从master  client
    //第二个参数是故障还是正常
    void processMasterOrClientStatus(QString, QString);

private slots:
    //参数时一个事件字段
    //该函数是接收事件UDP发过来的事件，并传给UI
    void processEventFromEventUDP(MyDataBaseWord);

    //该函数是接收事件UDP发送过来的屏蔽设备消息
    //第一个参数是设备名
    //第二个参数是是屏蔽状态
    void processFireShieldStatusFromEventUDP(QString, QString);

    //该函数是接收事件UDP发送过来的屏蔽感烟设备消息
    //第一个参数是设备名
    //第二个参数是是子设备名
    //第三个参数是屏蔽状态
    void processSmogShieldStatusFromEventUDP(QString, QString, QString);

    //该函数是接收事件UDP发送过来的设置曲线值消息
    //一共四个参数
    void processQXZFromEventUDP(QString, QString, QString, QString);

    //该函数是接收时间UDP发送过来的设置感烟灵敏度的消息
    //就一个参数
    void processLMDFromEventUDP(QString);

    //设置IO的开关状态
    //里面是多个IO输出点的开关状态
    void processIOStatusFromEventUDP(QJsonObject);

    //设置复位
    void processResetStatusFromEventUDP();


    //在while里用信号通知该函数，启动某个表达式中的定时器
    //第一个参数是原因名(A框架加热器定时器)
    //第二个参数是doit:打开或关闭
    void startMultiLambdaTimerSlot( QString,  QString);

    //接受MVB数据
    void processDataFromMVB(MVBToCoreData);

    //发送MVB数据
    //该函数遍历所有的数据并将数据通过mvb发出去
    void processDataToMVB();

    //发送IOT数据
    //该函数遍历所有的数据并将数据通过IOT发出去
    void processDataToIOT();

    //设置系统时间
    //参数为时间字符串
    void setSystemDate(const QString);

    //处理两次按钮按下后的执行逻辑
    void processDoubleResetButton();

    //处理两次按钮按下后条件成立的延时处理设置状态隔离的逻辑
    void processDoubleResetButtonYanShi();

    //处理隔离复位的逻辑，在UpdateFireDeviceStatus函数中
    void processGeLiFuWeiLogic(const QString, const FireDeviceStatus);

    //处理隔离复位的逻辑，在UpdateSmogDeviceStatus函数中
    void processGeLiFuWeiLogic(const QString, const SmogDeviceStatus);

    //清空所有按钮的按压次数
    void clearButtonClickedCount();

    //清空某个设备的次数
    void clearDeviceButtonClickedCount(const QString);

    //隔离复位的延时函数，只执行隔离操作
    void processDoubleResetButtonYanShiCopy();

    //一秒中执行一次检测复位按钮是否按下
    void processResetButtonDownByOneMintue();

    //收集所有阀门的状态，发送给UI
    void processGateStatusToUI();

    //用户在屏幕上操作阀门，通过udp event发送过来，通过这个槽函数更新其它设备的状态
    void processUpdateGateStatus(QString, QString, bool);

    //遍历表达式中的定时器信息，将信息发送给从设备
    void sendFaultMessageToClient();

    //将气灭启动状态变量发送给从设备
    void sendQiMieStatusToClient();

    //故障消息从主设备通过udp event发送过来，从设备调用这个函数来处理
    void processFaultMessageToUI(QString);

    //事件消息从主设备通过udp event发送过来，从设备调用这个函数来处理
    void processEventMessageToUI(QString);

    //气灭启动状态从主设备通过udp event发送过来，从设备调用这个函数来处理
    void processQiMieStatusToVar(QJsonObject);

    //Core给UI发送阀门的状态消息
    void updateGateStatusToUI();

    //从设备发给主设备的IO输出点开关状态，用这个函数来更新，这个槽函数是主类连接
    void updateIODeviceStatusFromMasterOrClient(QHash<QString, QHash<QString, bool> >);

signals:
    //连接阀门的开关槽函数
    //第一个参数是设备名
    //第二个参数是左侧阀门名
    //第三个参数是左侧阀门开关状态
    //第三个参数为右侧阀门名
    //第四个参数为右侧阀门开关状态
    void setGateOpenCloseStatusSignal(QString, QString, bool, QString, bool);

    //连接感温探测器的槽函数
    //第一个参数时设备名
    void resetFireDeviceSignal(QString);

    //连接感烟探测器的槽函数
    //第一个参数是设备名
    void resetSmogDeviceSignal(QString);


    //连接感温探测器的槽函数
    //第一个参数为设备名
    void normalFireDeviceSignal(QString);

    //连接感烟探测器的槽函数
    //第一个参数为设备名
    void normalSmogDeviceSignal(QString);

    //连接IO模块开关输出点的槽函数
    //第一个参数是设备名
    //第二个参数是输出点名
    //第三个参数是true或false 开或关
    void setIODeviceStatusSignal(const QString,
                                 const QString, const bool,
                                 const QString, const bool,
                                 const QString, const bool,
                                 const QString, const bool,
                                 const QString, const bool,
                                 const QString, const bool,
                                 const int);

    //和数据库线程通信的唯一信号
    void insertDataToDataBaseSignal(const MyDataBaseWord);


    ///
    void sendMessage(const QJsonObject &);

    //通过该信号，告知数据库查询对应的数据，然后通过TCP发送给UI
    void selectDataByDateToUIUsedTCPSignal(const QString&, const quint64&, const quint64&);

    //设置感温设备的屏蔽状态
    //第一个参数是设备名
    //第二个参数是屏蔽状态
    void setFireSheildStatusSignal(QString, QString);

    //设置感烟设备的屏蔽状态
    //第一个参数是设备名
    //第二个参数是屏蔽状态
    void setSmogShieldStatusSignal(QString, QString, QString);

    //设置感温设备的曲线值
    //第一个参数是8个字节组成的四个值
    void setQXZSignal(QString, QString, QString, QString);

    //设置感烟设备的曲线值
    //就一个参数
    void setLMDSignal(QString);


    //UI发来的保存csv文件
    void saveCsv();

    //设置IO的输出点状态
    //第一个参数是设备名
    //第二个参数是更新几个设备
    //第三个参数是子设备名
    //第四个参数是子设备状态
    //以此类推
    void setIOOutputStatusSignal(QString,
                                 int,
                                 QString, bool,
                                 QString, bool,
                                 QString, bool,
                                 QString, bool,
                                 QString, bool,
                                 QString, bool);


    //在while里用信号通知该函数，启动某个表达式中的定时器
    //第一个参数是原因名(A框架加热器定时器)
    //第二个参数是doit:打开或关闭
    void startMultiLambdaTimerSignal(QString, QString);

    //在定时器里用信号告知mvb发送数据
    //第一个参数是数据结构体
    //第二个参数是端口高地址
    //第三个参数是端口低地址
    //第四个参数是是否自增
    void sendDataToMVB(DataRequestResponseFrame, q8, q8,bool);

    //在定时器里用信号告知iot发送数据
    //第一个数据是数据结构体
    void sendDataToIOT(const QList<quint16>);

    //告诉阀门类重新收集阀门状态
    void reGetGateStatus();

};


#endif // CORE_H
