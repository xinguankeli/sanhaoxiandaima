#include "myiotclient.h"
#include <QHostAddress>
#include <QDebug>
MyIOTClient::MyIOTClient(QObject *parent) : QTcpSocket(parent),m_isConnected(false),m_sendCount(0),m_sign(false)
{
    m_reconnectTimer = new QTimer(this);
    connect(m_reconnectTimer, &QTimer::timeout, this, &MyIOTClient::reconnectfunc);
    m_reconnectTimer->setInterval(5000);
    connect(this, &QTcpSocket::readyRead, this, &MyIOTClient::whenDataComing);

}

void MyIOTClient::setAddress(const QString devicename, const quint16 port, const QString ip)
{
    m_devicename = devicename;
    m_ip = ip;
    m_port = port;
}

void MyIOTClient::setCarInfomation(const QHash<QString, QString> hash)
{
    memset(&m_carInfo, 0, sizeof(quint8) * 3);
    //将字符串转换成16进制
    m_carInfo[0] = hash.value("citycode").toUInt(nullptr, 16);
    m_carInfo[1] = hash.value("linecode").toUInt(nullptr, 16);
    m_carInfo[2] = hash.value("carcode").toUInt(nullptr, 16);
}

void MyIOTClient::sendConnectRequest()
{
    //无论是否连接上+1
    m_sendCount += 1;
    if(m_sendCount > 2)
    {
        if(!m_reconnectTimer->isActive())
        {
            m_reconnectTimer->start();
            m_sendCount = 0;
            m_isConnected = false;
        }
    }
    if(m_isConnected)
    {
        QByteArray _arr;
//        memcpy(_arr.data(), &_senddata, sizeof(RequestData));
//        qDebug() << m_carInfo[0] << ":" << m_carInfo[1] << ":" << m_carInfo[2];
        _arr.append((unsigned char)0x00);
        _arr.append((unsigned char)0x00);
        _arr.append(m_carInfo[0]);
        _arr.append(m_carInfo[1]);
        _arr.append(m_carInfo[2]);
        _arr.append(0xA6);//A6 = 168个设备
        _arr.append((unsigned char)0x00);
        _arr.append(0x01);
//                qDebug() << _arr.size();
//                qDebug() << _arr.toHex();
//        qDebug() << __FUNCTION__ << _arr.size() << ":" << _arr.toHex();
        write(_arr, _arr.size());
        flush();
    }
}

void MyIOTClient::startConnectIOT()
{
    connectToHost(m_ip, m_port);
//    qDebug() << m_ip     << ":" << m_port;
    if(!waitForConnected(3000))
    {
        qDebug() << "connect IOT error.";
        m_reconnectTimer->start();
    }else
    {
        qDebug() << "connect IOT succ.";
        m_isConnected = true;
        //给服务器发送请求连接标识
        sendConnectRequest();
    }

}

void MyIOTClient::sendDataToIOT(const QList<quint16> data)
{
    m_data = data;
    m_sign = true;
    sendConnectRequest();
}
void MyIOTClient::test()
{
    QByteArray _arr;
    _arr.append((unsigned char)0x00);
    _arr.append(0x01);
    _arr.append(m_carInfo[0]);
    _arr.append(m_carInfo[1]);
    _arr.append(m_carInfo[2]);
    _arr.append(0xFF);
    _arr.append(0x01);
    _arr.append(0x01);
    _arr.append(0xB4);

    quint16 _tempData[IOTDATACOUNT];
    for(int i =0;i<IOTDATACOUNT;i++)
    {
        QByteArray _tempArray;

        _tempData[i] = m_data.at(i);
        quint16 _tempV[1];
        _tempV[0] = _tempData[i];
        _tempArray.append((char*)_tempV, sizeof(quint16));
        std::reverse(_tempArray.begin(), _tempArray.end());
        _arr.append(_tempArray);
        //将字节反转
    }

//    QByteArray _tempArr;
//    _tempArr.append((char *)_tempData, sizeof(quint16) * IOTDATACOUNT);
    _arr.append(_arr);


//    qDebug() << write(_arr, _arr.size()) << __FUNCTION__ << ":" << _arr.toHex();
    flush();

}
void MyIOTClient::whenDataComing()
{
    m_sendCount = 0;
    QByteArray _arr = readAll();
//    qDebug() << _arr.toHex() << __FUNCTION__;
    if(_arr.size() == 6)
    {
        ResponseData _recvdata;
        memset(&_recvdata, 0, sizeof(ResponseData));
        memcpy(&_recvdata, _arr.data(), sizeof(ResponseData));
        if(_recvdata.statuscode[0] == 0x01)
        {
            //01代表请求连接成功
            qDebug() << "IOT server request connect succ.";
            if(m_sign)
            {
                test();
            }
        }else if(_recvdata.statuscode[0] == 0x02)
        {
            //车辆编号错误
            qDebug() << "car number error";
        }else if(_recvdata.statuscode[0] == 0x03)
        {
            qDebug() << "device number error";
        }else if(_recvdata.statuscode[0] == 0x04)
        {
            qDebug() << "yuzhi data error";
        }
    }
}

void MyIOTClient::reconnectfunc()
{
    abort();
    connectToHost(m_ip, m_port);
    if(!waitForConnected(3000))
    {
        qDebug() << "connect IOT error.";
    }else
    {
        m_isConnected = true;
        //给服务器发送请求连接标识
        sendConnectRequest();
        if(m_reconnectTimer->isActive())
        {
            m_reconnectTimer->stop();
        }
    }

}
