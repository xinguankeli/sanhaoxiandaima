#ifndef MYIOTCLIENT_H
#define MYIOTCLIENT_H

#include <QObject>
#include <QTcpSocket>
#include <QTimer>
//物联网传输数据用的TCPSocket类，只负责添加数据并定时发送
#include "global/IOTGlobal.h"
//我向服务器发送请求连接标识
struct RequestData
{
    //请求标识
    quint16 requestcode;
    //车辆数据
    quint8 carnumber[3];
    //设备数量
    quint8 devicenumber;
    //预置数据
    quint16 yuzhidata;
};
//服务器向我回连接是否成功标识
struct ResponseData
{
    //请求标识连接
    unsigned char requestsign[2];
    //车辆编号
    unsigned char carnumber[3];
    //状态码
    unsigned char statuscode[1];
};

class MyIOTClient : public QTcpSocket
{
    Q_OBJECT
public:
    explicit MyIOTClient(QObject *parent = nullptr);
    //设备名、端口、IP
    void setAddress(const QString, const quint16, const QString);
    //外部从配置文件中读取的车辆信息，设置到类里
    //第一个参数是
    void setCarInfomation(const QHash<QString, QString>);
private:
    //ip
    QString m_ip;
    //port
    quint16 m_port;
    //设备名字
    QString m_devicename;
    //是否与物联网服务器保持连接
    bool m_isConnected;
    //计数发送数据的次数
    int m_sendCount;
    //车辆信息
    quint8 m_carInfo[3];
    //重连定时器
    QTimer *m_reconnectTimer;

    QList<quint16> m_data;

    bool m_sign;
private:
    //向服务器发送请求连接标识
    void sendConnectRequest();
    void test();
public slots:
    //在子线程启动连接物联网
    void startConnectIOT();
    //外部调用此函数，将数据发送进来，在内部拼装并发送
    void sendDataToIOT(const QList<quint16>);
private slots:
    //当数据到达
    void whenDataComing();
    //重新连接
    void reconnectfunc();

signals:

};

#endif // MYIOTCLIENT_H
