#ifndef MYDATABASE_H
#define MYDATABASE_H

#include <QObject>
#include <QMutex>
#include <QSqlDatabase>
#include <QHash>
#include <QTimer>
#include "conf/myreadconf.h"
#include <QSqlQuery>
#include <QSqlError>
#include "global/Global.h"
#include "express/mydatabaseserver.h"
#include <QDateTime>
#include <QFile>
struct MyDataBaseSqlStr
{
    QString tableName;
    QString createTableSql;
    QString insertTableSql;
    QString updateTableSql;
    QString deleteTableSql;
};

class MyDataBase : public QObject
{
    Q_OBJECT
public:
    explicit MyDataBase(QString hostname, QString dbname, QString username, QString password, QObject *parent = nullptr);
    MyDataBase(QObject *parent = nullptr);
private:
    QMutex *m_mutex;//要在所有调用Qsql全局函数中加原子锁
    QSqlDatabase m_conn;
    QString m_hostName;
    QString m_dbName;
    QString m_userName;
    QString m_passWord;
    MyReadConf *m_readConf;
    QHash<QString, MyDataBaseSqlStr> m_tableNameMap;

    MyDataBaseServer *m_dataBaseServer;

    QTimer *m_reconnectTimer;
private:
    void init();
    bool isCreateTable(QString tableName);
    QString getSql(const QString &t, const quint64 &s, const quint64 &e);
    QList<MyDataBaseWord> selectQuery(QString, QString);//第一个参数是fireTable或者faultTable或者eventTable,第二个是查询语句
    //传入对应的数据库字段，返回字符串数组 value是(x:x:x)
    QList<QString> getStrData(const QList<MyDataBaseWord> &);
public slots:
    void startMyDataBase();//在子线程中启动数据库
    void insertQuery(QString, MyDataBaseWord);//插入语句，第一个参数是fireAlarm或者fault或者event，第二个参数是值
    void updateQuery(QString, MyDataBaseWord, MyDataBaseWord);//第一个参数是fireAlarm或者fault或者event,将第二个的值更新到第一个上
    void deleteQuery(QString, MyDataBaseWord);//第一个参数是fireAlarm或者fault或者event,第二个是要删除的条目
    void testQuery();//主要用来测试防止mysql超时断开连接
    QList<MyDataBaseWord> selectDataByDate(const QString&, const quint64&, const quint64 &);//第一个参数是表明，第二个参数是起始时间，第三个参数是结束时间
    void insertDataToDataBaseSlot(const MyDataBaseWord &);//TODO 外部就用这个信号插入数据就行
    void selectDataByDateToUIUsedTCP(const QString &, const quint64 &, const quint64 &);//core通过信号告知数据库查询信息，然后通过数据库内部的类，将数据发送给UI


    //查询所有fire、fault、event表的数据存储到本地
    void selectAllDataToLocal();
};

#endif // MYDATABASE_H
