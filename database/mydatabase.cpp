#include "mydatabase.h"
#include <QDebug>
#include <QDateTime>
#include <QThread>
MyDataBase::MyDataBase(QString hostname, QString dbname, QString username, QString password, QObject *parent)
    :QObject(parent),
      m_hostName(hostname),
      m_passWord(password),
      m_dbName(dbname),
      m_userName(username)
{
    init();

}

MyDataBase::MyDataBase(QObject *parent) : QObject(parent)
{
    m_readConf = new MyReadConf;
    DataBaseConfigWord _dbcw = m_readConf->getDataBaseConfigWord();
    m_hostName = _dbcw.hostname;
    m_userName = _dbcw.username;
    m_dbName = _dbcw.databasename;
    m_passWord = _dbcw.password;
    init();
}

void MyDataBase::init()
{
    m_mutex = new QMutex;
    //先做一个故障表
    MyDataBaseSqlStr _faultTableInfo;
    _faultTableInfo.tableName = "faultTable";
    _faultTableInfo.createTableSql = "create table if not exists %1(id int unsigned auto_increment,device_name varchar(100),fault_date varchar(50),fault_cause varchar(50), fault_is_clear varchar(50),primary key(id))";
    _faultTableInfo.insertTableSql = "insert into %1 (device_name,fault_date,fault_cause,fault_is_clear) values(:dn,:fd,:fc,:fic)";
    _faultTableInfo.updateTableSql = "update %1 set device_name=:fdn,fault_date=:ffd,fault_is_clear=:ffic where device_name=:sdn and fault_date=:sfd and fault_cause=:sfc and fault_is_clear=:sfic";
    _faultTableInfo.deleteTableSql = "delete from %1 where device_name='%2' and fault_date='%3' and fault_cause='%4' and fault_is_clear='%5'";
    m_tableNameMap.insert(FAULTTABLE, _faultTableInfo);
    //火警表
    MyDataBaseSqlStr _fireTableInfo;
    _fireTableInfo.tableName = "fireTable";
    _fireTableInfo.createTableSql = "create table if not exists %1(id int unsigned auto_increment,device_name varchar(100),fire_date varchar(50),fire_cause varchar(50), fire_is_clear varchar(50),primary key(id))";
    _fireTableInfo.insertTableSql = "insert into %1 (device_name,fire_date,fire_cause,fire_is_clear) values(:dn,:fd,:fc,:fic)";
    _fireTableInfo.updateTableSql = "update %1 set device_name=:fdn,fire_date=:ffd,fire_is_clear=:ffic where device_name=:sdn and fire_date=:sfd and fire_cause=:sfc and fire_is_clear=:sfic";
    _fireTableInfo.deleteTableSql = "delete from %1 where device_name='%2' and fire_date='%3' and fire_cause='%4' and fire_is_clear='%5'";
    m_tableNameMap.insert(FIRETABLE, _fireTableInfo);
    //事件表
    MyDataBaseSqlStr _eventTableInfo;
    _eventTableInfo.tableName = "eventTable";
    _eventTableInfo.createTableSql = "create table if not exists %1(id int unsigned auto_increment,device_name varchar(100),event_date varchar(50),event_cause varchar(50), event_is_clear varchar(50),primary key(id))";
    _eventTableInfo.insertTableSql = "insert into %1 (device_name,event_date,event_cause,event_is_clear) values(:dn,:fd,:fc,:fic)";
    _eventTableInfo.updateTableSql = "update %1 set device_name=:fdn,event_date=:ffd,event_is_clear=:ffic where device_name=:sdn and event_date=:sfd and event_cause=:sfc and event_is_clear=:sfic";
    _eventTableInfo.deleteTableSql = "delete from %1 where device_name='%2' and event_date='%3' and event_cause='%4' and event_is_clear='%5'";
    m_tableNameMap.insert(EVENTTABLE, _eventTableInfo);
    //烟雾表
    MyDataBaseSqlStr _smogTableInfo;
    _smogTableInfo.tableName = "smogTable";
    _smogTableInfo.createTableSql = "create table if not exists %1(id int unsigned auto_increment,device_name varchar(100),smog_date varchar(50),smog_cause varchar(50), smog_is_clear varchar(50),primary key(id))";
    _smogTableInfo.insertTableSql = "insert into %1 (device_name,smog_date,smog_cause,smog_is_clear) values(:dn,:fd,:fc,:fic)";
    _smogTableInfo.updateTableSql = "update %1 set device_name=:fdn,smog_date=:ffd,smog_is_clear=:ffic where device_name=:sdn and smog_date=:sfd and smog_cause=:sfc and smog_is_clear=:sfic";
    _smogTableInfo.deleteTableSql = "delete from %1 where device_name='%2' and smog_date='%3' and smog_cause='%4' and smog_is_clear='%5'";
    m_tableNameMap.insert(SMOGTABLE, _smogTableInfo);

    m_reconnectTimer = new QTimer;
    m_reconnectTimer->setInterval(1000 * 60);
    connect(m_reconnectTimer, &QTimer::timeout, [&]{
        testQuery();
    });
    m_reconnectTimer->start();
}

bool MyDataBase::isCreateTable(QString tableName)
{
    if(!m_tableNameMap.contains(tableName)){
        return false;
    }
    bool _ret;
    QSqlQuery _query(m_conn);
    QString _sql = m_tableNameMap[tableName].createTableSql + " CHARSET=utf8";
    _sql = _sql.arg(m_tableNameMap[tableName].tableName);
    m_mutex->lock();
    _ret = _query.exec(_sql);
    m_mutex->unlock();
    if(!_ret){
        qDebug() << _query.lastError();
        return false;
    }
    return true;
}

void MyDataBase::startMyDataBase()
{
    m_mutex->lock();//qt启动时有很多地方加载plugin，使用数据库的全局唯一方法，不加锁就出现资源争夺问题
    qDebug() << "hostname:" << m_hostName;
    qDebug() << "dbname:" << m_dbName;
    qDebug() << "username:" << m_userName;
    qDebug() << "password:" << m_passWord;
    m_conn = QSqlDatabase::addDatabase("QMYSQL");
//    m_conn.setConnectOptions("MYSQL_OPT_RECONNECT=1;MYSQL_OPT_CONNECT_TIMEOUT=1;");

    m_conn.setHostName(m_hostName);
    m_conn.setDatabaseName(m_dbName);
    m_conn.setUserName(m_userName);
    m_conn.setPassword(m_passWord);
    if(!m_conn.open()){
        //初始化三个表
        QSqlError error = m_conn.lastError();
        qDebug() << error.text();
        exit(0);//数据库没启动，整个程序也没必要起来
    }
    m_mutex->unlock();
    isCreateTable(FAULTTABLE);
    isCreateTable(FIRETABLE);
    isCreateTable(EVENTTABLE);
    isCreateTable(SMOGTABLE);

    //在子线程里创建数据库的数据tcp server
    m_dataBaseServer = new MyDataBaseServer(65431);
    QThread *_thread = new QThread;
    m_dataBaseServer->moveToThread(_thread);
    _thread->start();
    connect(_thread, &QThread::started, m_dataBaseServer, &MyDataBaseServer::startDataBaseServer);
}

void MyDataBase::insertQuery(QString tableName, MyDataBaseWord mdbw)
{
    if(!m_tableNameMap.contains(tableName)){
        return;
    }
    QString _sql = m_tableNameMap[tableName].insertTableSql;
    m_mutex->lock();
    QSqlQuery _query(m_conn);
    _sql = _sql.arg(m_tableNameMap[tableName].tableName);
    bool _ret = _query.prepare(_sql);
    if(!_ret){
        m_mutex->unlock();
        qDebug() << _query.lastError();
        return;
    }
    if(mdbw.type == FIRETABLE){
        _query.bindValue(":dn", mdbw.fire_word.devicename);
        _query.bindValue(":fd", QString::number(mdbw.fire_word.date));
        _query.bindValue(":fc", mdbw.fire_word.casue);
        _query.bindValue(":fic", mdbw.fire_word.isclear);
    }else if(mdbw.type == FAULTTABLE){
        _query.bindValue(":dn", mdbw.fault_word.devicename);
        _query.bindValue(":fd", QString::number(mdbw.fault_word.date));
        _query.bindValue(":fc", mdbw.fault_word.casue);
        _query.bindValue(":fic", mdbw.fault_word.isclear);
    }else if(mdbw.type == EVENTTABLE){
        _query.bindValue(":dn", mdbw.event_word.devicename);
        _query.bindValue(":fd", QString::number(mdbw.event_word.date));
        _query.bindValue(":fc", mdbw.event_word.casue);
        _query.bindValue(":fic", mdbw.event_word.isclear);
    }else if(mdbw.type == SMOGTABLE){
        _query.bindValue(":dn", mdbw.smog_word.devicename);
        _query.bindValue(":fd", QString::number(mdbw.smog_word.date));
        _query.bindValue(":fc", mdbw.smog_word.casue);
        _query.bindValue(":fic", mdbw.smog_word.isclear);
    }
    _ret = _query.exec();
    m_mutex->unlock();
    if(!_ret){
        qDebug() << _query.lastError();
        return;
    }
}

void MyDataBase::updateQuery(QString devicename, MyDataBaseWord first, MyDataBaseWord second)
{
    QSqlQuery _query(m_conn);
    QString _sql;
    _sql = m_tableNameMap[devicename].updateTableSql;
    _sql = _sql.arg(devicename);
    bool ret = _query.prepare(_sql);
    if(!ret){
        qDebug() << _query.lastError();
        return;
    }

    if(devicename == FIRETABLE){
        _query.bindValue(":fdn", first.fire_word.devicename);
        _query.bindValue(":fdd", QString::number(first.fire_word.date));
        _query.bindValue(":ffic", first.fire_word.isclear);
        _query.bindValue(":sdn", second.fire_word.devicename);
        _query.bindValue(":sfd", QString::number(second.fire_word.date));
        _query.bindValue(":sfc", second.fire_word.casue);
        _query.bindValue(":sfic", second.fire_word.isclear);
    }else if(devicename == EVENTTABLE){
        _query.bindValue(":fdn", first.event_word.devicename);
        _query.bindValue(":fdd", QString::number(first.event_word.date));
        _query.bindValue(":ffic", first.event_word.isclear);
        _query.bindValue(":sdn", second.event_word.devicename);
        _query.bindValue(":sfd", QString::number(second.event_word.date));
        _query.bindValue(":sfc", second.event_word.casue);
        _query.bindValue(":sfic", second.event_word.isclear);
    }else if(devicename == FAULTTABLE){
        _query.bindValue(":fdn", first.fault_word.devicename);
        _query.bindValue(":fdd", QString::number(first.fault_word.date));
        _query.bindValue(":ffic", first.fault_word.isclear);
        _query.bindValue(":sdn", second.fault_word.devicename);
        _query.bindValue(":sfd", QString::number(second.fault_word.date));
        _query.bindValue(":sfc", second.fault_word.casue);
        _query.bindValue(":sfic", second.fault_word.isclear);
    }else if(devicename == SMOGTABLE){
        _query.bindValue(":fdn", first.smog_word.devicename);
        _query.bindValue(":fdd", QString::number(first.smog_word.date));
        _query.bindValue(":ffic", first.smog_word.isclear);
        _query.bindValue(":sdn", second.smog_word.devicename);
        _query.bindValue(":sfd", QString::number(second.smog_word.date));
        _query.bindValue(":sfc", second.smog_word.casue);
        _query.bindValue(":sfic", second.smog_word.isclear);
    }
    ret = _query.exec();
    if(!ret){
        qDebug() << _query.lastError();
        return;
    }
}

void MyDataBase::deleteQuery(QString devicename, MyDataBaseWord word)
{
    if(m_conn.isOpen()){
        QSqlQuery _query(m_conn);
        QString _sql = m_tableNameMap[devicename].deleteTableSql;
        if(devicename ==  FIRETABLE){
            _sql = _sql
                    .arg(devicename)
                    .arg(word.fire_word.devicename)
                    .arg(QString::number(word.fire_word.date))
                    .arg(word.fire_word.casue)
                    .arg(word.fire_word.isclear);
        }else if(devicename == EVENTTABLE){
            _sql = _sql
                    .arg(devicename)
                    .arg(word.event_word.devicename)
                    .arg(QString::number(word.event_word.date))
                    .arg(word.event_word.casue)
                    .arg(word.event_word.isclear);
        }else if(devicename == FAULTTABLE){
            _sql = _sql
                    .arg(devicename)
                    .arg(word.fault_word.devicename)
                    .arg(QString::number(word.fault_word.date))
                    .arg(word.fault_word.casue)
                    .arg(word.fault_word.isclear);
        }else if(devicename == SMOGTABLE){
            _sql = _sql
                    .arg(devicename)
                    .arg(word.smog_word.devicename)
                    .arg(QString::number(word.smog_word.date))
                    .arg(word.smog_word.casue)
                    .arg(word.smog_word.isclear);
        }

        bool ret = _query.prepare(_sql);
        if(!ret){
            qDebug() << _query.lastError();
            return;
        }
        ret = _query.exec();
        if(!ret){
            qDebug() << _query.lastError();
            return;
        }
    }
}

void MyDataBase::testQuery()
{
    selectQuery("", "select * from fireTable  limit 0,1");
}

QList<MyDataBaseWord> MyDataBase::selectQuery(QString type, QString sql)
{
    //如果sql为空，那就使用默认查询语句，否则使用sql
    QSqlQuery _query(m_conn);
    QList<MyDataBaseWord> _list;
    bool ret;
    ret = _query.prepare(sql);
    if(!ret){
        qDebug() << _query.lastError();
        return _list;
    }
    _query.exec();
    while(_query.next() == true){
        MyDataBaseWord dbw;
        //在这里做判断
        if(type == FIRETABLE){
            dbw.type = FIRETABLE;
            dbw.fire_word.devicename = _query.value("device_name").toString();
            dbw.fire_word.date = _query.value("fire_date").toUInt();
            dbw.fire_word.casue = _query.value("fire_cause").toString();
            dbw.fire_word.isclear = _query.value("fire_is_clear").toString();
        }else if(type == FAULTTABLE){
            dbw.type = FAULTTABLE;
            dbw.fault_word.devicename = _query.value("device_name").toString();
            dbw.fault_word.date = _query.value("fault_date").toUInt();
            dbw.fault_word.casue = _query.value("fault_cause").toString();
            dbw.fault_word.isclear = _query.value("fault_is_clear").toString();
        }else if(type == EVENTTABLE){
            dbw.type = EVENTTABLE;
            dbw.event_word.devicename = _query.value("device_name").toString();
            dbw.event_word.date = _query.value("event_date").toUInt();
            dbw.event_word.casue = _query.value("event_cause").toString();
            dbw.event_word.isclear = _query.value("event_is_clear").toString();
        }else if(type == SMOGTABLE){
            dbw.type = SMOGTABLE;
            dbw.smog_word.devicename = _query.value("device_name").toString();
            dbw.smog_word.date = _query.value("smog_date").toUInt();
            dbw.smog_word.casue = _query.value("smog_cause").toString();
            dbw.smog_word.isclear = _query.value("smog_is_clear").toString();
        }
        _list.append(dbw);
    }
    return _list;
}

QList<QString> MyDataBase::getStrData(const QList<MyDataBaseWord> & list)
{
    QList<QString> _list;
    for(int i =0;i<list.size();i++)
    {
        MyDataBaseWord _word = list.at(i);
        if(_word.type == FIRETABLE)
        {
            QString _str("%1^%2^%3");
            _str = _str.arg(_word.fire_word.devicename)
                    .arg(_word.fire_word.casue)
                    .arg(QDateTime::fromTime_t(_word.fire_word.date).toString("yyyy-MM-dd hh:mm:ss"));
            _list.append(_str);

        }else if(_word.type == FAULTTABLE)
        {
            QString _str("%1^%2^%3");
            _str = _str.arg(_word.fault_word.devicename)
                    .arg(_word.fault_word.casue)
                    .arg(QDateTime::fromTime_t(_word.fault_word.date).toString("yyyy-MM-dd hh:mm:ss"));
            _list.append(_str);
        }else if(_word.type == EVENTTABLE)
        {
            QString _str("%1^%2^%3");
            _str = _str.arg(_word.event_word.devicename)
                    .arg(_word.event_word.casue)
                    .arg(QDateTime::fromTime_t(_word.event_word.date).toString("yyyy-MM-dd hh:mm:ss"));
            _list.append(_str);
        }
    }
    return _list;
}

QList<MyDataBaseWord> MyDataBase::selectDataByDate(const QString &type, const quint64 &start, const quint64 &end)
{
    QString _sql = getSql(type, start, end);
    return selectQuery(type, _sql);
}
QString MyDataBase::getSql(const QString &t, const quint64 &s, const quint64 &e)
{
    QString _str("select * from %1 where CAST(%2 as UNSIGNED) > %3 AND CAST(%4 as UNSIGNED) <%5 order by id desc");
    if(t == FIRETABLE)
    {
        _str = _str
                .arg(FIRETABLE)
                .arg("fire_date")
                .arg(QString::number(s))
                .arg("fire_date")
                .arg(QString::number(e));
    }else if(t == FAULTTABLE)
    {
        _str = _str
                .arg(FAULTTABLE)
                .arg("fault_date")
                .arg(QString::number(s))
                .arg("fault_date")
                .arg(QString::number(e));
    }else if(t == EVENTTABLE)
    {
        _str = _str
                .arg(EVENTTABLE)
                .arg("event_date")
                .arg(QString::number(s))
                .arg("event_date")
                .arg(QString::number(e));
    }
    return _str;
}
void MyDataBase::insertDataToDataBaseSlot(const MyDataBaseWord &word)//插入到数据库中
{
    QString _sql = m_tableNameMap[word.type].insertTableSql;
    m_mutex->lock();
    QSqlQuery _query(m_conn);
    _sql = _sql.arg(m_tableNameMap[word.type].tableName);
    bool _ret = _query.prepare(_sql);
    if(!_ret)
    {
        m_mutex->unlock();
        qDebug() << _query.lastError();
        return;
    }

//    qDebug() << _sql;
    if(word.type == FIRETABLE)
    {
        _query.bindValue(":dn", word.fire_word.devicename);
        _query.bindValue(":fd", word.fire_word.date);
        _query.bindValue(":fc", word.fire_word.casue);
        _query.bindValue(":fic", word.fire_word.isclear);
    }else if(word.type == FAULTTABLE)
    {
        _query.bindValue(":dn", word.fault_word.devicename);
        _query.bindValue(":fd", word.fault_word.date);
        _query.bindValue(":fc", word.fault_word.casue);
        _query.bindValue(":fic", word.fault_word.isclear);
//        if(word.fault_word.devicename == "A框架氮气瓶一压力低")
//        {
//            qDebug() << word.fault_word.devicename
//                     << word.fault_word.date
//                     << word.fault_word.casue
//                     << word.fault_word.isclear;
//        }

    }else if(word.type == EVENTTABLE)
    {
//        qDebug() << word.event_word.devicename
//                 << word.event_word.date
//                 << word.event_word.casue
//                 << word.event_word.isclear;
        _query.bindValue(":dn", word.event_word.devicename);
        _query.bindValue(":fd", word.event_word.date);
        _query.bindValue(":fc", word.event_word.casue);
        _query.bindValue(":fic", word.event_word.isclear);
    }
    _ret = _query.exec();
    m_mutex->unlock();
    if(!_ret)
    {
        qDebug() << _query.lastError();
        return;
    }
}

void MyDataBase::selectDataByDateToUIUsedTCP(const QString &type, const quint64 &start, const quint64 &end)
{
    QList<MyDataBaseWord> _list;
    QList<QString> _strList;

    if(type == ALLTABLE)
    {
        _list.append(selectDataByDate(FIRETABLE, start, end));
        _list.append(selectDataByDate(FAULTTABLE, start, end));
        _list.append(selectDataByDate(EVENTTABLE, start, end));
//        qDebug() << _list.size();
    }else if(type == FIRETABLE)
    {
        _list.append(selectDataByDate(FIRETABLE, start, end));
//        qDebug() << _list.size();
    }else if(type == FAULTTABLE)
    {
        _list.append(selectDataByDate(FAULTTABLE, start, end));
//        qDebug() << _list.size();
    }else if(type == EVENTTABLE)
    {
        _list.append(selectDataByDate(EVENTTABLE, start, end));
//        qDebug() << _list.size();
    }
    qDebug() << __FUNCTION__ << __LINE__ << ":" << _list.size();
    _strList.append(getStrData(_list));
    qDebug() << _strList.size();
    //将_strList用数据网络发出去
    m_dataBaseServer->sendData(_strList);
}

void MyDataBase::selectAllDataToLocal()
{
    QList<MyDataBaseWord> _fireList = selectQuery(FIRETABLE, "select * from fireTable");
        QList<MyDataBaseWord> _faultList = selectQuery(FIRETABLE, "select * from faultTable");
        QList<MyDataBaseWord> _eventList = selectQuery(FIRETABLE, "select * from eventTable");

        QString _path = "./data.csv";
        QFile csvFile(_path);
        if (!csvFile.open(QIODevice::WriteOnly | QIODevice::Truncate)) {
            qDebug() << "无法打开文件进行写入！";
            return;
        }

        // 将数据写入到CSV文件中
           QTextStream out(&csvFile);
           out << "列名1" << "," << "列名2" << "\n"; // CSV文件第一行为表头
           for (int i = 0; i < _fireList.size(); ++i) {
               out << _fireList.at(i).fire_word.devicename
                   << "," << QDateTime::fromTime_t(_fireList.at(i).fire_word.date).toString("yyyy-MM-dd hh:mm:ss")
                   << "," << _fireList.at(i).fire_word.casue << "\n";
           }
           for (int i = 0; i < _faultList.size(); ++i) {
               out << _faultList.at(i).fault_word.devicename
                   << "," << QDateTime::fromTime_t(_faultList.at(i).fault_word.date).toString("yyyy-MM-dd hh:mm:ss")
                   << "," << _faultList.at(i).fault_word.casue << "\n";
           }
           for (int i = 0; i < _eventList.size(); ++i) {
               out << _eventList.at(i).event_word.devicename
                   << "," << QDateTime::fromTime_t(_eventList.at(i).event_word.date).toString("yyyy-MM-dd hh:mm:ss")
                   << "," << _eventList.at(i).event_word.casue << "\n";
           }

           // 关闭文件
           csvFile.close();
}

